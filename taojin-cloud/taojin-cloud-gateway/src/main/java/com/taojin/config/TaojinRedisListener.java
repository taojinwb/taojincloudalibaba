package com.taojin.config;


import com.taojin.util.BaseMap;

public interface TaojinRedisListener {
    void onMessage(BaseMap message);
}
