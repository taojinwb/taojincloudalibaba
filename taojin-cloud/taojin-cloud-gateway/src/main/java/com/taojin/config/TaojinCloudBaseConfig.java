package com.taojin.config;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component("TaojinCloudBaseConfig")
@ConfigurationProperties(
        prefix = "taojin"
)
public class TaojinCloudBaseConfig {
    private String signatureSecret = "dd05f1c54d63749eda95f9fa6d49v442a";
    private String signUrls;

    public TaojinCloudBaseConfig() {
    }

    public String getSignatureSecret() {
        return this.signatureSecret;
    }

    public void setSignatureSecret(String signatureSecret) {
        this.signatureSecret = signatureSecret;
    }

    public String getSignUrls() {
        return this.signUrls;
    }

    public void setSignUrls(String signUrls) {
        this.signUrls = signUrls;
    }
}
