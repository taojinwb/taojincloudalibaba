package com.taojin.config;


import com.alibaba.cloud.commons.lang.StringUtils;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.alibaba.fastjson.support.springfox.SwaggerJsonSerializer;
import com.alibaba.fastjson2.JSON;
import com.taojin.util.HttpUtils;
import com.taojin.util.PathMatcherUtil;
import com.taojin.util.SignUtil;
import com.taojin.util.UserTokenContext;
import feign.Feign;
import feign.Logger.Level;
import feign.RequestInterceptor;
import feign.codec.Decoder;
import feign.codec.Encoder;
import feign.form.spring.SpringFormEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignAutoConfiguration;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.SortedMap;

@ConditionalOnClass({Feign.class})
@AutoConfigureBefore({FeignAutoConfiguration.class})
@Configuration
public class FeignConfig {
    private static final Logger log = LoggerFactory.getLogger(FeignConfig.class);
    @Resource
    TaojinCloudBaseConfig taojinCloudBaseConfig;
    public static final String X_ACCESS_TOKEN = "X-Access-Token";
    public static final String X_SIGN = "X-Sign";
    public static final String X_TIMESTAMP = "X-TIMESTAMP";
    public static final String TENANT_ID = "X-Tenant-Id";

    public FeignConfig() {
    }

    @Bean
    public RequestInterceptor requestInterceptor() {
        return (requestTemplate) -> {
            ServletRequestAttributes attributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
            String token;
            String queryLine;
            String signUrls;
            if (null != attributes) {
                HttpServletRequest request = attributes.getRequest();
                log.debug("Feign request: {}", request.getRequestURI());
                token = request.getHeader("X-Access-Token");
                if (token == null || "".equals(token)) {
                    token = request.getParameter("token");
                    if (StringUtils.isEmpty(token)) {
                        token = UserTokenContext.getToken();
                    }
                }

                log.info("Feign Login Request token: {}", token);
                requestTemplate.header("X-Access-Token", new String[]{token});
                queryLine = request.getHeader("X-Tenant-Id");
                if (queryLine == null || "".equals(queryLine)) {
                    queryLine = request.getParameter("X-Tenant-Id");
                }

                log.info("Feign Login Request tenantId: {}", queryLine);
                requestTemplate.header("X-Tenant-Id", new String[]{queryLine});
            } else {
                signUrls = UserTokenContext.getToken();
                log.info("Feign no Login token: {}", signUrls);
                requestTemplate.header("X-Access-Token", new String[]{signUrls});
                token = TenantContext.getTenant();
                log.info("Feign no Login tenantId: {}", token);
                requestTemplate.header("X-Tenant-Id", new String[]{token});
            }

            signUrls = this.taojinCloudBaseConfig.getSignUrls();
            token = null;
            List signUrlsArray;
            if (StringUtils.isNotBlank(signUrls)) {
                signUrlsArray = Arrays.asList(signUrls.split(","));
            } else {
                signUrlsArray = Arrays.asList(PathMatcherUtil.SIGN_URL_LIST);
            }

            if (PathMatcherUtil.matches(signUrlsArray, requestTemplate.path())) {
                try {
                    log.info("============================ [begin] fegin starter url ============================");
                    log.info(requestTemplate.path());
                    log.info(requestTemplate.method());
                    queryLine = requestTemplate.queryLine();
                    if (queryLine != null && queryLine.startsWith("?")) {
                        queryLine = queryLine.substring(1);
                    }

                    log.info(queryLine);
                    if (requestTemplate.body() != null) {
                        log.info(new String(requestTemplate.body()));
                    }

                    SortedMap<String, String> allParams = HttpUtils.getAllParams(requestTemplate.path(), queryLine, requestTemplate.body(), requestTemplate.method());
                    String sign = SignUtil.getParamsSign(allParams);
                    log.info(" Feign request params sign: {}", sign);
                    log.info("============================ [end] fegin starter url ============================");
                    requestTemplate.header("X-Sign", new String[]{sign});
                    requestTemplate.header("X-TIMESTAMP", new String[]{String.valueOf(System.currentTimeMillis())});
                } catch (IOException var8) {
                    var8.printStackTrace();
                }
            }

        };
    }

    @Bean
    Level feignLoggerLevel() {
        return Level.FULL;
    }

    @Bean
    @Primary
    @Scope("prototype")
    public Encoder multipartFormEncoder(ObjectFactory<HttpMessageConverters> messageConverters) {
        return new SpringFormEncoder(new SpringEncoder(messageConverters));
    }

    @Bean
    public Encoder feignEncoder() {
        return new SpringEncoder(this.feignHttpMessageConverter());
    }

    @Bean
    public Decoder feignDecoder() {
        return new SpringDecoder(this.feignHttpMessageConverter());
    }

    private ObjectFactory<HttpMessageConverters> feignHttpMessageConverter() {
        HttpMessageConverters httpMessageConverters = new HttpMessageConverters(new HttpMessageConverter[]{this.getFastJsonConverter()});
        return () -> {
            return httpMessageConverters;
        };
    }

    private FastJsonHttpMessageConverter getFastJsonConverter() {
        FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
        List<MediaType> supportedMediaTypes = new ArrayList();
        MediaType mediaTypeJson = MediaType.valueOf("application/json");
        supportedMediaTypes.add(mediaTypeJson);
        converter.setSupportedMediaTypes(supportedMediaTypes);
        FastJsonConfig config = new FastJsonConfig();
        config.getSerializeConfig().put(JSON.class, new SwaggerJsonSerializer());
        config.setSerializerFeatures(new SerializerFeature[]{SerializerFeature.DisableCircularReferenceDetect});
        converter.setFastJsonConfig(config);
        return converter;
    }
}
