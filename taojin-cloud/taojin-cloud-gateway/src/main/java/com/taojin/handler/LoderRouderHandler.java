package com.taojin.handler;

import lombok.extern.slf4j.Slf4j;
import com.taojin.util.BaseMap;
import com.taojin.constant.GlobalConstants;
import com.taojin.config.TaojinRedisListener;
import com.taojin.loader.DynamicRouteLoader;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 路由刷新监听（实现方式：redis监听handler）
 * @author sujg
 * @date: 2022/4/21 10:55
 */
@Slf4j
@Component(GlobalConstants.LODER_ROUDER_HANDLER)
public class LoderRouderHandler implements TaojinRedisListener {

    @Resource
    private DynamicRouteLoader dynamicRouteLoader;


    @Override
    public void onMessage(BaseMap message) {
        dynamicRouteLoader.refresh(message);
    }

}