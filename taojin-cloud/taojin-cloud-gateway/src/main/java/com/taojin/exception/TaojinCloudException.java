package com.taojin.exception;


public class TaojinCloudException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public TaojinCloudException(String message) {
        super(message);
    }
}
