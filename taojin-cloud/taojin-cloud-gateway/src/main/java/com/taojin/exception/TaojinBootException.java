package com.taojin.exception;


public class TaojinBootException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public TaojinBootException(String message) {
        super(message);
    }

    public TaojinBootException(Throwable cause) {
        super(cause);
    }

    public TaojinBootException(String message, Throwable cause) {
        super(message, cause);
    }
}