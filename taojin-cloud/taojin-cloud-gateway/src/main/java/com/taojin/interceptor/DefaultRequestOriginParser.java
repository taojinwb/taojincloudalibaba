package com.taojin.interceptor;


import com.alibaba.cloud.commons.lang.StringUtils;
import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.RequestOriginParser;

import com.taojin.util.IpUtils;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class DefaultRequestOriginParser implements RequestOriginParser {
    public DefaultRequestOriginParser() {
    }

    public String parseOrigin(HttpServletRequest request) {
        String origin = request.getParameter("origin");
        if (StringUtils.isNotEmpty(origin)) {
            return origin;
        } else {
            String ip = IpUtils.getIpAddr(request);
            return ip;
        }
    }
}
