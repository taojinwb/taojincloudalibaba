package com.taojin.util;


import com.alibaba.fastjson2.JSONObject;
import com.taojin.config.TaojinCloudBaseConfig;
import com.taojin.exception.TaojinBootException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.util.SortedMap;

public class SignUtil {
    private static final Logger log = LoggerFactory.getLogger(SignUtil.class);
    public static final String X_PATH_VARIABLE = "x-path-variable";
    private static final String DOLLAR = "$";
    private static final String LEFT_CURLY_BRACKET = "{";

    public SignUtil() {
    }

    public static boolean verifySign(SortedMap<String, String> params, String headerSign) {
        if (params != null && !StringUtils.isEmpty(headerSign)) {
            String paramsSign = getParamsSign(params);
            log.info("Param Sign : {}", paramsSign);
            return !StringUtils.isEmpty(paramsSign) && headerSign.equals(paramsSign);
        } else {
            return false;
        }
    }

    public static String getParamsSign(SortedMap<String, String> params) {
        params.remove("_t");
        String paramsJsonStr = JSONObject.toJSONString(params);
        log.info("Param paramsJsonStr : {}", paramsJsonStr);
        TaojinCloudBaseConfig jeecgBaseConfig = (TaojinCloudBaseConfig)SpringContextHolder.getBean(TaojinCloudBaseConfig.class);
        String signatureSecret = jeecgBaseConfig.getSignatureSecret();
        String curlyBracket = "${";
        if (!StringUtils.isEmpty(signatureSecret) && !signatureSecret.contains(curlyBracket)) {
            return DigestUtils.md5DigestAsHex((paramsJsonStr + signatureSecret).getBytes()).toUpperCase();
        } else {
            throw new TaojinBootException("签名密钥 ${taojin.signatureSecret} 缺少配置 ！！");
        }
    }
}
