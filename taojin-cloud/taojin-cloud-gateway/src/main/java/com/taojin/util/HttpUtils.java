package com.taojin.util;


import com.alibaba.cloud.commons.lang.StringUtils;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;

public class HttpUtils {
    private static final Logger log = LoggerFactory.getLogger(HttpUtils.class);
    private static final String COMMA = ",";

    public HttpUtils() {
    }

    public static SortedMap<String, String> getAllParams(HttpServletRequest request) throws IOException {
        SortedMap<String, String> result = new TreeMap();
        String pathVariable = request.getRequestURI().substring(request.getRequestURI().lastIndexOf("/") + 1);
        if (pathVariable.contains(",")) {
            log.info(" pathVariable: {}", pathVariable);
            String deString = URLDecoder.decode(pathVariable, "UTF-8");
            log.info(" pathVariable decode: {}", deString);
            result.put("x-path-variable", deString);
        }

        Map<String, String> urlParams = getUrlParams(request);
        Iterator var4 = urlParams.entrySet().iterator();

        while(var4.hasNext()) {
            Map.Entry entry = (Map.Entry)var4.next();
            result.put((String)entry.getKey(), (String)entry.getValue());
        }

        Map<String, String> allRequestParam = new HashMap(16);
        if (!HttpMethod.GET.name().equals(request.getMethod())) {
            allRequestParam = getAllRequestParam(request);
        }

        if (allRequestParam != null) {
            Iterator var9 = ((Map)allRequestParam).entrySet().iterator();

            while(var9.hasNext()) {
                Map.Entry entry = (Map.Entry)var9.next();
                result.put((String)entry.getKey(), (String)entry.getValue());
            }
        }

        return result;
    }

    public static SortedMap<String, String> getAllParams(String url, String queryString, byte[] body, String method) throws IOException {
        SortedMap<String, String> result = new TreeMap();
        String pathVariable = url.substring(url.lastIndexOf("/") + 1);
        if (pathVariable.contains(",")) {
            log.info(" pathVariable: {}", pathVariable);
            String deString = URLDecoder.decode(pathVariable, "UTF-8");
            log.info(" pathVariable decode: {}", deString);
            result.put("x-path-variable", deString);
        }

        Map<String, String> urlParams = getUrlParams(queryString);
        Iterator var7 = urlParams.entrySet().iterator();

        while(var7.hasNext()) {
            Map.Entry entry = (Map.Entry)var7.next();
            result.put((String)entry.getKey(), (String)entry.getValue());
        }

        Map<String, String> allRequestParam = new HashMap(16);
        if (!HttpMethod.GET.name().equals(method)) {
            allRequestParam = getAllRequestParam(body);
        }

        if (allRequestParam != null) {
            Iterator var12 = ((Map)allRequestParam).entrySet().iterator();

            while(var12.hasNext()) {
                Map.Entry entry = (Map.Entry)var12.next();
                result.put((String)entry.getKey(), (String)entry.getValue());
            }
        }

        return result;
    }

    public static Map<String, String> getAllRequestParam(final HttpServletRequest request) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream()));
        String str = "";
        StringBuilder wholeStr = new StringBuilder();

        while((str = reader.readLine()) != null) {
            wholeStr.append(str);
        }

        return (Map)JSONObject.parseObject(wholeStr.toString(), Map.class);
    }

    public static Map<String, String> getAllRequestParam(final byte[] body) throws IOException {
        if (body == null) {
            return null;
        } else {
            String wholeStr = new String(body);
            return (Map)JSONObject.parseObject(wholeStr.toString(), Map.class);
        }
    }

    public static Map<String, String> getUrlParams(HttpServletRequest request) {
        Map<String, String> result = new HashMap(16);
        if (StringUtils.isEmpty(request.getQueryString())) {
            return result;
        } else {
            String param = "";

            try {
                param = URLDecoder.decode(request.getQueryString(), "utf-8");
            } catch (UnsupportedEncodingException var9) {
                var9.printStackTrace();
            }

            String[] params = param.split("&");
            String[] var4 = params;
            int var5 = params.length;

            for(int var6 = 0; var6 < var5; ++var6) {
                String s = var4[var6];
                int index = s.indexOf("=");
                result.put(s.substring(0, index), s.substring(index + 1));
            }

            return result;
        }
    }

    public static Map<String, String> getUrlParams(String queryString) {
        Map<String, String> result = new HashMap(16);
        if (StringUtils.isEmpty(queryString)) {
            return result;
        } else {
            String param = "";

            try {
                param = URLDecoder.decode(queryString, "utf-8");
            } catch (UnsupportedEncodingException var9) {
                var9.printStackTrace();
            }

            String[] params = param.split("&");
            String[] var4 = params;
            int var5 = params.length;

            for(int var6 = 0; var6 < var5; ++var6) {
                String s = var4[var6];
                int index = s.indexOf("=");
                result.put(s.substring(0, index), s.substring(index + 1));
            }

            return result;
        }
    }
}
