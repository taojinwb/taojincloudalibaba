
package com.alibaba.csp.sentinel.dashboard;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import com.alibaba.csp.sentinel.init.InitExecutor;
import lombok.extern.slf4j.Slf4j;

/**
 * Sentinel dashboard application.
 *
 * @author Carpenter Lee
 */
@SpringBootApplication
@Slf4j
public class TaojinSentinelApplication {

    public static void main(String[] args) {
        System.setProperty("csp.sentinel.app.type", "1");

        triggerSentinelInit();
        ConfigurableApplicationContext application = SpringApplication.run(TaojinSentinelApplication.class, args);
        Environment env = application.getEnvironment();
        String port = env.getProperty("server.port");

        System.setProperty("sentinel.dashboard.auth.username", env.getProperty("sentinel.dashboard.auth.username"));
        System.setProperty("sentinel.dashboard.auth.password",env.getProperty("sentinel.dashboard.auth.password"));
        log.info("\n----------------------------------------------------------\n\t" +
                "Application SentinelDashboard is running! Access URLs:\n\t" +
                "Local: \t\thttp://localhost:" + port  + "/\n\t" +
                "----------------------------------------------------------");
    }

    private static void triggerSentinelInit() {
        new Thread(() -> InitExecutor.doInit()).start();
    }

}
