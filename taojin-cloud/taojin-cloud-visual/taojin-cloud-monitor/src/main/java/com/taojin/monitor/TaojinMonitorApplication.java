package com.taojin.monitor;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

/**
 * 监控服务
 * @author sujg
 * @date: 2022/4/21 10:55
 */
@SpringBootApplication
@EnableAdminServer
public class TaojinMonitorApplication {
    public static void main(String[] args) {
        SpringApplication.run(TaojinMonitorApplication.class, args);
    }


}
