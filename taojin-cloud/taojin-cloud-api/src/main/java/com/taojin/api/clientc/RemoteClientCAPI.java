package com.taojin.api.clientc;

import com.taojin.api.config.FeignClientConfig;
import com.taojin.api.vo.ResultBean;
import com.taojin.common.page.PageParam;
import com.taojin.framework.vo.ApiResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.web.bind.annotation.*;

/**
 * C 端微服务接口
 *  调用远程C端接口，不在同一个注册中心下的。
 * @author sujg
 *
 */

//@FeignClient(name = "likedong-games", url="${taojin.remote.ClientCServer}", configuration = FeignClientConfig.class)
public interface RemoteClientCAPI {

    @GetMapping("/order/detail/222")
    ApiResult getResource();


    /**
     * 查询订单详情
     * @param gamesId 总订单主键
     * @return
     */
    @RequestMapping(value = "/match/detail/{gamesId}", method = RequestMethod.GET)
    ResultBean getOrderByOrderId(@PathVariable(value = "gamesId") Long gamesId);


    /**
     * 赛事投票列表
     * @param id 赛事id
     */
    @RequestMapping(value = "/match/vote/list/{id}", method = RequestMethod.GET)
    ResultBean voteList(@PathVariable("id") Long id);


    /**
     * 获取单项赛报名列表
     * @param  pageSize 每页展示条数
     * @param  pageNum 当前页
     * @param  search 真实姓名/电话号码
     * @param  projectId 项目主键
     * @param  groupId 项目组别主键 为空表示全部
     * @param  teamsId 队伍主键 根据队伍查询队员时使用
     * @param  status 报名状态， 30 报名成功， 40 退出比赛， 不传查全部
     */
    @RequestMapping(value = "/match/listApply", method = RequestMethod.GET)
    ResultBean listApply(@RequestParam("pageSize") Integer pageSize, @RequestParam("pageNum") Integer pageNum,
                                               @RequestParam("search") String search, @RequestParam("projectId") Long projectId, @RequestParam("groupId") Long groupId,
                                               @RequestParam("teamsId") Long teamsId, @RequestParam("status") Integer status);


    /**
     * 获取团队赛报名列表
     * @param  pageSize 每页展示条数
     * @param  pageNum 当前页
     * @param  search 真实姓名/电话号码
     * @param  projectId 项目主键
     * @param  groupId 项目组别主键 为空表示全部
     * @param  status 报名状态， 30 报名成功， 40 退出比赛， 不传查全部
     * @return
     */
    @RequestMapping(value = "/match/teams/listApply", method = RequestMethod.GET)
    ResultBean listApplyTeams(@RequestParam("search") String search, @RequestParam("groupId") Long groupId, @RequestParam("projectId") Long projectId,
                                                    @RequestParam("status") Integer status, @RequestParam("pageSize") Integer pageSize, @RequestParam("pageNum") Integer pageNum);



    /**
     * 根据赛事赛事项目主键查询项目组别列表
     * @param projectId 项目id
     */
    @RequestMapping(value = "/match/getGroup/{projectId}", method = RequestMethod.GET)
    ResultBean getGroupByProjectId(@PathVariable("projectId") Long projectId);



    /** 获取参赛人员必填信息列表 */
    @RequestMapping(value = "/match/getFileds", method = RequestMethod.GET)
    ResultBean getFields();


    /** 获取线上跑赛事排名 */
    @RequestMapping(value = "/match/ranking/run/{gameId}", method = RequestMethod.GET)
    ResultBean ranking(@PathVariable("gameId") Long gameId,
                                             @RequestParam("projectId") Integer projectId, @RequestParam("groupId") Integer groupId, @SpringQueryMap PageParam pageParam );



    @RequestMapping(value = "/match/run/sign/{userId}", method = RequestMethod.GET)
    ResultBean getUserRunGameSignList(@RequestParam("gameId") Long gameId, @PathVariable("userId") Long userId, @SpringQueryMap PageParam pageParam);


}
