package com.taojin.api.vo;

/**
 * 接收外部接口返回数据
 */

import java.io.Serializable;
import java.util.List;


public class ResultBean implements Serializable {

    private int code;

    private Object response;


    private String message;

    private long totalNum;

    public ResultBean() {
    }

    public ResultBean(int code, Object response, String message) {
        this.code = code;
        this.response = response;
        this.message = message;
    }

    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public Object getResponse() {
        return response;
    }
    public void setResponse(Object response) {
        this.response = response;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public long getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(long totalNum) {
        this.totalNum = totalNum;
    }

    @Override
    public String toString() {
        return "ResultBean{" +
                "code=" + code +
                ", response=" + response +
                ", message='" + message + '\'' +
                ", totalNum='" + totalNum + '\'' +
                '}';
    }
}
