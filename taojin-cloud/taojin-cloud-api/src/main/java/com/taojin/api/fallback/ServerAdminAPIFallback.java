package com.taojin.api.fallback;

import com.taojin.api.admin.ServerAdminAPI;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

@Slf4j
public class ServerAdminAPIFallback implements ServerAdminAPI {
    @Setter
    private Throwable cause;

    public String reduceBalance(@RequestParam("userId") Long userId, @RequestParam("amount") BigDecimal amount) {
        log.error("发送消息失败 {}", cause);
        return "";
    }
}
