package com.taojin.api.admin;

import com.taojin.api.config.FeignClientConfig;
import com.taojin.api.constant.ServerConstant;
import com.taojin.framework.vo.ApiResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
/**
 * admin 端微服务接口
 *  调用远程admin端接口，不在同一个注册中心下的。
 * @author sujg
 *
 */
@FeignClient(name = ServerConstant.GATEWAY_MODULE_ADMIN, url = "${webserver.remote.server}{webserver.remote.ports.Admin}" , configuration = FeignClientConfig.class)
public interface RemoteAdminAPI {
        @GetMapping("/order/detail/222")
        ApiResult getResource();
}
