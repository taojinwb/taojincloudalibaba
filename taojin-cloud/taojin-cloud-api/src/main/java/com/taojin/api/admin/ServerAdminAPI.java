package com.taojin.api.admin;

import com.taojin.api.constant.ServerConstant;
import com.taojin.api.factory.ServerAdminAPIFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * @author sujg
 */
@FeignClient(value = ServerConstant.SERVER_MODULE_ADMIN, fallbackFactory = ServerAdminAPIFallbackFactory.class)
public interface ServerAdminAPI {

//    /**
//     *  扣减余额
//     * @param userId
//     * @param amount
//     * @return
//     */
    @PostMapping("/test/seata/account/reduceBalance")
 public  String reduceBalance(@RequestParam("userId") Long userId, @RequestParam("amount") BigDecimal amount);





}