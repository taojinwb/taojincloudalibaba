package com.taojin.api.games;

import com.taojin.api.config.FeignClientConfig;
import com.taojin.api.constant.ServerConstant;
import com.taojin.framework.vo.ApiResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
/**
 * games 端微服务接口
 *  调用远程games端接口，不在同一个注册中心下的。
 * @author sujg
 *
 */
@FeignClient(name = ServerConstant.GATEWAY_MODULE_GAMES, url="${webserver.remote.server}{webserver.remote.ports.Games}", configuration = FeignClientConfig.class)
public interface RemoteGameAPI {
    @GetMapping("/order/detail/222")
    ApiResult getResource();
}
