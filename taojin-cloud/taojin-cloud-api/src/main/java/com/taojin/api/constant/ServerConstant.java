package com.taojin.api.constant;

public class ServerConstant {
    /**
     * Admin 模块名称
     */
    public static final String SERVER_MODULE_ADMIN = "taojin-module-admin";
    /**
     * 服务外部admin 模块
     */
    public static final String GATEWAY_MODULE_ADMIN = "likedong-admin";
    /**
     * 服务外部c 模块
     */
    public static final String GATEWAY_MODULE_ClientC = "likedong-c";
    /**
     * 服务外部games 模块
     */
    public static final String GATEWAY_MODULE_GAMES = "likedong-games";
    /**
     * 服务外部saas 模块
     */
    public static final String GATEWAY_MODULE_SAAS = "likedong-saas";


}
