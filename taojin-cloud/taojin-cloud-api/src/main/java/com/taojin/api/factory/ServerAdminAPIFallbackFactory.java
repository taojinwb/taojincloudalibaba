package com.taojin.api.factory;

import com.taojin.api.admin.ServerAdminAPI;
import com.taojin.api.fallback.ServerAdminAPIFallback;
import org.springframework.cloud.openfeign.FallbackFactory;

public class ServerAdminAPIFallbackFactory implements FallbackFactory<ServerAdminAPI> {
    @Override
    public ServerAdminAPI create(Throwable throwable) {
        ServerAdminAPIFallback fallback = new ServerAdminAPIFallback();
        fallback.setCause(throwable);
        return fallback;
    }
}
