package com.taojin.api.vo;

import lombok.Data;

@Data
public class SysUserVo {
    private String token;
    private String unionid;
    private Long userId;
}
