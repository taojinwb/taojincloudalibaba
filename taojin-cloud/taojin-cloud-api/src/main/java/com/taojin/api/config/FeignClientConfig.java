package com.taojin.api.config;

import cn.hutool.core.bean.BeanUtil;
import com.taojin.api.vo.SysUserVo;
import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@Configuration
public class FeignClientConfig {

    @Bean
    public RequestInterceptor requestInterceptor() {
        return new RequestInterceptor() {
            @Override
            public void apply(RequestTemplate template) {
                // 使用 Spring Security 获取当前认证信息
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (authentication != null && authentication.isAuthenticated()) {
                    // 获取 token 并添加到请求头
                    SysUserVo  sysUserVo = new SysUserVo();
                    BeanUtil.copyProperties(authentication.getPrincipal(),sysUserVo);
                    String token = sysUserVo.getToken();
                    // template.header("Authorization", "Bearer " + token);
                    template.header("token",  token);
                }
            }
        };
    }
}