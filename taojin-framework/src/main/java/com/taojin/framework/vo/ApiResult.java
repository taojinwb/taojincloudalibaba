package com.taojin.framework.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.io.Serializable;
import java.util.List;

/**
 *   接口返回数据格式
 * @author sujg
 *   2019年1月19日
 */
@Data
@ApiModel(value="接口返回对象", description="接口返回对象")
public class ApiResult<T>  implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 成功标志
     */
    @ApiModelProperty(value = "成功标志")
    private boolean success = true;

    /**
     * 返回处理消息
     */
    @ApiModelProperty(value = "返回处理消息")
    private String message = "";

    /**
     * 返回代码
     */
    @ApiModelProperty(value = "返回代码")
    private Integer code = 0;

    @ApiModelProperty(value = "记录总数")
    private Long total = 0L;

    /**
     * 返回数据对象 data
     */
    @ApiModelProperty(value = "返回数据对象")
    private T data;

    /**
     * 时间戳
     */
    @ApiModelProperty(value = "时间戳")
    private long timestamp = System.currentTimeMillis();

    public ApiResult() {
    }


    /**
     * 兼容VUE3版token失效不跳转登录页面
     * @param code 错误代码
     * @param message 错误信息
     */
    public ApiResult(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public ApiResult<T> success(String message) {
        this.message = message;
        this.code = ApiCode.TJ_200;
        this.success = true;
        return this;
    }

    public static<T> ApiResult<T> ok() {
        ApiResult<T> r = new ApiResult<>();
        r.setSuccess(true);
        r.setCode(ApiCode.TJ_200);
        return r;
    }

    public static<T> ApiResult<T> ok(String msg) {
        ApiResult<T> r = new ApiResult<>();
        r.setSuccess(true);
        r.setCode(ApiCode.TJ_200);
        //Result OK(String msg)方法会造成兼容性问题 issues/I4IP3D
        r.setData((T) msg);
        r.setMessage(msg);
        return r;
    }

    public static<T> ApiResult<T> ok(T data) {
        ApiResult<T> r = new ApiResult<>();
        r.setSuccess(true);
        r.setCode(ApiCode.TJ_200);
        r.setData(data);
        return r;
    }

    public static<T> ApiResult<T> OK() {
        ApiResult<T> r = new ApiResult<>();
        r.setSuccess(true);
        r.setCode(ApiCode.TJ_200);
        return r;
    }

    /**
     * 此方法是为了兼容升级所创建
     *
     * @param msg 错误信息
     * @param <T> 返回类型
     * @return
     */
    public static<T> ApiResult<T> OK(String msg) {
        ApiResult<T> r = new ApiResult<>();
        r.setSuccess(true);
        r.setCode(ApiCode.TJ_200);
        r.setMessage(msg);
        r.setData((T) msg);
        return r;
    }

    public static<T> ApiResult<T> OK(T data) {
        ApiResult<T> r = new ApiResult<>();
        r.setSuccess(true);
        r.setCode(ApiCode.TJ_200);
        r.setData(data);
        return r;
    }
    public static<T> ApiResult<List<T>> OK(Page<T> page) {
        ApiResult<List<T>> r = new ApiResult<>();
        r.setSuccess(true);
        r.setCode(ApiCode.TJ_200);
        r.setTotal(page.getTotalElements());
        r.setData(page.getContent());
        return r;
    }

    public static<T> ApiResult<T> OK(String msg, T data) {
        ApiResult<T> r = new ApiResult<>();
        r.setSuccess(true);
        r.setCode(ApiCode.TJ_200);
        r.setMessage(msg);
        r.setData(data);
        return r;
    }

    public static<T> ApiResult<T> error(String msg, T data) {
        ApiResult<T> r = new ApiResult<>();
        r.setSuccess(false);
        r.setCode(ApiCode.TJ_500);
        r.setMessage(msg);
        r.setData(data);
        return r;
    }

    public static<T> ApiResult<T> error(String msg) {
        return error(ApiCode.TJ_500, msg);
    }

    public static<T> ApiResult<T> error(int code, String msg) {
        ApiResult<T> r = new ApiResult<>();
        r.setCode(code);
        r.setMessage(msg);
        r.setSuccess(false);
        return r;
    }

    public ApiResult<T> error500(String message) {
        this.message = message;
        this.code = ApiCode.TJ_500;
        this.success = false;
        return this;
    }



    @JsonIgnore
    private String onlTable;

}
