package com.taojin.framework.vo;

/**
 * 错误代码
 */
public interface ApiCode {
    /**
     * 正常
     */
    Integer TJ_200 = 200;

    /**
     * 服务器错误，请联系管理员!
     */
    Integer TJ_500 = 500;

    /**
     * 访问权限认证未通过
     */
    Integer TJ_510 = 510;

    /**
     * 认证失败，无法访问系统资源
     */
    Integer TJ_401 = 401;

    /**
     * 请求方法不支持
     */
    Integer TJ_405 = 405;

    /**
     * 访问资源不存在
     */
    Integer TJ_404 = 404;

    /**
     * 字段映射失败 jpadao
     */
    Integer TJ_407 = 407;
    /**
     * 服务器限流
     */
    Integer TJ_408 = 408;

    /**
     * rocketmq 发送消息失败
     */
    Integer TJ_808 = 808;

}
