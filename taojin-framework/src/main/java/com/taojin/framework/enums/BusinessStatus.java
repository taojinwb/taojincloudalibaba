package com.taojin.framework.enums;

/**
 * 操作状态
 * 
 * @author sujg
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
