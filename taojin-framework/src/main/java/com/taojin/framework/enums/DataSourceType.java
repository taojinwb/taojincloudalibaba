package com.taojin.framework.enums;

/**
 * 数据源
 *
 * @author sujg
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
