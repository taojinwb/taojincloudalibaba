package com.taojin.framework.utils.encryption;

import lombok.Data;

/**
 * @Description: EncryptedString
 * @author: sujg
 */
@Data
public class  EncryptedString {

    /**
     * 长度为16个字符
     */
    public static  String key = "1234567890adbcde";

    /**
     * 长度为16个字符
     */
    public static  String iv  = "1234567890hjlkew";
}