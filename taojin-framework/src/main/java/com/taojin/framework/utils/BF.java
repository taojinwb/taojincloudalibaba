package com.taojin.framework.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletRequest;
import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

@Slf4j
public class BF {

    /**
     * 数字金额大写转换，思想先写个完整的然后将如零拾替换成零
     * 要用到正则表达式
     */
    public static String digitUppercase(double n){
        String fraction[] = {"角", "分"};
        String digit[] = { "零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖" };
        String unit[][] = {{"元", "万", "亿"},
                {"", "拾", "佰", "仟"}};

        String head = n < 0? "负": "";
        n = Math.abs(n);

        String s = "";
        for (int i = 0; i < fraction.length; i++) {
            s += (digit[(int)(Math.floor(n * 10 * Math.pow(10, i)) % 10)] + fraction[i]).replaceAll("(零.)+", "");
        }
        if(s.length()<1){
            s = "整";
        }
        int integerPart = (int)Math.floor(n);

        for (int i = 0; i < unit[0].length && integerPart > 0; i++) {
            String p ="";
            for (int j = 0; j < unit[1].length && n > 0; j++) {
                p = digit[integerPart%10]+unit[1][j] + p;
                integerPart = integerPart/10;
            }
            s = p.replaceAll("(零.)*零$", "").replaceAll("^$", "零") + unit[0][i] + s;
        }
        return head + s.replaceAll("(零.)*零元", "元").replaceFirst("(零.)+", "").replaceAll("(零.)+", "零").replaceAll("^整$", "零元整");
    }

    public static String getRandom(int numvalue){
        String str="";
        Random rd = new Random();
        if(BF.APPCompare(numvalue, "<=", "0")){
            numvalue = 10;
        }
        for(int i=0;i<numvalue;i++){
            str = str + String.valueOf(rd.nextInt(10));
        }

        // System.out.println(str);
        return str;
    }
    public static String getNull(String value){
        if(BF.APPCompare(value, "=", "")){
            return null;
        }
        return value;
    }

    public static double getDouble(String value){
        String V = "0";
        if(BF.APPCompare(BF.getThisString(value), "=", "")){
            V = "0";
        }else{
            V = value;
        }
        return NumberUtils.createDouble(APPISNULL(V,"0")).doubleValue();
    }

    public static int toInt(String value){
        String V="0";
        if(BF.APPCompare(BF.getThisString(value), "=", "")){
            V = "0";
        }else{
            V = value;
        }
        if(V.indexOf(".")>0){
            V = V.substring(0,V.indexOf("."));
        }
        return Integer.parseInt(V);
    }

    public static int toInt(Double value){
        String V= "";
        if(BF.APPCompare(BF.getThisString(value), "=", "")){
            V = "0";
        }else{
            V = String.valueOf(value);
        }
        if(V.indexOf(".")>0){
            V = V.substring(0,V.indexOf("."));
        }
        return Integer.parseInt(V);
    }


    /**
     * Method 取对象的ToString值.
     * @param
     * @return String
     */
    public static String getThisString(char obj) {
        Character c = new Character(obj);
        return c.toString();
    }
    /**
     * Method 取对象的ToString值.
     * @param  obj
     * @param
     * @return String
     */
    public static String getThisString(int obj) {
        String sobjString = "";
        sobjString = Integer.toString(obj);
        sobjString.trim();
        return sobjString;
    }
    /**
     * Method 取对象的ToString值.
     * @param  obj
     * @param
     * @return String
     */
    public static String getThisString(Object obj) {
        String sobjString = "";
        if (obj != null) {
            sobjString = obj.toString().trim();
        }
        return sobjString;
    }


    public static String getUUID(){
        return java.util.UUID.randomUUID().toString().replaceAll("-", "");
    }
    /**
     * 返回在给定日期时间基础上加若干个单位后的日期时间， type表示单位的类型，1-年，2-月，3-日，4-时，5-分，6-秒，7-周
     * numeric_exp正整数为加，负整数为减
     */
    //时间函数
    public static String APPDTADD(String DateValue, String pos, String Type) {
        String sContent = "";
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(StringToTime(DateValue));
        switch (Integer.parseInt(Type)) {
            case 1:
                calendar.add(Calendar.YEAR, Integer.parseInt(pos));
                break;
            case 2:
                calendar.add(Calendar.MONTH, Integer.parseInt(pos));
                break;
            case 3:
                calendar.add(Calendar.DATE, Integer.parseInt(pos));
                break;
            case 4:
                calendar.add(Calendar.HOUR, Integer.parseInt(pos));
                break;
            case 5:
                calendar.add(Calendar.MINUTE, Integer.parseInt(pos));
                break;
            case 6:
                calendar.add(Calendar.SECOND, Integer.parseInt(pos));
                break;
            case 7:
                calendar.add(Calendar.DATE, Integer.parseInt(pos) * 7);
                break;
            default:
                break;
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        sContent = formatter.format(calendar.getTime());
        DateValue = StringUtils.replace(DateValue, "-", "");
        DateValue = StringUtils.replace(DateValue, "/", "");
        DateValue = StringUtils.replace(DateValue, ":", "");
        DateValue = StringUtils.replace(DateValue, " ", "");
        sContent = StringUtils.substring(sContent, 0, DateValue.length());
        return sContent;
    }

    /**
     * 返回在给定两个日期时间之间的差， type表示单位的类型，1-年，2-月，3-日，4-时，5-分，6-秒，7-周，
     * 返回正整数表示前者比后者早，负整数反之
     */
    public static String APPDTDIF(String DataVaule1, String DateValue2,
                                  String Type) {
        String sContent = "";
        Date d1 = StringToTime(DataVaule1);
        Date d2 = StringToTime(DateValue2);
        //System.out.println("d1:"+d1);
        //System.out.println("d2:"+d2);

        long diff = d2.getTime() - d1.getTime();
        //System.out.println("diff:"+diff);
        switch (Integer.parseInt(Type)) {
            case 1:
                //sContent = Long.toString(diff / (1000 * 60 * 60 * 24 * 365));
                sContent = Long.toString(diff /1000/60/60/24/365);
                break;
            case 2:
                //sContent = Long.toString(diff / (1000 * 60 * 60 * 24 * 30));
                sContent = Long.toString(diff/1000/60/60/24/30);
                break;
            case 3:
                //sContent = Long.toString(diff / (1000 * 60 * 60 * 24));
                sContent = Long.toString(diff /1000/60/60/24);
                //System.out.println(sContent);
                //System.out.println(diff /1000/60/60/24);
                break;
            case 4:
                //sContent = Long.toString(diff / (1000 * 60 * 60));
                sContent = Long.toString(diff /1000/60/60);
                break;
            case 5:
                //sContent = Long.toString(diff / (1000 * 60));
                sContent = Long.toString(diff /1000/60);
                break;
            case 6:
                sContent = Long.toString(diff / (1000));
                break;
            case 7:
                //sContent = Long.toString(diff / (1000 * 60 * 60 * 24 * 7));
                sContent = Long.toString(diff /1000/60/60/24/7);
                break;
            default:
                break;
        }
        return sContent;
    }
    /**
     * 返回日期类型。 如果不合法则返回1900-01-01 00:00:00
     */
    public static Date StringToTime(String DateValue) {
        DateValue = StringUtils.replace(DateValue, "-", "");
        DateValue = StringUtils.replace(DateValue, "/", "");
        DateValue = StringUtils.replace(DateValue, ":", "");
        DateValue = StringUtils.replace(DateValue, " ", "");
        String YYYY = "1999";
        if (StringUtils.substring(DateValue, 0, 4).length() == 4) {
            YYYY = StringUtils.substring(DateValue, 0, 4);
        }

        String MM = "01";
        if (StringUtils.substring(DateValue, 4, 6).length() == 2) {
            MM = StringUtils.substring(DateValue, 4, 6);
        }
        String dd = "01";
        if (StringUtils.substring(DateValue, 6, 8).length() == 2) {
            dd = StringUtils.substring(DateValue, 6, 8);
        }
        String hh = "00";
        if (StringUtils.substring(DateValue, 8, 10).length() == 2) {
            hh = StringUtils.substring(DateValue, 8, 10);
        }
        String mm = "00";
        if (StringUtils.substring(DateValue, 10, 12).length() == 2) {
            mm = StringUtils.substring(DateValue, 10, 12);
        }
        String ss = "00";
        if (StringUtils.substring(DateValue, 12, 14).length() == 2) {
            ss = StringUtils.substring(DateValue, 12, 14);
        }
        DateValue = YYYY + "-" + MM + "-" + dd + " " + hh + ":" + mm + ":" + ss;
        DateFormat formatter = DateFormat.getDateTimeInstance();
        java.util.Date temp = null;
        try {
            formatter.setLenient(true);
            temp = formatter.parse(DateValue);
        } catch (Exception e) {
        }
        return temp;
    }
    /**
     * 返回格式化后的。 如果不合法则返回1900-01-01 00:00:00
     */
    public static String FormatDateString(String DateValue) {
        String dstr="";
        DateValue = StringUtils.replace(DateValue, "-", "");
        DateValue = StringUtils.replace(DateValue, "/", "");
        DateValue = StringUtils.replace(DateValue, ":", "");
        DateValue = StringUtils.replace(DateValue, " ", "");
        String YYYY = "1999";
        if (StringUtils.substring(DateValue, 0, 4).length() == 4) {
            YYYY = StringUtils.substring(DateValue, 0, 4);
        }
        dstr = YYYY;
        String MM = "01";
        if (StringUtils.substring(DateValue, 4, 6).length() == 2) {
            MM = StringUtils.substring(DateValue, 4, 6);
        }
        dstr = dstr +"-"+MM;
        String dd = "01";
        if (StringUtils.substring(DateValue, 6, 8).length() == 2) {
            dd = StringUtils.substring(DateValue, 6, 8);
        }
        dstr = dstr +"-"+dd;
        String hh = "00";
        if (StringUtils.substring(DateValue, 8, 10).length() == 2) {
            hh = StringUtils.substring(DateValue, 8, 10);
            dstr = dstr +" "+hh;
        }
        String mm = "00";
        if (StringUtils.substring(DateValue, 10, 12).length() == 2) {
            mm = StringUtils.substring(DateValue, 10, 12);
            dstr = dstr +":"+mm;
        }
        String ss = "00";
        if (StringUtils.substring(DateValue, 12, 14).length() == 2) {
            ss = StringUtils.substring(DateValue, 12, 14);
            dstr = dstr +":"+ss;
        }
        //DateValue = YYYY + "-" + MM + "-" + dd + " " + hh + ":" + mm + ":" + ss;
        return dstr;
    }
    /**
     * 返回日期加减天数后的日期。
     */
    public static String APPGETSHIFTDATE(String DateValue, String pos) {
        String sContent = "";
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(StringToTime(DateValue));
        calendar.add(Calendar.DATE, Integer.parseInt(pos));
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyyMMdd");
        return formatter1.format(calendar.getTime());
    }
    /**
     * 返回给定数字表达式的绝对值。
     */

    public static String APPABS(String Vaule) {
        String sContent = "";
        return sContent;
    }

    /**
     * 返回大于或等于所给数字表达式的最小整数。
     */
    public static String APPCEIL(String Vaule) {
        String sContent = "";
        String A[] = StringUtils.split(Vaule, ".");
        if (A.length == 2) {
            sContent = A[0];
        } else {
            sContent = Vaule;
        }
        int iVaule = Integer.parseInt(sContent);
        if (sContent.indexOf("-") < 0 && A.length == 2) {
            iVaule = iVaule + 1;
        }
        sContent = Integer.toString(iVaule);
        return sContent;
    }

    /**
     * 返回小于或等于所给数字表达式的最大整数。
     */

    public static String APPFLOOR(String Vaule) {
        String sContent = "";
        String A[] = StringUtils.split(Vaule, ".");
        if (A.length == 2) {
            sContent = A[0];
        } else {
            sContent = Vaule;
        }
        int iVaule = Integer.parseInt(sContent);
        if (sContent.indexOf("-") >= 0 && A.length == 2) {
            iVaule = iVaule - 1;
        }
        sContent = Integer.toString(iVaule);
        return sContent;
    }

    /**
     * 返回数字表达式并四舍五入为指定的长度或精度。
     */
    public static String APPROUNDEx(String Vaule, String length) {
        if (Vaule.indexOf(".")==-1 ){
            return Vaule;
        }else{
            return APPROUND(Vaule, length);
        }
    }


    /**
     * 返回数字表达式并四舍五入为指定的长度或精度。
     */
    public static String APPROUND(String Vaule, String length) {
        String sContent = "";
        if (Integer.parseInt(length) < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        BigDecimal b = new BigDecimal(Vaule);
        BigDecimal one = new BigDecimal("1");
        double A = b.divide(one, Integer.parseInt(length),
                BigDecimal.ROUND_HALF_UP).doubleValue();

        return Double.toString(A);
    }
    /**
     * 返回数字表达式并四舍五入为指定的长度或精度。
     */
    public static double APPROUND(String Vaule, int length) {
        double V = BF.getDouble(Vaule);

        return APPROUND(V,length);
    }

    /**
     * 返回数字表达式并四舍五入为指定的长度或精度。
     * 超过8为会出错
     */
    public static double APPROUND(double Vaule, int length) {
        String sContent = "";

        if (length < 0) {
            throw new IllegalArgumentException(
                    "The scale must be a positive integer or zero");
        }
        BigDecimal b = new BigDecimal(Vaule);
        BigDecimal one = new BigDecimal("1");
        double A = b.divide(one, length, BigDecimal.ROUND_HALF_UP).doubleValue();
        return A;
    }

    /**
     * 返回数字表达式并四舍五入为指定的长度或精度。
     */
    public static BigDecimal DecimalRound(double Vaule, int length) {
        BigDecimal mData = new BigDecimal(Vaule).setScale(2, BigDecimal.ROUND_HALF_UP);
        // System.out.println("mData=" + mData);
        return mData;
    }

    /**
     * 返回两数的差数。
     */
    public static String APPCOMPAREFLOAT(String Vaule1, String Vaule2,
                                         String length) {
        String sContent = "";
        return sContent;
    }

    /**
     * 返回给定表达式的正 (+1)、零 (0) 或负 (-1) 号。
     */

    public static String APPSIGN(String Value) {
        String sContent = "0";
        if (StringUtils.isEmpty(Value)) {
            sContent = "0";
        } else {
            if (StringUtils.substring(Value, 0, 1).equals("+")) {
                sContent = "+";
            }
            if (StringUtils.substring(Value, 0, 1).equals("-")) {
                sContent = "-";
            }
        }
        return sContent;
    }

    /**
     * 使用指定的替换值替换空值。
     */
    public static String APPISNULL(String Value, String sReturn) {
        if (Value == null) {
            return sReturn;
        } else {
            return Value;
        }

    }

    /**
     * >
     */
    public static boolean APPBIG(String Value1, String Value2) {
        Value1 = APPISNULL(Value1, "0");
        Value2 = APPISNULL(Value2, "0");
        if (!NumberUtils.isNumber(Value1)) {
            Value1 = "0";
        }
        if (!NumberUtils.isNumber(Value2)) {
            Value2 = "0";
        }
        double v1 = NumberUtils.createDouble(Value1).doubleValue();
        double v2 = NumberUtils.createDouble(Value2).doubleValue();
        if (v1 > v2) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * <
     */
    public static boolean APPLESS(String Value1, String Value2) {
        Value1 = APPISNULL(Value1, "0");
        Value2 = APPISNULL(Value2, "0");
        if (!NumberUtils.isCreatable(Value1)) {
            Value1 = "0";
        }
        if (!NumberUtils.isCreatable(Value2)) {
            Value2 = "0";
        }
        double v1 = NumberUtils.createDouble(Value1).doubleValue();
        double v2 = NumberUtils.createDouble(Value2).doubleValue();
        if (v1 < v2) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * >=
     */
    public static boolean APPBIGEQUAL(String Value1, String Value2) {
        return !APPLESS(Value1, Value2);
    }

    /**
     * <=
     */
    public static boolean APPLESSEQUAL(String Value1, String Value2) {
        return !APPBIG(Value1, Value2);
    }

    /**
     * =
     */
    public static boolean APPEQUAL(String Value1, String Value2) {
        boolean bool = false;
        Value1 = APPISNULL(Value1, "");
        Value2 = APPISNULL(Value2, "");
        if (StringUtils.equalsIgnoreCase(StringUtils.trim(Value1), StringUtils
                .trim(Value2))) {
            bool = true;
        }
        return bool;
    }
    public static boolean isInt(String Value){
        Value = Value.replaceAll(".00", "");
        Value = Value.replaceAll(".0", "");
        Pattern pattern = Pattern.compile("[0-9]*");
        return pattern.matcher(Value).matches();
    }
    /**
     * !=<>
     */
    public static boolean APPNOTEQUAL(String Value1, String Value2) {
        return !APPEQUAL(Value1, Value2);
    }
    public static String getShsj(String V){
        String shsj="";
        if(BF.APPCompare(V, "=", "1")){
            shsj = "工作日、双休日与假日均可送货";
        }else if(BF.APPCompare(V, "=", "2")){
            shsj = "只双休日、假日送货（工作日不用送）";
        }else if(BF.APPCompare(V, "=", "3")){
            shsj = "只工作日送货（双休日、假日不用送）（注：写字楼 / 商用地址客户请选择）";
        }else if(BF.APPCompare(V, "=", "4")){
            shsj = "学校地址 / 地址白天没人，请尽量安排其他时间送货（注：特别安排可能超出预计送货天数）";
        }else{
            shsj = "工作日、双休日与假日均可送货";
        }
        return shsj;
    }

    /**
     * 判断数值是否在字符串中存在。
     * @param str 字符串组
     * @param V 要查找的值
     * @param split 分割符合，默认为,
     * @return 有则true 否则false
     */
    public static boolean IsInStr(String str ,String V,String split){
        if(BF.APPCompare(split, "=", "")){
            split= ",";
        }
        String A[] = str.split(split);
        for(int i=0;i<A.length;i++){
            if(BF.APPCompare(A[i].trim(), "=", V.trim())){
                return true;
            }
        }
        return false;
    }
    /**
     * 计算两个数的和
     * @param V1
     * @param V2
     * @return 返回两个相加的结果 int
     */
    public static int sumsz(String V1,String V2){
        int a =0;
        int b =0;
        a = Integer.parseInt(V1);
        b = Integer.parseInt(V2);
        return a+b;
    }
    /**
     * 返回replace后的字串
     */
    public static String APPREPLACE(String svalue,String sfind,String sreplace) {
        return StringUtils.replace( svalue,sfind,sreplace);
    }
    /**
     * 转换为HTML转义字符表示
     * @param str
     */
    public static String htmlEscape(String str){
        return HtmlUtils.htmlEscape(str);
    }
    /**
     * 转义后字符串进行反向操作
     * @param str
     */
    public static String htmlUnEscape(String str){
        return HtmlUtils.htmlUnescape(str);
    }

    public static String URLEncode(String str){
        String s = "";
        try{
            s = java.net.URLEncoder.encode(str,"utf-8");
        }catch(Exception e){
            e.printStackTrace();
        }
        return s;
    }
    public static String URLDcode(String str){
        return URLDcode(str,"utf-8");
    }
    public static String URLDcode(String str,String ecode){
        String s = "";
        try{
            s = java.net.URLDecoder.decode(str,ecode);
        }catch(Exception e){
            e.printStackTrace();
        }
        return s;
    }

    public static boolean APPCompare(String Value1, String Type, String Value2) {
        boolean bool = false;
        boolean isString =false;
        try {
            double v1 = NumberUtils.createDouble(BF.getThisString(Value1)).doubleValue();
            double v2 = NumberUtils.createDouble(BF.getThisString(Value2)).doubleValue();
            bool = APPCompare(v1, Type, v2);
        }
        catch (Exception ex) {
            isString=true;
        }

        if (isString) {
            if (StringUtils.equalsIgnoreCase(Type, "=")) {
                bool = APPEQUAL(Value1, Value2);
            }
            if (StringUtils.equalsIgnoreCase(Type, "<>")
                    || StringUtils.equalsIgnoreCase(Type, "!=")) {
                bool = APPNOTEQUAL(Value1, Value2);
            }
            if (StringUtils.equalsIgnoreCase(Type, ">=")) {
                bool = APPBIGEQUAL(Value1, Value2);
            }
            if (StringUtils.equalsIgnoreCase(Type, ">")) {
                bool = APPBIG(Value1, Value2);
            }
            if (StringUtils.equalsIgnoreCase(Type, "<=")) {
                bool = APPLESSEQUAL(Value1, Value2);
            }
            if (StringUtils.equalsIgnoreCase(Type, "<")) {
                bool = APPLESS(Value1, Value2);
            }
        }
        return bool;
    }

    public static boolean APPCompare(int Value1, String Type, int Value2) {
        boolean bool = false;
        if (StringUtils.equalsIgnoreCase(Type, "=")) {
            bool = (Value1 == Value2);
        }
        if (StringUtils.equalsIgnoreCase(Type, "<>")
                || StringUtils.equalsIgnoreCase(Type, "!=")) {
            bool = (Value1 != Value2);
        }
        if (StringUtils.equalsIgnoreCase(Type, ">=")) {
            bool = (Value1 >= Value2);
        }
        if (StringUtils.equalsIgnoreCase(Type, ">")) {
            bool = (Value1 > Value2);
        }
        if (StringUtils.equalsIgnoreCase(Type, "<=")) {
            bool = (Value1 <= Value2);
        }
        if (StringUtils.equalsIgnoreCase(Type, "<")) {
            bool = (Value1 < Value2);
        }
        return bool;
    }

    public static boolean APPCompare(float Value1, String Type, float Value2) {
        String sV1 = Float.toString(Value1);
        String sV2 = Float.toString(Value2);
        double V1 = NumberUtils.createDouble(sV1).doubleValue();
        double V2 = NumberUtils.createDouble(sV2).doubleValue();
        return APPCompare(V1, Type, V2);
    }

    public static boolean APPCompare(double Value1, String Type, double Value2) {
        boolean bool = false;
        Type = Type.trim();
        Value1 = APPROUND(Value1, 9);
        Value2 = APPROUND(Value2, 9);
        if (StringUtils.equalsIgnoreCase(Type, "=")) {
            bool = (Value1 == Value2);
        }
        if (StringUtils.equalsIgnoreCase(Type, "<>")
                || StringUtils.equalsIgnoreCase(Type, "!=")) {
            bool = (Value1 != Value2);
        }
        if (StringUtils.equalsIgnoreCase(Type, ">=")) {
            bool = (Value1 >= Value2);
        }
        if (StringUtils.equalsIgnoreCase(Type, ">")) {
            bool = (Value1 > Value2);
        }
        if (StringUtils.equalsIgnoreCase(Type, "<=")) {
            bool = (Value1 <= Value2);
        }
        if (StringUtils.equalsIgnoreCase(Type, "<")) {
            bool = (Value1 < Value2);
        }
        return bool;
    }

    public static boolean APPCompare(int Value1, String Type, String Value2) {
        String sValue1 = Integer.toString(Value1);
        return APPCompare(sValue1, Type, Value2);
    }

    public static boolean APPCompare(String Value1, String Type, int Value2) {
        String sValue2 = Integer.toString(Value2);
        return APPCompare(Value1, Type, sValue2);
    }

    public static boolean APPCompare(String Value1, String Type, float Value2) {
        String sValue2 = Float.toString(Value2);
        return APPCompare(Value1, Type, sValue2);
    }

    public static boolean APPCompare(float Value1, String Type, String Value2) {
        String sValue1 = Float.toString(Value1);
        return APPCompare(sValue1, Type, Value2);
    }

    public static boolean APPCompare(double Value1, String Type, String Value2) {
        String sValue1 = Double.toString(Value1);
        return APPCompare(sValue1, Type, Value2);
    }

    public static boolean APPCompare(String Value1, String Type, double Value2) {
        String sValue2 = Double.toString(Value2);
        return APPCompare(Value1, Type, sValue2);
    }

    private static boolean IsNumber(String expression) {
        boolean bool = true;
        String sContent = "";
        sContent = StringUtils.replace(expression,"+"," ");
        sContent = StringUtils.replace(sContent," "," ");
        sContent = StringUtils.replace(sContent,"-"," ");
        sContent = StringUtils.replace(sContent,"*"," ");
        sContent = StringUtils.replace(sContent,"/"," ");
        sContent = StringUtils.replace(sContent,"("," ");
        sContent = StringUtils.replace(sContent,")"," ");
//		sContent = StringUtils.replace(sContent,".","");
        String A[] = StringUtils.split(sContent," " );
        double v1=0;
        for (int i=0 ;i<A.length;i++ ){
            try {
                if( StringUtils.equals( A[i].trim(),"")){A[i]="0";}
                v1=NumberUtils.createDouble(A[i]).doubleValue();
                //double v1 = NumberUtils.createDouble(sContent).doubleValue();
            }catch (Exception ex) {
                bool=false;
            }
        }
        return bool;
    }
    private static String Replaceminus(String expression) {
        expression = ReplaceSingleminus(expression, "+-");
        expression = ReplaceSingleminus(expression, "--");
        expression = ReplaceSingleminus(expression, "*-");
        expression = ReplaceSingleminus(expression, "/-");
        expression = ReplaceSingleminus(expression, "(-");
        //System.out.println(expression);
        if (expression.substring(0, 1).equals("-")) {
            String exNoNumRight = "";
            String exNum = expression;
            for (int i = 1; i < expression.length(); i++) {
                if (expression.substring(i, i + 1).equals("+")
                        || expression.substring(i, i + 1).equals("-")
                        || expression.substring(i, i + 1).equals("*")
                        || expression.substring(i, i + 1).equals("/")
                        || expression.substring(i, i + 1).equals("(")
                        || expression.substring(i, i + 1).equals(")")) {
                    exNum = expression.substring(1, i);
                    exNoNumRight = expression.substring(i, expression.length());
                    break;
                }
            }
            //System.out.println(expression);
            //System.out.println(exNum);
            //System.out.println(exNoNumRight);
            if (StringUtils.equals( exNoNumRight,"" )){
                expression = "(0" + exNum + ")" ;
            }else{
                expression = "(0-" + exNum + ")" + exNoNumRight;
            }

        }
        //System.out.println( expression);
        return expression;
    }

    private static String ReplaceSingleminus(String expression, String OP) {
        while (expression.indexOf(OP) > 0) {
            int iP = expression.indexOf(OP);
            String exLeft = expression.substring(0, iP);
            String exRight = expression.substring(iP + 2, expression.length());
            String exNoNumRight = "";
            String exNum = exRight;
            for (int i = 0; i < exRight.length(); i++) {
                if (exRight.substring(i, i + 1).equals("+")
                        || exRight.substring(i, i + 1).equals("-")
                        || exRight.substring(i, i + 1).equals("*")
                        || exRight.substring(i, i + 1).equals("/")
                        || exRight.substring(i, i + 1).equals("(")
                        || exRight.substring(i, i + 1).equals(")")) {
                    exNum = exRight.substring(0, i);
                    exNoNumRight = exRight.substring(i, exRight.length());
                    break;
                }
            }
            expression = exLeft + OP.substring(0, 1) + "(0-" + exNum + ")"
                    + exNoNumRight;
        }
        return expression;
    }


    /**
     * 将字符串通过base64转码
     * @param str 要转码的字符串
     * @return 返回转码后的字符串
     */
    public static String strToBase64Str(String str) {
        return new String(encode(str.getBytes()));
    }

    /**
     * 将base64码反转成字符串
     * @param base64Str base64码
     * @return 返回转码后的字符串
     */
    public static String base64StrToStr(String base64Str) {
        char[] dataArr = new char[base64Str.length()];
        base64Str.getChars(0, base64Str.length(), dataArr, 0);
        return new String(decode(dataArr));
    }

    /**
     * 将一个字节数组转换成base64的字符数组
     * @param data 字节数组
     * @return base64字符数组
     */
    private static char[] encode(byte[] data) {
        char[] out = new char[((data.length + 2) / 3) * 4];
        for (int i = 0, index = 0; i < data.length; i += 3, index += 4) {
            boolean quad = false;
            boolean trip = false;
            int val = (0xFF & (int) data[i]);
            val <<= 8;
            if ((i + 1) < data.length) {
                val |= (0xFF & (int) data[i + 1]);
                trip = true;
            }
            val <<= 8;
            if ((i + 2) < data.length) {
                val |= (0xFF & (int) data[i + 2]);
                quad = true;
            }
            out[index + 3] = alphabet[(quad ? (val & 0x3F) : 64)];
            val >>= 6;
            out[index + 2] = alphabet[(trip ? (val & 0x3F) : 64)];
            val >>= 6;
            out[index + 1] = alphabet[val & 0x3F];
            val >>= 6;
            out[index + 0] = alphabet[val & 0x3F];
        }
        return out;
    }

    /**
     * 将一个base64字符数组解码成一个字节数组
     * @param data base64字符数组
     * @return 返回解码以后的字节数组
     */
    private static byte[] decode(char[] data) {
        int len = ((data.length + 3) / 4) * 3;
        if (data.length > 0 && data[data.length - 1] == '=') --len;
        if (data.length > 1 && data[data.length - 2] == '=') --len;
        byte[] out = new byte[len];
        int shift = 0;
        int accum = 0;
        int index = 0;
        for (int ix = 0; ix < data.length; ix++) {
            int value = codes[data[ix] & 0xFF];
            if (value >= 0) {
                accum <<= 6;
                shift += 6;
                accum |= value;
                if (shift >= 8) {
                    shift -= 8;
                    out[index++] = (byte) ((accum >> shift) & 0xff);
                }
            }
        }
        if (index != out.length)
            throw new Error("miscalculated data length!");
        return out;
    }

    /**
     * base64字符集 0..63
     */
    static private char[] alphabet =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
                    .toCharArray();

    /**
     * 初始化base64字符集表
     */
    static private byte[] codes = new byte[256];

    static {
        for (int i = 0; i < 256; i++) codes[i] = -1;
        for (int i = 'A'; i <= 'Z'; i++) codes[i] = (byte) (i - 'A');
        for (int i = 'a'; i <= 'z'; i++) codes[i] = (byte) (26 + i - 'a');
        for (int i = '0'; i <= '9'; i++) codes[i] = (byte) (52 + i - '0');
        codes['+'] = 62;
        codes['/'] = 63;
    }
    public static  boolean IsMoblieUser(HttpServletRequest request) {
        boolean isMoblie = false;
        String[] mobileAgents = { "iphone", "android", "phone", "mobile", "wap", "netfront", "java", "opera mobi",
                "opera mini", "ucweb", "windows ce", "symbian", "series", "webos", "sony", "blackberry", "dopod",
                "nokia", "samsung", "palmsource", "xda", "pieplus", "meizu", "midp", "cldc", "motorola", "foma",
                "docomo", "up.browser", "up.link", "blazer", "helio", "hosin", "huawei", "novarra", "coolpad", "webos",
                "techfaith", "palmsource", "alcatel", "amoi", "ktouch", "nexian", "ericsson", "philips", "sagem",
                "wellcom", "bunjalloo", "maui", "smartphone", "iemobile", "spice", "bird", "zte-", "longcos",
                "pantech", "gionee", "portalmmm", "jig browser", "hiptop", "benq", "haier", "^lct", "320x320",
                "240x320", "176x220", "w3c ", "acs-", "alav", "alca", "amoi", "audi", "avan", "benq", "bird", "blac",
                "blaz", "brew", "cell", "cldc", "cmd-", "dang", "doco", "eric", "hipt", "inno", "ipaq", "java", "jigs",
                "kddi", "keji", "leno", "lg-c", "lg-d", "lg-g", "lge-", "maui", "maxo", "midp", "mits", "mmef", "mobi",
                "mot-", "moto", "mwbp", "nec-", "newt", "noki", "oper", "palm", "pana", "pant", "phil", "play", "port",
                "prox", "qwap", "sage", "sams", "sany", "sch-", "sec-", "send", "seri", "sgh-", "shar", "sie-", "siem",
                "smal", "smar", "sony", "sph-", "symb", "t-mo", "teli", "tim-", "tosh", "tsm-", "upg1", "upsi", "vk-v",
                "voda", "wap-", "wapa", "wapi", "wapp", "wapr", "webc", "winw", "winw", "xda", "xda-",
                "Googlebot-Mobile" };
        if (request.getHeader("User-Agent") != null) {
            String ua = request.getHeader("User-Agent").toLowerCase();
            for (String mobileAgent : mobileAgents) {
                if (ua.indexOf(mobileAgent) >= 0) {
                    isMoblie = true;
                    if(ua.indexOf("os x") >= 0 && ua.indexOf("phone")<0 && ua.indexOf("iphone")<0){
                        isMoblie = false;
                    }
                    break;
                }

            }
        }
        return isMoblie;
    }


    public static String str2HexStr(String str) {
        char[] chars = "0123456789ABCDEF".toCharArray();
        StringBuilder sb = new StringBuilder("");
        byte[] bs = str.getBytes();
        int bit;
        for (int i = 0; i < bs.length; i++) {
            bit = (bs[i] & 0x0f0) >> 4;
            sb.append(chars[bit]);
            bit = bs[i] & 0x0f;
            sb.append(chars[bit]);
        }
        return sb.toString();
    }
    public static String formatSingleZero(double s){
        DecimalFormat fmt = new DecimalFormat("0.0");
        return fmt.format(s);
    }
    public static String formatDoubleZero(double s){
        DecimalFormat fmt = new DecimalFormat("0.00");
        return fmt.format(s);
    }

    public static String parseMoney(String pattern, BigDecimal bd){
        DecimalFormat df = new DecimalFormat(pattern);
        return df.format(bd);
    }

    public static String formatMoney(String str){
        BigDecimal db = new BigDecimal(str);
        String s = BF.parseMoney(",###,##0.00", db);
        return s;
    }
    public static String convertToSpecial(String input) {
        StringBuilder result = new StringBuilder();
        boolean isFirstChar = true;
        for (char c : input.toCharArray()) {
            if (Character.isUpperCase(c)) {
                if (!isFirstChar) {
                    result.append("_" + Character.toLowerCase(c));
                } else {
                    result.append(Character.toLowerCase(c));
                }
            } else {
                result.append(c);
            }
            isFirstChar = Character.isWhitespace(c);
        }
        return result.toString();
    }
    public static void main(String[] args) {
        try {

           String[] A = new String[50000];
           String d="1,2,3,4,5,6,7,8,9,10";
            String dateString="2023-10-28";
            if(dateString.trim().length()==10){
                dateString = dateString+" 00:00:00";
            }
            
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
             dateFormat.parse(dateString);
//           for(int i=0;i<A.length;i++){
//               A[i] = d;
//           }
//            Cs sysUser=new Cs();
//            sysUser.setUserId(null);
//            log.info(System.currentTimeMillis()+"========kkk"+convertToSpecial("SysUserNameFl"));
//          //  final PropertyDescriptor[] targetPds = BeanUtils.getPropertyDescriptors(Cs.class);
//            for(int i=0;i<500000;i++){
//                final  PropertyDescriptor[] targetPds = BeanUtils.getPropertyDescriptors(Cs.class);
//                for(int j=0;j<targetPds.length;j++){
//                    //log.info(targetPds[j].getName());
//                }
//            }
//
//            log.info(System.currentTimeMillis()+"=======jjj");
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.print(ex.getMessage() + "发送验证邮件失败！");
        }
    }
}
