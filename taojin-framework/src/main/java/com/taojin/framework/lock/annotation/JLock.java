package com.taojin.framework.lock.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.taojin.framework.lock.enums.LockModel;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface JLock {
    LockModel lockModel() default LockModel.AUTO;

    String[] lockKey() default {};

    String keyConstant() default "";

    long expireSeconds() default 30000L;

    long waitTime() default 10000L;

    String failMsg() default "获取锁失败，请稍后重试";
}
