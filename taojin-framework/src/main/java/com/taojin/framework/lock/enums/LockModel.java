package com.taojin.framework.lock.enums;

public enum LockModel {
    REENTRANT,
    FAIR,
    MULTIPLE,
    REDLOCK,
    READ,
    WRITE,
    AUTO;

    private LockModel() {
    }
}
