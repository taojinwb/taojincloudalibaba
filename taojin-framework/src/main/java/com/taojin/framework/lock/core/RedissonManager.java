package com.taojin.framework.lock.core;


import cn.hutool.core.lang.Assert;
import com.taojin.framework.lock.core.strategy.RedissonConfigStrategy;
import com.taojin.framework.lock.core.strategy.impl.ClusterRedissonConfigStrategyImpl;
import com.taojin.framework.lock.core.strategy.impl.MasterslaveRedissonConfigStrategyImpl;
import com.taojin.framework.lock.core.strategy.impl.SentinelRedissonConfigStrategyImpl;
import com.taojin.framework.lock.core.strategy.impl.StandaloneRedissonConfigStrategyImpl;
import com.taojin.framework.lock.enums.RedisConnectionType;
import com.taojin.framework.lock.prop.RedissonProperties;
import org.redisson.Redisson;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RedissonManager {
    private static final Logger log = LoggerFactory.getLogger(RedissonManager.class);
    private Config config = new Config();
    private Redisson redisson = null;

    public RedissonManager() {
    }

    public RedissonManager(RedissonProperties redissonProperties) {
        Boolean enabled = redissonProperties.getEnabled();
        if (enabled) {
            try {
                this.config = RedissonManager.RedissonConfigFactory.getInstance().createConfig(redissonProperties);
                this.redisson = (Redisson)Redisson.create(this.config);
            } catch (Exception var4) {
                log.error("Redisson初始化错误", var4);
            }
        }

    }

    public Redisson getRedisson() {
        return this.redisson;
    }

    static class RedissonConfigFactory {
        private static volatile RedissonConfigFactory factory = null;

        private RedissonConfigFactory() {
        }

        public static RedissonConfigFactory getInstance() {
            if (factory == null) {
                Class var0 = Object.class;
                synchronized(Object.class) {
                    if (factory == null) {
                        factory = new RedissonConfigFactory();
                    }
                }
            }

            return factory;
        }

        Config createConfig(RedissonProperties redissonProperties) {
            Assert.notNull(redissonProperties);
            Assert.notNull(redissonProperties.getAddress(), "redis地址未配置");

            RedisConnectionType connectionType = redissonProperties.getType();
            Object redissonConfigStrategy;
            if (connectionType.equals(RedisConnectionType.SENTINEL)) {
                redissonConfigStrategy = new SentinelRedissonConfigStrategyImpl();
            } else if (connectionType.equals(RedisConnectionType.CLUSTER)) {
                redissonConfigStrategy = new ClusterRedissonConfigStrategyImpl();
            } else if (connectionType.equals(RedisConnectionType.MASTERSLAVE)) {
                redissonConfigStrategy = new MasterslaveRedissonConfigStrategyImpl();
            } else {
                redissonConfigStrategy = new StandaloneRedissonConfigStrategyImpl();
            }

            Assert.notNull(redissonConfigStrategy, "连接方式创建异常");
            return ((RedissonConfigStrategy)redissonConfigStrategy).createRedissonConfig(redissonProperties);
        }
    }
}
