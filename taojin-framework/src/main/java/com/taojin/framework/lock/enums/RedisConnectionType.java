package com.taojin.framework.lock.enums;


public enum RedisConnectionType {
    STANDALONE("standalone", "单机部署方式"),
    SENTINEL("sentinel", "哨兵部署方式"),
    CLUSTER("cluster", "集群方式"),
    MASTERSLAVE("masterslave", "主从部署方式");

    private final String code;
    private final String name;

    public String getCode() {
        return this.code;
    }

    public String getName() {
        return this.name;
    }

     RedisConnectionType(final String code, final String name) {
        this.code = code;
        this.name = name;
    }
}
