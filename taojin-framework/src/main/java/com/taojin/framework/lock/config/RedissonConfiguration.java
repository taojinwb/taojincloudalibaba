package com.taojin.framework.lock.config;


import com.taojin.framework.lock.core.RedissonManager;
import com.taojin.framework.lock.prop.RedissonProperties;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass({RedissonProperties.class})
@EnableConfigurationProperties({RedissonProperties.class})
public class RedissonConfiguration {
    private static final Logger log = LoggerFactory.getLogger(RedissonConfiguration.class);

    public RedissonConfiguration() {
    }

    @Bean
    @ConditionalOnMissingBean({RedissonClient.class})
    public RedissonClient redissonClient(RedissonProperties redissonProperties) {
        RedissonManager redissonManager = new RedissonManager(redissonProperties);
        if(redissonProperties.getEnabled()) {
            log.info("RedissonManager初始化完成,当前连接方式:" + redissonProperties.getType() + ",连接地址:" + redissonProperties.getAddress());
        }
        return redissonManager.getRedisson();
    }
}
