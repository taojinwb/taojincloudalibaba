package com.taojin.framework.lock.core.strategy;


import com.taojin.framework.lock.prop.RedissonProperties;
import org.redisson.config.Config;

public interface RedissonConfigStrategy {
    Config createRedissonConfig(RedissonProperties redissonProperties);
}
