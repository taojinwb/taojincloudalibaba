package com.taojin.framework.lock.core.strategy.impl;


import java.util.ArrayList;
import java.util.List;

import com.taojin.framework.lock.core.strategy.RedissonConfigStrategy;
import com.taojin.framework.lock.prop.RedissonProperties;
import org.apache.commons.lang3.StringUtils;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MasterslaveRedissonConfigStrategyImpl implements RedissonConfigStrategy {
    private static final Logger log = LoggerFactory.getLogger(MasterslaveRedissonConfigStrategyImpl.class);

    public MasterslaveRedissonConfigStrategyImpl() {
    }

    public Config createRedissonConfig(RedissonProperties redissonProperties) {
        Config config = new Config();

        try {
            String address = redissonProperties.getAddress();
            String password = redissonProperties.getPassword();
            int database = redissonProperties.getDatabase();
            String[] addrTokens = address.split(",");
            String masterNodeAddr = addrTokens[0];
            config.useMasterSlaveServers().setMasterAddress(masterNodeAddr);
            if (StringUtils.isNotBlank(password)) {
                config.useMasterSlaveServers().setPassword(password);
            }

            config.useMasterSlaveServers().setDatabase(database);
            List<String> slaveList = new ArrayList();
            String[] var9 = addrTokens;
            int var10 = addrTokens.length;

            for(int var11 = 0; var11 < var10; ++var11) {
                String addrToken = var9[var11];
                slaveList.add("redis://" + addrToken);
            }

            slaveList.remove(0);
            config.useMasterSlaveServers().addSlaveAddress((String[])((String[])slaveList.toArray()));
            log.info("初始化主从方式Config,redisAddress:" + address);
        } catch (Exception var13) {
            log.error("主从Redisson初始化错误", var13);
            var13.printStackTrace();
        }

        return config;
    }
}
