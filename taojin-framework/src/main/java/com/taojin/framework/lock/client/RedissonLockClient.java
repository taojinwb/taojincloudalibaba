package com.taojin.framework.lock.client;

import java.util.concurrent.TimeUnit;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

@Configuration
public class RedissonLockClient {
    private static final Logger log = LoggerFactory.getLogger(RedissonLockClient.class);
    @Autowired
    private RedissonClient redissonClient;
    @Autowired
    private RedisTemplate redisTemplate;

    public RedissonLockClient() {
    }

    public RLock getLock(String lockKey) {
        return this.redissonClient.getLock(lockKey);
    }

    public boolean tryLock(String lockName, long expireSeconds) {
        return this.tryLock(lockName, 0L, expireSeconds);
    }

    public boolean tryLock(String lockName, long waitTime, long expireSeconds) {
        RLock rLock = this.getLock(lockName);
        boolean getLock = false;

        try {
            getLock = rLock.tryLock(waitTime, expireSeconds, TimeUnit.SECONDS);
            if (getLock) {
                log.info("获取锁成功,lockName={}", lockName);
            } else {
                log.info("获取锁失败,lockName={}", lockName);
            }
        } catch (InterruptedException var9) {
            log.error("获取式锁异常，lockName=" + lockName, var9);
            getLock = false;
        }

        return getLock;
    }

    public boolean fairLock(String lockKey, TimeUnit unit, int leaseTime) {
        RLock fairLock = this.redissonClient.getFairLock(lockKey);

        try {
            boolean existKey = this.existKey(lockKey);
            return existKey ? false : fairLock.tryLock(3L, (long)leaseTime, unit);
        } catch (InterruptedException var6) {
            var6.printStackTrace();
            return false;
        }
    }

    public boolean existKey(String key) {
        return this.redisTemplate.hasKey(key);
    }

    public RLock lock(String lockKey) {
        RLock lock = this.getLock(lockKey);
        lock.lock();
        return lock;
    }

    public RLock lock(String lockKey, long leaseTime) {
        RLock lock = this.getLock(lockKey);
        lock.lock(leaseTime, TimeUnit.SECONDS);
        return lock;
    }

    public void unlock(String lockName) {
        try {
            this.redissonClient.getLock(lockName).unlock();
        } catch (Exception var3) {
            log.error("解锁异常，lockName=" + lockName, var3);
        }

    }
}
