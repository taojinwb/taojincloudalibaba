package com.taojin.framework.lock.core.strategy.impl;


import com.taojin.framework.lock.core.strategy.RedissonConfigStrategy;
import com.taojin.framework.lock.prop.RedissonProperties;
import org.apache.commons.lang3.StringUtils;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SentinelRedissonConfigStrategyImpl implements RedissonConfigStrategy {
    private static final Logger log = LoggerFactory.getLogger(SentinelRedissonConfigStrategyImpl.class);

    public SentinelRedissonConfigStrategyImpl() {
    }

    public Config createRedissonConfig(RedissonProperties redissonProperties) {
        Config config = new Config();

        try {
            String address = redissonProperties.getAddress();
            String password = redissonProperties.getPassword();
            int database = redissonProperties.getDatabase();
            String[] addrTokens = address.split(",");
            String sentinelAliasName = addrTokens[0];
            config.useSentinelServers().setMasterName(sentinelAliasName);
            config.useSentinelServers().setDatabase(database);
            if (StringUtils.isNotBlank(password)) {
                config.useSentinelServers().setPassword(password);
            }

            for(int i = 1; i < addrTokens.length; ++i) {
                config.useSentinelServers().addSentinelAddress(new String[]{"redis://" + addrTokens[i]});
            }

            log.info("初始化哨兵方式Config,redisAddress:" + address);
        } catch (Exception var9) {
            log.error("哨兵Redisson初始化错误", var9);
            var9.printStackTrace();
        }

        return config;
    }
}
