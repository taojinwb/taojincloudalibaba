package com.taojin.framework.lock.core.strategy.impl;


import com.taojin.framework.lock.core.strategy.RedissonConfigStrategy;
import com.taojin.framework.lock.prop.RedissonProperties;
import org.apache.commons.lang3.StringUtils;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClusterRedissonConfigStrategyImpl implements RedissonConfigStrategy {
    private static final Logger log = LoggerFactory.getLogger(ClusterRedissonConfigStrategyImpl.class);

    public ClusterRedissonConfigStrategyImpl() {
    }

    public Config createRedissonConfig(RedissonProperties redissonProperties) {
        Config config = new Config();

        try {
            String address = redissonProperties.getAddress();
            String password = redissonProperties.getPassword();
            String[] addrTokens = address.split(",");

            for(int i = 0; i < addrTokens.length; ++i) {
                config.useClusterServers().addNodeAddress(new String[]{"redis://" + addrTokens[i]});
                if (StringUtils.isNotBlank(password)) {
                    config.useClusterServers().setPassword(password);
                }
            }

            log.info("初始化集群方式Config,连接地址:" + address);
        } catch (Exception var7) {
            log.error("集群Redisson初始化错误", var7);
            var7.printStackTrace();
        }

        return config;
    }
}
