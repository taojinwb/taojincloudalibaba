package com.taojin.framework.lock.aspect;


import java.util.Objects;
import java.util.concurrent.TimeUnit;
import javax.annotation.Resource;

import com.taojin.framework.exception.BaseException;
import com.taojin.framework.lock.annotation.JRepeat;
import com.taojin.framework.lock.client.RedissonLockClient;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class RepeatSubmitAspect extends BaseAspect {
    @Resource
    private RedissonLockClient redissonLockClient;

    public RepeatSubmitAspect() {
    }

    @Pointcut("@annotation(jRepeat)")
    public void pointCut(JRepeat jRepeat) {
    }

    @Around("pointCut(jRepeat)")
    public Object repeatSubmit(ProceedingJoinPoint joinPoint, JRepeat jRepeat) throws Throwable {
        String[] parameterNames = (new LocalVariableTableParameterNameDiscoverer()).getParameterNames(((MethodSignature)joinPoint.getSignature()).getMethod());
        if (Objects.nonNull(jRepeat)) {
            Object[] args = joinPoint.getArgs();
            new StringBuffer();
            String key = (String)this.getValueBySpEL(jRepeat.lockKey(), parameterNames, args, "RepeatSubmit").get(0);
            boolean isLocked = false;

            Object var8;
            try {
                isLocked = this.redissonLockClient.fairLock(key, TimeUnit.SECONDS, jRepeat.lockTime());
                if (!isLocked) {
                    throw new BaseException("请勿重复提交");
                }

                var8 = joinPoint.proceed();
            } finally {
                if (isLocked) {
                    this.redissonLockClient.unlock(key);
                }

            }

            return var8;
        } else {
            return joinPoint.proceed();
        }
    }
}
