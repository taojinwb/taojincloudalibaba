package com.taojin.framework.lock.aspect;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.taojin.framework.lock.annotation.JLock;
import com.taojin.framework.lock.enums.LockModel;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.redisson.RedissonMultiLock;
import org.redisson.RedissonRedLock;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class DistributedLockHandler extends BaseAspect {
    private static final Logger log = LoggerFactory.getLogger(DistributedLockHandler.class);
    @Autowired(
            required = false
    )
    private RedissonClient redissonClient;

    public DistributedLockHandler() {
    }

    @Around("@annotation(jLock)")
    public Object around(ProceedingJoinPoint joinPoint, JLock jLock) throws Throwable {
        try {
            Object obj = null;
            log.info("进入RedisLock环绕通知...");
            RLock rLock = this.getLock(joinPoint, jLock);
            boolean res = false;
            long expireSeconds = jLock.expireSeconds();
            long waitTime = jLock.waitTime();
            if (rLock != null) {
                try {
                    if (waitTime == -1L) {
                        res = true;
                        rLock.lock(expireSeconds, TimeUnit.MILLISECONDS);
                    } else {
                        res = rLock.tryLock(waitTime, expireSeconds, TimeUnit.MILLISECONDS);
                    }
                    if (res) {
                        obj = joinPoint.proceed();
                    } else {
                        log.error("获取锁异常");
                    }
                } finally {
                    if (res) {
                        rLock.unlock();
                    }

                }
            }

            log.info("结束RedisLock环绕通知...");
            return obj;
        } catch (Throwable var14) {
            throw var14;
        }
    }

    private RLock getLock(ProceedingJoinPoint joinPoint, JLock jLock) {
        try {
            String[] keys = jLock.lockKey();
            if (keys.length == 0) {
                throw new RuntimeException("keys不能为空");
            } else {
                String[] parameterNames = (new LocalVariableTableParameterNameDiscoverer()).getParameterNames(((MethodSignature)joinPoint.getSignature()).getMethod());
                Object[] args = joinPoint.getArgs();
                LockModel lockModel = jLock.lockModel();
                RLock rLock = null;
                String keyConstant = jLock.keyConstant();
                if (lockModel.equals(LockModel.AUTO)) {
                    if (keys.length > 1) {
                        lockModel = LockModel.REDLOCK;
                    } else {
                        lockModel = LockModel.REENTRANT;
                    }
                }

                if (!lockModel.equals(LockModel.MULTIPLE) && !lockModel.equals(LockModel.REDLOCK) && keys.length > 1) {
                    throw new RuntimeException("参数有多个,锁模式为->" + lockModel.name() + ".无法锁定");
                } else {
                    ArrayList rLocks;
                    RLock[] locks;
                    int index;
                    Iterator var22;
                    RLock r;
                    switch (lockModel) {
                        case FAIR:
                            rLock = this.redissonClient.getFairLock((String)this.getValueBySpEL(keys[0], parameterNames, args, keyConstant).get(0));
                            break;
                        case REDLOCK:
                            rLocks = new ArrayList();
                            String[] var20 = keys;
                            index = keys.length;

                            for(int var23 = 0; var23 < index; ++var23) {
                                String key = var20[var23];
                                List<String> valueBySpEL = this.getValueBySpEL(key, parameterNames, args, keyConstant);
                                Iterator var29 = valueBySpEL.iterator();

                                while(var29.hasNext()) {
                                    String s = (String)var29.next();
                                    rLocks.add(this.redissonClient.getLock(s));
                                }
                            }

                            locks = new RLock[rLocks.size()];
                            index = 0;

                            for(var22 = rLocks.iterator(); var22.hasNext(); locks[index++] = r) {
                                r = (RLock)var22.next();
                            }

                            rLock = new RedissonRedLock(locks);
                            break;
                        case MULTIPLE:
                            rLocks = new ArrayList();
                            String[] var21 = keys;
                            int var24 = keys.length;

                            for(int var27 = 0; var27 < var24; ++var27) {
                                String key = var21[var27];
                                List<String> valueBySpEL = this.getValueBySpEL(key, parameterNames, args, keyConstant);
                                Iterator var17 = valueBySpEL.iterator();

                                while(var17.hasNext()) {
                                    String s = (String)var17.next();
                                    rLocks.add(this.redissonClient.getLock(s));
                                }
                            }

                            locks = new RLock[rLocks.size()];
                            index = 0;

                            for(var22 = rLocks.iterator(); var22.hasNext(); locks[index++] = r) {
                                r = (RLock)var22.next();
                            }

                            rLock = new RedissonMultiLock(locks);
                            break;
                        case REENTRANT:
                            List<String> valueBySpEL = this.getValueBySpEL(keys[0], parameterNames, args, keyConstant);
                            if (valueBySpEL.size() == 1) {
                                rLock = this.redissonClient.getLock((String)valueBySpEL.get(0));
                            } else {
                                locks = new RLock[valueBySpEL.size()];
                                index = 0;

                                String s;
                                for(Iterator var13 = valueBySpEL.iterator(); var13.hasNext(); locks[index++] = this.redissonClient.getLock(s)) {
                                    s = (String)var13.next();
                                }

                                rLock = new RedissonRedLock(locks);
                            }
                            break;
                        case READ:
                            rLock = this.redissonClient.getReadWriteLock((String)this.getValueBySpEL(keys[0], parameterNames, args, keyConstant).get(0)).readLock();
                            break;
                        case WRITE:
                            rLock = this.redissonClient.getReadWriteLock((String)this.getValueBySpEL(keys[0], parameterNames, args, keyConstant).get(0)).writeLock();
                    }

                    return (RLock)rLock;
                }
            }
        } catch (Throwable var19) {
            throw var19;
        }
    }
}
