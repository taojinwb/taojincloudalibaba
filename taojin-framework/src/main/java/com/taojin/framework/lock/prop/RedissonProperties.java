package com.taojin.framework.lock.prop;


import com.taojin.framework.lock.enums.RedisConnectionType;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(
        prefix = "redislock.redisson"
)
public class RedissonProperties {
    private String address;
    private RedisConnectionType type;
    private String password;
    private int database;
    private Boolean enabled = true;

    public RedissonProperties() {
    }

    public String getAddress() {
        return this.address;
    }

    public RedisConnectionType getType() {
        return this.type;
    }

    public String getPassword() {
        return this.password;
    }

    public int getDatabase() {
        return this.database;
    }

    public Boolean getEnabled() {
        return this.enabled;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public void setType(final RedisConnectionType type) {
        this.type = type;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public void setDatabase(final int database) {
        this.database = database;
    }

    public void setEnabled(final Boolean enabled) {
        this.enabled = enabled;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof RedissonProperties)) {
            return false;
        } else {
            RedissonProperties other = (RedissonProperties)o;
            if (!other.canEqual(this)) {
                return false;
            } else if (this.getDatabase() != other.getDatabase()) {
                return false;
            } else {
                label61: {
                    Object this$enabled = this.getEnabled();
                    Object other$enabled = other.getEnabled();
                    if (this$enabled == null) {
                        if (other$enabled == null) {
                            break label61;
                        }
                    } else if (this$enabled.equals(other$enabled)) {
                        break label61;
                    }

                    return false;
                }

                label54: {
                    Object this$address = this.getAddress();
                    Object other$address = other.getAddress();
                    if (this$address == null) {
                        if (other$address == null) {
                            break label54;
                        }
                    } else if (this$address.equals(other$address)) {
                        break label54;
                    }

                    return false;
                }

                Object this$type = this.getType();
                Object other$type = other.getType();
                if (this$type == null) {
                    if (other$type != null) {
                        return false;
                    }
                } else if (!this$type.equals(other$type)) {
                    return false;
                }

                Object this$password = this.getPassword();
                Object other$password = other.getPassword();
                if (this$password == null) {
                    if (other$password != null) {
                        return false;
                    }
                } else if (!this$password.equals(other$password)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof RedissonProperties;
    }

    public int hashCode() {
      //  int PRIME = true;
        int result = 1;
        result = result * 59 + this.getDatabase();
        Object $enabled = this.getEnabled();
        result = result * 59 + ($enabled == null ? 43 : $enabled.hashCode());
        Object $address = this.getAddress();
        result = result * 59 + ($address == null ? 43 : $address.hashCode());
        Object $type = this.getType();
        result = result * 59 + ($type == null ? 43 : $type.hashCode());
        Object $password = this.getPassword();
        result = result * 59 + ($password == null ? 43 : $password.hashCode());
        return result;
    }

    public String toString() {
        return "RedissonProperties(address=" + this.getAddress() + ", type=" + this.getType() + ", password=" + this.getPassword() + ", database=" + this.getDatabase() + ", enabled=" + this.getEnabled() + ")";
    }
}
