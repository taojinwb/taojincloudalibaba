package com.taojin.framework.lock.core.strategy.impl;


import com.taojin.framework.lock.core.strategy.RedissonConfigStrategy;
import com.taojin.framework.lock.prop.RedissonProperties;
import org.apache.commons.lang3.StringUtils;
import org.redisson.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StandaloneRedissonConfigStrategyImpl implements RedissonConfigStrategy {
    private static final Logger log = LoggerFactory.getLogger(StandaloneRedissonConfigStrategyImpl.class);

    public StandaloneRedissonConfigStrategyImpl() {
    }

    public Config createRedissonConfig(RedissonProperties redissonProperties) {
        Config config = new Config();

        try {
            String address = redissonProperties.getAddress();
            String password = redissonProperties.getPassword();
            int database = redissonProperties.getDatabase();
            String redisAddr = "redis://" + address;
            config.useSingleServer().setAddress(redisAddr);
            config.useSingleServer().setDatabase(database);
            if (StringUtils.isNotBlank(password)) {
                config.useSingleServer().setPassword(password);
            }

            log.info("初始化Redisson单机配置,连接地址:" + address);
        } catch (Exception var7) {
            log.error("单机Redisson初始化错误", var7);
            var7.printStackTrace();
        }

        return config;
    }
}
