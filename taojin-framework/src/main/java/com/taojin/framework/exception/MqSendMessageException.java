package com.taojin.framework.exception;

/**
 * rocketmq发送消息异常
 */
public class MqSendMessageException extends RuntimeException{
    public MqSendMessageException(String message, Throwable cause) {
        super(message, cause);
    }
}
