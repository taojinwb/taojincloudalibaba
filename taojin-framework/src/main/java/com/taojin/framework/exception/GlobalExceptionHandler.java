package com.taojin.framework.exception;


import com.taojin.framework.enums.SentinelErrorInfoEnum;
import com.taojin.framework.utils.MF;
import com.taojin.framework.vo.ApiCode;
import com.taojin.framework.vo.ApiResult;
import org.apache.tomcat.websocket.AuthenticationException;
import org.springframework.beans.FatalBeanException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.redis.connection.PoolException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.NoHandlerFoundException;

import lombok.extern.slf4j.Slf4j;

import java.io.UnsupportedEncodingException;


/**
 * 异常处理器
 * &#064;Author  scott
 * &#064;Date  2019
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {


    /**
     * 处理自定义异常
     */
    @ExceptionHandler(BaseException.class)
    public ApiResult<?> handleTaoJinBootException(BaseException e){
        log.error(e.getMessage(), e);
        return ApiResult.error(e.getMessage());
    }

    /**
     * 字段影射失败异常
     */
    @ExceptionHandler(FatalBeanException.class)
    public ApiResult<?> handleTaoJinBootException(FatalBeanException e){
        log.error(e.getMessage(), e);
        return ApiResult.error(ApiCode.TJ_407,e.getMessage());
    }
    @ExceptionHandler(MqSendMessageException.class)
    public ApiResult<?> handleTaoJinRocketmqException(MqSendMessageException e){
        log.error(e.getMessage(),e);
        return ApiResult.error(ApiCode.TJ_808,e.getMessage());
    }

//    /**
//     * 处理自定义微服务异常
//     */
//    @ExceptionHandler(TaoJinCloudException.class)
//    public ApiResult<?> handleTaoJinCloudException(TaoJinCloudException e){
//        log.error(e.getMessage(), e);
//        return Result.error(e.getMessage());
//    }

    /**
     * 处理自定义异常
     */
    @ExceptionHandler(TaoJin401Exception.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public ApiResult<?> handleTaoJin401Exception(TaoJin401Exception e){
        log.error(e.getMessage(), e);
        return new ApiResult(ApiCode.TJ_401,e.getMessage());
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ApiResult<?> handlerNoFoundException(Exception e) {
        log.error(e.getMessage(), e);
        return ApiResult.error(ApiCode.TJ_404, "路径不存在，请检查路径是否正确");
    }

    @ExceptionHandler(DuplicateKeyException.class)
    public ApiResult<?> handleDuplicateKeyException(DuplicateKeyException e){
        log.error(e.getMessage(), e);
        return ApiResult.error("数据库中已存在该记录");
    }

    @ExceptionHandler({SecurityException.class, AuthenticationException.class})
    public ApiResult<?> handleAuthorizationException(AuthenticationException e){
        log.error(e.getMessage(), e);
        return ApiResult.error(ApiCode.TJ_510,"没有权限，请联系管理员授权");
    }

    @ExceptionHandler(Exception.class)
    public ApiResult<?> handleException(Exception e){
        //e.printStackTrace();
        log.error(e.getMessage(), e);
        Throwable throwable = e.getCause();
        SentinelErrorInfoEnum errorInfoEnum = SentinelErrorInfoEnum.getErrorByException(throwable);
        if (MF.isNotEmpty(errorInfoEnum)) {
            return ApiResult.error(ApiCode.TJ_408,errorInfoEnum.getError());
        }
        return ApiResult.error("操作失败，"+e.getMessage());
    }

    /**
     * &#064;Author  政辉
     * @param e d
     * @return d
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ApiResult<?> httpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e){
        StringBuffer sb = new StringBuffer();
        sb.append("不支持");
        sb.append(e.getMethod());
        sb.append("请求方法，");
        sb.append("支持以下");
        String [] methods = e.getSupportedMethods();
        if(methods!=null){
            for(String str:methods){
                sb.append(str);
                sb.append("、");
            }
        }
        log.error(sb.toString(), e);
        return ApiResult.error(ApiCode.TJ_405,sb.toString());
    }

    /**
     * spring默认上传大小100MB 超出大小捕获异常MaxUploadSizeExceededException
     */
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ApiResult<?> handleMaxUploadSizeExceededException(MaxUploadSizeExceededException e) {
        log.error(e.getMessage(), e);
        return ApiResult.error("文件大小超出10MB限制, 请压缩或降低文件质量! ");
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ApiResult<?> handleDataIntegrityViolationException(DataIntegrityViolationException e) {
        log.error(e.getMessage(), e);
        //【issues/3624】数据库执行异常handleDataIntegrityViolationException提示有误 #3624
        return ApiResult.error("执行数据库异常,违反了完整性例如：违反惟一约束、违反非空限制、字段内容超出长度等");
    }
    /**
     * 自定义验证异常
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ApiResult<?> handleMethodArgumentNotValidException(MethodArgumentNotValidException e)
    {
       log.error(e.getMessage(), e);
        StringBuffer sb = new StringBuffer();
        for (FieldError error : e.getBindingResult().getFieldErrors()) {
            sb.append("『"+error.getDefaultMessage()+"』\r\n");
        }
        return ApiResult.error(sb.toString());

    }

    @ExceptionHandler(PoolException.class)
    public ApiResult<?> handlePoolException(PoolException e) {
        log.error(e.getMessage(), e);
        return ApiResult.error("Redis 连接异常!");
    }

}
