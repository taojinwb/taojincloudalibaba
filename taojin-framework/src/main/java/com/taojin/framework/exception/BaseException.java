package com.taojin.framework.exception;
/**
 * &#064;Description:  自定义异常
 * &#064;author:  sujg
 */
public class BaseException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public BaseException(String message){
        super(message);
    }

    public BaseException(Throwable cause)
    {
        super(cause);
    }

    public BaseException(String message, Throwable cause)
    {
        super(message,cause);
    }
}

