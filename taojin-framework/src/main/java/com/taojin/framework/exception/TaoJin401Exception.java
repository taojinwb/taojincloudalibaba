package com.taojin.framework.exception;

/**
 * &#064;Description:  自定义401异常
 * &#064;author:  sujg
 */
public class TaoJin401Exception extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public TaoJin401Exception(String message){
        super(message);
    }

    public TaoJin401Exception(Throwable cause)
    {
        super(cause);
    }

    public TaoJin401Exception(String message, Throwable cause)
    {
        super(message,cause);
    }
}
