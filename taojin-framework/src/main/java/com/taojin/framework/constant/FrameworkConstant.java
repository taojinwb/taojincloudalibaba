package com.taojin.framework.constant;


/**
 * &#064;Description:  通用常量
 * &#064;author:  sujg
 */
public interface FrameworkConstant {



    /**
     * 删除标志
     */
    Integer DEL_FLAG_1 = 1;
    /** 登录用户Token令牌缓存KEY前缀 */
    String PREFIX_USER_TOKEN  = "prefix_user_token:";

    /**
     * 是否用户已被冻结 1正常(解冻) 2冻结 3离职
     */
    Integer USER_UNFREEZE = 1;
    Integer USER_FREEZE = 2;
    Integer USER_QUIT = 3;


    /**===============================================================================================*/
    /**
     * ::非常重要::
     * 注意：这四个常量值如果修改，需要与 其他 类中的值保持一致。
     */
    String X_ACCESS_TOKEN = "X-Access-Token";
    String X_SIGN = "X-Sign";
    String X_TIMESTAMP = "X-TIMESTAMP";
    /** 租户请求头 更名为：X-Tenant-Id */
    String TENANT_ID = "X-Tenant-Id";
    /**===============================================================================================*/

}
