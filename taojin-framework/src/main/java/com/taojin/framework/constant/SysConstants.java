package com.taojin.framework.constant;


/**
 * 通用常量信息
 *
 * @author sujg
 */
public interface SysConstants
{
    /**
     * UTF-8 字符集
     */
       String UTF8 = "UTF-8";

    /**
     * GBK 字符集
     */
     String GBK = "GBK";

    /**
     * www主域
     */
     String WWW = "www.";

    /**
     * http请求
     */
     String HTTP = "http://";

    /**
     * https请求
     */
     String HTTPS = "https://";

    /**
     * 通用成功标识
     */
     String SUCCESS = "0";

    /**
     * 通用失败标识
     */
     String FAIL = "1";

    /**
     * 登录成功
     */
     String LOGIN_SUCCESS = "Success";

    /**
     * 注销
     */
     String LOGOUT = "Logout";

    /**
     * 注册
     */
     String REGISTER = "Register";

    /**
     * 登录失败
     */
    String LOGIN_FAIL = "Error";

    /**
     * 验证码有效期（分钟）
     */
     Integer CAPTCHA_EXPIRATION = 2;


    /**
     * 令牌前缀
     */
     String TOKEN_PREFIX = "Bearer ";



    /**
     * 资源映射路径 前缀
     */
     String RESOURCE_PREFIX = "/profile";

    /**
     * 自动识别json对象白名单配置（仅允许解析的包名，范围越小越安全）
     */
    String[] JSON_WHITELIST_STR = { "org.springframework", "com.taojin" };


}
