package com.taojin.framework.config;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.FatalBeanException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.core.convert.support.GenericConversionService;

import javax.annotation.PostConstruct;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 可能会出现缓存错误，暂时不建议使用
 * &#064;description
 * @author sujg
 * &#064;date  2020/3/26 17:51
 * @since JDK1.8
 */
@Configuration
@Slf4j
public class ZeusJpaConfiguration {

    @Autowired
    private ApplicationContext applicationContext;


    /**
     * 初始化注入@JpaDto对应的Converter
     */
    @PostConstruct
    public void init() {
        Map<String, Object> map = applicationContext.getBeansWithAnnotation(JpaDto.class);
        GenericConversionService genericConversionService = ((GenericConversionService) DefaultConversionService.getSharedInstance());
        for (Object o : map.values()) { //所有jpadto注解的类，
             Class c = o.getClass();
             log.info("JpaDto添加map转换器,class={}", c.getName());
             genericConversionService.addConverter(Map.class, c, m -> BeanUtil.mapToBean(m,c,true,getCopyOptions()));
        }
    }
    /**
     * 获取拷贝属性配置，默认
     * @return
     */
    public static CopyOptions getCopyOptions(){
        CopyOptions copyOptions = CopyOptions.create();
        //	设置忽略空值，当源对象的值为null时，忽略而不注入此值
        copyOptions.setIgnoreNullValue(false);
        //	是否忽略字段注入错误
        copyOptions.setIgnoreError(false);
        //是否忽略大小写
        copyOptions.setIgnoreCase(true);
        //设置是否覆盖目标值，如果不覆盖，会先读取目标对象的值，为null则写，否则忽略。
        copyOptions.setOverride(true);
        //设置是否支持transient关键字修饰和@Transient注解，如果支持，被修饰的字段或方法对应的字段将被忽略。
        copyOptions.setTransientSupport(false);
        //设置忽略的目标对象中属性列表，设置一个属性列表，不拷贝这些属性值
        // copyOptions.setIgnoreProperties("id","a");
        return copyOptions;
    }


}
