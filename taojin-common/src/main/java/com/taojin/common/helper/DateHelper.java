package com.taojin.common.helper;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import com.taojin.common.constant.CommonConstant;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;

/**
 * 日期工具类，日期统一采取 2023-11-22 23:59:59格式
 *
 */
@Slf4j
public class DateHelper extends cn.hutool.core.date.DateUtil{

    /**
     * 根据日期返回是周几，
     * @param date
     * @return
     */
    public static String getWeekStr(Date date){
      return   CommonConstant.WEEK_MARK_SHOW[DateUtil.dayOfWeek(date)-1];
    }

    /**
     * 获取参数日期，当天的结束时间
     * @return  2023-11-22 23:59:59
     */
    public static DateTime endOfDay(Date date){
        if(StrHelper.isEmpty(date)){
            return null;
        }
        return DateUtil.endOfDay(date);
    }

    /**
     * 获取参数日期，当天的开始时间
     * @param date 2023-11-22 00:00:00
     * @return
     */
    public static DateTime beginOfDay(Date date){
        if(StrHelper.isEmpty(date)){
            return null;
        }
        return DateUtil.beginOfDay(date);
    }

    /**
     *  把字符串转换为Date 格式"yyyy-MM-dd HH:mm:ss"
     * @param datestr
     * @return
     */
    public static Date StringToDate(String datestr,String format){
        datestr = datestr.replaceAll("/","-");
        datestr = datestr.replaceAll(".","-");
        if(StrHelper.isEmpty(format)){
           format = "yyyy-MM-dd HH:mm:ss";
        }
        return  DateUtil.parse(datestr,format);
    }

    /**
     * 把字符串转换为Date
     * @param datestr
     * @return
     */
    public static Date StringToDate(String datestr){
        return DateUtil.parse(datestr);
    }
    public static void main(String[] args) {

        Date date = DateUtil.parse("2023.10.22 22:22:22");
        log.info(( date)+"==");
    }
}
