package com.taojin.common.helper;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.convert.ConvertException;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;



/**
 * 字符串处理工具类
 */
@Slf4j
public class StrHelper extends cn.hutool.core.convert.Convert{

    /**
     * 判断对象是否为空串
     *  StrUtil.isEmptyIfStr(null) // true StrUtil.isEmptyIfStr("") // true
     * @param obj
     * @return
     */
    public static boolean isEmpty(Object obj){
        return StrUtil.isEmptyIfStr(obj);
    }

    /**
     * 返回指定的类型
     * @param type 类型
     * @param value 值
     * @return
     * @param <T>
     * @throws ConvertException
     */
    public static <T> T convert(Class<T> type,
                                Object value)
            throws ConvertException {
        return Convert.convert(type,value);
    }
    public static void main(String[] args) {
        System.out.println( convert(Integer.class,"111"));
    }
}
