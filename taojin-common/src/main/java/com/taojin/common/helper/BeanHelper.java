package com.taojin.common.helper;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.alibaba.fastjson2.util.BeanUtils;
import lombok.Data;
import lombok.ToString;

import java.util.*;

/**
 * bean 处理工具类
 *
 */
public class BeanHelper {
    /**
     * 获取拷贝属性配置，默认
     * @return
     */
    public static CopyOptions getCopyOptions(){
        CopyOptions copyOptions = CopyOptions.create();
        //	设置忽略空值，当源对象的值为null时，忽略而不注入此值
        copyOptions.setIgnoreNullValue(false);
        //	是否忽略字段注入错误
        copyOptions.setIgnoreError(false);
        //是否忽略大小写
        copyOptions.setIgnoreCase(true);
        //设置是否覆盖目标值，如果不覆盖，会先读取目标对象的值，为null则写，否则忽略。
        copyOptions.setOverride(true);
        //设置是否支持transient关键字修饰和@Transient注解，如果支持，被修饰的字段或方法对应的字段将被忽略。
        copyOptions.setTransientSupport(false);
        //设置忽略的目标对象中属性列表，设置一个属性列表，不拷贝这些属性值
       // copyOptions.setIgnoreProperties("id","a");
        return copyOptions;
    }

    /**
     *  复制集合中的Bean属性  ,源和目标对象都需要有get/set方法
     * 此方法遍历集合中每个Bean，复制其属性后加入一个新的List中。
     * @param source 源集合
     * @param targetType 目标bean
     * @param copyOptions copy配置
     * @return
     * @param <T>
     */
    public static <T> List<T> copyToList(Collection<?> source,
                                         Class<T> targetType,
                                         CopyOptions copyOptions){
        System.out.printf("ta=="+targetType);
        return BeanUtil.copyToList(source,targetType,copyOptions);
    }
    /**
     *  复制集合中的Bean属性  ,源和目标对象都需要有get/set方法
     * 此方法遍历集合中每个Bean，复制其属性后加入一个新的List中。
     * @param source 源集合
     * @param targetType 目标bean
     * @return
     * @param <T>
     */
    public static <T> List<T> copyToList(Collection<?> source,
                                         Class<T> targetType){

        return BeanUtil.copyToList(source,targetType,getCopyOptions());
    }

    /**
     *  用map填充bean
     * @param map map
     * @param bean bean对象
     * @param isToCamelCase 下划线转驼峰
     * @param isIgnoreError 是否忽略注入错误
     * @return
     * @param <T>
     */
    public static <T> T fillBeanWithMap(Map<?,?> map,
                                        T bean,
                                        boolean isToCamelCase,
                                        boolean isIgnoreError){
        return  (T)BeanUtil.fillBeanWithMap(map,bean,isToCamelCase,isIgnoreError);
    }
    /**
     *  用map填充bean
     * @param map map
     * @param bean bean对象
     * @return
     * @param <T>
     */
    public static <T> T fillBeanWithMap(Map<?,?> map,
                                        T bean){
        return  (T)BeanUtil.fillBeanWithMap(map,bean,true,true);
    }

    /**
     * Map转换为Bean对象
     * @param map
     * @param beanClass bean
     * @param isToCamelCase 下划线转驼峰
     * @param copyOptions copy配置
     * @return
     * @param <T>
     */
    public static <T> T mapToBean(Map<?,?> map,
                                  Class<T> beanClass,
                                  boolean isToCamelCase,
                                  CopyOptions copyOptions){
        return BeanUtil.mapToBean(map,beanClass,isToCamelCase,copyOptions);
    }
    /**
     * Map转换为Bean对象
     * @param map
     * @param beanClass bean
     * @return
     * @param <T>
     */
    public static <T> T mapToBean(Map<?,?> map,
                                  Class<T> beanClass){
        return BeanUtil.mapToBean(map,beanClass,true,getCopyOptions());
    }


    public static void main(String[] args) {
        List<Map<String,Object>> la = new ArrayList<>();
        Map<String ,Object> m = new HashMap<>();
        m.put("aa","2222");
        m.put("a",3344);
        m.put("id","9999");
//        la.add(m);
//        m = new HashMap<>();
//        m.put("dd_sa_bz","4444");
//        m.put("a",1111);
//        m.put("id","23232");
        for(int i=0;i<1;i++) {
            la.add(m);
        }
        TestB z = new TestB();

        List<TestB> lb = new ArrayList<>();
        System.out.println(System.currentTimeMillis());
        Long ddd = System.currentTimeMillis();
        lb = BeanHelper.copyToList(la,TestB.class,BeanHelper.getCopyOptions());
        for(int i=0;i<lb.size();i++) {
            System.out.println(lb.get(i).toString());
        }
        System.out.println(ddd+"----"+System.currentTimeMillis());
    }
}
