package com.taojin.common.file.upyun;


import cn.hutool.core.date.DateUtil;
import com.UpYun;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.util.HashMap;

/**
 * 描述:
 *
 * @author jianchun.chen
 * Date: 2020-03-18
 * Time: 10:11
 */
@Component
public class UpYunClient {

    private final Logger log = LoggerFactory.getLogger(getClass());
    @Value("${upyun.bucketName}")
    private String upyunBucketName;
    @Value("${upyun.userName}")
    private String upyunUserName;
    @Value("${upyun.password}")
    private String upyunPassword;
    @Value("${upyun.upDomain}")
    private String upyunUpDomain;


    public String upload(File file, HashMap<String, String> map) throws Exception {
        UpYun upyun = new UpYun(upyunBucketName, upyunUserName, upyunPassword);
        String fileName = file.getName();
        String md5key = UpYun.md5(file);
        String suffix = StringUtils.substringAfterLast(fileName, ".");
        String filePath = "/" + DateUtil.format(DateUtil.date(),"yyyyMMdd") + "/" + md5key + "." + suffix;
        upyun.setContentMD5(md5key);
        if (upyun.writeFile(filePath, file, true)) {
            String url = upyunUpDomain + filePath;
            log.info("上传U盘云成功  url = {}",url);
            MultipartFileToFile.delteTempFile(file);
            map.put("showUrl", "https://"+url);
            map.put("url", filePath);
            return "https://"+url;
        }
        log.error("上传U盘云失败");
        return "";
    }


    public String upload(MultipartFile multipartFile, HashMap<String, String> map) throws Exception {
        File file = MultipartFileToFile.multipartFileToFile(multipartFile);
        return upload(file,map);
    }

}
