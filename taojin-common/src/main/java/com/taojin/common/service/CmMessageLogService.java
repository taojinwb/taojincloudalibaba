package com.taojin.common.service;

import com.taojin.common.entity.CmMessageLog;
import com.taojin.common.repository.CmMessageLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 消息发送日志Service业务层处理
 *
 * @author sujg
 * @date Wed Dec 20 15:25:50 CST 2023
 */
@Service
public class CmMessageLogService
{
    @Autowired
    private CmMessageLogRepository cmMessageLogRepository;

    /**
     * 记录mq发送失败的日志
     * @param topic 主题
     * @param tag 标签
     * @param key key
     * @param value 值
     * @param mqtype 类型
     */
    public void InsertMqLog(String topic,String tag,String key,String value,String mqtype){
        CmMessageLog cmMessageLog = new CmMessageLog();
        cmMessageLog.setTopic(topic);
        cmMessageLog.setTag(tag);
        cmMessageLog.setMskey(key);
        cmMessageLog.setMsvalue(value);
        cmMessageLog.setMqtype(mqtype);
        cmMessageLog.setStatus("Z0");
        cmMessageLog.setType("L1");
        cmMessageLog.setRetryCount(1);
        cmMessageLogRepository.save(cmMessageLog);
    }
}