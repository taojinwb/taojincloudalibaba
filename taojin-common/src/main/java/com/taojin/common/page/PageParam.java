package com.taojin.common.page;

public class PageParam {


    /** 页号 */
    private Integer pageNum = 1;
    /** 页面大小 */
    private Integer pageSize = 10;
    /** 分页起始索引，客户端展示的起始索引，一般都是从1开始 */
    private int pageIndex = 1;
    /** 启用分页参数，true表示启用分页查询，false表示不启用分页查询, 默认启用 */
    private boolean enable = true;
    /** 排序方式 */
    private String sort ;
    /** 排序字段 */
    private String order;


    public PageParam() {
    }


    public PageParam(Integer pageNum, Integer pageSize) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }


    public PageParam(Integer pageNum, Integer pageSize, String order) {
        this(pageNum, pageSize, "ASC", order);
    }


    public PageParam(Integer pageNum, Integer pageSize, String sort, String order) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.sort = sort;
        this.order = order;
    }


    public Integer getPageSize() {
        return pageSize;
    }
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
    public Integer getPageNum() {
        return pageNum;
    }
    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }
    public String getSort() {
        return sort;
    }
    public void setSort(String sort) {
        this.sort = sort;
    }
    public String getOrder() {
        return order;
    }
    public void setOrder(String order) {
        this.order = order;
    }
    public boolean isEnable() {
        return enable;
    }
    public void setEnable(boolean enable) {
        this.enable = enable;
    }
    public int getPageIndex() {
        return pageIndex;
    }
    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }


    @Override
    public String toString() {
        return "PageParam{" +
                "pageNum=" + pageNum +
                ", pageSize=" + pageSize +
                ", pageIndex=" + pageIndex +
                ", enable=" + enable +
                ", sort='" + sort + '\'' +
                ", order='" + order + '\'' +
                '}';
    }
}
