package com.taojin.common.repository;

import com.taojin.common.entity.CmMessageLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * 消息发送日志repository
 *
 * @author sujg
 * @date Wed Dec 20 15:13:19 CST 2023
 */
public interface CmMessageLogRepository extends  JpaRepository<CmMessageLog, Long>, JpaSpecificationExecutor<CmMessageLog> {

}
