package com.taojin.common.message.wxmsg;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OperationVO {
    public OperationVO(String first,String keyword1,String keyword2,String keyword3,String remark){
        this.first = first;
        this.keyword1 = keyword1;
        this.keyword2 = keyword2;
        this.keyword3 = keyword3;
    }
    private String first="";
    private String keyword1="";
    private String keyword2="";
    private String keyword3="";
    private String keyword4="";
    private String keyword5="";
    private String remark="";
}
