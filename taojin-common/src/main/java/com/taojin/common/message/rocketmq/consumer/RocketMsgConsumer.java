package com.taojin.common.message.rocketmq.consumer;

import com.taojin.common.message.rocketmq.enums.MessageGroup;
import com.taojin.common.message.rocketmq.model.ConsumerMode;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.AllocateMessageQueueStrategy;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.rebalance.AllocateMessageQueueAveragelyByCircle;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.protocol.heartbeat.MessageModel;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 订阅了主题mq-activities 的消息。
 */
@Slf4j
@Component
public class RocketMsgConsumer implements InitializingBean {


    @Autowired
    private RocketMsgListener listener;

    //MQ 服务地址

    @Autowired
    private ConsumerMode consumerMode;


    @Override
    public void afterPropertiesSet() throws Exception {
        // TODO Auto-generated method stub
        if (!consumerMode.isEnable()) { //不启用mq消费者
            return ;
        }

        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(MessageGroup.CONSUMER_GROUP_ACTIVITIES);
        consumer.setNamesrvAddr(consumerMode.getNamesrvAddr());
        consumer.setConsumeThreadMin(consumerMode.getConsumeThreadMin());
        consumer.setConsumeThreadMax(consumerMode.getConsumeThreadMax());
        //消费者实例
        consumer.setInstanceName("activities-consumer");
        // 从消息队列头开始消费
        consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);
        //集群消费模式
        consumer.setMessageModel(MessageModel.CLUSTERING);

        //集群分配策略（平均轮询分配）
        AllocateMessageQueueStrategy aqs = new AllocateMessageQueueAveragelyByCircle();
        consumer.setAllocateMessageQueueStrategy(aqs);

        try {
            //订阅消息
            consumer.subscribe(MessageGroup.ACTIVITIES_TOPIC, MessageGroup.CONSUMER_TAGS);
            //设置监听器
            consumer.registerMessageListener(listener);
            //启动
            consumer.start();
            log.info("activities Consumer 启动成功!");

        } catch (MQClientException e) {
            // TODO Auto-generated catch block
            log.error("activities  consumer subscribe  fail: {}", e);

        }

    }

}
