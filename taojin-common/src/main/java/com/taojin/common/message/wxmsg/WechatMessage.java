package com.taojin.common.message.wxmsg;


import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson2.JSON;
import com.taojin.common.constant.CommonConstant;
import com.taojin.common.constant.WxConstant;
import com.taojin.common.redis.RedisCatchUtil;
import com.taojin.common.util.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * @author shiyi on 2022/3/22 9:32
 */
@Slf4j
@Component
public class WechatMessage<T> {

    private static String TEMPLATE_URL = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=";

    private static String NOTICE_URL = "https://api.weixin.qq.com/cgi-bin/message/subscribe/bizsend?access_token=";




    @Autowired
    private RedisCatchUtil redisClient;

    /**
     * 根据公众号openid获取缓存access_token
     * @param openid 公众号openid
     * @return
     */
    public String getAccessToken(String openid){
        String access_token ="";
        access_token = (String)redisClient.get(CommonConstant.WX_ACCESS_TOKEN_OPENID+openid);
        if(StringUtils.isEmpty(access_token)){
            access_token = getAccessToken();
        }
        return access_token;
    }
    /**
     * 获取access_token存入redis,设置过期时间
     */
    public String getAccessToken() {
        String access_token ="";
        try {
            //获取token url
            String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + WxConstant.APPLET_ID + "&secret=" + WxConstant.APPLET_SECRET;

            url = "https://api.weixin.qq.com/cgi-bin/stable_token";
            String result = HttpUtils.sendPost(url, "{\"grant_type\":\"client_credential\",\"appid\":\"wx266d7dd4e7e67a2f\",\"secret\":\"adc46436b1518fd72aac31bdc1592b68\"}\n");
            HashMap<String, Object> resultMap = JSON.parseObject(result, HashMap.class);
            log.info("ac get = {} ", resultMap);
            //获取access_token
            access_token = (String) resultMap.get("access_token");
            //获取expires_in
            Integer expires_in = (Integer) resultMap.get("expires_in");
            //存到redis里,并设置过期时间 7200s
            redisClient.set("access_token", access_token, expires_in);
        }catch (Exception e){
            e.printStackTrace();
        }
        return access_token;
    }
    /**
     * 微信模板消息，通用*
     * @param templateId 模板ID
     * @param openId 接收消息的公众号openid
     * @param rurl 跳转url，可为空
     * @param t  消息内容OperationVO
     * @return
     */
    public void sendPublicMessage(String templateId, String openId,String rurl, T t) {

        // 获取access_token
        String accessToken = getAccessToken(openId);
        // 设置模板消息基本参数
        Map<String, Object> map = getStringObjectMap(templateId, openId,rurl);
        // 获取实例
        Class<?> tClass = t.getClass();
        try {
            sendMessage(WechatMessage.TEMPLATE_URL + accessToken, tClass, map, t);
        } catch (IllegalAccessException | InstantiationException e) {
            log.error("发送模板消息异常:{}", e.getMessage());
            e.printStackTrace();
        }
    }
    /**
     * 微信通知消息，通用*
     * @param templateId 模板ID
     * @param openId 接收消息的公众号openid
     * @param rurl 跳转url，可为空
     * @param t  消息内容OperationVO
     * @return
     */
    public void sendNoticeMessage(String templateId, String openId,String rurl, T t) {

        // 获取access_token
        String accessToken = getAccessToken(openId);
        // 设置模板消息基本参数
        Map<String, Object> map = getStringObjectMap(templateId, openId,rurl);
        // 获取实例
        Class<?> tClass = t.getClass();
        try {
            sendMessage(WechatMessage.NOTICE_URL + accessToken, tClass, map, t);
        } catch (IllegalAccessException | InstantiationException e) {
            log.error("发送模板消息异常:{}", e.getMessage());
            e.printStackTrace();
        }
    }
    /**
     *
     * @param templateId 模板ID
     * @param openId 接收消息的公众号openid
     * @param rurl 跳转url，可为空
     * @return
     */
    private Map<String, Object> getStringObjectMap(String templateId, String openId,String rurl) {
        Map<String, Object> map = new HashMap<>();
        map.put("touser", openId);
        map.put("template_id", templateId);
        map.put("appid", WxConstant.PUBLIC_ID);
        map.put("url", rurl);
        return map;
    }

    /**
     * 发送消息
     * @param url 微信模板消息url
     */
    private void sendMessage(String url, Class<?> aClass, Map<String, Object> map, T t) throws IllegalAccessException, InstantiationException {
        // 通过反射拿到多个参数设置，然后发送模板消息
        Map<String, Object> valueBean = new HashMap<>();
        Field[] fields = aClass.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            String name = field.getName();
            Map<Object, Object> colorBean = new HashMap<>();
            //colorBean.put("color","#009100");
            if(!field.get(t).equals("")){
                valueBean.put(name, colorBean);
                colorBean.put("value",field.get(t));
            }
        }
        map.put("data", valueBean);
        String msg = JSON.toJSONString(map);
        log.error("发送模板消息体：{}", msg);
        String post = HttpUtil.post(url, msg);
        log.error("发送结果:{}", post);
    }

    public  void testSendMessage(String templateId, String openId,String rurl, T t){
        try {

            WechatMessage wx = new WechatMessage();
            String accessToken = "";
            if(StringUtils.isEmpty(accessToken)) {
                OperationVO tempVO = new OperationVO("您好，您有一个运维订单状态更新", "通过", "阿斯蒂芬", "ddddd", "");
                // wx.sendPublicMessage("755BWMFm_6f73yWwdt94LRzO", "o8g6Ww_GQmaoS","",tempVO);
                HashMap<String, Object> resultMap = null;
                String gzh_code = "091yZ40w3q1Mp13o7A3w3GyAe32yZ40X";
                String openid = "";
                String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + WxConstant.PUBLIC_ID + "&secret=" + WxConstant.PUBLIC_SECRET + "&code=" + gzh_code + "&grant_type=authorization_code";

                //第一步获取access_token,expires_in
                String result = HttpUtils.sendGet(url);
                resultMap = JSON.parseObject(result, HashMap.class);
                openid = (String) resultMap.get("openid");
                System.out.println(resultMap);
                url = "https://api.weixin.qq.com/cgi-bin/stable_token";

                result = HttpUtils.sendPost(url, "{\"grant_type\":\"client_credential\",\"appid\":\"" + WxConstant.PUBLIC_ID + "\",\"secret\":\"" + WxConstant.PUBLIC_SECRET + "\"}\n");
                resultMap = JSON.parseObject(result, HashMap.class);
                System.out.println("tempid= {} "+ resultMap);
                // wx.setAccess_token((String) resultMap.get("access_token"));
                // 获取access_token
                accessToken = (String) resultMap.get("access_token");
            }
            // 设置模板消息基本参数
            Map<String, Object> map = getStringObjectMap(templateId, openId,rurl);
            // 获取实例
            Class<?> tClass = t.getClass();
            try {
                sendMessage(WechatMessage.NOTICE_URL + accessToken, tClass, map, t);
            } catch (IllegalAccessException | InstantiationException e) {
                log.error("发送模板消息异常:{}", e.getMessage());
                e.printStackTrace();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] str){

        WechatMessage wx = new WechatMessage();
        OperationVO tempVO = new OperationVO("您发布的约战有人应战啦!",  "asdfas3432" , "2wefasf", "asdasfas阿斯蒂","");
        wx.testSendMessage("FetDNElTE3hUv-JYmdEwKC2uW1cCWd6hsapyK_khHMg","o8bXMwFxYC-6MBePaXJ6TiVDJ4NM","/fightPages/fightdetail/fightdetail?id=",tempVO);

      /*  WechatMessage wx = new WechatMessage();
        OperationVO tempVO = new OperationVO("您好，您有一个运维订单状态更新",  "通过" ,"阿斯蒂芬", "ddddd","");
       // wx.sendPublicMessage("755BWMFm_6f73yWwdt94LRzO", "o8g6Ww_GQmaoS","",tempVO);
        HashMap<String, Object> resultMap = null;
        String gzh_code="011xPxFa1enS4G0J2cJa1KCd5r2xPxFD";
        String openid="";
        String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + Configuration.PUBLIC_ID + "&secret=" + Configuration.PUBLIC_SECRET+"&code=" +gzh_code+"&grant_type=authorization_code";
        try {
            //第一步获取access_token,expires_in
            String result = HttpUtils.doGet(url);
            resultMap = JSON.parseObject(result, HashMap.class);
            openid = (String) resultMap.get("openid");
            System.out.println(resultMap);
            url = "https://api.weixin.qq.com/cgi-bin/stable_token";

            result = HttpUtils.doPost(url, "{\"grant_type\":\"client_credential\",\"appid\":\"" + Configuration.PUBLIC_ID + "\",\"secret\":\"" + Configuration.PUBLIC_SECRET + "\"}\n");
            resultMap = JSON.parseObject(result, HashMap.class);
            //logger.info("tempid= {} ", resultMap);
           // wx.setAccess_token((String) resultMap.get("access_token"));



            url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token="+(String) resultMap.get("access_token")+"&openid="+openid+"&lang=zh_CN";
            result = HttpUtils.doGet(url);
            resultMap = JSON.parseObject(result, HashMap.class);
            System.out.println("-----"+resultMap);

        }catch (Exception e){

        }*/

    }
}