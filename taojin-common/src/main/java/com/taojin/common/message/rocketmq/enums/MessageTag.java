package com.taojin.common.message.rocketmq.enums;

/**
 * mq tag 消息标签
 */
public interface MessageTag {
    //赛事审核通过(需要发送赛事完成修改消息以及赛事结算消息)
   String  GANMES_CHECK_SUCCESS  = "games_check_success";
}
