package com.taojin.common.message.wxmsg;



import cn.hutool.crypto.PemUtil;
import com.taojin.common.constant.WxConstant;

import java.io.InputStream;
import java.security.PublicKey;
import java.util.UUID;

/**
 * @Author : dingjiabin
 * @Date : 2020/06/05
 * <p>
 * 全局配置参数
 **/
public class WxConfig {





    /**
     * ◆ 参数名ASCII码从小到大排序（字典序）；
     * ◆ 如果参数的值为空不参与签名；
     * ◆ 参数名区分大小写；
     * ◆ 验证调用返回或微信主动通知签名时，传送的sign参数不参与签名，将生成的签名与该sign值作校验。
     * ◆ 微信接口可能增加字段，验证签名时必须支持增加的扩展字段
     */


    /**
     * 微信 ->  https request Address
     */
    public static final String LOGIN_ADDRESS = "https://api.weixin.qq.com/sns/jscode2session?appid=%s&secret=%s&js_code=%s&grant_type=authorization_code";


    /*获取 nonce_str 參數*/
    public static String getNonceStr() {
        return UUID.randomUUID().toString().replace("-", "").toUpperCase();
    }


    /**
     * 支付入参
     */
    public static final String PAY_FOR_ADDRESS = "https://api.mch.weixin.qq.com/pay/unifiedorder";
    public static final String PAY_FOR_XML_SDP =
            "   <xml>\n" +
                    "   <appid>%s</appid>\n" +
                    "   <body>%s</body>\n" +
                    "   <mch_id>%s</mch_id>\n" +
                    "   <nonce_str>%s</nonce_str>\n" +
                    "   <attach>%s</attach>\n" +
                    "   <notify_url>%s</notify_url>\n" +
                    "   <openid>%s</openid>\n" +
                    "   <out_trade_no>%s</out_trade_no>\n" +  //订单号
                    "   <spbill_create_ip>%s</spbill_create_ip>\n" +
                    "   <total_fee>%d</total_fee>\n" +
                    "   <trade_type>JSAPI</trade_type>\n" +
                    "   <sign>%s</sign>\n" +
                    "   </xml>";

    public static final String PAY_FOR_XML_SDP_Title =
            "   <xml>\n" +
                    "   <appid>%s</appid>\n" +
                    "   <body>%s</body>\n" +
                    "   <mch_id>%s</mch_id>\n" +
                    "   <nonce_str>%s</nonce_str>\n" +
                    "   <notify_url>%s</notify_url>\n" +
                    "   <openid>%s</openid>\n" +
                    "   <out_trade_no>%s</out_trade_no>\n" +  //订单号
                    "   <spbill_create_ip>%s</spbill_create_ip>\n" +
                    "   <total_fee>%d</total_fee>\n" +
                    "   <trade_type>JSAPI</trade_type>\n" +
                    "   <sign>%s</sign>\n" +
                    "   </xml>";

    public static final String PAY_FOR_HTTP_SDP = "appid=%s&attach=%s&body=%s&mch_id=%s&nonce_str=%s&notify_url=%s&openid=%s&out_trade_no=%s&spbill_create_ip=%s&" +
            "total_fee=%d&trade_type=JSAPI&key=%s";

    public static final String PAY_FOR_HTTP_SDP_Title = "appid=%s&body=%s&mch_id=%s&nonce_str=%s&notify_url=%s&openid=%s&out_trade_no=%s&spbill_create_ip=%s&" +
            "total_fee=%d&trade_type=JSAPI&key=%s";

    public static final String TX_FOR_HTTP_SDP = "amount=%s&check_name=FORCE_CHECK&desc=用户提现&mch_appid=%s&mchid=%s&nonce_str=%s&openid=%s&partner_trade_no=%s&" +
            "re_user_name=%s&key=%s";

    public static final String getXmlSdp(String nonceStr, String openId, String outTradeNo,String attach, Integer totalFee, String sign, String callbackUri, String title) {
        String format = String.format(PAY_FOR_XML_SDP, WxConstant.APPLET_ID, title, WxConstant.MERCHANTS_NUM, nonceStr, attach ,callbackUri, openId, outTradeNo, WxConstant.NATIVE_IP, totalFee, sign);
        return format;
    }


    public static final String getXmlSdp(String nonceStr, String openId, String outTradeNo, Integer totalFee, String sign, String callbackUri, String title) {
        String format = String.format(PAY_FOR_XML_SDP_Title, WxConstant.APPLET_ID, title, WxConstant.MERCHANTS_NUM, nonceStr, callbackUri, openId, outTradeNo, WxConstant.NATIVE_IP, totalFee, sign);
        return format;
    }

    public static final String getXmlSdp(String openId, String nonceStr, Integer moneySum, String sign, String partnerTradeNo, String name) {
        String format = String.format(TX_FOR_XML_SDP, WxConstant.APPLET_ID, WxConstant.MERCHANTS_NUM, nonceStr, partnerTradeNo, openId, name, moneySum, sign);
        return format;
    }


    public PublicKey getPublicKey(String keyPemPath) {

        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(keyPemPath);
        if (inputStream == null) {
            throw new RuntimeException("公钥文件不存在");
        }
        return PemUtil.readPemPublicKey(inputStream);
    }

    public static final String getSignStr(String nonceStr,String attach, String openId, String outTradeNo, Integer totalFee, String callbackUri, String title) {
        return String.format(PAY_FOR_HTTP_SDP, WxConstant.APPLET_ID, attach, title, WxConstant.MERCHANTS_NUM, nonceStr, callbackUri, openId, outTradeNo, WxConstant.NATIVE_IP, totalFee, WxConstant.MERCHANTS_KEY);
    }

    public static final String getSignStr(String nonceStr, String openId, String outTradeNo, Integer totalFee, String callbackUri, String title) {
        return String.format(PAY_FOR_HTTP_SDP_Title, WxConstant.APPLET_ID, title, WxConstant.MERCHANTS_NUM, nonceStr, callbackUri, openId, outTradeNo, WxConstant.NATIVE_IP, totalFee, WxConstant.MERCHANTS_KEY);
    }

    public static final String getSignStr(String nonceStr, String openId, Integer num, String partnerTradeNo, String name) {
        return String.format(TX_FOR_HTTP_SDP, num, WxConstant.APPLET_ID, WxConstant.MERCHANTS_NUM, nonceStr, openId, partnerTradeNo, name, WxConstant.MERCHANTS_KEY);
    }


    /**
     * 第二次签名模板
     */
    public static final String PAY_SIGN = "appId=%s&nonceStr=%s&package=prepay_id=%s&signType=MD5&timeStamp=%s&key=%s";

    public static String getPaySignStr(String prepayId, String nonceStr, String timeStamp) {
        return String.format(PAY_SIGN, WxConstant.APPLET_ID, nonceStr, prepayId, timeStamp, WxConstant.MERCHANTS_KEY);
    }


    /**
     * 订单查询 sign模板
     */
    public static final String QUERY_ORDER_ADDRESS = "https://api.mch.weixin.qq.com/pay/orderquery";
    public static final String QUERY_ORDER_SDP =
            "   <xml>\n" +
                    "   <appid>%s</appid>\n" +
                    "   <mch_id>%s</mch_id>\n" +
                    "   <nonce_str>%s</nonce_str>\n" +
                    "   <out_trade_no>%s</out_trade_no>\n" +
                    "   <sign>%s</sign>\n" +
                    "   </xml>";

    public static final String QUERY_SIGN_SDP = "appid=%s&mch_id=%s&nonce_str=%s&out_trade_no=%s";

    public static String getQueryOrderSignStr(String nonceStr, Long orderNo) {
        return String.format(QUERY_SIGN_SDP, WxConstant.APPLET_ID, WxConstant.MERCHANTS_NUM, nonceStr, orderNo);
    }

    public static String getQueryOrderSdp(String nonceStr, Long orderNo, String sign) {
        return String.format(QUERY_ORDER_SDP, WxConstant.APPLET_ID, WxConstant.MERCHANTS_NUM, nonceStr, orderNo, sign);
    }


    /**
     * 退款相关信息
     */
    public static final String ORDER_REFUND_ADDRESS = "https://api.mch.weixin.qq.com/secapi/pay/refund";

    public static final String REFUND_SDP =
            "   <xml>\n" +
                    "   <appid>%s</appid>\n" +
                    "   <mch_id>%s</mch_id>\n" +
                    "   <nonce_str>%s</nonce_str>\n" +
                    "   <out_refund_no>%s</out_refund_no>\n" +
                    "   <out_trade_no>%s</out_trade_no>\n" +
                    "   <refund_fee>%d</refund_fee>\n" +
                    "   <total_fee>%d</total_fee>\n" +
                    "   <sign>%s</sign>\n" +
                    "   </xml>";

    public static final String REFUND_SIGN_STR = "appid=%s&mch_id=%s&nonce_str=%s&out_refund_no=%s&out_trade_no=%s&refund_fee=%s&total_fee=%d&key=%s";

    public static String getRefundSignStr(String nonceStr, String orderNo, String refundOrderNo, Integer refund, Integer money) {
        return String.format(REFUND_SIGN_STR, WxConstant.APPLET_ID, WxConstant.MERCHANTS_NUM, nonceStr, refundOrderNo, orderNo, refund, money, WxConstant.MERCHANTS_KEY);
    }

    public static String getRefundSdp(String nonceStr, String orderNo, String refundOrderNo, Integer refund, Integer money, String sign) {
        return String.format(REFUND_SDP, WxConstant.APPLET_ID, WxConstant.MERCHANTS_NUM, nonceStr, refundOrderNo, orderNo, refund, money, sign);
    }


    /**
     * 查询退款
     */
    public static final String QUERY_ORDER_REFUND_STATUS_ADDRESS = "https://api.mch.weixin.qq.com/pay/refundquery";

    public static final String QUERY_ORDER_REFUND_STATUS_SDP = "<xml>\n" +
            "   <appid>%s</appid>\n" +
            "   <mch_id>%s</mch_id>\n" +
            "   <nonce_str>%s</nonce_str>\n" +
            "   <out_trade_no>%s</out_trade_no>\n" +
            "   <sign>%s</sign>\n" +
            "</xml>";

    public static String QUERY_ORDER_REFUND_STATUS_STR = "appid=%s&mch_id=%s&nonce_str=%s&out_trade_no=%s";

    public static String getQueryOrderRefundStatusStr(String nonceStr, Long refundOrderNo) {
        return String.format(QUERY_ORDER_REFUND_STATUS_STR, WxConstant.APPLET_ID, WxConstant.MERCHANTS_NUM, nonceStr, refundOrderNo);
    }

    public static String getQueryOrderRefundStatusSDP(String nonceStr, Long refundOrderNo, String sign) {
        return String.format(QUERY_ORDER_REFUND_STATUS_SDP, WxConstant.APPLET_ID, WxConstant.MERCHANTS_NUM, nonceStr, refundOrderNo, sign);
    }


    /**
     * 二维码动态生成接口相关配置
     * 1. ACCESS_TOKEN_ADDRESS : 动态获取二维码生成所需的token
     * 2. QR_CODE_ADDRESS : 获取二维码的地址
     */
    public final static String ACCESS_TOKEN_ADDRESS = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + WxConstant.APPLET_ID + "&secret=" + WxConstant.APPLET_SECRET;

    public final static String QR_CODE_ADDRESS = "https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=";


    /**
     * 提现
     */
    public static final String TX_FOR_ADDRESS = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers";
    //public static final String TX_FOR_ADDRESS_QUERY = "https://api.mch.weixin.qq.com/mmpaymkttransfers/gettransferinfo";
    public static final String TX_FOR_XML_SDP =
            "   <xml>\n" +
                    "<mch_appid>%s</mch_appid>\n" +
                    "<mchid>%s</mchid>\n" +
                    "<nonce_str>%s</nonce_str>\n" +
                    "<partner_trade_no>%s</partner_trade_no>\n" +
                    "<openid>%s</openid>\n" +
                    "<check_name>FORCE_CHECK</check_name>\n" +
                    "<re_user_name>%s</re_user_name>\n" +
                    "<amount>%s</amount>\n" +
                    "<desc>用户提现</desc>\n" +
                    "<sign>%s</sign>\n" +
                    "</xml>";
}
