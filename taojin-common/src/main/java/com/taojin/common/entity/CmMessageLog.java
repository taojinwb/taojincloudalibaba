package com.taojin.common.entity;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import org.jeecgframework.poi.excel.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Date;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * 消息发送日志对象 message_log
 *  项目实体类不能重复，否则会出现冲突. common 模块统一加前缀Cm
 * @author sujg
 * @date Wed Dec 20 15:13:19 CST 2023
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "message_log")
@EntityListeners(AuditingEntityListener.class)
public class CmMessageLog
{
    private static final long serialVersionUID = 1L;


    /**  */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id")
    private Long id;

    /** 消息内容 */
    @Excel(name = "消息内容")
    @Column(name ="content")
    private String content;

    /** 状态Z0:初始Z9:成功 Z91:失败 */
    @Excel(name = "状态Z0:初始Z9:成功 Z91:失败")
    @Column(name ="status")
    private String status;

    /** 类型L1:mq L2:短信L3:站内信L4:微信消息 */
    @Excel(name = "类型L1:mq L2:短信L3:站内信L4:微信消息")
    @Column(name ="type")
    private String type;

    /** 重发次数 */
    @Excel(name = "重发次数")
    @Column(name ="retry_count")
    private Integer retryCount;

    /** mq主题 */
    @Excel(name = "mq主题")
    @Column(name ="topic")
    private String topic;

    /** mq标签 */
    @Excel(name = "mq标签")
    @Column(name ="tag")
    private String tag;

    /** mq键 */
    @Excel(name = "mq键")
    @Column(name ="mskey")
    private String mskey;

    /** mq值 */
    @Excel(name = "mq值")
    @Column(name ="msvalue")
    private String msvalue;

    /** mq消息类型，T1:同步T2:单向T3:批量T4:事务T5:有序T6:延迟T7:异步 */
    @Excel(name = "mq消息类型，T1:同步T2:单向T3:批量T4:事务T5:有序T6:延迟T7:异步")
    @Column(name ="mqtype")
    private String mqtype;

    @CreatedBy
    @ApiModelProperty(value = "创建人", hidden = true)
    @Column(name = "create_by", updatable = false)
    public String createBy;

    @CreatedDate
    @ApiModelProperty(value = "创建时间", hidden = true)
    @Column(name = "create_time", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date createTime;

    @LastModifiedBy
    @ApiModelProperty(value = "更新人", hidden = true)
    @Column(name = "update_by",insertable = false)
    public String updateBy;

    @LastModifiedDate
    @ApiModelProperty(value = "更新时间", hidden = true)
    @Column(name = "update_time",insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date updateTime;

    @ApiModelProperty(value = "租户ID", hidden = true)
    @Column(name = "tenant_id",updatable = false)
    public Long tenantId = 0L;

    /** 搜索值 */
    @Transient
    @JsonIgnore
    private String searchValue;

    @Transient
    @JsonIgnore
    private boolean flag;


}
