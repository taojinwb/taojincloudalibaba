package com.taojin.common.websocket;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * websocket 前端将token放到子协议里传入 与后端建立连接时需要用到http协议，此处用于校验token的有效性
 * 暂时不做过滤
 * @Author sujg
 * @Date 2022/4/21 17:01
 **/
@Slf4j
public class WebsocketFilter implements Filter {

    private static final String TOKEN_KEY = "Sec-WebSocket-Protocol";



    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String token = request.getHeader(TOKEN_KEY);

        if (token == null || !isValidToken(token)) {
            log.warn("Invalid or missing WebSocket token");
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Invalid or missing WebSocket token");
            return;
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    private boolean isValidToken(String token) {
        // 实现你的Token验证逻辑，例如查询数据库或者调用外部认证服务
        // 这里假设token等于"valid-token"即为合法
        return "valid-token".equals(token);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // 初始化逻辑（如果有）
    }

    @Override
    public void destroy() {
        // 销毁逻辑（如果有）
    }

}
