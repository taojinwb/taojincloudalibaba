package com.taojin.common.websocket;


import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import okhttp3.WebSocket;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.http.HttpEntity;

import java.io.IOException;
import java.util.Base64;
import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;

/*** 前端
 * <script>
 *     import store from '@/store/'
 *
 *     export default {
 *         data() {
 *             return {
 *             }
 *         },
 *         mounted() {
 *               //初始化websocket
 *               this.initWebSocket()
 *         },
 *         destroyed: function () { // 离开页面生命周期函数
 *               this.websocketclose();
 *         },
 *         methods: {
 *             initWebSocket: function () {
 *                 // WebSocket与普通的请求所用协议有所不同，ws等同于http，wss等同于https
 *                 var userId = store.getters.userInfo.id;
 *                 var url = window._CONFIG['domianURL'].replace("https://","ws://").replace("http://","ws://")+"/websocket/"+userId;
 *                 this.websock = new WebSocket(url);
 *                 this.websock.onopen = this.websocketonopen;
 *                 this.websock.onerror = this.websocketonerror;
 *                 this.websock.onmessage = this.websocketonmessage;
 *                 this.websock.onclose = this.websocketclose;
 *               },
 *               websocketonopen: function () {
 *                 console.log("WebSocket连接成功");
 *               },
 *               websocketonerror: function (e) {
 *                 console.log("WebSocket连接发生错误");
 *               },
 *               websocketonmessage: function (e) {
 *                 var data = eval("(" + e.data + ")");
 *                  //处理订阅信息
 *                 if(data.cmd == "topic"){
 *                    //TODO 系统通知
 *
 *                 }else if(data.cmd == "user"){
 *                    //TODO 用户消息
 *
 *                 }
 *               },
 *               websocketclose: function (e) {
 *                 console.log("connection closed (" + e.code + ")");
 *               }
 *         }
 *     }
 * </script>
 */

@Slf4j
@Service
@ServerEndpoint(value="/websocket/{did}")
public class WebSocketUtil {
    private Session session;
    private static final ConcurrentHashMap<String, Session> sessionPool = new ConcurrentHashMap<>();
    private static CopyOnWriteArraySet<WebSocketUtil> webSockets =new CopyOnWriteArraySet<>();
    private String ipAddress;
    @Autowired
    public void setService(){
    }

    @OnOpen
    public void onOpen(Session session, @PathParam("did") String did){
        try {
            // 进行设备认证
            if (!authenticateDevice(did)) {
                session.close(new CloseReason(CloseReason.CloseCodes.CANNOT_ACCEPT, "Unauthorized device"));
                return;
            }
                this.session = session;
                webSockets.add(this);
                sessionPool.put(did, session);
                log.info("【websocket消息】有新的连接，总数为:"+webSockets.size());

        }catch (Exception e){
            log.info("【websocket连接失败】，设备连接出错！设备deviceid为：" + did);
            e.printStackTrace();
        }
    }
    private boolean authenticateDevice(String did) {
        // 实现你的设备认证逻辑，例如查询数据库或者调用外部认证服务
        return true; // 返回认证结果
    }
    @OnClose
    public void onClose(@PathParam("did") Long did){
        try {
            sessionPool.remove(String.valueOf(did));
            log.info("【系统 WebSocket】连接断开，总数为:" + sessionPool.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnMessage
    public void onMessage(String message, @PathParam(value = "did")  Long did){
        try {
            log.info("【websocket消息】收到客户端消息:"+message);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @OnError
    public void onError(Session session, Throwable t) {
        log.warn("【系统 WebSocket】消息出现错误");
        t.printStackTrace();
    }


    // 此为广播消息
    public void sendAllMessage(String message) {
        log.info("【websocket消息】广播消息:"+message);
        for(WebSocketUtil webSocket : webSockets) {
            try {
                if(webSocket.session.isOpen()) {
                    webSocket.session.getAsyncRemote().sendText(message);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    // 此为单点消息
    public void sendOneMessage(String userId, String message) {
        Session session = sessionPool.get(userId);
        if (session != null&&session.isOpen()) {
            try {
                log.info("【websocket消息】 单点消息:"+message);
                session.getAsyncRemote().sendText(message);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    // 此为单点消息(多人)
    public void sendMoreMessage(String[] userIds, String message) {
        for(String userId:userIds) {
            Session session = sessionPool.get(userId);
            if (session != null&&session.isOpen()) {
                try {
                    log.info("【websocket消息】 单点消息:"+message);
                    session.getAsyncRemote().sendText(message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }
    /**
     * ws推送消息
     *
     * @param message
     * @param did
     */
    public static void sendInfo(String message,Long did) {
        for (Map.Entry<String, Session> item : sessionPool.entrySet()) {
            //userId key值= {用户id + "_"+ 登录token的md5串}
            //TODO vue2未改key新规则，暂时不影响逻辑
            if (item.getKey().contains(String.valueOf(did))) {
                Session session = item.getValue();
                try {
                    synchronized (session){
                        log.info("【系统 WebSocket】推送设备{} 消息:{}" ,did ,message);
                        session.getBasicRemote().sendText(message);
                    }
                } catch (Exception e) {
                    log.error(e.getMessage(),e);
                }
            }
        }
    }
    public static  Long getDateTime(){
        Long   d = System.currentTimeMillis() / 1000L;
        return d;
    }

    public static void main(String[] str){
        String imageUrl = "https://image.likedong.top/20231019/a486f164be8ad9ba7b896b111c623fbf.jpg";

        System.out.println( "{ \"method\": \"pushRelayOut\", \"params\" : {\"DevIdx\": 0,\"Delay\":5 }, \"req_id\": "+22+", }");
    }

}
