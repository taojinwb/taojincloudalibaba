package com.taojin.common.dto;

import javax.persistence.*;
import java.sql.Timestamp;

import com.taojin.framework.config.JpaDto;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;


/**
 * 消息发送日志对象 message_log
 *
 * @author sujg
 * @date Wed Dec 20 15:13:19 CST 2023
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JpaDto
public class MessageLogDTO
{
    private static final long serialVersionUID = 1L;


    /**  */
    private Long id;

    /** 消息内容 */
    @Excel(name = "消息内容")
    private String content;

    /** 状态Z0:初始Z9:成功 Z91:失败 */
    @Excel(name = "状态Z0:初始Z9:成功 Z91:失败")
    private String status;

    /** 类型L1:mq L2:短信L3:站内信L4:微信消息 */
    @Excel(name = "类型L1:mq L2:短信L3:站内信L4:微信消息")
    private String type;

    /** 重发次数 */
    @Excel(name = "重发次数")
    private Integer retryCount;

    /** mq主题 */
    @Excel(name = "mq主题")
    private String topic;

    /** mq标签 */
    @Excel(name = "mq标签")
    private String tag;

    /** mq键 */
    @Excel(name = "mq键")
    private String key;

    /** mq值 */
    @Excel(name = "mq值")
    private String value;

    /** mq消息类型，T1:同步T2:单向T3:批量T4:事务T5:有序T6:延迟T7:异步 */
    @Excel(name = "mq消息类型，T1:同步T2:单向T3:批量T4:事务T5:有序T6:延迟T7:异步")
    private String mqtype;

    /** 创建人 */
    private String createBy;

    /** 创建时间 */
    private Date createTime;

    /** 更新人 */
    private String updateBy;

    /** 更新时间 */
    private Date updateTime;

    /** 租户ID */
    @Excel(name = "租户ID")
    private Long tenantId;



    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Map<String, Object> params = new HashMap<>();


}
