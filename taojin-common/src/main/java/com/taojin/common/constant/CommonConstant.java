package com.taojin.common.constant;

public interface CommonConstant {
    String WX_ACCESS_TOKEN_OPENID="wx:access:token:";
    /**
     * UTF-8 字符集
     */
     String UTF8 = "UTF-8";
    /** 周几的展示 默认从周日开始 */
     String[] WEEK_MARK_SHOW = {"周日","周一","周二","周三","周四","周五","周六"};
}
