package com.taojin.common.constant;

public interface MqConstant {

    /****************  topic ********************/
    //订单
     String DEFAULT_TOPIC = "mq-c";
    //活动
     String ACTIVITIES_TOPIC = "mq-activities";
    //赛事
     String GAMES_TOPIC = "mq-games";
    //商城
     String SHOP_TOPIC = "mq-shop";
    //教培
     String TRAIN_TOPIC = "mq-train";
    //约战
     String FIGHT_TOPIC = "mq-fight";
    //动动圈
     String DYNAMIC_TOPIC = "mq-dynamic";


    /****************  生产者组 ********************/
    //C端
     String PRODUCER_GROUP = "c-producer";

    /****************  消费者组 ********************/
    //订单
     String CONSUMER_GROUP = "orders-consumer";
    //活动
     String CONSUMER_GROUP_ACTIVITIES = "activities-consumer";
    //赛事
     String CONSUMER_GROUP_GAMES = "games-consumer";
    //商城
     String CONSUMER_GROUP_SHOP = "shop-consumer";
    //教培
     String CONSUMER_GROUP_TRAIN = "trian-consumer";
    //约战
     String CONSUMER_GROUP_FIGHT = "fight-consumer";

     String CONSUMER_GROUP_DYNAMIC = "dynamic-consumer";


    //Consumer tags
     String CONSUMER_TAGS = "*";

    //Consumer C端订单消费者订阅的消息
     String ORDERS_TAGS = "create_order||finished||entry_notice_order||wait_use||wait_send||wait_pay_out||send_out||refund_success||user_cancel||fixed_site||cannel_order||notice_order||switch_court||transfer_send";

}
