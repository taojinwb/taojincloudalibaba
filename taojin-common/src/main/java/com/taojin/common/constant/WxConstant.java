package com.taojin.common.constant;

public interface WxConstant {
    /**
     * 订单相关
     */

     Long SCENE_ORDER_TIMEOUT = 60 * 5l; //秒

    /**
     * 线上参数
     */
     String NATIVE_IP = "47.114.114.128";


    /**
     * 小程序appid与密钥
     */
     String APPLET_ID = "wx507b9c287ae6a263";
     String APPLET_SECRET = "1deca17411ccefe171801eef85fb55b6";

    /**
     * 商户id与密钥
     */
     String MERCHANTS_NUM = "1560834661";
     String MERCHANTS_KEY = "2B87N8KDasKV6RY2C8JPKCKTLIKEDONG";

    /**
     * 栎刻动体育公众号
     */
     String PUBLIC_ID = "wx266d7dd4e7e67a2f";
     String PUBLIC_SECRET = "adc46436b1518fd72aac31bdc1592b68";


    /**
     *  微信支付证书地址
     */
     String PAY_CERT_PATH = "/home/service/likedong-c/cert/apiclient_cert.p12";


    //本地地址
     String LOCAL_PAY_CERT_PATH = "/Users/fantasy/apiclient_cert.p12";


    /**
     * 俱乐部申请通知
     */
    public final static String ClubApply_template_id="UjA4Jk_lnTAAi1XmJ8IEDrjNzTMwUn8jlT0W6ejQdd8";

    /**
     * 活动报名结果通知nBeWFb-xR4A0oQSj-N4Qq_llCPvwzDQnkmRB6OXBZko
     * {{first.DATA}}
     * 活动名称：{{keyword1.DATA}}
     * 活动地点：{{keyword2.DATA}}
     * 活动时间：{{keyword3.DATA}}
     * {{remark.DATA}}
     */
    public final static String hdbmjgtz_template_id="nBeWFb-xR4A0oQSj-N4Qq_llCPvwzDQnkmRB6OXBZko";
    /**
     * 上课提醒通知vMWHsn80w1B6O8DRPsX6DmINngAv-y1bxuvUhz6c5eE
     * {{first.DATA}}
     * 课程名：{{keyword1.DATA}}
     * 授课教练：{{keyword2.DATA}}
     * 上课地址：{{keyword3.DATA}}
     * {{remark.DATA}}
     */
    public final static String sktxtz_template_id="vMWHsn80w1B6O8DRPsX6DmINngAv-y1bxuvUhz6c5eE";
    /**
     * 场地预定提醒sUYjIe8Wt7jOvutpZgP-pZ8vQ9C5qB1e734-Ma7PMuU
     * {{first.DATA}}
     * 场地号：{{keyword1.DATA}}
     * 预定时段：{{keyword2.DATA}}
     * {{remark.DATA}}
     */
    public final static String cdyjtx_template_id="sUYjIe8Wt7jOvutpZgP-pZ8vQ9C5qB1e734-Ma7PMuU";
    /**
     * 场地预订提醒 含电话oduvuiWYTq46cds7Pp2W4gjxdUwgLi3XKfgQyC2tbPc
     * {{first.DATA}}
     * 订单编号：{{keyword1.DATA}}
     * 场地信息：{{keyword2.DATA}}
     * 预订时间：{{keyword3.DATA}}
     * 联系电话：{{keyword4.DATA}}
     * {{remark.DATA}}     *
     */
    public final static String cdyjtxdh_template_id="oduvuiWYTq46cds7Pp2W4gjxdUwgLi3XKfgQyC2tbPc";
    /**
     * 退款成功提醒rOe4HIS_JXRzTqGhpopvE8x92nDM3NMqc4MMhCUOv5E
     * {{first.DATA}}
     * 订单号：{{keyword1.DATA}}
     * 退款商品：{{keyword2.DATA}}
     * 退款时间：{{keyword3.DATA}}
     * 退款金额：{{keyword4.DATA}}
     * {{remark.DATA}}
     */
    public final static String tkcgtx_template_id="rOe4HIS_JXRzTqGhpopvE8x92nDM3NMqc4MMhCUOv5E";
    /**
     * 报名成功提醒nS4pV_p_dux37Lx-ovdMw4nH0floIzZJY-6jJmrcxUk
     * {{first.DATA}}
     * 活动名称：{{keyword1.DATA}}
     * 用户姓名：{{keyword2.DATA}}
     * 联系方式：{{keyword3.DATA}}
     * {{remark.DATA}}
     */
    public final static String bmcgtx_template_id="nS4pV_p_dux37Lx-ovdMw4nH0floIzZJY-6jJmrcxUk";
    /**
     * 预报名成功提醒grBrnURPkgnMZtC0QdDnmE_1lsmo3sjoU6E-z52zNzQ
     * {{first.DATA}}
     * 赛事名称：{{keyword1.DATA}}
     * 项目名称：{{keyword2.DATA}}
     * 姓名：{{keyword3.DATA}}
     * 手机号码：{{keyword4.DATA}}
     * {{remark.DATA}}
     */
    public final static String ybmcgtx_template_id="grBrnURPkgnMZtC0QdDnmE_1lsmo3sjoU6E-z52zNzQ";
    /**
     * 退款申请提醒d6tBTRLwFJzakWhO58um3W9hvQ75nChrUaRTXhWXQ1Y
     * {{first.DATA}}
     * 订单商品：{{keyword1.DATA}}
     * 订单编号：{{keyword2.DATA}}
     * 下单时间：{{keyword3.DATA}}
     * 订单金额：{{keyword4.DATA}}
     * 退款申请时间：{{keyword5.DATA}}
     * {{remark.DATA}}
     */
    public final static String tksqtx_template_id="d6tBTRLwFJzakWhO58um3W9hvQ75nChrUaRTXhWXQ1Y";
    /**
     * 课程取消预约提醒Z7YC73WBxBEUpKu3KTYQzPaVkSsAVN7CL-s6XvO19_8
     * {{first.DATA}}
     * 课程名称：{{keyword1.DATA}}
     * 学员姓名：{{keyword2.DATA}}
     * 原定上课时间：{{keyword3.DATA}}
     * 上课门店：{{keyword4.DATA}}
     * {{remark.DATA}}
     */
    public final static String kcqxyytx_template_id="Z7YC73WBxBEUpKu3KTYQzPaVkSsAVN7CL-s6XvO19_8";
    /**
     * 培训课程预约成功通知YqiD2BIfLtMv8ApbcwsID29gYs3_ZItx5vMulSmnem0
     * {{first.DATA}}
     * 预约用户：{{keyword1.DATA}}
     * 预约电话：{{keyword2.DATA}}
     * 预约内容：{{keyword3.DATA}}
     * {{remark.DATA}}
     */
    public final static String pxkcyycg_template_id="YqiD2BIfLtMv8ApbcwsID29gYs3_ZItx5vMulSmnem0";
    /**
     * 会员申请成功通知UjA4Jk_lnTAAi1XmJ8IEDrjNzTMwUn8jlT0W6ejQdd8
     * {{first.DATA}}
     * 真实姓名：{{keyword1.DATA}}
     * 电话号码：{{keyword2.DATA}}
     * 申请时间：{{keyword3.DATA}}
     * {{remark.DATA}}
     */
    public final static String hysqcgtz_template_id="UjA4Jk_lnTAAi1XmJ8IEDrjNzTMwUn8jlT0W6ejQdd8";
    /**
     * 订场取消通知Teq7C4gDMlaRQBb8qt4qN07-3Rj8JU2dEpImExwjU14
     * {{first.DATA}}
     * 场馆名称：{{keyword1.DATA}}
     * 订场日期：{{keyword2.DATA}}
     * 场地详情：{{keyword3.DATA}}
     * 退款详情：{{keyword4.DATA}}
     * {{remark.DATA}}
     */
    public final static String dcqxtz_template_id="Teq7C4gDMlaRQBb8qt4qN07-3Rj8JU2dEpImExwjU14";
    /**
     * 上课提醒Qf3993SO9Ur5W09xiufbsjD1Ma_0vZVH5tJwNPU9aE0
     * {{first.DATA}}
     * 课程名称：{{keyword1.DATA}}
     * 课程时间：{{keyword2.DATA}}
     * 课程地点：{{keyword3.DATA}}
     * {{remark.DATA}}
     */
    public final static String sktx_template_id="Qf3993SO9Ur5W09xiufbsjD1Ma_0vZVH5tJwNPU9aE0";
    /**
     * 赛事报名成功提醒MOhTU4iRR0MFdydtMKH-xHtNpnbOORE6Mvs3exGU9FA
     * {{first.DATA}}
     * 赛事名称：{{keyword1.DATA}}
     * 项目组别：{{keyword2.DATA}}
     * 报名人：{{keyword3.DATA}}
     * 联系方式：{{keyword4.DATA}}
     * 报名费用：{{keyword5.DATA}}
     * {{remark.DATA}}
     */
    public final static String ssbmcg_template_id="MOhTU4iRR0MFdydtMKH-xHtNpnbOORE6Mvs3exGU9FA";
    /**
     *签课成功通知 IGbP8Mv6Me51r9x87Hmp0YsEWhuQiFlPd_suhk70mVQ
     * {{first.DATA}}
     * 课程信息：{{keyword1.DATA}}
     * 签课时间：{{keyword2.DATA}}
     * 上课地点：{{keyword3.DATA}}
     * 剩余次数：{{keyword4.DATA}}
     * {{remark.DATA}}
     */
    public final static String qkcgtz_template_id="IGbP8Mv6Me51r9x87Hmp0YsEWhuQiFlPd_suhk70mVQ";
    /**
     * 用户下单成功提醒9TC9cYQh5-0NhoahiWTjahBsbScgev5zQgfFqeOg29w
     * {{first.DATA}}
     * 订单时间：{{keyword1.DATA}}
     * 订单类型：{{keyword2.DATA}}
     * 用户信息：{{keyword3.DATA}}
     * 订单编号：{{keyword4.DATA}}
     * 订单信息：{{keyword5.DATA}}
     * {{remark.DATA}}
     */
    public final static String yhxdcgtx_template_id="9TC9cYQh5-0NhoahiWTjahBsbScgev5zQgfFqeOg29w";
    /**
     * 打卡结果提醒7oJ1pNAUbB2DRctmQok-6KbW3zj5ATTWsn_FWNv8zkA
     * {{first.DATA}}
     * 今日打卡：{{keyword1.DATA}}
     * 上次打卡：{{keyword2.DATA}}
     * 连续打卡：{{keyword3.DATA}}
     * {{remark.DATA}}
     */
    public final static String dkjgtx_template_id="7oJ1pNAUbB2DRctmQok-6KbW3zj5ATTWsn_FWNv8zkA";
    /**
     *活动报名取消通知 7Hc56_Jt8hzvksGzuHrX1sMkrx-vN0XyzXS5Xo7Terk
     * {{first.DATA}}
     * 活动名称：{{keyword1.DATA}}
     * 活动时间：{{keyword2.DATA}}
     * 取消人：{{keyword3.DATA}}
     * 取消人数：{{keyword4.DATA}}
     * 取消时间：{{keyword5.DATA}}
     * {{remark.DATA}}
     */
    public final static String hdbmqx_template_id="7Hc56_Jt8hzvksGzuHrX1sMkrx-vN0XyzXS5Xo7Terk";
    /**
     * 消费成功通知-mvX5KGHHAbcXVE_FwIci6y0-g4lL6TV7QslojgDQhQ
     * {{first.DATA}}
     * 商户名称：{{keyword1.DATA}}
     * 交易单号：{{keyword2.DATA}}
     * 消费金额：{{keyword3.DATA}}
     * 消费时间：{{keyword4.DATA}}
     * {{remark.DATA}}
     */
    public final static String xfcgtz_template_id="mvX5KGHHAbcXVE_FwIci6y0-g4lL6TV7QslojgDQhQ";
}
