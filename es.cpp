es  kibana创建索引，
https://blog.csdn.net/SimpleSimpleSimples/article/details/133420445?ops_request_misc=&request_id=&biz_id=102&utm_term=kibana%20%E8%AF%A6%E7%BB%86%E6%95%99%E7%A8%8B&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-1-133420445.142^v100^pc_search_result_base2&spm=1018.2226.3001.4187
PUT /employee
{
    "settings": {
        "index": {
            "refresh_interval": "1s",//这个设置指定了索引的刷新间隔
            "number_of_shards": 1,//这个设置指定了索引被分成的主分片（primary shard）的数量
            "max_result_window": "10000",//这个设置指定了从一个查询请求中可以获取的最大文档数目
            "number_of_replicas": 0//这个设置指定了每个主分片的副本数量
        }
    },
    "mappings": {
        "properties": {
            "id": {
                "type": "long"
            },
            "name": {
                "type": "keyword"
            },
            "age": {
                "type": "integer"
            },
            "create_date": {
                "type": "date",
                    "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
            },
            "address": {
                "type": "keyword"
            },
            "desc": {
                "type": "text",
                "fields": {
                    "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                    }
                }
            },
            "leader": {
                "type": "object"
            },
            "car": {
                "type": "nested",
                "properties": {
                    "brand": {
                        "type": "keyword",
                        "ignore_above": 256
                    },
                    "number": {
                        "type": "keyword",
                        "ignore_above": 256
                    },
                    
                    "make": {
                        "type": "keyword",
                        "ignore_above": 256
                    }
                }
            }
        }
    }
}       