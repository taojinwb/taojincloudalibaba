package com.taojin.admin.controller.monitor;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.taojin.admin.framework.common.BaseController;
import com.taojin.admin.framework.constant.CacheConstant;
import com.taojin.admin.dto.LoginUser;
import com.taojin.admin.framework.annotation.Log;
import com.taojin.admin.service.sys.SysUserOnlineService;
import com.taojin.admin.vo.SysUserOnline;
import com.taojin.framework.enums.BusinessType;
import com.taojin.framework.redis.RedisUtil;
import com.taojin.framework.utils.MF;
import com.taojin.framework.vo.ApiResult;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 在线用户监控
 *
 * @author sujg
 */
@RestController
@RequestMapping("/monitor/online")
public class SysUserOnlineController extends BaseController
{
    @Autowired
    private SysUserOnlineService userOnlineService;

    @Autowired
    private RedisUtil redisCache;

    @PreAuthorize("@ss.hasPermi('monitor:online:list')")
    @GetMapping("/list")
    public ApiResult list(String ipaddr, String userName)
    {
        Collection<String> keys = redisCache.keys(CacheConstant.LOGIN_TOKEN_KEY + "*");
        List<SysUserOnline> userOnlineList = new ArrayList<SysUserOnline>();
        for (String key : keys)
        {
            LoginUser user = redisCache.get(key);
            if (StringUtils.isNotEmpty(ipaddr) && StringUtils.isNotEmpty(userName))
            {
                userOnlineList.add(userOnlineService.selectOnlineByInfo(ipaddr, userName, user));
            }
            else if (StringUtils.isNotEmpty(ipaddr))
            {
                userOnlineList.add(userOnlineService.selectOnlineByIpaddr(ipaddr, user));
            }
            else if (StringUtils.isNotEmpty(userName) && MF.isNotNull(user.getUser()))
            {
                userOnlineList.add(userOnlineService.selectOnlineByUserName(userName, user));
            }
            else
            {
                userOnlineList.add(userOnlineService.loginUserToUserOnline(user));
            }
        }
        Collections.reverse(userOnlineList);
        userOnlineList.removeAll(Collections.singleton(null));
        return ApiResult.OK(userOnlineList);
    }

    /**
     * 强退用户
     */
    @PreAuthorize("@ss.hasPermi('monitor:online:forceLogout')")
    @Log(title = "在线用户", businessType = BusinessType.FORCE)
    @GetMapping("/{tokenId}")
    public ApiResult forceLogout(@PathVariable String tokenId)
    {
        redisCache.del(CacheConstant.LOGIN_TOKEN_KEY + tokenId);
        return ApiResult.OK();
    }
}
