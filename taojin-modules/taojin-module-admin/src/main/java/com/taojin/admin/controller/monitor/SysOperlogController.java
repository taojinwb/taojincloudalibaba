package com.taojin.admin.controller.monitor;


import javax.servlet.http.HttpServletResponse;

import com.taojin.admin.framework.common.BaseController;
import com.taojin.admin.entity.sys.SysOperLog;
import com.taojin.admin.framework.annotation.Log;
import com.taojin.admin.service.sys.SysOperLogService;
import com.taojin.framework.enums.BusinessType;
import com.taojin.framework.vo.ApiResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 操作日志记录
 *
 * @author sujg
 */
@RestController
@RequestMapping("/monitor/operlog")
@Slf4j
public class SysOperlogController extends BaseController
{
    @Autowired
    private SysOperLogService operLogService;

    @PreAuthorize("@ss.hasPermi('monitor:operlog:list')")
    @GetMapping("/list")
    public ApiResult list(SysOperLog operLog)
    {
        Page<SysOperLog> list= operLogService.selectOperLogList(operLog,this.getPageable(operLog.getParams()));
        //log.info(list.getSize()+"---------");
        return ApiResult.OK(list);
    }

    @Log(title = "操作日志", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('monitor:operlog:export')")
    @PostMapping("/export")
    public ModelAndView export(HttpServletResponse response, SysOperLog operLog)
    {
        return super.exportXls(operLogService.selectOperLogList(operLog) , operLog, SysOperLog.class, "操作日志");
    }

    @Log(title = "操作日志", businessType = BusinessType.DELETE)
    @PreAuthorize("@ss.hasPermi('monitor:operlog:remove')")
    @DeleteMapping("/{operIds}")
    public ApiResult remove(@PathVariable Long[] operIds)
    {
        operLogService.deleteOperLogByIds(operIds);
        return ApiResult.OK();
    }

    @Log(title = "操作日志", businessType = BusinessType.CLEAN)
    @PreAuthorize("@ss.hasPermi('monitor:operlog:remove')")
    @DeleteMapping("/clean")
    public ApiResult clean()
    {
        operLogService.cleanOperLog();
        return ApiResult.OK();
    }
}
