package com.taojin.admin.elastic.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

public class activitesShowVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 活动ID
	 */
	@ApiModelProperty(value = "活动ID")
	private long id;
	
	/**
	 * 浏览量
	 */
	@ApiModelProperty(value = "浏览量")
	private long total;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	@Override
	public String toString() {
		return "activitesShowVo [id=" + id + ", total=" + total + "]";
	}
	
	

}
