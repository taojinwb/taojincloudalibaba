package com.taojin.admin.controller.sys;


import com.taojin.admin.framework.config.TaojinConfig;
import com.taojin.admin.service.sys.SysConfigService;
import com.taojin.admin.framework.constant.CacheConstant;
import com.taojin.framework.constant.SysConstants;
import com.taojin.framework.redis.RedisUtil;
import com.taojin.framework.utils.MF;
import com.taojin.framework.utils.id.IdUtils;
import com.taojin.framework.utils.sign.Base64;
import com.taojin.framework.vo.ApiResult;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FastByteArrayOutputStream;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.code.kaptcha.Producer;
import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 验证码操作处理
 *
 * @author sujg
 */
@RestController
@Slf4j
@Api(tags = "验证码操作处理",value = "验证码操作处理")
public class CaptchaController
{
    @Resource(name = "captchaProducer")
    private Producer captchaProducer;

    @Resource(name = "captchaProducerMath")
    private Producer captchaProducerMath;

    @Autowired
    private RedisUtil redisCache;

    @Autowired
    private SysConfigService configService;
    /**
     * 生成验证码
     */
    @GetMapping("/captchaImage")
    public ApiResult getCode(HttpServletResponse response) throws IOException
    {
        Map ajax = new HashMap<>();
        boolean captchaEnabled = configService.selectCaptchaEnabled();

        ajax.put("captchaEnabled", captchaEnabled);
        if (!captchaEnabled)
        {
            return ApiResult.OK(ajax);
        }

        // 保存验证码信息
        String uuid = IdUtils.simpleUUID();
        String verifyKey = CacheConstant.CAPTCHA_CODE_KEY + uuid;

        String capStr = null, code = null;
        BufferedImage image = null;

        // 生成验证码
        String captchaType = TaojinConfig.getCaptchaType();
        if(MF.isEmpty(captchaType)){captchaType="math";}
        if ("char".equals(captchaType))
        {
            capStr = code = captchaProducer.createText();
            image = captchaProducer.createImage(capStr);
        }else if ("math".equals(captchaType))
        {
            String capText = captchaProducerMath.createText();
            capStr = capText.substring(0, capText.lastIndexOf("@"));
            code = capText.substring(capText.lastIndexOf("@") + 1);
            image = captchaProducerMath.createImage(capStr);
        }
        redisCache.set(verifyKey, code, SysConstants.CAPTCHA_EXPIRATION*60);
        // 转换流信息写出
        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        try
        {
            ImageIO.write(image, "jpg", os);
        }
        catch (IOException e)
        {
            return ApiResult.error(e.getMessage());
        }

        ajax.put("uuid", uuid);
        ajax.put("img", Base64.encode(os.toByteArray()));
        return ApiResult.OK(ajax);
    }
}
