package com.taojin.admin.modules.gamesinfo.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.persistence.*;

import lombok.NonNull;
import org.jeecgframework.poi.excel.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.format.annotation.DateTimeFormat;
import com.taojin.admin.entity.BaseEntity;
import lombok.Data;

/**
 * 赛事信息 对象 games_info
 *
 * @author sujg
 * @date Mon Dec 18 13:44:04 CST 2023
 */
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Data
@Entity(name = "games_info")
public class GamesInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    /** ID 赛事ID */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id")
    private Long id;

    /** 发布者ID 发布者ID ，对应 b_venue_authentication.id */
    @Excel(name = "发布者ID 发布者ID ，对应 b_venue_authentication.id")
    @Column(name ="buss_id")
    private Long bussId;

    /** 发布者名称 */
    @Excel(name = "发布者名称")
    @Column(name ="buss_nick")
    private String bussNick;

    /** 赛事名称 */
    @Excel(name = "赛事名称")
    @Column(name ="title")
    private String title;

    /** 运动类型 对应 games_sports_type.id */
    @Excel(name = "运动类型 对应 games_sports_type.id")
    @Column(name ="sports_type")
    private Integer sportsType;

    /** 赛事类型 10：校园联赛   20：企业联赛  30：民间联赛 40：官方联赛  50：机构联赛  60：业余联赛 */
    @Excel(name = "赛事类型 10：校园联赛   20：企业联赛  30：民间联赛 40：官方联赛  50：机构联赛  60：业余联赛")
    @Column(name ="type")
    private Integer type;

    /** 赛制类型 赛制类型（10：单项赛   20：团体赛    30：单项+团体  40: 综合赛事） */
    @Excel(name = "赛制类型 赛制类型")
    @Column(name ="rule_type")
    private Integer ruleType;

    /** 是否公开 10: 公开    20： 不公开 */
    @Excel(name = "是否公开 10: 公开    20： 不公开")
    @Column(name ="is_public")
    private Integer isPublic;

    /** 状态 状态（0：初始状态    10：待审核  15：审核失败  20：报名中   50：已结束    100:已经取消） */
    @Excel(name = "状态 状态")
    @Column(name ="status")
    private Integer status;

    /** 主办单位 主办方，多个用英文逗号（,） 隔开 */
    @Excel(name = "主办单位 主办方，多个用英文逗号")
    @Column(name ="host_org")
    private String hostOrg;

    /** 比赛地址 */
    @Excel(name = "比赛地址")
    @Column(name ="address")
    private String address;

    /** 场馆名称 */
    @Excel(name = "场馆名称")
    @Column(name ="venue_name")
    private String venueName;

    /** 赛事头图 */
    @Excel(name = "赛事头图")
    @Column(name ="head_imgage")
    private String headImgage;

    /** 地址经度 */
    @Excel(name = "地址经度")
    @Column(name ="address_longitude")
    private BigDecimal addressLongitude;

    /** 地址纬度 */
    @Excel(name = "地址纬度")
    @Column(name ="address_latitude")
    private BigDecimal addressLatitude;

    /** 比赛开始时间 */
    @Excel(name = "比赛开始时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name ="start_time")
    private Date startTime;

    /** 比赛结束时间 */
    @Excel(name = "比赛结束时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name ="end_time")
    private Date endTime;

    /** 报名开始时间 */
    @Excel(name = "报名开始时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name ="apply_start_time")
    private Date applyStartTime;

    /** 截止报名时间 */
    @Excel(name = "截止报名时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name ="stop_time")
    private Date stopTime;

    /** 名单公示开始时间 */
    @Excel(name = "名单公示开始时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name ="open_list_start_time")
    private Date openListStartTime;

    /** 名单公示结束时间 */
    @Excel(name = "名单公示结束时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name ="open_list_start_end")
    private Date openListStartEnd;

    /** 抽签公示开始时间 */
    @Excel(name = "抽签公示开始时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name ="open_draw_time_start")
    private Date openDrawTimeStart;

    /** 抽签公示结束时间 */
    @Excel(name = "抽签公示结束时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name ="open_draw_time_end")
    private Date openDrawTimeEnd;

    /** 单项赛兼项数量 */
    @Excel(name = "单项赛兼项数量")
    @Column(name ="single_repeat")
    private Integer singleRepeat;

    /** 团体赛兼项数量 */
    @Excel(name = "团体赛兼项数量")
    @Column(name ="group_repeat")
    private Integer groupRepeat;

    /** 是否允许退赛 10: 允许退赛    20：不允许退赛 */
    @Excel(name = "是否允许退赛 10: 允许退赛    20：不允许退赛")
    @Column(name ="is_exit")
    private Integer isExit;

    /** 退赛截止时间 */
    @Excel(name = "退赛截止时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name ="exit_end_time")
    private Date exitEndTime;

    /** 退赛处罚金额占比 百分制（50  =  50%） */
    @Excel(name = "退赛处罚金额占比 百分制")
    @Column(name ="exit_ratio")
    private Integer exitRatio;

    /** 单项赛参赛人员必填信息 对应报名信息字段表games_fields.id , 多个用逗号分开(eg: 1,34,23) */
    @Excel(name = "单项赛参赛人员必填信息 对应报名信息字段表games_fields.id , 多个用逗号分开(eg: 1,34,23)")
    @Column(name ="single_fields")
    private String singleFields;

    /** 团体赛参赛人员必填信息 对应报名信息字段表games_fields.id , 多个用逗号分开(eg: 1,34,23) */
    @Excel(name = "团体赛参赛人员必填信息 对应报名信息字段表games_fields.id , 多个用逗号分开(eg: 1,34,23)")
    @Column(name ="group_fields")
    private String groupFields;

    /** 0: 正常    1：删除 */
    @Excel(name = "0: 正常    1：删除")
    @Column(name ="is_del")
    private Integer isDel;


}
