package com.taojin.admin.modules.orders.entity;

import javax.persistence.*;
import java.sql.Timestamp;

import com.taojin.admin.modules.orders.dto.OrdersDTO;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.taojin.admin.entity.BaseEntity;
import lombok.Data;
import com.taojin.admin.entity.BaseEntity;

/**
 * 订单主 对象 orders
 *
 * @author sujg
 * @date Thu Dec 14 16:22:23 CST 2023
 */
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Data
@Entity(name = "orders")
public class Orders extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    /** ID 自增ID */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id")
    private Long id;

    /** 订单ID */
    @Column(name ="oid")
    private String oid;

    /** 父订单ID */
    @Excel(name = "父订单ID")
    @Column(name ="parent_oid")
    private String parentOid;

    /** 外部订单的订单号，如果该订单是来自外部系统，则存在该订单号 */
    @Excel(name = "外部订单的订单号，如果该订单是来自外部系统，则存在该订单号")
    @Column(name ="exterior_oid")
    private String exteriorOid;

    /** 产品标题 */
    @Excel(name = "产品标题")
    @Column(name ="items_title")
    private String itemsTitle;

    /** 产品ID */
    @Excel(name = "产品ID")
    @Column(name ="items_id")
    private Long itemsId;

    /** 产品图片地址 */
    @Excel(name = "产品图片地址")
    @Column(name ="items_image")
    private String itemsImage;

    /** 商品单价 单位（分） */
    @Excel(name = "商品单价 单位")
    @Column(name ="items_price")
    private Integer itemsPrice;

    /** 商品类型 0：默认    5: 活动报名  10:场馆预约  12:会员卡   15: 会员卡充值   20: 赛事报名   30：商品 */
    @Excel(name = "商品类型 0：默认    5: 活动报名  10:场馆预约  12:会员卡   15: 会员卡充值   20: 赛事报名   30：商品")
    @Column(name ="items_type")
    private Integer itemsType;

    /** 项目类目 */
    @Excel(name = "项目类目")
    @Column(name ="items_category")
    private Long itemsCategory;

    /** 商品规格 */
    @Excel(name = "商品规格")
    @Column(name ="items_specif")
    private String itemsSpecif;

    /** skuId */
    @Excel(name = "skuId")
    @Column(name ="sku_id")
    private Long skuId;

    /** 购买数量 个/件/次/场/单 */
    @Excel(name = "购买数量 个/件/次/场/单")
    @Column(name ="items_num")
    private Integer itemsNum;

    /** 订单价格 单位（分），单价*数量+邮费 */
    @Excel(name = "订单价格 单位")
    @Column(name ="order_price")
    private Integer orderPrice;

    /** 实际支付价格 单位（分），单价*数量-优惠+邮费 */
    @Excel(name = "实际支付价格 单位")
    @Column(name ="pay_price")
    private Integer payPrice;

    /** 优惠价格 单位（分） */
    @Excel(name = "优惠价格 单位")
    @Column(name ="disc_price")
    private Integer discPrice;

    /** 邮费 单位（分） */
    @Excel(name = "邮费 单位")
    @Column(name ="post_fee")
    private Integer postFee;

    /** 支付方式 */
    @Excel(name = "支付方式")
    @Column(name ="pay_way")
    private String payWay;

    /** 支付方式-二级分类 */
    @Excel(name = "支付方式-二级分类")
    @Column(name ="pay_son_type")
    private String paySonType;

    /** 订单状态 订单状态（0：订单创建  10: 待确认   20: 待支付    30: 待发货  35: 已发货   40: 待使用   50：已完成   60: 待结算  70：已结算 100: 系统取消  101：用户取消   120：订单删除） */
    @Excel(name = "订单状态 订单状态")
    @Column(name ="status")
    private Integer status;

    /** 订单退款状态 0: 无退款   10：退款待审核/确认   20: 全额退款成功   30：部分退款成功  40：退款失败 */
    @Excel(name = "订单退款状态 0: 无退款   10：退款待审核/确认   20: 全额退款成功   30：部分退款成功  40：退款失败")
    @Column(name ="ref_status")
    private Integer refStatus;

    /** 评价状态 */
    @Excel(name = "评价状态")
    @Column(name ="rate_status")
    private Integer rateStatus;

    /** 用户ID */
    @Excel(name = "用户ID")
    @Column(name ="user_id")
    private Long userId;

    /** 场馆会员标号（场馆下单是创建的会员，场馆自行导入的会员） 如果items_type为30: 店铺订单 （且该商品为商城商品 该字段的意思为分销商的用户id 对应c_user.id，如果为供应链商品，该字段代表场馆某个场馆的id 对应b_venue_detail.id、表示该订单是某个场馆销售出去的） */
    @Excel(name = "场馆会员标号")
    @Column(name ="buss_user_id")
    private Long bussUserId;

    /** 用户昵称 */
    @Excel(name = "用户昵称")
    @Column(name ="user_nick")
    private String userNick;

    /** 商户ID 单位（分） */
    @Excel(name = "商户ID 单位")
    @Column(name ="buss_id")
    private Long bussId;

    /** 商户名称 */
    @Excel(name = "商户名称")
    @Column(name ="buss_name")
    private String bussName;

    /** 买家备注 */
    @Excel(name = "买家备注")
    @Column(name ="user_notes")
    private String userNotes;

    /** 商户备注 */
    @Excel(name = "商户备注")
    @Column(name ="buss_notes")
    private String bussNotes;

    /** 订单旗帜 商户备注旗帜，用于商户标记订单级别（0: 默认  10：蓝旗  20：红旗  30：绿旗  40：黄旗  50:黑旗  ） */
    @Excel(name = "订单旗帜 商户备注旗帜，用于商户标记订单级别")
    @Column(name ="o_flag")
    private Integer oFlag;

    /** 订单来源 */
    @Excel(name = "订单来源")
    @Column(name ="source")
    private Integer source;


    public void setId(Long id){this.id = id;}

    public Long getId(){ return id;}
    public void setOid(String oid){this.oid = oid;}

    public String getOid(){ return oid;}
    public void setParentOid(String parentOid){this.parentOid = parentOid;}

    public String getParentOid(){ return parentOid;}
    public void setExteriorOid(String exteriorOid){this.exteriorOid = exteriorOid;}

    public String getExteriorOid(){ return exteriorOid;}
    public void setItemsTitle(String itemsTitle){this.itemsTitle = itemsTitle;}

    public String getItemsTitle(){ return itemsTitle;}
    public void setItemsId(Long itemsId){this.itemsId = itemsId;}

    public Long getItemsId(){ return itemsId;}
    public void setItemsImage(String itemsImage){this.itemsImage = itemsImage;}

    public String getItemsImage(){ return itemsImage;}
    public void setItemsPrice(Integer itemsPrice){this.itemsPrice = itemsPrice;}

    public Integer getItemsPrice(){ return itemsPrice;}
    public void setItemsType(Integer itemsType){this.itemsType = itemsType;}

    public Integer getItemsType(){ return itemsType;}
    public void setItemsCategory(Long itemsCategory){this.itemsCategory = itemsCategory;}

    public Long getItemsCategory(){ return itemsCategory;}
    public void setItemsSpecif(String itemsSpecif){this.itemsSpecif = itemsSpecif;}

    public String getItemsSpecif(){ return itemsSpecif;}
    public void setSkuId(Long skuId){this.skuId = skuId;}

    public Long getSkuId(){ return skuId;}
    public void setItemsNum(Integer itemsNum){this.itemsNum = itemsNum;}

    public Integer getItemsNum(){ return itemsNum;}
    public void setOrderPrice(Integer orderPrice){this.orderPrice = orderPrice;}

    public Integer getOrderPrice(){ return orderPrice;}
    public void setPayPrice(Integer payPrice){this.payPrice = payPrice;}

    public Integer getPayPrice(){ return payPrice;}
    public void setDiscPrice(Integer discPrice){this.discPrice = discPrice;}

    public Integer getDiscPrice(){ return discPrice;}
    public void setPostFee(Integer postFee){this.postFee = postFee;}

    public Integer getPostFee(){ return postFee;}
    public void setPayWay(String payWay){this.payWay = payWay;}

    public String getPayWay(){ return payWay;}
    public void setPaySonType(String paySonType){this.paySonType = paySonType;}

    public String getPaySonType(){ return paySonType;}
    public void setStatus(Integer status){this.status = status;}

    public Integer getStatus(){ return status;}
    public void setRefStatus(Integer refStatus){this.refStatus = refStatus;}

    public Integer getRefStatus(){ return refStatus;}
    public void setRateStatus(Integer rateStatus){this.rateStatus = rateStatus;}

    public Integer getRateStatus(){ return rateStatus;}
    public void setUserId(Long userId){this.userId = userId;}

    public Long getUserId(){ return userId;}
    public void setBussUserId(Long bussUserId){this.bussUserId = bussUserId;}

    public Long getBussUserId(){ return bussUserId;}
    public void setUserNick(String userNick){this.userNick = userNick;}

    public String getUserNick(){ return userNick;}
    public void setBussId(Long bussId){this.bussId = bussId;}

    public Long getBussId(){ return bussId;}
    public void setBussName(String bussName){this.bussName = bussName;}

    public String getBussName(){ return bussName;}
    public void setUserNotes(String userNotes){this.userNotes = userNotes;}

    public String getUserNotes(){ return userNotes;}
    public void setBussNotes(String bussNotes){this.bussNotes = bussNotes;}

    public String getBussNotes(){ return bussNotes;}
    public void setoFlag(Integer oFlag){this.oFlag = oFlag;}

    public Integer getoFlag(){ return oFlag;}
    public void setSource(Integer source){this.source = source;}

    public Integer getSource(){ return source;}


}
