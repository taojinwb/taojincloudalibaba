package com.taojin.admin.modules.gamesinfo.repository;

import com.taojin.admin.modules.gamesinfo.dto.GamesInfoDTO;
import com.taojin.admin.modules.gamesinfo.entity.GamesInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;
/**
 * 赛事信息 repository
 *
 * @author sujg
 * @date Mon Dec 18 13:44:04 CST 2023
 */
public interface GamesInfoRepository extends  JpaRepository<GamesInfo, Long>, JpaSpecificationExecutor<GamesInfo> {

    /**
     * 查询赛事列表
     * @param status 状态（0：初始状态    10：待审核  15：审核失败  20：报名中   50：已结束    100:已经取消）
     * @param ruleType 赛事类型（10：单项赛   20：团体赛    30：单项+团体）
     * @param search 搜索字段(赛事名称)
     * @param sportsTypeId 运动项目
     * @param pageable
     * @return
     */
    @Query(value = "select g.id, g.buss_nick,g.title,g.rule_type, g.status,g.head_imgage,  g.start_time, g.end_time,  ex.play_number " +
            " from games_info g " +
            " left join play_info ex on ex.games_id = g.id " +
            " where g.is_del = 0 " +
            " and (COALESCE(:status, null) is null or g.status in :status) " +
            " and (COALESCE(:ruleType, null) is null or g.rule_type = :ruleType) " +
            " and (COALESCE(:sportsTypeId, null) is null or g.sports_type = :sportsTypeId) " +
            " and (COALESCE(:search, null) is null or g.title like :search or g.buss_nick like :search ) ",nativeQuery = true)
    Page<GamesInfoDTO>  findAllByParams(@Param("status") List<Integer> status, @Param("ruleType") Integer ruleType,
                                        @Param("search") String search, @Param("sportsTypeId") Integer sportsTypeId, Pageable pageable);


    /**
     * 根据主键修改赛事删除标识
     * @param id 主键
     */
    @Modifying
    @Query(value =" update games_info set is_del = 1 where id = ?1 ", nativeQuery = true)
    int updateIsDelById(Long id);


    /**
     * 根据主键修改赛事状态
     * @param status 状态 15 审核不通过 20 审核通过(报名中)
     * @param id 主键
     */
    @Modifying
    @Query(value ="update games_info set status = ?1 where id = ?2 ", nativeQuery = true)
    int updateStatusById(Integer status, Long id);

}
