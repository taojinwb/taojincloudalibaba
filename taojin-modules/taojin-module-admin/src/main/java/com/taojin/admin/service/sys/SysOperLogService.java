package com.taojin.admin.service.sys;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.taojin.admin.framework.common.BaseService;
import com.taojin.admin.entity.sys.SysOperLog;
import com.taojin.admin.repository.sys.SysOperLogRepository;
import com.taojin.framework.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * 操作日志 服务层处理
 * 
 * @author sujg
 */
@Service
public class SysOperLogService extends BaseService<SysOperLog,Long>
{


    @Autowired
    private SysOperLogRepository sysOperLogRepository;

    public SysOperLogService(SysOperLogRepository repository) {
        super(repository,repository);
    }

    /**
     * 新增操作日志
     *
     * @param operLog 操作日志对象
     */
    public void insertOperlog(SysOperLog operLog)
    {
        sysOperLogRepository.save(operLog);
    }

    /**
     * 查询系统操作日志集合
     *
     * @param operLog 操作日志对象
     * @return 操作日志集合
     */
    public Page<SysOperLog> selectOperLogList(SysOperLog operLog, Pageable pageable)
    {

        if(operLog.getParams().get("beginTime")!=null&&operLog.getParams().get("endTime")!=null){
            HashMap<String, Object> hashMap = new HashMap<>();
            String beginTime = DateUtils.dateformat(operLog.getParams().get("beginTime") + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
            String endTime = DateUtils.dateformat(operLog.getParams().get("endTime") + " 23:59:59", "yyyy-MM-dd HH:mm:ss");
            hashMap.put("beginTime",beginTime);
            hashMap.put("endTime",endTime);
            operLog.setParams(hashMap);
        }
        return sysOperLogRepository.selectOperLogList(operLog,pageable);
    }
    public List<SysOperLog> selectOperLogList(SysOperLog operLog)
    {
        return sysOperLogRepository.findAll(filterByFields(operLog));
    }

    /**
     * 批量删除系统操作日志
     *
     * @param operIds 需要删除的操作日志ID
     * @return 结果
     */
    public void deleteOperLogByIds(Long[] operIds)
    {

        Iterable<Long> iterable = Arrays.asList(operIds);

         sysOperLogRepository.deleteAllByIdInBatch(iterable);
    }

    /**
     * 查询操作日志详细
     *
     * @param operId 操作ID
     * @return 操作日志对象
     */
    public SysOperLog selectOperLogById(Long operId)
    {
        return sysOperLogRepository.findById(operId).orElse(null);
    }

    /**
     * 清空操作日志
     */
    public void cleanOperLog()
    {
        sysOperLogRepository.cleanOperLog();
    }

}
