package com.taojin.admin.modules.gamesinfo.repository;

import com.taojin.admin.modules.gamesinfo.entity.GamesInfoExtd;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


/**
 * 赛事信息Repository
 *
 *
 */
public interface GamesInfoExtdRepository extends JpaRepository<GamesInfoExtd, Long>, JpaSpecificationExecutor<GamesInfoExtd> {

    /**
     * 根据赛事主键查询赛事扩展表
     * @param gamesId 赛事主键
     * @return
     */
    GamesInfoExtd findByGamesId(Long gamesId);



    /**
            * 修改赛事直播地址
	 * @param playUrl 直播地址
	 * @param id 扩展表主键
	 */
    @Modifying
    @Query(value ="update games_info_extd set playUrl = ?1 where id = ?2 ")
    void updatePlayUrlByGamesId(String playUrl, Long id);


    /**
     * 根据赛事主键修改审核备注
     * @param auditNotes 审核备注
     * @param gamesId 赛事主键
     * @return
     */
    @Modifying
    @Query(value ="update games_info_extd set fail_reason = ?1 where games_id = ?2 ", nativeQuery = true)
    int updateFailReasonByGamesId(String auditNotes, Long gamesId);

    /**
     * 根据赛事主键修改赛事人工审核状态
     * @param manualAuditStatus 人工审核状态 0 待审核 1 审核通过
     * @param gamesId 赛事主键
     */
    @Modifying
    @Query(value ="update games_info_extd set manual_audit_status = ?1 where games_id = ?2 ", nativeQuery = true)
    void updateManualAuditStatusByGamesId(int manualAuditStatus, Long gamesId);

}
