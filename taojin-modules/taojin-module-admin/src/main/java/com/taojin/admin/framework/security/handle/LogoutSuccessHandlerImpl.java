package com.taojin.admin.framework.security.handle;


import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson2.JSON;
import com.taojin.admin.dto.LoginUser;
import com.taojin.admin.framework.factory.AsyncFactory;
import com.taojin.admin.service.sys.TokenService;
import com.taojin.framework.constant.SysConstants;
import com.taojin.framework.utils.MF;
import com.taojin.framework.utils.ServletUtils;
import com.taojin.framework.vo.ApiResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import com.taojin.framework.manager.AsyncManager;

/**
 * 自定义退出处理类 返回成功
 *
 * @author taojin
 */
@Configuration
public class LogoutSuccessHandlerImpl implements LogoutSuccessHandler
{
    @Autowired
    private TokenService tokenService;

    /**
     * 退出处理
     *
     * @return
     */
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException
    {
        LoginUser loginUser = tokenService.getLoginUser(request);
        if (MF.isNotNull(loginUser))
        {
            String userName = loginUser.getUsername();
            // 删除用户缓存记录
            tokenService.delLoginUser(loginUser.getToken());
            // 记录用户退出日志
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(userName, SysConstants.LOGOUT, "退出成功"));
        }
        ServletUtils.renderString(response, JSON.toJSONString(ApiResult.ok("退出成功")));
    }
}
