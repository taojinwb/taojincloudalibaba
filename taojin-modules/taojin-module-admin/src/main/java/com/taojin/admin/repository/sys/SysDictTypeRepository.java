package com.taojin.admin.repository.sys;

import com.taojin.admin.entity.sys.SysDictType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SysDictTypeRepository extends  JpaRepository<SysDictType, Long>, JpaSpecificationExecutor<SysDictType> {
    SysDictType findFirstByDictType(String dictType);
    List<SysDictType> findAllByDictType(String dictType);



    @Query(value = "select * from sys_dict_type as dt" +
            " where (:#{#dictData.params['beginTime']} IS NULL OR   date_format(dt.create_time,'%Y-%m-%d %H:%m:%s') >= :#{#dictData.params['beginTime']}) " +
            " and (:#{#dictData.params['endTime']} IS NULL OR   date_format(dt.create_time,'%Y-%m-%d %H:%m:%s') <= :#{#dictData.params['endTime']}) " +
            " and (:#{#dictData.dictName} IS NULL OR dt.dict_name like %:#{#dictData.dictName}%)" +
            " and (:#{#dictData.status} IS NULL OR  dt.status = :#{#dictData.status})"+
            " and (:#{#dictData.dictType} IS NULL OR dt.dict_type = :#{#dictData.dictType})",nativeQuery = true)
    Page<SysDictType> selectDictTypeList(@Param("dictData") SysDictType dictData, Pageable pageable);
}
