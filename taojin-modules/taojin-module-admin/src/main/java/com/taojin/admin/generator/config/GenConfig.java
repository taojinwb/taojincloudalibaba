package com.taojin.admin.generator.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 读取代码生成相关配置
 * 
 * @author taojin
 */
@Component
public class GenConfig
{
    /** 作者 */
    @Value("${gen.author}")
    public  String author;

    /** 生成包路径 */
    @Value("${gen.packageName}")
    public  String packageName;

    /** 自动去除表前缀，默认是false */
    @Value("${gen.autoRemovePre}")
    public  boolean autoRemovePre;

    /** 表前缀(类名不会包含表前缀) */
    @Value("${gen.tablePrefix}")
    public  String tablePrefix;

    public  String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public boolean isAutoRemovePre() {
        return autoRemovePre;
    }

    public void setAutoRemovePre(boolean autoRemovePre) {
        this.autoRemovePre = autoRemovePre;
    }

    public String getTablePrefix() {
        return tablePrefix;
    }

    public void setTablePrefix(String tablePrefix) {
        this.tablePrefix = tablePrefix;
    }
}
