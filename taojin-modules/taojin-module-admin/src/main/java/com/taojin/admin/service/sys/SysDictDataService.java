package com.taojin.admin.service.sys;

import com.taojin.admin.framework.common.BaseService;
import com.taojin.admin.entity.sys.SysDictData;
import com.taojin.admin.framework.util.DictUtils;
import com.taojin.admin.repository.sys.SysDictDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 字典 业务层处理
 * 
 * @author sujg
 */
@Service
public class SysDictDataService extends BaseService<SysDictData,Long> {
    @Autowired
    private SysDictDataRepository sysDictRepository;

    public SysDictDataService(SysDictDataRepository repository) {
        super(repository,repository);
    }

    /**
     * 根据条件分页查询字典数据
     *
     * @param dictData 字典数据信息
     * @return 字典数据集合信息
     */
    public Page<SysDictData> selectDictDataList(SysDictData dictData, Pageable pageable)
    {
        return sysDictRepository.findAll(filterByFields(dictData),pageable);
    }

    /**
     * 根据字典类型和字典键值查询字典数据信息
     *
     * @param dictType 字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    public String selectDictLabel(String dictType, String dictValue)
    {
        return sysDictRepository.findAllByDictCodeAndDictType(dictType, dictValue).getDictLabel();
    }
    public  List<SysDictData> selectDictDataByType(String dicttype){
        return sysDictRepository.findAllByDictTypeAndStatusOrderByDictSort(dicttype,"0");
    }
    /**
     * 根据字典数据ID查询信息
     *
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    public SysDictData selectDictDataById(Long dictCode)
    {
        return sysDictRepository.findSysDictDataByDictCode(dictCode);
    }

    /**
     * 批量删除字典数据信息
     *
     * @param dictCodes 需要删除的字典数据ID
     */
    public void deleteDictDataByIds(Long[] dictCodes)
    {
        for (Long dictCode : dictCodes)
        {
            SysDictData data = selectDictDataById(dictCode);
            sysDictRepository.deleteById(dictCode);
            List<SysDictData> dictDatas = sysDictRepository.findAllByDictTypeAndStatusOrderByDictSort(data.getDictType(),"0");
            DictUtils.setDictCache(data.getDictType(), dictDatas);
        }
    }

    /**
     * 新增保存字典数据信息
     *
     * @param data 字典数据信息
     * @return 结果
     */
    public void insertDictData(SysDictData data)
    {
         sysDictRepository.save(data);
            List<SysDictData> dictDatas = sysDictRepository.findAllByDictTypeAndStatusOrderByDictSort(data.getDictType(),"0");
            DictUtils.setDictCache(data.getDictType(), dictDatas);

    }

    /**
     * 修改保存字典数据信息
     *
     * @param data 字典数据信息
     * @return 结果
     */
    public void updateDictData(SysDictData data)
    {

            sysDictRepository.save(data);
            List<SysDictData> dictDatas = sysDictRepository.findAllByDictTypeAndStatusOrderByDictSort(data.getDictType(),"0");
            DictUtils.setDictCache(data.getDictType(), dictDatas);
    }
}
