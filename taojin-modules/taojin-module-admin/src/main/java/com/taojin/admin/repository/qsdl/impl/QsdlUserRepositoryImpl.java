//package com.taojin.admin.repository.qsdl.impl;
//
//import com.querydsl.core.BooleanBuilder;
//import com.querydsl.core.QueryResults;
//import com.querydsl.core.types.OrderSpecifier;
//import com.querydsl.core.types.Projections;
//import com.querydsl.jpa.impl.JPAQuery;
//import com.querydsl.jpa.impl.JPAQueryFactory;
//import com.taojin.admin.entity.sys.*;
//import com.taojin.admin.repository.qsdl.QsdlUserRepository;
//import com.taojin.admin.repository.sys.SysUserRepository;
//import com.taojin.admin.dto.sys.SysRoleDTO;
//import com.taojin.admin.dto.sys.SysUserDTO;
//import com.taojin.common.helper.StrHelper;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.PageRequest;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
///***
// * 也不方便，暂时不启用
// */
//@Repository
//@Slf4j
//public class QsdlUserRepositoryImpl  implements QsdlUserRepository {
//
//
//    @Autowired
//    private  JPAQueryFactory queryFactory;
//  @Autowired
//  private SysUserRepository sysUserRepository;
//
//
//
//
//    /**
//     * 根据用户获取该用户的部门、权限相关信息
//     * @param sysUser
//     * @param sortProperty
//     * @return
//     */
//    public List<SysUserDTO> getSysUserVoBySyUser(SysUser sysUser, String sortProperty){
//
//
//        log.info(System.currentTimeMillis()+"=====222=====");
//       // PageRequest pa = PageRequest.of(0, 150000);
//     //   List<SysUser> ss =  sysUserRepository.selectUserByUserName("",  pa);
//     //  log.info(System.currentTimeMillis()+"------"+ss.size()+"-----"+ss.get(0));
//      //  List<SysUserDTO> ss = sysUserRepository.selectUserByUserName();
//        //log.info(System.currentTimeMillis()+"-----------"+ss.get(0));
//
//        QSysUser qSysUser = QSysUser.sysUser;
//        QSysDept qSysDept = QSysDept.sysDept;
//        QSysUserRole qSysUserRole = QSysUserRole.sysUserRole;
//        QSysRole qSysRole = QSysRole.sysRole;
//        BooleanBuilder booleanBuilder = new BooleanBuilder();
//        booleanBuilder.and(qSysRole.delFlag.eq("0"));
//        if(!StrHelper.isEmpty(sysUser.getUserName())){
//            booleanBuilder.and(qSysUser.userName.eq(sysUser.getUserName()));
//        }
//        if(!StrHelper.isEmpty(sysUser.getUserId())){
//            booleanBuilder.and(qSysUser.userId.eq(sysUser.getUserId()));
//        }
//        List<SysUserDTO> results =  queryFactory.select(
//                        Projections.bean( SysUserDTO.class,qSysUser.userName,qSysDept.deptName,qSysUserRole,qSysRole)
//                )
//                .from(qSysUser)
//                .leftJoin(qSysDept).on(qSysDept.deptId.longValue().eq(qSysUser.deptId.longValue()))
//                .leftJoin(qSysUserRole).on(qSysUserRole.userId.longValue().eq(qSysUser.userId.longValue()))
//                .leftJoin(qSysRole).on(qSysRole.roleId.longValue().eq(qSysUserRole.roleId.longValue()))
//                .where( booleanBuilder)
//                .orderBy(qSysUser.userId.asc())
//                .offset(1)
//                .limit(20)
//                        .fetch();
//        log.info(System.currentTimeMillis()+"=========="+results.stream().count());
//        return results;
//    }
//
//    public List<SysRoleDTO> selectRoleList(SysRole sysRole, int page, int size, String sortProperty) {
////        QSysUser qSysUser = QSysUser.sysUser;
////        QSysDept qSysDept = QSysDept.sysDept;
////        QSysUserRole qSysUserRole = QSysUserRole.sysUserRole;
////        QSysRole qSysRole = QSysRole.sysRole;
////
////        BooleanBuilder booleanBuilder = new BooleanBuilder();
////        booleanBuilder.and(qSysRole.delFlag.eq("0"));
////        if(!MF.isEmpty(sysRole.getRoleId())){
////            booleanBuilder.and(qSysRole.roleId.eq(sysRole.getRoleId()));
////        }
////        if (!MF.isEmpty(sysRole.getRoleName())) {
////            booleanBuilder.and(qSysRole.roleName.like(sysRole.getRoleName()));
////        }
////        if(!MF.isEmpty(sysRole.getStatus())){
////            booleanBuilder.and(qSysRole.status.eq(sysRole.getStatus()));
////        }
////        if(!MF.isEmpty(sysRole.getRoleKey())){
////            booleanBuilder.and(qSysRole.roleKey.like(sysRole.getRoleKey()));
////        }
////        if(!MF.isEmpty(sysRole.getParams().get("beginTime"))){
////            booleanBuilder.and(qSysRole.createTime.gt(DateUtils.str2Timestamp(sysRole.getParams().get("beginTime"))));
////        }
////        if(!MF.isEmpty(sysRole.getParams().get("endTime"))){
////            booleanBuilder.and(qSysRole.createTime.lt(DateUtils.str2Timestamp(sysRole.getParams().get("endTime"))));
////        }
////
////        List<SysRoleDTO> results =  queryFactory.select(
////                        Projections.bean( SysRoleDTO.class,qSysUser,qSysDept,qSysUserRole,qSysRole)
////                )
////                .from(qSysRole)
////                .leftJoin(qSysUserRole).on(qSysUserRole.roleId.longValue().eq(qSysRole.roleId.longValue()))
////                .leftJoin(qSysUser).on(qSysUser.userId.longValue().eq(qSysUserRole.userId.longValue()))
////                .leftJoin(qSysDept).on(qSysDept.deptId.longValue().eq(qSysUser.deptId.longValue()))
////                .where( booleanBuilder)
////                .orderBy(sortProperty != null ? getSortExpression(qSysRole, sortProperty) : qSysRole.roleId.asc())
////                .offset(page * size)
////                .limit(size)
////                .fetch();
//
//        return null;
//    }
//
//    private OrderSpecifier<?> getSortExpression(QSysRole qSysRole, String sortProperty) {
//        switch (sortProperty) {
//            case "username":
//                return qSysRole.roleName.asc();
//            default:
//                return qSysRole.roleSort.asc();
//        }
//
//    }
//}
//
