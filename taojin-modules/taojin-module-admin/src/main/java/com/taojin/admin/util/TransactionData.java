package com.taojin.admin.util;
import cn.hutool.core.io.FileUtil;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TransactionData {

    private int code;

    @JsonProperty("response")
    private List<Transaction> transactions;

    public int getCode() {
        return code;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public static class Transaction {

        @JsonProperty("serialId")
        private String serialId;

        @JsonProperty("bankAcc")
        private String bankAcc;

        @JsonProperty("bankName")
        private String bankName;

        @JsonProperty("accName")
        private String accName;

        @JsonProperty("oppAccNo")
        private String oppAccNo;

        @JsonProperty("oppAccName")
        private String oppAccName;

        @JsonProperty("oppAccBank")
        private String oppAccBank;

        @JsonProperty("cdSign")
        private String cdSign;

        @JsonProperty("amt")
        private String amt;

        @JsonProperty("bal")
        private String bal;

        @JsonProperty("voucherNo")
        private String voucherNo;

        @JsonProperty("transDate")
        private String transDate;

        @JsonProperty("cur")
        private String cur;

        @JsonProperty("uses")
        private String uses;

        @JsonProperty("abs")
        private String abs;

        @JsonProperty("transTime")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
        private Date transTime;

        // Getters for all fields

        public String getSerialId() {
            return serialId;
        }

        public String getBankAcc() {
            return bankAcc;
        }

        public String getBankName() {
            return bankName;
        }

        public String getAccName() {
            return accName;
        }

        public String getOppAccNo() {
            return oppAccNo;
        }

        public String getOppAccName() {
            return oppAccName;
        }

        public String getOppAccBank() {
            return oppAccBank;
        }

        public String getCdSign() {
            return cdSign;
        }

        public String getAmt() {
            return amt;
        }

        public String getBal() {
            return bal;
        }

        public String getVoucherNo() {
            return voucherNo;
        }

        public String getTransDate() {
            return transDate;
        }

        public String getCur() {
            return cur;
        }

        public String getUses() {
            return uses;
        }

        public String getAbs() {
            return abs;
        }

        public Date getTransTime() {
            return transTime;
        }
    }
    private static String readFileContent(String filePath) {
        StringBuilder sb = new StringBuilder();
        // 指定目录路径
        String directoryPath = "D:\\download\\ru";

        // 读取目录下所有文件的内容
        List<File> fileList = FileUtil.loopFiles(directoryPath);
        for (File file : fileList) {
            String fstr = (FileUtil.readString(file, "UTF-8"));

            // Parse JSON using Jackson
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                TransactionData transactionData = objectMapper.readValue(fstr, TransactionData.class);

                // Access parsed data
                System.out.println("Code: " + transactionData.getCode());
                List<Transaction> transactions = transactionData.getTransactions();
                if (transactions != null && !transactions.isEmpty()) {
                    for(int i=0;i<transactions.size();i++) {
                        Transaction firstTransaction = transactions.get(i);
                        System.out.println(firstTransaction.getTransDate().substring(0,10)+"==="+firstTransaction.getAmt()+"==="+firstTransaction.getBal()+"==="+firstTransaction.getOppAccName());
                        //  System.out.println("bal: " + firstTransaction.getBal());
                        // Access other fields as needed11	22
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }

    public static void main(String[] args) {
        // 指定文件路径

//        // 读取文件内容
        String jsonData = readFileContent("");

    }
}
