package com.taojin.admin.dto.sys;

import com.taojin.admin.entity.sys.SysDept;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-30
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysUserDTO{
	/** 用户ID */
	private Long userId;

	/** 部门ID */
	private Long deptId;

	/** 用户账号 */
	private String userName;

	/** 用户昵称 */
	private String nickName;

	/** 用户邮箱 */
	private String email;

	/** 手机号码 */
	private String phonenumber;

	/** 用户性别 */
	private String sex;

	/** 用户头像 */
	private String avatar;

	/** 密码 */
	private String password;

	/** 帐号状态（0正常 1停用） */
	private String status;

	/** 删除标志（0代表存在 2代表删除） */
	private String delFlag;

	/** 最后登录IP */
	private String loginIp;
	/** unionid */
	private String unionid;
	/** 最后登录时间 */



	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date loginDate;


	/** 部门对象 */

	private SysDept dept;

	/** 角色对象 */
	private List<SysRoleDTO> roles;

	/** 角色组 */
	private Long[] roleIds;

	/** 岗位组 */
	private Long[] postIds;

	/** 角色ID */
	private Long roleId;

	private String salt;

	private String tenantid;


	/** 父部门id*/
	private Long parentId;

	/** 祖级列表*/
	private java.lang.String ancestors;

	/** 部门名称*/
	private java.lang.String deptName;

	/** 显示顺序*/
	private java.lang.Integer orderNum;

	/** 负责人*/
	private java.lang.String leader;

	/** 联系电话*/
	private java.lang.String phone;

	/** 创建者*/
	private java.lang.String createBy;

	private String roleStatus;
	/** 创建时间*/


	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private java.util.Date createTime;

	/** 更新者*/
	private java.lang.String updateBy;

	/** 更新时间*/


	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private java.util.Date updateTime;


	/** 角色名称*/
	private java.lang.String roleName;

	/** 角色权限字符串*/
	private java.lang.String roleKey;

	/** 显示顺序*/
	private java.lang.Integer roleSort;

	/** 数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）*/
	private java.lang.String dataScope;

	/** 菜单树选择项是否关联显示*/
	private java.lang.Integer menuCheckStrictly;

	/** 部门树选择项是否关联显示*/
	private java.lang.Integer deptCheckStrictly;


	/** 备注*/
	private java.lang.String remark;
	private String deptStatus;
	private String userType;

	public SysUserDTO(Long userId) {
		this.userId = userId;
	}

	public SysUserDTO(Long userId, Long deptId, String userName, String nickName, String email, String avatar, String phonenumber, String password, String sex,
					  String status, String delFlag, String loginIp, Date loginDate, String createBy, Date createTime, String remark,
					  Long parentId, String ancestors, String deptName, Integer orderNum, String leader, String deptStatus, String userType) {
		this.userId = userId;
		this.deptId = deptId;
		this.userName = userName;
		this.nickName = nickName;
		this.email = email;
		this.avatar = avatar;
		this.phonenumber = phonenumber;
		this.password = password;
		this.sex = sex;
		this.status = status;
		this.delFlag = delFlag;
		this.loginIp = loginIp;
		this.loginDate = loginDate;
		this.createBy = createBy;
		this.createTime = createTime;
		this.remark = remark;
		this.parentId = parentId;
		this.ancestors = ancestors;
		this.deptName = deptName;
		this.orderNum = orderNum;
		this.leader = leader;
		this.deptStatus = deptStatus;
		this.userType = userType;
	}
	public SysUserDTO(Long userId, Long deptId, String userName, String nickName, String email, String avatar, String phonenumber, String password, String sex, String status, String delFlag, String loginIp, Date loginDate, String createBy, Date createTime, String remark, String unionid, Long parentId, String ancestors, String deptName, Integer orderNum, String leader, String deptStatus, Long roleId, String roleName, String roleKey, Integer roleSort, String dataScope, String roleStatus, String userType, String updateBy, Date updateTime) {
		this.userId = userId;
		this.deptId = deptId;
		this.userName = userName;
		this.nickName = nickName;
		this.email = email;
		this.avatar = avatar;
		this.phonenumber = phonenumber;
		this.password = password;
		this.sex = sex;
		this.status = status;
		this.delFlag = delFlag;
		this.loginIp = loginIp;
		this.loginDate = loginDate;
		this.createBy = createBy;
		this.createTime = createTime;
		this.remark = remark;
		this.unionid = unionid;
		this.parentId = parentId;
		this.ancestors = ancestors;
		this.deptName = deptName;
		this.orderNum = orderNum;
		this.leader = leader;
		this.deptStatus = deptStatus;
		this.roleId = roleId;
		this.roleName = roleName;
		this.roleKey = roleKey;
		this.roleSort = roleSort;
		this.dataScope = dataScope;
		this.roleStatus = roleStatus;
		this.userType = userType;
		this.updateBy = updateBy;
		this.updateTime = updateTime;
	}


	public SysUserDTO(Long userId, String userName, String nickName, String phonenumber, String status, Date createTime, String deptName) {
		this.userId = userId;
		this.userName = userName;
		this.nickName = nickName;
		this.phonenumber = phonenumber;
		this.status = status;
		this.createTime = createTime;
		this.deptName = deptName;
	}


	public SysUserDTO(Long userId, Long deptId, String userName, String nickName, String userType, String email, String phonenumber, String sex, String avatar, String password, String status, String delFlag, String loginIp, Date loginDate, String createBy, Date createTime, String updateBy, Date updateTime, String remark) {
		this.userId = userId;
		this.deptId = deptId;
		this.userName = userName;
		this.nickName = nickName;
		this.userType = userType;
		this.email = email;
		this.phonenumber = phonenumber;
		this.sex = sex;
		this.avatar = avatar;
		this.password = password;
		this.status = status;
		this.delFlag = delFlag;
		this.loginIp = loginIp;
		this.loginDate = loginDate;
		this.createBy = createBy;
		this.createTime = createTime;
		this.updateBy = updateBy;
		this.updateTime = updateTime;
		this.remark = remark;
	}

	public boolean isAdmin()
	{
		return isAdmin(this.userId);
	}

	public static boolean isAdmin(Long userId)
	{
		return userId != null && 1L == userId;
	}



}
