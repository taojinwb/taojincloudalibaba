package com.taojin.admin.service.sys;

import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;

import com.taojin.admin.framework.common.BaseService;
import com.taojin.admin.entity.sys.SysConfig;
import com.taojin.admin.repository.sys.SysConfigRepository;
import com.taojin.admin.framework.constant.CacheConstant;
import com.taojin.framework.constant.UserConstants;
import com.taojin.framework.exception.ServiceException;
import com.taojin.framework.redis.RedisUtil;
import com.taojin.framework.utils.ConvertUtil;
import com.taojin.framework.utils.DateUtils;
import com.taojin.framework.utils.MF;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 参数配置 服务层实现
 *
 * @author sujg
 */
@Service
public class SysConfigService extends BaseService<SysConfig,Long>
{
    @Autowired
    private SysConfigRepository configRepository;

    @Autowired
    private RedisUtil redisUtil;

    public SysConfigService(SysConfigRepository repository) {
        super(repository,repository);
    }

    /**
     * 项目启动时，初始化参数到缓存
     */
    @PostConstruct
    public void init()
    {
        loadingConfigCache();
    }

    /**
     * 查询参数配置信息
     *
     * @param configId 参数配置ID
     * @return 参数配置信息
     */
    public SysConfig selectConfigById(Long configId)
    {
        return configRepository.findById(configId).orElse(null);
    }

    /**
     * 根据键名查询参数配置信息
     *
     * @param configKey 参数key
     * @return 参数键值
     */
    public String selectConfigByKey(String configKey)
    {
        String configValue = ConvertUtil.toStr(redisUtil.get(getCacheKey(configKey)));
        if (MF.isNotEmpty(configValue))
        {
            return configValue;
        }
        SysConfig retConfig = this.configRepository.findSysConfigByPzkeyAndSfky(configKey,"1");
        if (MF.isNotNull(retConfig))
        {
            redisUtil.set(getCacheKey(configKey), retConfig.getPzvalue());
            return retConfig.getPzvalue();
        }
        return StringUtils.EMPTY;
    }

    /**
     * 获取验证码开关
     *
     * @return true开启，false关闭
     */
    public boolean selectCaptchaEnabled()
    {
        String captchaEnabled = selectConfigByKey("sys.account.captchaEnabled");
        if (StringUtils.isEmpty(captchaEnabled))
        {
            return true;
        }
        return ConvertUtil.toBool(captchaEnabled);
    }

    /**
     * 查询参数配置列表
     *
     * @param config 参数配置信息
     * @return 参数配置集合
     */
    public List<SysConfig> selectConfigList(SysConfig config)
    {
        if(config.getParams().get("beginTime")!=null&&config.getParams().get("endTime")!=null){
            HashMap<String, Object> hashMap = new HashMap<>();
            String beginTime = DateUtils.dateformat(config.getParams().get("beginTime") + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
            String endTime = DateUtils.dateformat(config.getParams().get("endTime") + " 23:59:59", "yyyy-MM-dd HH:mm:ss");
            hashMap.put("beginTime",beginTime);
            hashMap.put("endTime",endTime);
            config.setParams(hashMap);
        }
        return this.configRepository.selectConfigList(config);
    }

    /**
     * 新增参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    public void insertConfig(SysConfig config)
    {
          this.configRepository.save(config);
         redisUtil.set(getCacheKey(config.getPzkey()), config.getPzvalue());
    }

    /**
     * 修改参数配置
     *
     * @param config 参数配置信息
     * @return 结果
     */
    public void updateConfig(SysConfig config)
    {
        SysConfig temp = this.configRepository.findById(config.getId().longValue()).orElse(null);
        if (!StringUtils.equals(temp.getPzkey(), config.getPzkey()))
        {
            redisUtil.del(getCacheKey(temp.getPzkey()));
        }

        this.configRepository.save(config);
        redisUtil.set(getCacheKey(config.getPzkey()), config.getPzvalue());
    }

    /**
     * 批量删除参数信息
     *
     * @param configIds 需要删除的参数ID
     */
    public void deleteConfigByIds(Long[] configIds)
    {
        for (Long configId : configIds)
        {
            SysConfig config = selectConfigById(configId);
            if (StringUtils.equals(UserConstants.YES, config.getPztype()))
            {
                throw new ServiceException(String.format("内置参数【%1$s】不能删除 ", config.getPzkey()));
            }
            this.configRepository.deleteById(configId);
            redisUtil.del(getCacheKey(config.getPzkey()));
        }
    }

    /**
     * 加载参数缓存数据
     */
    public void loadingConfigCache()
    {
        List<SysConfig> configsList = this.configRepository.findAll();
        for (SysConfig config : configsList)
        {
            redisUtil.set(getCacheKey(config.getPzkey()), config.getPzvalue());
        }
    }

    /**
     * 清空参数缓存数据
     */
    public void clearConfigCache()
    {
       redisUtil.deleteKeysWithPrefix(CacheConstant.SYS_CONFIG_KEY );
    }

    /**
     * 重置参数缓存数据
     */
    public void resetConfigCache()
    {
        clearConfigCache();
        loadingConfigCache();
    }

    /**
     * 校验参数键名是否唯一
     *
     * @param config 参数配置信息
     * @return 结果
     */
    public boolean checkConfigKeyUnique(SysConfig config)
    {
        Long configId = MF.isNull(config.getId()) ? -1L : config.getId();
        SysConfig info = this.configRepository.findSysConfigByPzkeyAndSfky(config.getPzkey(),"1");
        if (MF.isNotNull(info) && info.getId().longValue() != configId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 设置cache key
     *
     * @param configKey 参数键
     * @return 缓存键key
     */
    private String getCacheKey(String configKey)
    {
        return CacheConstant.SYS_CONFIG_KEY + configKey;
    }
}
