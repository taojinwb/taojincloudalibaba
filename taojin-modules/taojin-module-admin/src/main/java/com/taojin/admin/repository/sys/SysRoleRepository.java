package com.taojin.admin.repository.sys;

import com.taojin.admin.entity.sys.SysRole;
import com.taojin.admin.dto.sys.SysRoleDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SysRoleRepository extends  JpaRepository<SysRole, Long>, JpaSpecificationExecutor<SysRole> {
@Query(value="  select distinct r.* \n" +
        "        from sys_role r\n" +
        "        left join sys_user_role ur on ur.role_id = r.role_id\n" +
        "        left join sys_user u on u.user_id = ur.user_id\n" +
        "        left join sys_dept d on u.dept_id = d.dept_id" +
        " where r.role_id = :roleId",nativeQuery = true)
SysRole selectRoleById(@Param("roleId") Long roleId);



        @Query(value="  select distinct new com.taojin.admin.dto.sys.SysRoleDTO(r.roleName, r.roleKey, r.roleId, r.roleSort, r.dataScope, r.menuCheckStrictly, r.deptCheckStrictly, r.status, r.delFlag, r.createBy,r.createTime, r.updateBy, r.updateTime, r.remark )" +
        "        from sys_role r " +
        "        left join sys_user_role ur on ur.roleId = r.roleId" +
        "        left join sys_user u on u.userId = ur.userId" +
        "        left join sys_dept d on u.deptId = d.deptId" +
        " where r.delFlag = '0' " +
        " AND (:#{#sysRole.roleId} IS NULL OR  r.roleId = :#{#sysRole.roleId}) " +
        " AND (:#{#sysRole.roleName} IS NULL OR  r.roleName like concat('%',:#{#sysRole.roleName}, '%')) " +
        " AND (:#{#sysRole.status} IS NULL OR  r.status = :#{#sysRole.status}) " +
        " AND (:#{#sysRole.roleKey} IS NULL OR  r.roleKey like concat('%', :#{#sysRole.roleKey}, '%')) " +
       " AND (:#{#sysRole.params['beginTime']} IS NULL OR   date_format(r.createTime,'%Y-%m-%d %H:%m:%s') >= date_format(:#{#sysRole.params['beginTime'] +' 00:00:00'},'%Y-%m-%d %H:%m:%s')) " +
       " AND (:#{#sysRole.params['endTime']} IS NULL OR     date_format(r.createTime,'%Y-%m-%d %H:%m:%s') <= date_format(:#{#sysRole.params['endTime'] +' 23:59:59'},'%Y-%m-%d %H:%m:%s') )" +
        "  order by r.roleSort ")
    Page<SysRoleDTO> selectRoleList(@Param("sysRole") SysRoleDTO sysRole, Pageable pageable);
    @Query(value="select  new com.taojin.admin.dto.sys.SysRoleDTO( r.roleName, r.roleKey, r.roleId, r.roleSort, r.dataScope, r.menuCheckStrictly," +
            " r.deptCheckStrictly, r.status, r.delFlag, r.createBy,r.createTime, r.updateBy, r.updateTime, r.remark )" +
            "        from sys_role r\n" +
            "        left join sys_user_role ur on ur.roleId = r.roleId" +
            "        left join sys_user u on u.userId = ur.userId" +
            "       left join sys_dept d on u.deptId = d.deptId" +
            "  WHERE r.delFlag = '0' and r.status='0' and ur.userId = :userId")
    List<SysRoleDTO>  selectRolePermissionByUserId(@Param("userId") Long userId);

    @Query(value="select r.role_id        from sys_role r" +
            "       left join sys_user_role ur on ur.role_id = r.role_id      " +
            " left join sys_user u on u.user_id = ur.user_id   where u.user_id = :userId",nativeQuery = true)
    List<Long>selectRoleListByUserId(@Param("userId") Long userId);

    @Query(value="  select  distinct r.* \n" +
            "        from sys_role r\n" +
            "        left join sys_user_role ur on ur.role_id = r.role_id\n" +
            "        left join sys_user u on u.user_id = ur.user_id\n" +
            "        left join sys_dept d on u.dept_id = d.dept_id" +
            " where r.role_name=:roleName and r.del_flag = '0' limit 1",nativeQuery = true)
    SysRole checkRoleNameUnique(@Param("roleName") String roleName);

    @Query(value="  select distinct r.*\n" +
            "        from sys_role r\n" +
            "        left join sys_user_role ur on ur.role_id = r.role_id\n" +
            "        left join sys_user u on u.user_id = ur.user_id\n" +
            "        left join sys_dept d on u.dept_id = d.dept_id" +
            " where r.role_key=:roleKey and r.del_flag = '0' limit 1",nativeQuery = true)
    SysRole checkRoleKeyUnique(@Param("roleKey") String roleKey);

    @Query(value="update sys_role set del_flag = '2' where role_id = :roleId",nativeQuery = true)
    int deleteRoleById(@Param("roleId") Long roleId);

    int deleteSysRolesByRoleIdIn(Long[] roleIds);

    @Query(value=" select distinct r.* \n" +
            "        from sys_role r\n" +
            "\t        left join sys_user_role ur on ur.role_id = r.role_id\n" +
            "\t        left join sys_user u on u.user_id = ur.user_id\n" +
            "\t        left join sys_dept d on u.dept_id = d.dept_id " +
            " where r.del_flag = '0' and u.user_name = :userName ",nativeQuery = true)
   List<SysRole>selectRolesByUserName(@Param("userName") String userName);


}
