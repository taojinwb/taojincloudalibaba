package com.taojin.admin.controller.monitor;

import com.taojin.framework.server.Server;
import com.taojin.framework.vo.ApiResult;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务器监控
 *
 * @author sujg
 */
@RestController
@RequestMapping("/monitor/server")
public class ServerController
{
    @PreAuthorize("@ss.hasPermi('monitor:server:list')")
    @GetMapping()
    public ApiResult getInfo() throws Exception
    {
        Server server = new Server();
        server.copyTo();
        return ApiResult.ok(server);
    }
}
