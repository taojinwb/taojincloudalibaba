package com.taojin.admin.dto.sys;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.validation.constraints.NotNull;

/**
 * 登录表单
 *
 * @Author scott
 * @since  2019-01-18
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SysLoginDTO {
    /**
     * 用户名
     */
    @NotNull(message ="用户名不能为空！")
    private String username;

    /**
     * 用户密码
     */
    @NotNull(message ="密码不能为空！")
    private String password;

    /**
     * 验证码
     */
    private String code;

    /**
     * 唯一标识
     */
    private String uuid;

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getUuid()
    {
        return uuid;
    }

    public void setUuid(String uuid)
    {
        this.uuid = uuid;
    }

}
