package com.taojin.admin.framework.common;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.taojin.admin.dto.LoginUser;
import com.taojin.admin.framework.security.SecurityUtils;
import com.taojin.admin.service.sys.TokenService;
import com.taojin.framework.exception.BaseException;
import com.taojin.framework.utils.BF;
import com.taojin.framework.utils.MF;
import com.taojin.framework.utils.ServletUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Service;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public abstract class BaseService<T, ID> {
    @PersistenceContext
    private EntityManager entityManager;

    protected final  JpaRepository<T, ID> jpaRepository;
    protected final  JpaSpecificationExecutor<T> jpaSpecificationExecutor;
    public BaseService(JpaRepository<T, ID> repository,JpaSpecificationExecutor<T> specificationExecutor) {
        this.jpaRepository = repository;
        this.jpaSpecificationExecutor = specificationExecutor;
    }

    /**
     * 获取当前登录的用户名
     * @return
     */
    public String getUserName(){
        return  SecurityUtils.getUsername();
    }

    /**
     * 获取当前登录的用户详细信息
     * @return
     */
    public LoginUser getLoginUser()
    {
        return SecurityUtils.getLoginUser();
    }
    public static <T> Specification<T> filterByFields(T entity) {
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();
            // 获取实体类的Class对象
            Class<T> clazz = (Class<T>) entity.getClass();
            // 使用反射获取实体类的字段
            for (Field field : clazz.getDeclaredFields()) {
                try {
                    field.setAccessible(true);
                    Object value = field.get(entity);
                      if (!field.getName().equals("serialVersionUID")) {
                        // 如果字段值不为空，根据字段类型构建查询条件
                        if (MF.isNotEmpty(value)) {
                            if(StringUtils.contains(BF.getThisString(value),",")){//包含逗号的，用IN
                                predicates.add(root.get(field.getName()).in( value.toString().split(",")));
                            }else {
                                if (field.getType().equals(String.class)) {
                                    predicates.add(cb.like(root.get(field.getName()), "%" + value + "%"));
                                } else if (field.getType().equals(List.class)) {
                                    predicates.add(root.get(field.getName()).in((List<?>) value));
                                } else {
                                    predicates.add(cb.equal(root.get(field.getName()), value));
                                }
                            }
                        }
                    }
                } catch (IllegalAccessException e) {
                    // 处理异常
                    e.printStackTrace();
                }
            }

            // 动态组合查询条件
            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }

    /**
     * 插入新数据时，直接保存entity
     * @param entity
     * @return
     */
    public  T Insert(T entity) {
        return jpaRepository.save(entity);
    }
    /**
     * 更新新数据时，先查询该记录然后，更新不为空的值，在保存
     * @param entity
     * @return
     */
    public  T Update(T entity,ID id)  {
        T et =  jpaRepository.findById(id).orElse(null);
        //copy非空的值
        BeanUtil.copyProperties(entity,et,CopyOptions.create().setIgnoreNullValue(true).setIgnoreError(true));
        return jpaRepository.save(et);
    }


    /**
     * 批量插入数据
     * @param list
     * @return
     */
    public  List<T> InsertALl(List<T>  list) {
        Iterable<T> iterable = list;
        return  jpaRepository.saveAll(iterable);
    }



    /**
     *  查询数据库，返回一条记录，没有记录返回NULL
     * @param entity
     * @return
     */
    public T GetOneByFilter(T entity){
       return  jpaSpecificationExecutor.findOne(filterByFields(entity)).orElse(null);
    }

    /**
     *  查询数据库，返回一条记录，没有记录返回NULL
     * @param entity
     * @return
     */
    public T GetOneById(ID id){
        return  jpaRepository.findById(id).orElse(null);
    }


    /**
     * 查询所有记录，不分页，不排序
     * @param entity 查询条件，自动过滤值为空的字段
     * @return
     */
    public List<T>  GetAllByFilter(T entity){
        return  jpaSpecificationExecutor.findAll(filterByFields(entity));
    }


    /**
     * 查询所有记录，
     * @param entity 查询条件，自动过滤值为空的字段
     * @param pageable 分页 （如果有排序，构建在分页里面）
     * @return
     */
    public Page<T> GetAllByFilter(T entity, Pageable pageable){
        return  jpaSpecificationExecutor.findAll(filterByFields(entity),pageable);
    }

    /**
     * 查询所有记录，有排序，不分页
     * @param entity 查询条件，自动过滤值为空的字段
     * @param sort 构建排序字段
     * @return
     */
    public List<T>  GetAllByFilter(T entity,  Sort sort){
        return  jpaSpecificationExecutor.findAll(filterByFields(entity),sort);
    }

    /**
     * 返回记录数
     * @param entity 查询条件，自动过滤值为空的字段
     * @return
     */
    public long GetCountByFilter(T entity){
      return   jpaSpecificationExecutor.count(filterByFields(entity));
    }

    /**
     *
     * @param id
     */
    public void RemoveById(ID id){
        jpaRepository.deleteById(id);
    }

}
