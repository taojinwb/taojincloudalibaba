package com.taojin.admin.elastic.dto;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "批量查询活动浏览量参数")
public class activitesAggrDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "活动Id集合，eg: [1,33,32,443]")
	private List<Long> activites;

	public List<Long> getActivites() {
		return activites;
	}

	public void setActivites(List<Long> activites) {
		this.activites = activites;
	}
	
}
