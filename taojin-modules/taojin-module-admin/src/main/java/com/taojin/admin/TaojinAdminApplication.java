package com.taojin.admin;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableFeignClients(basePackages = "com.taojin.api")
@ComponentScan("com.taojin")
@EnableJpaRepositories("com.taojin")
@EntityScan("com.taojin")
@Slf4j
public class TaojinAdminApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(TaojinAdminApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  管理系统后台  taojin-module-admin启动成   ლ(´ڡ`ლ)ﾞ  \n" );
        Environment env = applicationContext.getEnvironment();
        printActiveProfiles(env);



    }
    private static void printActiveProfiles(Environment env) {
        String[] activeProfiles = env.getActiveProfiles();
        log.info("\n****************************************************************\n"+
                "*                                                              *\n"+
                "**********************启动了配置模式:【" + String.join(", ", activeProfiles)+"】成功********************\n"+
                "*                                                              *\n"+
                "****************************************************************");
    }

}
