package com.taojin.admin.elastic.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.taojin.framework.vo.ApiResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.taojin.admin.elastic.dto.activitesAggrDTO;
import com.taojin.admin.elastic.entity.ActivitiesCommentLike;
import com.taojin.admin.elastic.service.ActivitiesElasticService;
import com.taojin.admin.elastic.service.ClogProService;
import com.taojin.admin.elastic.vo.activitesShowVo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author boren
 *
 */

@RestController
@Api(tags = "API - BigController",value = "数据统计接口")
@RequestMapping(value = "/big")
public class BigController {

	@Autowired
	private ClogProService clogPro;
	
	
	@Autowired
	private ActivitiesElasticService actElasticService;
	
	 @ApiOperation(value = "活动浏览量查询", notes = "查询是精确查询,其结果只有一条数据", response = activitesShowVo.class)
	    @ApiImplicitParams({
	            @ApiImplicitParam(name = "activitesId", value = "活动ID", required = true, defaultValue = "0")
	    })
	    @GetMapping(value = "/activites/show/{activitesId}")
	    public ApiResult getActivitesShowById(@PathVariable long activitesId, HttpServletRequest request) {


	        activitesShowVo show =  clogPro.getActivites(activitesId);


	        return ApiResult.ok(show);

	    }
	 
	 
//	 @ApiOperation(value = "批量查询活动浏览量", notes = "", response = activitesShowVo.class)
//	 @ApiImplicitParams({})
//	 @RequestMapping(value = "/activites/shows", method = RequestMethod.POST)
//	 public ResultBean getActivitesShows(@RequestBody @Validated activitesAggrDTO Params, HttpServletRequest request)
//	 {
//
//		 List<activitesShowVo> shows = clogPro.getActivitesForIds(Params.getActivites());
//
//		 ResultBean resultBean = ResultBean.getInstance();
//
//		 resultBean.setResponse(shows);
//
//		 return resultBean;
//	 }
//
//
//	 @ApiOperation(value = "获取某个活动我点赞的数据", notes = "", response = ActivitiesCommentLike.class)
//	    @ApiImplicitParams({
//	            @ApiImplicitParam(name = "activitesId", value = "活动ID", required = true, defaultValue = "0")
//	    })
//	    @RequestMapping(value = "/activites/mylikes/{activitesId}", method = RequestMethod.GET)
//	    public ResultBean getActiviteslikeById(@PathVariable long activitesId, HttpServletRequest request) {
//
//		  	String token = request.getHeader("token");
//
//	        ResultBean result = ResultBean.getInstance();
//
//	        List<ActivitiesCommentLike>  likes = actElasticService.getMyLikesForActivitiesId(activitesId, token);
//
//	        result.setResponse(likes);
//
//	        return result;
//
//	    }
//
	 @ApiOperation(value =  "评论点赞", notes = "", response = ActivitiesCommentLike.class)
	 @ApiImplicitParams({})
	 @RequestMapping(value = "/activites/comment/add/like", method = RequestMethod.POST)
	 public ApiResult addLike(@RequestBody @Validated ActivitiesCommentLike Params, HttpServletRequest request)
	 {

		 ActivitiesCommentLike like = actElasticService.addLike(Params);



		 return ApiResult.ok(like);
	 }
	
}
