package com.taojin.admin.elastic.dto;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
/**
 * 新增点赞
 * @author boren
 *
 */
public class CommentLikeDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 文档ID
	 */
	private String _id;
	
	/**
	 * 活动ID
	 */
	@ApiModelProperty(value = "活动ID", name = "activities_id", required = true)
	private Long activities_id;
	
	
	/**
	 * 评论ID 
	 */
	@ApiModelProperty(value = "评论ID", name = "commont_id", required = true)
	private Long commont_id;
	
	/**
	 * 评论回复ID
	 */
	@ApiModelProperty(value = "评论回复ID", name = "replyId", required = true)
	private Long reply_id;
	
	/**
	 * 该评论的类型(1: 主评论   2:回复评论)
	 */
	@ApiModelProperty(value = "该评论的类型(1: 主评论   2:回复评论)", name = "log_type", required = true)
	private Integer log_type;
	
	/**
	 * 评论者ID
	 */
	@ApiModelProperty(value = "评论者ID", name = "user_id", required = true)
	private Long  user_id;
	
	/**
	 * 点赞者ID
	 */

	private Long liker_user_id;

	public Long getActivities_id() {
		return activities_id;
	}

	public void setActivities_id(Long activities_id) {
		this.activities_id = activities_id;
	}

	public Long getReply_id() {
		return reply_id;
	}

	public void setReply_id(Long reply_id) {
		this.reply_id = reply_id;
	}

	public Integer getLog_type() {
		return log_type;
	}

	public void setLog_type(Integer log_type) {
		this.log_type = log_type;
	}

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

	public Long getLiker_user_id() {
		return liker_user_id;
	}

	public void setLiker_user_id(Long liker_user_id) {
		this.liker_user_id = liker_user_id;
	}

	public Long getCommont_id() {
		return commont_id;
	}

	public void setCommont_id(Long commont_id) {
		this.commont_id = commont_id;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	@Override
	public String toString() {
		return "CommentLikeDTO [activities_id=" + activities_id + ", commont_id=" + commont_id + ", reply_id="
				+ reply_id + ", log_type=" + log_type + ", user_id=" + user_id + ", liker_user_id=" + liker_user_id
				+ "]";
	}

	
}
