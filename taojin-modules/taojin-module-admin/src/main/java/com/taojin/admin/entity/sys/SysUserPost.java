package com.taojin.admin.entity.sys;

import javax.persistence.*;
import java.sql.Timestamp;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import com.taojin.admin.entity.BaseEntity;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-26
*/

@Data
@Entity(name = "sys_user_post")

@IdClass(SysUserPostPK.class)
public class SysUserPost extends BaseEntity {

/** 用户ID*/
		@Id
		@Column(name = "user_id")
		private Long userId;

/** 岗位ID*/
		@Id
		@Column(name = "post_id")
		private Long postId;

}
