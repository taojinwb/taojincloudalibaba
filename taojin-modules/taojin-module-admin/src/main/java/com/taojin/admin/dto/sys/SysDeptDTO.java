package com.taojin.admin.dto.sys;

import com.taojin.admin.entity.sys.SysDept;
import lombok.NoArgsConstructor;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-30
*/
@Data
@NoArgsConstructor
public class SysDeptDTO {
		private Long deptId;

		@JsonSerialize(using = ToStringSerializer.class)
		@Excel(name ="父部门id")
		private Long parentId;

		@Excel(name ="祖级列表")
		private java.lang.String ancestors;

		@Excel(name ="部门名称")
		private java.lang.String deptName;

		@Excel(name ="显示顺序")
		private java.lang.Integer orderNum;

		@Excel(name ="负责人")
		private java.lang.String leader;

		@Excel(name ="联系电话")
		private java.lang.String phone;

		@Excel(name ="邮箱")
		private java.lang.String email;

		@Excel(name ="部门状态（0正常 1停用）")
		private java.lang.String status;

		@Excel(name ="删除标志（0代表存在 2代表删除）")
		private java.lang.String delFlag;

		@Excel(name ="创建者")
		private java.lang.String createBy;

		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
		@Excel(name ="创建时间")
		private java.util.Date createTime;

		@Excel(name ="更新者")
		private java.lang.String updateBy;

		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
		@Excel(name ="更新时间")
		private java.util.Date updateTime;
		@Excel(name ="上级部门名称")
		private java.lang.String parentName;
		/** 子部门 */
		private List<SysDeptDTO> children = new ArrayList<SysDeptDTO>();

	public SysDeptDTO(Long deptId, Long parentId, String ancestors, String deptName, Integer orderNum, String leader, String phone, String email, String status,String parentName) {
		this.deptId = deptId;
		this.parentId = parentId;
		this.ancestors = ancestors;
		this.deptName = deptName;
		this.orderNum = orderNum;
		this.leader = leader;
		this.phone = phone;
		this.email = email;
		this.status = status;
		this.parentName = parentName;
	}
}