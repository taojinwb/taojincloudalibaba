package com.taojin.admin.entity.sys;

import javax.persistence.*;
import java.sql.Timestamp;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import com.taojin.admin.entity.BaseEntity;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-26
*/
@Data
@Entity(name = "sys_role_dept")
@IdClass(SysRoleDeptPK.class)
public class SysRoleDept extends BaseEntity {

/** 角色ID*/
		@Id
		@Column(name = "role_id")
		private Long roleId;

/** 部门ID*/
		@Id
		@Column(name = "dept_id")
		private Long deptId;

}
