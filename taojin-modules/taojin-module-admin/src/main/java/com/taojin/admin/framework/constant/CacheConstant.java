package com.taojin.admin.framework.constant;


public interface CacheConstant {

    /**
     * CAS登录成功后的后台标识
     */
    public static final String CAS_TOKEN = "cas_token";

    /**
     * CAS登录成功后的前台Cookie的Key
     */
    public static final String WEB_TOKEN_KEY = "Admin-Token";

    /**
     * 前端传token，的head值
     * */
    public static final String HEAD_AUTHORIZATION ="Authorization";
    /**
     * 登录用户 redis key
     */
    public static final String LOGIN_TOKEN_KEY = "login_tokens:";

    /**
     * 登录用户unionid redis key
     */
    public static final String LOGIN_UNIONID_TOKEN_KEY = "login_union_tokens:";

    String TOKEN_IS_INVALID_MSG = "Token失效，请重新登录!";
    /**
     * 验证码 redis key
     */
    public static final String CAPTCHA_CODE_KEY = "captcha_codes:";

    /**
     * 参数管理 cache key
     */
    public static final String SYS_CONFIG_KEY = "sys_config:";
    /**
     * 限流 redis key
     */
    public static final String RATE_LIMIT_KEY = "rate_limit:";
    /**
     * 字典管理 cache key
     */
    public static final String SYS_DICT_KEY = "sys_dict:";

    /**
     * 防重提交 redis key
     */
    public static final String REPEAT_SUBMIT_KEY = "repeat_submit:";



    /**
     * 登录账户密码错误次数 redis key
     */
    public static final String PWD_ERR_CNT_KEY = "pwd_err_cnt:";
    /**
     * 网关路由相关
     */
    String GATEWAY_ROUTES = "sys:cache:cloud:gateway_routes";
    String ROUTE_JVM_RELOAD_TOPIC = "gateway_jvm_route_reload_topic";
    public static final String HANDLER_NAME = "handlerName";
    public static final String LODER_ROUDER_HANDLER = "loderRouderHandler";
    public static final String REDIS_TOPIC_NAME = "jeecg_redis_topic";

}
