package com.taojin.admin.modules.gamesinfo.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import javax.persistence.*;
import java.sql.Timestamp;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.taojin.admin.entity.BaseEntity;
import lombok.Data;
import com.taojin.admin.entity.BaseEntity;

/**
 * 直播间信息 对象 play_info
 *
 * @author sujg
 * @date Mon Dec 18 14:04:48 CST 2023
 */
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Data
@Entity(name = "play_info")
public class PlayInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;


    /** ID */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="id")
    private Long id;

    /** 直播间名称 */
    @Excel(name = "直播间名称")
    @Column(name ="title")
    private String title;

    /** 直播间类型 0:  个人直播间    10：商城直播间    20：赛事直播间 */
    @Excel(name = "直播间类型 0:  个人直播间    10：商城直播间    20：赛事直播间")
    @Column(name ="type")
    private Integer type;

    /** 用户id 对应c_user.id */
    @Excel(name = "用户id 对应c_user.id")
    @Column(name ="uid")
    private Long uid;

    /** 企业ID 对应 b_venue_authentication.id */
    @Excel(name = "企业ID 对应 b_venue_authentication.id")
    @Column(name ="buss_id")
    private Long bussId;

    /** 赛事ID 对应 games_info.id,  个人直播忽略该字段 */
    @Excel(name = "赛事ID 对应 games_info.id,  个人直播忽略该字段")
    @Column(name ="games_id")
    private Long gamesId;

    /** 状态 0: 初始默认值    10：审核中    20：正常     100：冻结 */
    @Excel(name = "状态 0: 初始默认值    10：审核中    20：正常     100：冻结")
    @Column(name ="status")
    private Integer status;

    /** 介绍说明 */
    @Excel(name = "介绍说明")
    @Column(name ="description")
    private String description;

    /** 直播背景图 */
    @Excel(name = "直播背景图")
    @Column(name ="bg_img")
    private String bgImg;

    /** 公众号图片地址 */
    @Excel(name = "公众号图片地址")
    @Column(name ="public_img")
    private String publicImg;

    /** 直播开始时间 */
    @Excel(name = "直播开始时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name ="play_start")
    private Date playStart;

    /** 直播结束时间 */
    @Excel(name = "直播结束时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name ="play_end")
    private Date playEnd;

    /** 直播房间号 对应目睹直播的房间号 */
    @Excel(name = "直播房间号 对应目睹直播的房间号")
    @Column(name ="play_number")
    private String playNumber;

    /** 直播房间的hash值 对应目睹直播的hash值 */
    @Excel(name = "直播房间的hash值 对应目睹直播的hash值")
    @Column(name ="play_hashkey")
    private String playHashkey;

    /** 推流地址 */
    @Excel(name = "推流地址")
    @Column(name ="push_addr")
    private String pushAddr;

    /** 审核失败原因 */
    @Excel(name = "审核失败原因")
    @Column(name ="reason")
    private String reason;


}