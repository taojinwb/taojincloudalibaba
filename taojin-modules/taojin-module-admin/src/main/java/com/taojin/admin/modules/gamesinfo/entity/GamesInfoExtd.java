package com.taojin.admin.modules.gamesinfo.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.taojin.admin.entity.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * 赛事信息扩展表
 *
 * @author ydd
 *
 */

@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Data
@Entity(name = "games_info_extd")
public class GamesInfoExtd{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(value = "ID")
    @Column(name = "id")
    private Long id ;
    /** 主表ID;对应 games_info.id */
    @ApiModelProperty(value = "主表ID;对应 games_info.id")
    private Long gamesId ;
    /** 审核失败原因 */
    @ApiModelProperty(value = "审核失败原因")
    private String failReason ;
    /** 人工审核状态;0 待审核 1 审核通过 */
    private Integer manualAuditStatus ;
    /** 报名是否需要审核;0: 不需要审核   10： 需要审核 */
    @ApiModelProperty(value = "报名是否需要审核;0: 不需要审核   10： 需要审核 ")
    private Integer regAudit ;
    /** 是否需要签到;0：不需要签到   10：需要签到 */
    @ApiModelProperty(value = "是否需要签到;0：不需要签到   10：需要签到")
    private Integer signed ;
    /** 联系人 */
    @ApiModelProperty(value = "联系人")
    private String contactName ;
    /** 联系人电话 */
    @ApiModelProperty(value = "联系人电话")
    private String contactPhone ;
    /** 比赛结果公示地址 */
    @ApiModelProperty(value = "比赛结果公示地址")
    private String resultUrl ;
    /** 承办单位;承办方，多个用英文逗号（,） 隔开 */
    @ApiModelProperty(value = "承办单位;承办方，多个用英文逗号（,） 隔开")
    private String executorOrg ;
    /** 协办单位 */
    @ApiModelProperty(value = "协办单位")
    private String assistOrg ;
    /** 合作媒体 */
    @ApiModelProperty(value = "合作媒体")
    private String cooperateMedia ;
    /** 冠名单位 */
    @ApiModelProperty(value = "冠名单位")
    private String namingOrg ;
    /** 医疗保障机构 */
    @ApiModelProperty(value = "医疗保障机构")
    private String healthcareOrg ;
    /** 赛事规程 */
    @ApiModelProperty(value = "赛事规程")
    private String detail ;
    /** 直播地址 */
    @ApiModelProperty(value = "直播地址")
    @Column(name = "play_url")
    private String playUrl ;
    /** 是否为微信号;手机号是否为微信号 0 否 1 是 */
    @ApiModelProperty(value = "是否为微信号;手机号是否为微信号 0 否 1 是")
    private Integer isWechat ;
    /** 名单公示字段集;对应报名信息字段表games_fields.ename , 多个用逗号分开(eg: name,phone) */
    private String listPublicityFields;
    /** 创建时间 */
    @CreatedDate
    @ApiModelProperty(value = "更新时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;
    /** 更新时间 */
    @LastModifiedDate
    @ApiModelProperty(value = "更新时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updateTime;



}
