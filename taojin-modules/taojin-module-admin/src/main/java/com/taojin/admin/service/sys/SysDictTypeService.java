package com.taojin.admin.service.sys;

import com.taojin.admin.framework.common.BaseService;
import com.taojin.admin.entity.sys.SysDictData;
import com.taojin.admin.entity.sys.SysDictType;
import com.taojin.admin.framework.util.DictUtils;
import com.taojin.admin.repository.sys.SysDictDataRepository;
import com.taojin.admin.repository.sys.SysDictTypeRepository;
import com.taojin.framework.constant.UserConstants;
import com.taojin.framework.exception.ServiceException;
import com.taojin.framework.utils.DateUtils;
import com.taojin.framework.utils.MF;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 字典 业务层处理
 * 
 * @author sujg
 */
@Service
@Slf4j
public class SysDictTypeService extends BaseService<SysDictType,Long> {

    @Autowired
    private SysDictTypeRepository sysDictTypeRepository;

    @Autowired
    private SysDictDataRepository sysDictDataRepository;

    public SysDictTypeService(SysDictTypeRepository repository) {
        super(repository,repository);
    }

    /**
     * 项目启动时，初始化字典到缓存
     */
    @PostConstruct
    public void init()
    {
        loadingDictCache();
    }

    /**
     * 根据条件分页查询字典类型
     *
     * @param dictType 字典类型信息
     * @return 字典类型集合信息
     */
    public List<SysDictType> selectDictTypeList(SysDictType dictType)
    {
        return sysDictTypeRepository.findAll(this.filterByFields(dictType));
    }
    /**
     * 根据条件分页查询字典数据
     *
     * @param dictData 字典数据信息
     * @return 字典数据集合信息
     */
    public Page<SysDictType> selectDictTypeList(SysDictType dictData, Pageable pageable)
    {
        if(dictData.getParams().get("beginTime")!=null&&dictData.getParams().get("endTime")!=null){
            HashMap<String, Object> hashMap = new HashMap<>();
            String beginTime = DateUtils.dateformat(dictData.getParams().get("beginTime") + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
            String endTime = DateUtils.dateformat(dictData.getParams().get("endTime") + " 23:59:59", "yyyy-MM-dd HH:mm:ss");
            hashMap.put("beginTime",beginTime);
            hashMap.put("endTime",endTime);
            dictData.setParams(hashMap);
        }
        return sysDictTypeRepository.selectDictTypeList(dictData,pageable);
    }
    /**
     * 根据所有字典类型
     *
     * @return 字典类型集合信息
     */
    public List<SysDictType> selectDictTypeAll()
    {
        return sysDictTypeRepository.findAll();
    }

    /**
     * 根据字典类型查询字典数据
     *
     * @param dictType 字典类型
     * @return 字典数据集合信息
     */
    public List<SysDictData> selectDictDataByType(String dictType)
    {
        List<SysDictData> dictDatas = DictUtils.getDictCache(dictType);
        if (MF.isNotEmpty(dictDatas))
        {
            return dictDatas;
        }
        dictDatas = sysDictDataRepository.findAllByDictType(dictType);
        if (MF.isNotEmpty(dictDatas))
        {
            DictUtils.setDictCache(dictType, dictDatas);
            return dictDatas;
        }
        return null;
    }

    /**
     * 根据字典类型ID查询信息
     *
     * @param dictId 字典类型ID
     * @return 字典类型
     */
    public SysDictType selectDictTypeById(Long dictId)
    {
        return sysDictTypeRepository.findById(dictId).orElse(null);
    }

    /**
     * 根据字典类型查询信息
     *
     * @param dictType 字典类型
     * @return 字典类型
     */
    public SysDictType selectDictTypeByType(String dictType)
    {
        return this.sysDictTypeRepository.findFirstByDictType(dictType);
    }

    /**
     * 批量删除字典类型信息
     *
     * @param dictIds 需要删除的字典ID
     */
    public void deleteDictTypeByIds(Long[] dictIds)
    {
        for (Long dictId : dictIds)
        {
            SysDictType dictType = selectDictTypeById(dictId);
            if (sysDictDataRepository.countByDictType(dictType.getDictType()) > 0)
            {
                throw new ServiceException(String.format("%1$s已分配,不能删除", dictType.getDictName()));
            }
            sysDictTypeRepository.deleteById(dictId);
            DictUtils.removeDictCache(dictType.getDictType());
        }
    }

    /**
     * 加载字典缓存数据
     */
    public void loadingDictCache()
    {
        SysDictData dictData = new SysDictData();
        dictData.setStatus("0");
        Map<String, List<SysDictData>> dictDataMap = sysDictDataRepository.findAll(this.filterByFields(dictData)).stream().collect(Collectors.groupingBy(SysDictData::getDictType));
        for (Map.Entry<String, List<SysDictData>> entry : dictDataMap.entrySet())
        {
            DictUtils.setDictCache(entry.getKey(), entry.getValue().stream().sorted(Comparator.comparing(SysDictData::getDictSort)).collect(Collectors.toList()));
        }

        log.info("加载数据字典成功...........");
    }

    /**
     * 清空字典缓存数据
     */
    public void clearDictCache()
    {
        DictUtils.clearDictCache();
    }

    /**
     * 重置字典缓存数据
     */
    public void resetDictCache()
    {
        clearDictCache();
        loadingDictCache();
    }

    /**
     * 新增保存字典类型信息
     *
     * @param dict 字典类型信息
     * @return 结果
     */
    public void insertDictType(SysDictType dict)
    {
        sysDictTypeRepository.save(dict);
         DictUtils.setDictCache(dict.getDictType(), null);
    }

    /**
     * 修改保存字典类型信息
     *
     * @param dict 字典类型信息
     * @return 结果
     */
    @Transactional
    public void updateDictType(SysDictType dict)
    {
        SysDictType oldDict = sysDictTypeRepository.findById(dict.getDictId()).orElse(null);
        sysDictDataRepository.updateDictDataType(oldDict.getDictType(), dict.getDictType());
        sysDictTypeRepository.save(dict);

        List<SysDictData> dictDatas = sysDictDataRepository.findAllByDictType(dict.getDictType());
        DictUtils.setDictCache(dict.getDictType(), dictDatas);

    }

    /**
     * 校验字典类型称是否唯一
     *
     * @param dict 字典类型
     * @return 结果
     */
    public boolean checkDictTypeUnique(SysDictType dict)
    {
        Long dictId = MF.isNull(dict.getDictId()) ? -1L : dict.getDictId();
        SysDictType dictType = sysDictTypeRepository.findFirstByDictType(dict.getDictType());
        if (MF.isNotNull(dictType) && dictType.getDictId().longValue() != dictId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }
}
