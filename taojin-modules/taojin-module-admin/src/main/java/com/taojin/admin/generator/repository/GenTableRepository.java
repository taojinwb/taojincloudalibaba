package com.taojin.admin.generator.repository;

import com.taojin.admin.entity.sys.SysConfig;
import com.taojin.admin.generator.dto.GenTableDTO;
import com.taojin.admin.generator.entity.GenTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface GenTableRepository  extends  JpaRepository<GenTable, Long>,JpaSpecificationExecutor<GenTable> {
    /**
     * 查询业务列表
     *
     * @param genTable 业务信息
     * @return 业务集合
     */
    @Query(value=" select * from gen_table " +

            " where 1=1 AND (:#{#genTable.tableName} IS NULL OR table_name like %:#{#genTable.tableName}%)" +
            " AND (:#{#genTable.tableComment} IS NULL OR table_comment like %:#{#genTable.tableComment}%)" +

            "",nativeQuery = true)
    public List<GenTable> selectGenTableList(GenTable genTable);

    /**
     * 查询据库列表
     *
     * @param genTable 业务信息
     * @return 数据库表集合
     */
    @Query(value="select table_name, table_comment, create_time, update_time from information_schema.tables\n" +
            "\t\twhere table_schema = (select database())\n" +
            "\t\tAND table_name NOT LIKE 'qrtz_%' AND table_name NOT LIKE 'gen_%'\n" +
            "\t\tAND table_name NOT IN (select table_name from gen_table) " +
            " AND (:#{#genTable.tableName} IS NULL OR table_name like %:#{#genTable.tableName}%)" +
            " AND (:#{#genTable.tableComment} IS NULL OR table_comment like %:#{#genTable.tableComment}%)" +

            " order by create_time desc" ,nativeQuery = true)
    public List<GenTableDTO> selectDbTableList(GenTable genTable);

    /**
     * 查询据库列表
     *
     * @param tableNames 表名称组
     * @return 数据库表集合
     */
    @Query(value="select table_name, table_comment, create_time, update_time from information_schema.tables\n" +
            "\t\twhere table_name NOT LIKE 'qrtz_%' and table_name NOT LIKE 'gen_%' and table_schema = (select database())\n" +
            "\t\tand table_name in (:tableNames)" ,nativeQuery = true)
    public List<GenTableDTO> selectDbTableListByNames(String tableNames);

    /**
     * 查询所有表信息
     *
     * @return 表信息集合
     */
    @Query(value="SELECT t.table_id, t.table_name, t.table_comment, t.sub_table_name, t.sub_table_fk_name, t.class_name, t.tpl_category, t.package_name, t.module_name, t.business_name, t.function_name, t.function_author, t.options, t.remark,\n" +
            "\t\t\t   c.column_id, c.column_name, c.column_comment, c.column_type, c.java_type, c.java_field, c.is_pk, c.is_increment, c.is_required, c.is_insert, c.is_edit, c.is_list, c.is_query, c.query_type, c.html_type, c.dict_type, c.sort\n" +
            "\t\tFROM gen_table t\n" +
            "\t\t\t LEFT JOIN gen_table_column c ON t.table_id = c.table_id\n" +
            "\t\torder by c.sort ",nativeQuery = true)
    public List<GenTableDTO> selectGenTableAll();

    /**
     * 查询表ID业务信息
     *
     * @param id 业务ID
     * @return 业务信息
     */
//    @Query(value = " SELECT t.table_id, t.table_name, t.table_comment, t.sub_table_name, t.sub_table_fk_name, t.class_name, t.tpl_category, t.package_name, t.module_name, t.business_name, t.function_name, t.function_author, t.gen_type, t.gen_path, t.options, t.remark,\n" +
//            "   c.column_id, c.column_name, c.column_comment, c.column_type, c.java_type, c.java_field, c.is_pk, c.is_increment, c.is_required, c.is_insert, c.is_edit, c.is_list, c.is_query, c.query_type, c.html_type, c.dict_type, c.sort\n" +
//            " FROM gen_table t\n" +
//            " LEFT JOIN gen_table_column c ON t.table_id = c.table_id\n" +
//            " where t.table_id = :tableId order by c.sort ",nativeQuery = true)
//    public GenTableDTO selectGenTableById(Long tableId);
//
    @Query(value = " SELECT * \n" +
            " FROM gen_table t\n" +
            " where t.table_id = :tableId  ",nativeQuery = true)
    public GenTableDTO selectGenTableById(@Param("tableId") Long tableId);


    /**
     * 查询表名称业务信息
     *
     * @param tableName 表名称
     * @return 业务信息
     */
    @Query(value=" SELECT t.table_id, t.table_name, t.table_comment, t.sub_table_name, t.sub_table_fk_name, t.class_name, t.tpl_category, t.package_name, t.module_name, t.business_name, t.function_name, t.function_author, t.gen_type, t.gen_path, t.options, t.remark,\n" +
            "   c.column_id, c.column_name, c.column_comment, c.column_type, c.java_type, c.java_field, c.is_pk, c.is_increment, c.is_required, c.is_insert, c.is_edit, c.is_list, c.is_query, c.query_type, c.html_type, c.dict_type, c.sort\n" +
            " FROM gen_table t\n" +
            " LEFT JOIN gen_table_column c ON t.table_id = c.table_id\n" +
            " where t.table_name = :tableName order by c.sort" ,nativeQuery = true)
    public List<GenTableDTO> selectGenListByName(@Param("tableName")String tableName);

    @Query(value=" SELECT * \n" +
            " FROM gen_table t\n" +
            " where t.table_name = :tableName " ,nativeQuery = true)
    public GenTableDTO selectGenTableByName(@Param("tableName")String tableName);




}
