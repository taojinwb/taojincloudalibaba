package com.taojin.admin.modules.orders.constant;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author: BBWZ
 * @date: 2021/3/24 10:09
 * @description:
 *          订单状态聚合枚举，将所有类型枚举，就是对多个订单状态聚合表示为一种状态
 *          如待支付 聚合订单状态 0: 订单创建  10: 待确认   20: 待支付
 */
public enum OrderStatusPolyEnum {
    /**
     * 枚举所有的订单状态聚合
     * 订单状态（0：订单创建  10: 待确认   20: 待支付    30: 待发货  35: 已发货   40: 待使用   50：已完成   60: 待结算  70：已结算 100: 系统取消  101：用户取消   120：订单删除）
     * 订单退款状态 （0: 无退款   10：退款待审核/确认   20: 全额退款成功   30：部分退款成功  40：退款失败）
     * */
    ORDER_STATUS_CODES_NULL(-1, Collections.EMPTY_LIST, Collections.EMPTY_LIST, "异常或空"),
    ORDER_STATUS_CODES_WAIT_PAY(0, Arrays.asList(0, 10, 20), Collections.EMPTY_LIST,"待支付"),
    ORDER_STATUS_CODES_WAIT_CANCEL(1, Arrays.asList(30, 35, 40), Collections.EMPTY_LIST, "待核销"),
    ORDER_STATUS_CODES_END_CANCEL(2, Arrays.asList(50, 60, 70), Arrays.asList(0, 40), "已核销"),
    ORDER_STATUS_CODES_END_STOP(4, Arrays.asList(100, 101), Collections.EMPTY_LIST,  "已取消"),
    ORDER_STATUS_CODES_END_REFUND(5, Collections.EMPTY_LIST, Arrays.asList(20, 30), "已退款"),


    ORDER_STATUS_CODES_POLY_VENUE_ORDER(100, Arrays.asList(30, 35, 40, 50, 60, 70, 100, 101), Arrays.asList(20, 30), "场馆展示的订单状态"),

    ORDER_STATUS_CODES_ALL(999, Arrays.asList(0, 10, 20, 30, 35, 40, 50, 60, 70, 100, 101), Arrays.asList(0, 20, 30, 40), "全部"),
    ;

    private static Map<Integer, OrderStatusPolyEnum> cache ;

    OrderStatusPolyEnum(Integer code, List<Integer> statusCodes, List<Integer> refStatusCodes, String remark) {
        this.code = code;
        this.statusCodes = statusCodes;
        this.refStatusCodes = refStatusCodes;
        this.remark = remark;
    }


    /** 订单聚合状态编号 */
    private Integer code;
    /** 该状态包含的订单状态编号 */
    private List<Integer> statusCodes;
    /** 该状态包含的订单退款状态编号 */
    private List<Integer> refStatusCodes;
    /** 备注信息 */
    private String remark;

    /**
     * 按照id 获取对应的状态聚合枚举
     * @param code 聚合状态编号
     * @return 如果找不到对应的聚合订单状态枚举，则返回 ORDER_STATUS_CODES_NULL， 表示寻找失败
     */
    public static OrderStatusPolyEnum forCode(Integer code){
        if(cache == null){
            cache = Arrays.stream(OrderStatusPolyEnum.values()).collect(Collectors.toMap(OrderStatusPolyEnum::getCode, v -> v));
        }
        return cache.getOrDefault(code, ORDER_STATUS_CODES_NULL);
    }



    public Integer getCode() {
        return code;
    }
    public void setCode(Integer code) {
        this.code = code;
    }
    public List<Integer> getStatusCodes() {
        return statusCodes;
    }
    public String getRemark() {
        return remark;
    }
    public List<Integer> getRefStatusCodes() {
        return refStatusCodes;
    }
}
