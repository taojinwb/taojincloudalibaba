package com.taojin.admin.repository.sys;

import com.taojin.admin.entity.sys.SysConfig;
import com.taojin.admin.entity.sys.SysOperLog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SysOperLogRepository extends  JpaRepository<SysOperLog, Long>, JpaSpecificationExecutor<SysOperLog>
{
    @Query(value=" truncate table sys_oper_log" ,nativeQuery = true)
    void cleanOperLog();


    @Query(value = "select * from sys_oper_log as operLog" +
            " where (:#{#operLog.params['beginTime']} IS NULL OR   date_format(operLog.create_time,'%Y-%m-%d %H:%m:%s') >= :#{#operLog.params['beginTime']}) " +
            " and (:#{#operLog.params['endTime']} IS NULL OR   date_format(operLog.create_time,'%Y-%m-%d %H:%m:%s') <= :#{#operLog.params['endTime']}) " +
            " and (:#{#operLog.operIp} IS NULL OR operLog.oper_ip like %:#{#operLog.operIp}%)" +
            " and (:#{#operLog.title} IS NULL OR  operLog.title like  %:#{#operLog.title}%)"+
            " and (:#{#operLog.operName} IS NULL OR  operLog.oper_name like  %:#{#operLog.operName}%)"+
            " and (:#{#operLog.status} IS NULL OR  operLog.status = :#{#operLog.status})"+
            " and (:#{#operLog.businessType} IS NULL OR operLog.business_type = :#{#operLog.businessType})",nativeQuery = true)
    Page<SysOperLog> selectOperLogList(@Param("operLog") SysOperLog operLog, Pageable pageable);
}
