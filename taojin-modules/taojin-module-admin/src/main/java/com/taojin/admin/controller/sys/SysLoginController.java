package com.taojin.admin.controller.sys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.taojin.admin.dto.sys.SysMenuDTO;
import com.taojin.admin.dto.sys.SysUserDTO;
import com.taojin.admin.entity.sys.vo.RouterVo;
import com.taojin.admin.framework.security.SecurityUtils;
import com.taojin.admin.service.sys.SysLoginService;
import com.taojin.admin.service.sys.SysMenuService;
import com.taojin.admin.service.sys.SysPermissionService;
import com.taojin.admin.dto.sys.SysLoginDTO;
import com.taojin.framework.vo.ApiResult;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登录验证
 *
 * @author sujg
 */
@RestController
@Slf4j
@Api(value="登录验证",tags = "登录验证")
public class SysLoginController
{
    @Autowired
    private SysLoginService loginService;

    @Autowired
    private SysMenuService menuService;

    @Autowired
    private SysPermissionService permissionService;

    /**
     * 登录方法
     *
     * @param loginBody 登录信息
     * @return 结果
     */
    @PostMapping("/login")
    @Operation(summary = "用户登录接口", description = "用户登录接口")
    public ApiResult<String> login(@Validated @RequestBody SysLoginDTO loginBody)
    {
        // 生成令牌
        log.info("---------"+loginBody.getUsername());
        String token = loginService.login(loginBody.getUsername(), loginBody.getPassword(), loginBody.getCode(),
                loginBody.getUuid());
        return ApiResult.OK(token);
    }

    /**
     * 获取用户信息
     *
     * @return 用户信息
     */
    @Operation(summary = "获取用户信息",description = "登录板块")
    @GetMapping("/getInfo")
    public ApiResult<Map<String, Object>> getInfo()
    {
        SysUserDTO user = SecurityUtils.getLoginUser().getUser();
        // 角色集合
        Set<String> roles = permissionService.getRolePermission(user);
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(user);
        Map ajax= new HashMap<String, Object>();
        ajax.put("user", user);
        ajax.put("roles", roles);
        ajax.put("permissions", permissions);
        return ApiResult.OK(ajax);
    }

    /**
     * 获取路由信息
     *
     * @return 路由信息
     */
    @Operation(summary = "获取路由信息",description = "登录板块")
    @GetMapping("getRouters")
    public ApiResult<List<RouterVo>> getRouters()
    {
        Long userId = SecurityUtils.getUserId();
        List<SysMenuDTO> menus = menuService.selectMenuTreeByUserId(userId);
        return ApiResult.ok(menuService.buildMenus(menus));
    }
}
