package com.taojin.admin.entity.sys;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
@Data
public class SysRoleMenuPK  implements Serializable {
    private Long roleId;

    private Long menuId;

}
