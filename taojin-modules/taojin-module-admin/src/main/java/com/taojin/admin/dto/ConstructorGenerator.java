package com.taojin.admin.dto;
import java.lang.reflect.Field;
import java.util.Arrays;

public class ConstructorGenerator {

    public static void main(String[] args) {
        String fields = "select com.taojin.admin.modules.orders.dto.OrdersDTO(parent_oid as orderCode,extd.start_time as orderStartTime, extd.end_time as orderEndTime, o.update_time as cancelTime ,\\n\" +\n" +
                "            \" user_nick as userNick,extd.buyer_phone as phone,  o.pay_way as payType, o.items_type as orderType,o.source as downOrderPlace,\\n\" +\n" +
                "            \" o.`status`,buss_notes as remark, ref_status as refStatus,disc_price as discPrice, sum(items_price) as sumMoney, sum(disc_price) as discount,sum(items_price- disc_price) as payMoney ";
        fields = fields.replaceAll("`","");
        String className = "com.taojin.admin.modules.orders.dto.OrdersDTO";
        generateConstructor(fields, className);
    }

    public static void generateConstructor(String fields, String className) {
        String[] fieldArray = fields.split(",");
        System.out.println(generateConstructorCode(fieldArray, className));
    }

    public static String generateConstructorCode(String[] fieldArray, String className) {
        String cname [] = className.split("\\.");
        StringBuilder constructorCode = new StringBuilder("public ").append(cname[cname.length-1]).append("(");

        for (String field : fieldArray) {
            String[] parts ;
            if(field.contains(" as ")){
                parts = field.split(" as ");
            }else {
                parts = field.split("\\.");
            }
            String fieldName="";
            if(parts.length>1) {
                 fieldName = parts[1].replaceAll("\\s","");
            }else{
                fieldName = parts[0].replaceAll("\\s","");
            }
            String fieldType = determineFieldType(fieldName, className);
            constructorCode.append(fieldType).append(" ").append(fieldName).append(", ");
        }

        // Remove the trailing comma and space
        constructorCode.setLength(constructorCode.length() - 2);

        constructorCode.append(") {").append(System.lineSeparator());

        for (String field : fieldArray) {
            String[] parts ;
            if(field.contains(" as ")){
                parts = field.split(" as ");
            }else {
                parts = field.split("\\.");
            }
            String fieldName ="";
            if(parts.length>1) {
                 fieldName = parts[1].replaceAll("\\s","");
            }else{
                 fieldName = parts[0].replaceAll("\\s","");
            }
            constructorCode.append("    this.").append(fieldName).append(" = ").append(fieldName).append(";").append(System.lineSeparator());
        }

        constructorCode.append("}");

        return constructorCode.toString();
    }

    public static String determineFieldType(String fieldName, String className) {
        try {
            Class<?> clazz = Class.forName(className);
            Field field = clazz.getDeclaredField(fieldName.replaceAll("\\s",""));
            return field.getType().getSimpleName();
        } catch (ClassNotFoundException | NoSuchFieldException e) {
            e.printStackTrace();
            // Handle the exception or return a default type if the field is not found
            return "Object";
        }
    }
}
