package com.taojin.admin.elastic.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.taojin.admin.elastic.entity.ActivitiesCommentLike;
import com.taojin.admin.elastic.repository.ActivitiesCommentLikeRepository;


/**
 * ES 中 活动相关数据 
 * @author boren
 *
 */
@Service
public class ActivitiesElasticService {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private ActivitiesCommentLikeRepository  actCommentLike;
//
//	@Autowired
//    private UserService userService;
	
	/**
	 * 保存用户点赞记录
	 * 
	 */
	public ActivitiesCommentLike  addLike(ActivitiesCommentLike  params)
	{

		params.setCreateTime(new Date());
		
		ActivitiesCommentLike  likeEntity = actCommentLike.save(params);
		
		return likeEntity;
	}

	/**
	 * 删除用户点赞记录
	 *
	 */
	public void unLike(String id)
	{
		actCommentLike.deleteById(id);
	}
	
//	/**
//	 * 当前用对某个活动评论的点赞数据记录
//	 * @param activitiesId
//	 * @param token
//	 * @return
//	 */
//	public List<ActivitiesCommentLike>  getMyLikesForActivitiesId(Long activitiesId,String token)
//	{
//		PageRequest  request = PageRequest.of(0,100);
//
//		Long userId = userService.getUserIdForToken(token);
//
//		try {
//			Page<ActivitiesCommentLike>  page = actCommentLike.findByActivitiesIdAndLikerUserId(activitiesId, userId,request);
//
//			return page.getContent();
//
//		} catch (Exception e) {
//
//			logger.info("serach err = {}",e.getMessage());
//			// TODO Auto-generated catch block
//			return new ArrayList<ActivitiesCommentLike>();
//		}
//	}
//
//
//	/**
//	 * 当前用对某个活动评论的点赞数据记录
//	 * @param param
//	 * @return
//	 */
//	public List<ActivitiesCommentLike> getMyLikesForIds(MyLikesDTO param) {
//
//		return actCommentLike.findByCommontIdInAndLikerUserId(param.getCommentIds(), param.getUid());
//
//	}
//
//
	

}
