package com.taojin.admin.framework.config;

import com.taojin.admin.dto.LoginUser;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import java.util.Optional;
import java.util.TimeZone;

/**
 * 自动生成jpa中entity的@Createby作者
 * 表示通过aop框架暴露该代理对象,AopContext能够访问
 * JPAQueryFactory  querydsl
 *
 */
@Configuration
// 表示通过aop框架暴露该代理对象,AopContext能够访问
@EnableAspectJAutoProxy(exposeProxy = true)
@EnableTransactionManagement
@EnableJpaAuditing
public class JpaConfig implements AuditorAware<String> {
    // 其他配置 暂时不启用，
    ////@Bean
    //public JPAQueryFactory jpaQuery(EntityManager entityManager) {
      //  return new JPAQueryFactory(entityManager);
   // }

    /**
     * 时区配置
     */
    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jacksonObjectMapperCustomization()
    {
        return jacksonObjectMapperBuilder -> jacksonObjectMapperBuilder.timeZone(TimeZone.getDefault());
    }

    @Override
    public Optional<String> getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication == null || !authentication.isAuthenticated()) {
            return Optional.empty();
        }

        LoginUser loginUser = (LoginUser) authentication.getPrincipal();

        return Optional.of(loginUser.getUsername());
    }
}
