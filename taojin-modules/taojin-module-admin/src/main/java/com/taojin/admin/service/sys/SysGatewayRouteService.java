package com.taojin.admin.service.sys;


import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.taojin.admin.entity.sys.SysDictType;
import com.taojin.admin.entity.sys.SysGatewayRoute;
import com.taojin.admin.framework.common.BaseService;
import com.taojin.admin.framework.constant.CacheConstant;
import com.taojin.admin.repository.sys.SysDictTypeRepository;
import com.taojin.admin.repository.sys.SysGatewayRouteRepository;
import com.taojin.framework.utils.BaseMap;
import jdk.nashorn.internal.runtime.GlobalConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: gateway路由管理
 * @Author: jeecg-boot
 * @Date: 2020-05-26
 * @Version: V1.0
 */
@Service
@Slf4j
public class SysGatewayRouteService extends BaseService<SysGatewayRoute,Long> {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    SysGatewayRouteRepository sysGatewayRouteRepository;
    private static final String STRING_STATUS = "status";

    public SysGatewayRouteService(SysGatewayRouteRepository repository) {
        super(repository,repository);
    }

    public void addRoute2Redis(String key) {
        List<SysGatewayRoute> ls = sysGatewayRouteRepository.findAll();
        redisTemplate.opsForValue().set(key, JSON.toJSONString(ls));
    }

    public void deleteById(Long id) {
        sysGatewayRouteRepository.deleteById(id);
        this.resreshRouter(id);
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateAll(JSONObject json) {
        log.info("--gateway 路由配置修改--");
        try {
            json = json.getJSONObject("router");
            Long id = cn.hutool.core.convert.Convert.toLong(json.getString("id"));
            //update-begin-author:taoyan date:20211025 for: oracle路由网关新增小bug /issues/I4EV2J
            SysGatewayRoute route;
            if(id == null){
                route = new SysGatewayRoute();
            }else{
                route = sysGatewayRouteRepository.findById(id).orElse(null);
            }
            //update-end-author:taoyan date:20211025 for: oracle路由网关新增小bug /issues/I4EV2J
            if (ObjectUtil.isEmpty(route)) {
                route = new SysGatewayRoute();
            }
            route.setRouterId(json.getString("routerId"));
            route.setName(json.getString("name"));
            route.setPredicates(json.getString("predicates"));
            String filters = json.getString("filters");
            if (ObjectUtil.isEmpty(filters)) {
                filters = "[]";
            }
            route.setFilters(filters);
            route.setUri(json.getString("uri"));
            if (json.get(STRING_STATUS) == null) {
                route.setStatus(1);
            } else {
                route.setStatus(json.getInteger(STRING_STATUS));
            }
            this.sysGatewayRouteRepository.save(route);
            resreshRouter(null);
        } catch (Exception e) {
            log.error("路由配置解析失败", e);
            resreshRouter(null);
            e.printStackTrace();
        }
    }

    /**
     * 更新redis路由缓存
     */
    private void resreshRouter(Long delRouterId) {
        //更新redis路由缓存
        addRoute2Redis(CacheConstant.GATEWAY_ROUTES);
        BaseMap params = new BaseMap();
        params.put(CacheConstant.HANDLER_NAME, CacheConstant.LODER_ROUDER_HANDLER);
        params.put("delRouterId", delRouterId);
        //刷新网关
        redisTemplate.convertAndSend(CacheConstant.REDIS_TOPIC_NAME, params);
    }

    public void clearRedis() {
        redisTemplate.opsForValue().set(CacheConstant.GATEWAY_ROUTES, null);
    }


}
