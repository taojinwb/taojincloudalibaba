package com.taojin.admin.framework.security.handle;

import com.alibaba.fastjson2.JSON;
import com.taojin.framework.utils.MF;
import com.taojin.framework.utils.ServletUtils;
import com.taojin.framework.vo.ApiCode;
import com.taojin.framework.vo.ApiResult;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

/**
 * 认证失败处理类 返回未授权
 *
 * @author taojin
 */
@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint, Serializable
{
    private static final long serialVersionUID = -8970718410437077606L;

    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
            throws IOException
    {
        String msg = MF.format("请求访问：{}，认证失败，无法访问系统资源", request.getRequestURI());
        ServletUtils.renderString(response, JSON.toJSONString(ApiResult.error(ApiCode.TJ_401, msg)));
    }
}
