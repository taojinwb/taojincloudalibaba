package com.taojin.admin.generator.repository;

import com.taojin.admin.generator.dto.GenTableColumnDTO;
import com.taojin.admin.generator.entity.GenTable;
import com.taojin.admin.generator.entity.GenTableColumn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface GenTableColumnRepository extends JpaRepository<GenTableColumn, Long>, JpaSpecificationExecutor<GenTableColumn> {

    /**
     * 根据表名称查询列信息
     *
     * @param tableName 表名称
     * @return 列信息
     */
    @Query(value="select column_name, (case when (is_nullable = 'no' and column_key != 'PRI') then '1' else null end) as is_required," +
            " (case when column_key = 'PRI' then '1' else '0' end) as is_pk, ordinal_position as sort, column_comment," +
            " (case when extra = 'auto_increment' then '1' else '0' end) as is_increment, column_type\n" +
            "\t\tfrom information_schema.columns where table_schema = (select database()) and table_name = (:tableName)\n" +
            "\t\torder by ordinal_position ",nativeQuery = true)
    public List<GenTableColumnDTO> selectDbTableColumnsByName(@Param("tableName") String tableName);

    /**
     * 查询业务字段列表
     *
     * @param tableId 业务字段编号
     * @return 业务字段集合
     */
    @Query(value = " select * from gen_table_column " +
            " where table_id = :tableId\n" +
            "        order by sort" +
            "" ,nativeQuery = true)
    public List<GenTableColumn> selectGenTableColumnListByTableId(@Param("tableId") Long tableId);

    @Query(value = " select * from gen_table_column " +
            " where table_name = :tableName\n" +
            "        order by sort" +
            "" ,nativeQuery = true)
    public List<GenTableColumn> selectGenTableColumnListByTableId(@Param("tableName") String tableName);


}
