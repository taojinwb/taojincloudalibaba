package com.taojin.admin.repository.sys;

import com.taojin.admin.entity.sys.SysConfig;
import com.taojin.admin.entity.sys.SysDept;
import com.taojin.admin.dto.sys.SysDeptDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SysDeptRepository extends  JpaRepository<SysDept, Long>, JpaSpecificationExecutor<SysDept> {
    @Query(value="select d.dept_id" +
            " from sys_dept d" +
            "            left join sys_role_dept rd on d.dept_id = rd.dept_id" +
            "        where rd.role_id = ?1" +
            "              and d.dept_id not in (select d.parent_id from sys_dept d inner join sys_role_dept rd on d.dept_id = rd.dept_id and rd.role_id = ?1)"+
            " order by d.parent_id, d.order_num" ,nativeQuery = true)
   List<Long> selectDeptListByRoleId( Long roleId,  Integer deptCheckStrictly);



    @Query(value="select d.dept_id" +
            " from sys_dept d" +
            "            left join sys_role_dept rd on d.dept_id = rd.dept_id" +
            "        where rd.role_id = :roleId " +
            " order by d.parent_id, d.order_num" ,nativeQuery = true)
    List<Long> selectDeptListByRoleId(@Param("roleId") Long roleId);

    @Query(value="select count(*) from sys_dept where status = 0 and del_flag = '0' and find_in_set(:deptId, ancestors)",nativeQuery = true)
    int selectNormalChildrenDeptById(@Param("deptId") Long deptId);
    @Query(value="select count(*) from sys_dept where del_flag ='0' and parent_id = ?1 ",nativeQuery = true)
    int hasChildByDeptId(Long deptId);

    SysDept findAllByDeptIdAndDelFlag(Long deptId, String delflag);

    //int countByDeptIdAndDelFlag(Long deptId,String delflag);

    @Query(value="select   * from sys_dept where dept_name=:deptName and parent_id=:parentId and del_flag=:delFlag",nativeQuery = true)
    SysDept findFirstByDeptNameAndParentIdAndDelFlag(@Param("deptName") String deptName,@Param("parentId") Long parentId,@Param("delFlag") Long delFlag);


    @Query(value="select distinct new com.taojin.admin.dto.sys.SysDeptDTO(d.deptId, d.parentId, d.ancestors, d.deptName, d.orderNum, d.leader, d.phone, d.email, d.status,e.deptName ) "+
            " from sys_dept d\n" +
            " left join sys_dept e on e.deptId = d.parentId\n"+
            " where d.deptId = :deptId")
    SysDeptDTO selectDeptById(@Param("deptId") Long deptId);

    @Modifying
    @Query("update sys_dept d set d.status = '0' where d.deptId in :deptIds")
    void updateStatusForDeptIds(@Param("deptIds") Long[] deptIds);

    @Query(value="select *  from sys_dept where find_in_set(:deptId, ancestors)",nativeQuery = true)
    List<SysDept> selectChildrenDeptById(@Param("deptId") Long deptId);


   @Query(value="select count(*) " +
           "  from sys_user as user" +
           "  where user.del_flag ='0' and user.status='0' and user.dept_id = ?1",nativeQuery = true)
   int selectDeptNumber(Long deptId);



   SysDept findByDeptId(Long deptId);
}
