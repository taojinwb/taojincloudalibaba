package com.taojin.admin.repository.sys;

import com.taojin.admin.entity.sys.SysGatewayRoute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SysGatewayRouteRepository extends JpaRepository<SysGatewayRoute, Long>, JpaSpecificationExecutor<SysGatewayRoute> {
}
