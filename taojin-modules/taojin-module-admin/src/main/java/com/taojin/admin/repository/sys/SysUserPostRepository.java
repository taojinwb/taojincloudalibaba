package com.taojin.admin.repository.sys;

import com.taojin.admin.entity.sys.SysConfig;
import com.taojin.admin.entity.sys.SysRole;
import com.taojin.admin.entity.sys.SysUserPost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SysUserPostRepository extends  JpaRepository<SysUserPost, Long>, JpaSpecificationExecutor<SysUserPost> {
    int countAllByPostId(Long postId);
    int deleteSysUserPostsByUserId(Long userId);
    int deleteSysUserPostsByUserIdIn(Long[] userId);
}
