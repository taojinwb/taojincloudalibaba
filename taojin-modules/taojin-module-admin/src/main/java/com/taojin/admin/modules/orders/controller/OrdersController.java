package com.taojin.admin.modules.orders.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.taojin.admin.framework.common.BaseController;
import com.taojin.admin.modules.orders.dto.OrdersDTO;
//import com.taojin.api.admin.RemoteAdminAPI;
import com.taojin.common.helper.DateHelper;
import com.taojin.framework.utils.ServletUtils;
import com.taojin.framework.vo.ApiResult;
import io.swagger.annotations.Api;
import javax.validation.constraints.NotNull;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.servlet.ModelAndView;
import com.taojin.admin.modules.orders.entity.Orders;
import com.taojin.admin.modules.orders.service.OrdersService;

/**
 * 订单主 Controller
 *
 * @author sujg
 * @date Thu Dec 14 16:22:23 CST 2023
 */
@RestController
@RequestMapping("/modules/orders")
@Api(value="订单信息 ",tags = "订单信息 ")
@Slf4j
public class OrdersController extends BaseController<Orders>
{
    @Autowired
    private OrdersService ordersService;
    //@Autowired
   // private RemoteAdminAPI remoteAdminAPI;

    /**
     * 查询订单主 列表
     */
    @PreAuthorize("@ss.hasPermi('modules:orders:list')")
    @GetMapping("/list")
    @ApiOperation(value = "查询订单信息 列表", tags = "订单信息 ")
    public ApiResult<List<OrdersDTO>> list(OrdersDTO ordersDTO)
    {

//      ApiResult ss = remoteAdminAPI.getResource();
//        log.info(ss.getMessage()+"---"+ss.getData()+"==="+ss.getCode()+"----"+ss.getTotal());
        ordersDTO.setDownOrderStartDate(DateHelper.beginOfDay(ordersDTO.getDownOrderStartDate()));
        ordersDTO.setDownOrderEndDate(DateHelper.endOfDay(ordersDTO.getDownOrderEndDate()));
        ordersDTO.setSessionStartDate(DateHelper.beginOfDay(ordersDTO.getSessionStartDate()));
        ordersDTO.setSessionEndDate(DateHelper.endOfDay(ordersDTO.getSessionEndDate()));
        ordersDTO.setCancelStartDate(DateHelper.beginOfDay(ordersDTO.getCancelStartDate()));
        ordersDTO.setCancelEndDate(DateHelper.endOfDay(ordersDTO.getCancelEndDate()));
         List<OrdersDTO> lst =ordersService.getOrderParentList(ordersDTO,this.getPageable(ordersDTO.getParams()));


        return ApiResult.OK(lst);
    }

    /**
     * 获取订单主 详细信息
     */
    @PreAuthorize("@ss.hasPermi('modules:orders:query')")
    @GetMapping(value = "/getDetail/{id}")
    @ApiOperation(value = "获取订单信息 详细信息", tags = "订单信息 ")
    public ApiResult<Orders> getDetail(@PathVariable("id") @NotNull Long id)
    {

        return ApiResult.OK(ordersService.GetOneById(id));
    }

    /**
     * 新增订单主
     */
    @PreAuthorize("@ss.hasPermi('modules:orders:add')")
 //   @Log(title = "订单主 ", businessType = BusinessType.INSERT)
    @ApiOperation(value = "新增订单信息 ", tags = "订单信息 ")
    @PostMapping("/add")
    public ApiResult<Orders> add(@RequestBody @Validated Orders orders)
    {
        return ApiResult.OK(ordersService.Insert(orders));
    }

    /**
     * 修改订单主
     */
    @PreAuthorize("@ss.hasPermi('modules:orders:edit')")
   // @Log(title = "订单主 ", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "修改订单信息 ", tags = "订单信息 ")
    @PostMapping("/edit")
    public ApiResult<Orders> edit(@RequestBody @Validated Orders orders)
    {
        return ApiResult.OK(ordersService.Update(orders,orders.getId()));
    }

    /**
     * 删除订单主
     */
    @PreAuthorize("@ss.hasPermi('modules:orders:remove')")
    //@Log(title = "订单主 ", businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除订单信息 ", tags = "订单信息 ")
	@PostMapping("/del/{id}")
    public ApiResult<String> remove(@PathVariable  @NotNull Long id)
    {
            ordersService.RemoveById(id);
        return ApiResult.OK("删除成功！");
    }
    /**
     * 导出订单主 列表
     */
    @PreAuthorize("@ss.hasPermi('modules:orders:export')")
    //@Log(title = "订单主 ", businessType = BusinessType.EXPORT)
    @ApiOperation(value="导出订单信息 列表",notes="导出订单信息 ",tags = "订单信息 ")
    @PostMapping("/exportXls")
    public ModelAndView exportXls(HttpServletResponse response, Orders orders)
    {

        return super.exportXls( ordersService.GetAllByFilter(orders), orders, Orders.class, "订单信息 数据");
    }

}
