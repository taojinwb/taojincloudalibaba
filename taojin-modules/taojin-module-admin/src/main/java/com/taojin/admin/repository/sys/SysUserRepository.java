package com.taojin.admin.repository.sys;

import com.taojin.admin.dto.sys.SysUserDTO;
import com.taojin.admin.entity.sys.SysUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface SysUserRepository extends  JpaRepository<SysUser, Long>, JpaSpecificationExecutor<SysUser>{
    @Query(value="select  new com.taojin.admin.dto.sys.SysUserDTO(userId) from sys_user where userName=?1")
    SysUserDTO findByUserName(String username);



    @Query(value = " select role_key from sys_role where role_id in (select role_id from sys_user_role where user_id = (select user_id from sys_user where user_name=:username))  ", nativeQuery = true)
    Set<String> selectRolesByUserName(@Param("username")  String username);


    @Query(value="select new com.taojin.admin.dto.sys.SysUserDTO(u.userId, u.deptId, u.userName, u.nickName, u.email, u.avatar, u.phonenumber, u.password, u.sex, u.status, u.delFlag, u.loginIp, u.loginDate," +
            " u.createBy, u.createTime, u.remark,u.unionid,\n" +
            "     d.parentId, d.ancestors, d.deptName, d.orderNum, d.leader, d.status as deptStatus,\n" +
            "    r.roleId, r.roleName, r.roleKey, r.roleSort, r.dataScope, r.status as roleStatus,u.userType,u.updateBy ,u.updateTime )\n" +
            "    from sys_user u\n" +
            "    left join sys_dept d on u.deptId = d.deptId\n" +
            "    left join sys_user_role ur on u.userId = ur.userId\n" +
            "    left join sys_role r on r.roleId = ur.roleId\n" +
            "    where  u.delFlag = '0' \n"+
            " AND (:#{#sysUserDTO.userName} IS NULL OR  u.userName = :#{#sysUserDTO.userName}) " +
            " AND (:#{#sysUserDTO.userId} IS NULL OR  u.userId = :#{#sysUserDTO.userId}) "
               )
    List<SysUserDTO> selectUserDTOBySysUser(@Param("sysUserDTO") SysUserDTO sysUserDTO);


    @Query(value="select new com.taojin.admin.dto.sys.SysUserDTO(u.userId, u.deptId, u.userName, u.nickName, u.email, u.avatar, u.phonenumber, u.password, " +
            " u.sex, u.status, u.delFlag, u.loginIp, u.loginDate, u.createBy, u.createTime, u.remark,\n" +
            "     d.parentId, d.ancestors, d.deptName, d.orderNum, d.leader, d.status as deptStatus,\n" +
            "    u.userType ) \n" +
            "    from sys_user u\n" +
            "    left join sys_dept d on u.deptId = d.deptId \n" +
            "    where u.userId = :userId and u.delFlag = '0'")
    SysUserDTO selectUserByUserId(@Param("userId") Long userId);
    @Query(value = "select  new com.taojin.admin.dto.sys.SysUserDTO(u.userId, u. deptId, u. userName, u. nickName, u. userType, u. email, u. phonenumber, u. sex, u. avatar, u. password, u. status, u. delFlag, u. loginIp, u. loginDate, u. createBy, u. createTime, u. updateBy, u. updateTime, u. remark)" +
            "   from sys_user u " +
            "   left join sys_dept d on u.deptId = d.deptId" +
            "   left join sys_user_role ur on u.userId = ur.userId" +
            "   left join sys_role r on r.roleId = ur.roleId" +
            "   where u.delFlag = '0' and r.roleId = :#{#sysUser.roleId} " +
            "  AND (:#{#sysUser.userName} IS NULL OR  u.userName = :#{#sysUser.userName}) " +
            "  AND (:#{#sysUser.phonenumber} IS NULL OR  u.phonenumber = :#{#sysUser.phonenumber}) ")
    Page<SysUserDTO> selectAllocatedList(@Param("sysUser") SysUser sysUser, Pageable pageable);

    @Query(value = "select distinct new com.taojin.admin.dto.sys.SysUserDTO(u.userId, u. deptId, u. userName, u. nickName, u. userType, u. email, u. phonenumber, u. sex, u. avatar, u. password, u. status, u. delFlag, u. loginIp, u. loginDate, u. createBy, u. createTime, u. updateBy, u. updateTime, u. remark)" +
            "  from sys_user as u" +
            "  left join sys_dept d on u.deptId = d.deptId  " +
            "  left join sys_user_role ur on u.userId = ur.userId  " +
            "  left join sys_role r on r.roleId = ur.roleId  where u.delFlag ='0' and (r.roleId != :#{#sysUser.roleId} or r.roleId IS NULL)" +
            "  and u.userId not in (select u.userId from sys_user u inner join sys_user_role ur on u.userId = ur.userId and ur.roleId = :#{#sysUser.roleId} )" +
            "  AND (:#{#sysUser.userName} IS NULL OR  u.userName = :#{#sysUser.userName}) " +
            "  AND (:#{#sysUser.phonenumber} IS NULL OR  u.phonenumber = :#{#sysUser.phonenumber}) " )

   Page<SysUserDTO>selectUnallocatedList(@Param("sysUser")SysUser sysUser,Pageable pageable);

    @Modifying
    @Query("UPDATE sys_user u SET u.loginIp=?1,u.loginDate=?2 "+
            " WHERE u.userId = ?3")
    void UpdateUserLogin(String loginip, Date logindate, Long uid);

    @Modifying
    @Query("UPDATE sys_user u SET u.nickName= COALESCE(:#{#sysUser.nickName}, u.nickName) " +
            ",u.deptId= COALESCE(:#{#sysUser.deptId}, u.deptId) " +
            ",u.phonenumber= COALESCE(:#{#sysUser.phonenumber}, u.phonenumber) " +
            ",u.email= COALESCE(:#{#sysUser.email}, u.email) " +
            ",u.sex= COALESCE(:#{#sysUser.sex}, u.sex) " +
            ",u.status= COALESCE(:#{#sysUser.status}, u.status) " +
            ",u.remark= COALESCE(:#{#sysUser.remark}, u.remark) " +
            ",u.updateBy= COALESCE(:#{#sysUser.updateBy}, u.updateBy) " +
            ",u.updateTime= COALESCE(:#{#sysUser.updateTime}, u.updateTime) " +
            "WHERE u.userId = :#{#sysUser.userId}")
    void updateFields(@Param("sysUser") SysUser sysUser);



    SysUser findFirstByUserNameAndDelFlag(String userName,String delFlag);
    SysUser findFirstByPhonenumberAndDelFlag(String phoneNumber,String delFlag);

    SysUser findFirstByEmailAndDelFlag(String email,String delFlag);

    @Modifying
    @Query("UPDATE sys_user u SET  u.delFlag = '2' WHERE u.userId = ?1")
    void UpdateDelFlag(Long userId);



    @Query("select new com.taojin.admin.dto.sys.SysUserDTO(user.userId,user.userName, user.nickName,user.phonenumber,user.status,user.createTime,dept.deptName)  " +
            " from sys_user as user  " +
            " left join sys_dept as dept on user.deptId = dept.deptId" +
            " where user.delFlag = '0' " +
            " and (:#{#sysUser.params['beginTime']} IS NULL OR   date_format(user.createTime,'%Y-%m-%d %H:%m:%s') >= :#{#sysUser.params['beginTime']}) " +
            " and (:#{#sysUser.params['endTime']} IS NULL OR   date_format(user.createTime,'%Y-%m-%d %H:%m:%s') <= :#{#sysUser?.params['endTime']}) " +
            " and (:#{#sysUser.userName} IS NULL OR user.userName like %:#{#sysUser.userName}%)" +
            " and (:#{#sysUser.phonenumber} IS NULL OR user.phonenumber like %:#{#sysUser.phonenumber}%)" +
            " and (:#{#sysUser.status} IS NULL OR  user.status = :#{#sysUser.status})"+
            " and (:#{#sysUser.deptId} IS NULL OR  user.deptId = :#{#sysUser.deptId})  "+
            " ORDER BY user.createTime DESC, user.userId asc"
    )
    Page<SysUserDTO> selectUser(@Param("sysUser") SysUser sysUser, Pageable pageable);
}
