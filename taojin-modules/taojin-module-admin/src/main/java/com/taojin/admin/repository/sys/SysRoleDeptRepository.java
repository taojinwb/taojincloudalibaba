package com.taojin.admin.repository.sys;

import com.taojin.admin.entity.sys.SysConfig;
import com.taojin.admin.entity.sys.SysRoleDept;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SysRoleDeptRepository extends  JpaRepository<SysRoleDept, Long>, JpaSpecificationExecutor<SysRoleDept> {
    void deleteSysRoleDeptByRoleId(Long roleId);
    void deleteSysRoleDeptsByRoleIdIn(Long[] roleIds);
}
