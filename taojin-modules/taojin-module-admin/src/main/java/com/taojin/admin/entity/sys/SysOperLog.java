package com.taojin.admin.entity.sys;

import javax.persistence.*;
import java.sql.Timestamp;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import com.taojin.admin.entity.BaseEntity;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-26
*/
@Data
@Entity(name = "sys_oper_log")
public class SysOperLog extends BaseEntity {

/** 日志主键*/
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "oper_id")
		private Long operId;

		/** 模块标题*/
		@Column(name = "title")
		@Excel(name ="模块标题")
		private java.lang.String title;

		/** 业务类型（0其它 1新增 2修改 3删除）*/
		@Column(name = "business_type")
		@Excel(name ="业务类型（0其它 1新增 2修改 3删除）")
		private java.lang.Integer businessType;

		/** 方法名称*/
		@Column(name = "method")
		@Excel(name ="方法名称")
		private java.lang.String method;

		/** 请求方式*/
		@Column(name = "request_method")
		@Excel(name ="请求方式")
		private java.lang.String requestMethod;

		/** 操作类别（0其它 1后台用户 2手机端用户）*/
		@Column(name = "operator_type")
		@Excel(name ="操作类别（0其它 1后台用户 2手机端用户）")
		private java.lang.Integer operatorType;

		/** 操作人员*/
		@Column(name = "oper_name")
		@Excel(name ="操作人员")
		private java.lang.String operName;

		/** 部门名称*/
		@Column(name = "dept_name")
		@Excel(name ="部门名称")
		private java.lang.String deptName;

		/** 请求URL*/
		@Column(name = "oper_url")
		@Excel(name ="请求URL")
		private java.lang.String operUrl;

		/** 主机地址*/
		@Column(name = "oper_ip")
		@Excel(name ="主机地址")
		private java.lang.String operIp;

		/** 操作地点*/
		@Column(name = "oper_location")
		@Excel(name ="操作地点")
		private java.lang.String operLocation;

		/** 请求参数*/
		@Column(name = "oper_param")
		@Excel(name ="请求参数")
		private java.lang.String operParam;

		/** 返回参数*/
		@Column(name = "json_result")
		@Excel(name ="返回参数")
		private java.lang.String jsonResult;

		/** 操作状态（0正常 1异常）*/
		@Column(name = "status")
		@Excel(name ="操作状态（0正常 1异常）")
		private java.lang.Integer status;

		/** 错误消息*/
		@Column(name = "error_msg")
		@Excel(name ="错误消息")
		private java.lang.String errorMsg;

		/** 操作时间*/
		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
		@Column(name = "oper_time")
		@Excel(name ="操作时间")
		private java.util.Date operTime;

		/** 消耗时间*/
		@JsonSerialize(using = ToStringSerializer.class)
		@Column(name = "cost_time")
		@Excel(name ="消耗时间")
		private Long costTime;

}
