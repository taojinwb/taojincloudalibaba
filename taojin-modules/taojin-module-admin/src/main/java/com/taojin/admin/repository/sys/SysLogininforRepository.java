package com.taojin.admin.repository.sys;

import com.taojin.admin.entity.sys.SysConfig;
import com.taojin.admin.entity.sys.SysLogininfor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SysLogininforRepository extends  JpaRepository<SysLogininfor, Long>, JpaSpecificationExecutor<SysLogininfor> {
    @Query(value=" truncate table sys_logininfor" ,nativeQuery = true)
    void cleanLogininfor();


    @Query(value = "select * from sys_logininfor as logininfor" +
            " where (:#{#logininfor.params['beginTime']} IS NULL OR   date_format(logininfor.create_time,'%Y-%m-%d %H:%m:%s') >= :#{#logininfor.params['beginTime']}) " +
            " and (:#{#logininfor.params['endTime']} IS NULL OR   date_format(logininfor.create_time,'%Y-%m-%d %H:%m:%s') <= :#{#logininfor.params['endTime']}) " +
            " and (:#{#logininfor.ipaddr} IS NULL OR logininfor.ipaddr like %:#{#logininfor.ipaddr}%)" +
            " and (:#{#logininfor.userName} IS NULL OR  logininfor.user_name like  %:#{#logininfor.userName}%)"+
            " and (:#{#logininfor.status} IS NULL OR  logininfor.status = :#{#logininfor.status})",nativeQuery = true)
    Page<SysLogininfor> selectLogininforList(@Param("logininfor") SysLogininfor logininfor, Pageable pageable);
}

