package com.taojin.admin.dto.sys;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-30
*/
@Data
@NoArgsConstructor
public class SysMenuDTO {
	/** 菜单ID */
	private Long menuId;

	/** 菜单名称 */
	private String menuName;

	/** 父菜单名称 */
	private String parentName;

	/** 父菜单ID */
	private Long parentId;

	/** 显示顺序 */
	private Integer orderNum;

	/** 路由地址 */
	private String path;

	/** 组件路径 */
	private String component;

	/** 路由参数 */
	private String query;

	/** 是否为外链（0是 1否） */
	private String isFrame;

	/** 是否缓存（0缓存 1不缓存） */
	private String isCache;

	/** 类型（M目录 C菜单 F按钮） */
	private String menuType;

	/** 显示状态（0显示 1隐藏） */
	private String visible;

	/** 菜单状态（0正常 1停用） */
	private String status;

	/** 权限字符串 */
	private String perms;

	/** 菜单图标 */
	private String icon;

	/** 子菜单 */
	private List<SysMenuDTO> children = new ArrayList<SysMenuDTO>();

	/** 请求参数,要初始化 */
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private Map<String, Object> params = new HashMap<>();
	/** 创建者*/
	private java.lang.String createBy;

	/** 创建时间*/


	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private java.util.Date createTime;

	/** 更新者*/
	private java.lang.String updateBy;

	/** 更新时间*/


	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private java.util.Date updateTime;


	private String remark;

	private Long tenantId;

	public SysMenuDTO(Long menuId, Long parentId, String menuName, String path, String component, String query, String visible, String status,
					  String perms, String isFrame, String isCache, String menuType, String icon, Integer orderNum, Date createTime,
					  String createBy, Date updateTime, String updateBy, String remark, Long tenantId) {
		this.menuId = menuId;
		this.parentId = parentId;
		this.menuName = menuName;
		this.path = path;
		this.component = component;
		this.query = query;
		this.visible = visible;
		this.status = status;
		this.perms = perms;
		this.isFrame = isFrame;
		this.isCache = isCache;
		this.menuType = menuType;
		this.icon = icon;
		this.orderNum = orderNum;
		this.createTime = createTime;
		this.createBy = createBy;
		this.updateTime = updateTime;
		this.updateBy = updateBy;
		this.remark = remark;
		this.tenantId = tenantId;
	}


}
