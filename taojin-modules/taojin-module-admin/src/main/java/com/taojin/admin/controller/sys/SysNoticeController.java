package com.taojin.admin.controller.sys;


import com.taojin.admin.framework.common.BaseController;
import com.taojin.admin.entity.sys.SysNotice;
import com.taojin.admin.framework.annotation.Log;
import com.taojin.admin.service.sys.SysNoticeService;
import com.taojin.framework.enums.BusinessType;
import com.taojin.framework.vo.ApiResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 公告 信息操作处理
 *
 * @author sujg
 */
@RestController
@RequestMapping("/system/notice")
@Api(tags = "公告 信息操作处理相关接口" ,value = "公告 信息操作处理相关接口" )
public class SysNoticeController extends BaseController
{
    @Autowired
    private SysNoticeService noticeService;



    /**
     * 获取通知公告列表
     */
    @ApiOperation(value = "获取通知公告列表",tags = "公告 信息操作处理相关接口")
    @PreAuthorize("@ss.hasPermi('system:notice:list')")
    @GetMapping("/list")
    public ApiResult list(SysNotice notice)
    {
        Page<SysNotice> list = noticeService.selectNoticeList(notice,this.getPageable(notice.getParams()));
        return ApiResult.ok(list);
    }

    /**
     * 根据通知公告编号获取详细信息
     */
    @ApiOperation(value = "根据通知公告编号获取详细信息",tags = "公告 信息操作处理相关接口")
    @PreAuthorize("@ss.hasPermi('system:notice:query')")
    @GetMapping(value = "/{noticeId}")
    public ApiResult getInfo(@PathVariable Long noticeId)
    {
        return ApiResult.ok(noticeService.selectNoticeById(noticeId));
    }

    /**
     * 新增通知公告
     */
    @ApiOperation(value = "新增通知公告",tags = "公告 信息操作处理相关接口")
    @PreAuthorize("@ss.hasPermi('system:notice:add')")
    @Log(title = "通知公告", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public ApiResult add(@Validated @RequestBody SysNotice notice)
    {
        //notice.setCreateBy(this.getLoginUser().getUsername());

        return ApiResult.ok(noticeService.Insert(notice));
    }

    /**
     * 修改通知公告
     */
    @ApiOperation(value = "修改通知公告",tags = "公告 信息操作处理相关接口")
    @PreAuthorize("@ss.hasPermi('system:notice:edit')")
    @Log(title = "通知公告", businessType = BusinessType.UPDATE)
    @PutMapping
    public ApiResult edit(@Validated @RequestBody SysNotice notice)
    {
        //notice.setUpdateBy(this.getLoginUser().getUsername());
        noticeService.updateNotice(notice);
        return ApiResult.ok("更新成功");
    }

    /**
     * 删除通知公告
     */
    @ApiOperation(value = "删除通知公告",tags = "公告 信息操作处理相关接口")
    @PreAuthorize("@ss.hasPermi('system:notice:remove')")
    @Log(title = "通知公告", businessType = BusinessType.DELETE)
    @DeleteMapping("/{noticeIds}")
    public ApiResult remove(@PathVariable Long[] noticeIds)
    {
        noticeService.deleteNoticeByIds(noticeIds);
        return ApiResult.ok("删除成功");
    }
}
