package com.taojin.admin.dto.sys;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-30
*/
@Data
@NoArgsConstructor
public class SysRoleDTO {
		private Long roleId;

		@Excel(name ="角色名称")
		private java.lang.String roleName;

		@Excel(name ="角色权限字符串")
		private java.lang.String roleKey;

		@Excel(name ="显示顺序")
		private java.lang.Integer roleSort;

		@Excel(name ="数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）")
		private java.lang.String dataScope;

		@Excel(name ="菜单树选择项是否关联显示")
		private java.lang.String  menuCheckStrictly;

		@Excel(name ="部门树选择项是否关联显示")
		private java.lang.String  deptCheckStrictly;

		@Excel(name ="角色状态（0正常 1停用）")
		private java.lang.String status;

		@Excel(name ="删除标志（0代表存在 2代表删除）")
		private java.lang.String delFlag;

		@Excel(name ="创建者")
		private java.lang.String createBy;

		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
		@Excel(name ="创建时间")
		private java.util.Date createTime;

		@Excel(name ="更新者")
		private java.lang.String updateBy;

		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
		@Excel(name ="更新时间")
		private java.util.Date updateTime;

		@Excel(name ="备注")
		private java.lang.String remark;

		/** 用户是否存在此角色标识 默认不存在 */
		private boolean flag = false;

		/** 角色菜单权限 */
		private Set<String> permissions;

		/** 菜单组 */
		private Long[] menuIds;

		/** 部门组（数据权限） */
		private Long[] deptIds;

		private String beginTime;

		private String endTime;


		@JsonInclude(JsonInclude.Include.NON_EMPTY)
		private Map<String, Object> params = new HashMap<>();

		public boolean isAdmin()
		{
			return isAdmin(this.roleId);
		}

		public static boolean isAdmin(Long roleId)
		{
			return roleId != null && 1L == roleId;
		}

	public SysRoleDTO(String roleName, String roleKey, Long roleId, Integer roleSort, String dataScope, String menuCheckStrictly,
					  String deptCheckStrictly, String status, String delFlag, String createBy, Date createTime, String updateBy, Date updateTime, String remark) {
		this.roleName = roleName;
		this.roleKey = roleKey;
		this.roleId = roleId;
		this.roleSort = roleSort;
		this.dataScope = dataScope;
		this.menuCheckStrictly = menuCheckStrictly;
		this.deptCheckStrictly = deptCheckStrictly;
		this.status = status;
		this.delFlag = delFlag;
		this.createBy = createBy;
		this.createTime = createTime;
		this.updateBy = updateBy;
		this.updateTime = updateTime;
		this.remark = remark;
	}

}
