package com.taojin.admin.repository.sys;

import com.taojin.admin.entity.sys.SysConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * 系统配置表
 * @author sujg
 *
 */


public interface SysConfigRepository extends  JpaRepository<SysConfig, Long>, JpaSpecificationExecutor<SysConfig>{
    SysConfig findSysConfigByPzkeyAndSfky(String configkey,String sfky);


    @Query(value = "select * from sys_config as config" +
            " where (:#{#config.params['beginTime']} IS NULL OR   date_format(config.create_time,'%Y-%m-%d %H:%m:%s') >= :#{#config.params['beginTime']}) " +
            " and (:#{#config.params['endTime']} IS NULL OR   date_format(config.create_time,'%Y-%m-%d %H:%m:%s') <= :#{#config.params['endTime']}) " +
            " and (:#{#config.pzmc} IS NULL OR config.pzmc like %:#{#config.pzmc}%)" +
            " and (:#{#config.pzkey} IS NULL OR  config.pzkey like  %:#{#config.pzkey}%)"+
            " and (:#{#config.pztype} IS NULL OR config.pztype = :#{#config.pztype}) order by id desc",nativeQuery = true)
    List<SysConfig> selectConfigList(@Param("config") SysConfig config);
}
