package com.taojin.admin.elastic.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
/**
 * 动动圈点赞
 * @author boren
 * @mapping 
 * PUT  /in_comment-like
{
  "settings": {
     "index":{
  		"number_of_shards":3,
  		"number_of_replicas":1
    	}	
  },
  "mappings":{
            "properties":{
              "dynamic_id":{
                    "type": "long"
                },
                "commont_id":{
                    "type": "long"
                },
                "reply_id":{
                    "type": "long"
                },
                "log_type":{
                    "type": "integer"
                },
                "user_id":{
                    "type": "long"
                }, 
                "liker_user_id":{
                    "type": "long"
                },
                "create_time":{
                    "type": "date"
                }
            }
    }
}
 *
 */
@Document(indexName = "in_comment-like", shards = 5, replicas = 0)
public class InCommentLike implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 文档ID
	 */
	@Id
	private String _id;
	
	/**
	 * 动态ID
	 */
	@Field(type = FieldType.Long)
	@ApiModelProperty(value = "动态ID", name = "dynamic_id", required = true)
	private Long dynamicId;
	
	
	/**
	 * 评论ID 
	 */
	@Field(type = FieldType.Long)
	@ApiModelProperty(value = "评论ID", name = "commontId", required = true)
	private Long commontId;
	
	/**
	 * 评论回复ID
	 */
	@Field(type = FieldType.Long)
	@ApiModelProperty(value = "评论回复ID", name = "replyId", required = true)
	private Long replyId;
	
	/**
	 * 该评论的类型(1: 主评论   2:回复评论)
	 */
	@Field(type = FieldType.Integer)
	@ApiModelProperty(value = "该评论的类型(1: 主评论   2:回复评论)", name = "logType", required = true)
	private Integer logType;
	
	/**
	 * 评论者ID
	 */
	@Field(type = FieldType.Long)
	@ApiModelProperty(value = "评论者ID", name = "userId", required = true)
	private Long  userId;
	
	/**
	 * 点赞者ID
	 */
	@Field(type = FieldType.Long)
	@ApiModelProperty(value = "点赞者ID", name = "likerUserId", required = true)
	private Long likerUserId;
	
	/**
	 * 点赞时间
	 */
	@Field(type = FieldType.Date)
	@CreatedDate
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public Long getDynamicId() {
		return dynamicId;
	}

	public void setDynamicId(Long dynamicId) {
		this.dynamicId = dynamicId;
	}

	public Long getCommontId() {
		return commontId;
	}

	public void setCommontId(Long commontId) {
		this.commontId = commontId;
	}

	public Long getReplyId() {
		return replyId;
	}

	public void setReplyId(Long replyId) {
		this.replyId = replyId;
	}

	public Integer getLogType() {
		return logType;
	}

	public void setLogType(Integer logType) {
		this.logType = logType;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getLikerUserId() {
		return likerUserId;
	}

	public void setLikerUserId(Long likerUserId) {
		this.likerUserId = likerUserId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "InCommentLike [_id=" + _id + ", dynamicId=" + dynamicId + ", commontId=" + commontId + ", replyId="
				+ replyId + ", logType=" + logType + ", userId=" + userId + ", likerUserId=" + likerUserId
				+ ", createTime=" + createTime + "]";
	}
	
	
	public InCommentLike(Long dynamicId, Long commontId, Long replyId, Integer logType, Long userId,
			Long likerUserId, Date createTime) {
		super();
		this.dynamicId = dynamicId;
		this.commontId = commontId;
		this.replyId = replyId;
		this.logType = logType;
		this.userId = userId;
		this.likerUserId = likerUserId;
		this.createTime = createTime;
	}

	public InCommentLike() {
	}
	
	


}
