package com.taojin.admin.entity.sys;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.taojin.admin.entity.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-26
*/
//@Entity
@Data
@Entity(name = "sys_user")
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
public class SysUser extends BaseEntity {

	/** 用户ID*/
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "user_id")
		private Long userId;

		/** 部门ID*/
		@JsonSerialize(using = ToStringSerializer.class)
		@Column(name = "dept_id")
		@Excel(name ="部门ID")
		private Long deptId;

		/** 用户账号*/
		@Column(name = "user_name")
		@Excel(name ="用户账号")
		private String userName;

		/** 用户昵称*/
		@Column(name = "nick_name")
		@Excel(name ="用户昵称")
		private java.lang.String nickName;

		/** 用户类型（00系统用户）*/
		@Column(name = "user_type")
		@Excel(name ="用户类型（00系统用户）")
		private java.lang.String userType;

		/** 用户邮箱*/
		@Column(name = "email")
		@Excel(name ="用户邮箱")
		@Email(message = "邮箱格式不正确")
		private java.lang.String email;

		/** 手机号码*/
		@Column(name = "phonenumber")
		@Excel(name ="手机号码")
		private java.lang.String phonenumber;

		/** 用户性别（0男 1女 2未知）*/
		@Column(name = "sex")
		@Excel(name ="用户性别（0男 1女 2未知）")
		private java.lang.String sex;

		/** 头像地址*/
		@Column(name = "avatar")
		@Excel(name ="头像地址")
		private java.lang.String avatar;

		/** 密码*/
		@Column(name = "password")
		@Excel(name ="密码")
		private java.lang.String password;

		/** 帐号状态（0正常 1停用）*/
		@Column(name = "status")
		@Excel(name ="帐号状态（0正常 1停用）")
		private java.lang.String status;

		/** 删除标志（0代表存在 2代表删除）*/
		@Column(name = "del_flag")
		@Excel(name ="删除标志（0代表存在 2代表删除）")
		private java.lang.String delFlag;

		/** 最后登录IP*/
		@Column(name = "login_ip")
		@Excel(name ="最后登录IP")
		private java.lang.String loginIp;

		/** 最后登录时间*/
		@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
		@Column(name = "login_date")
		@Excel(name ="最后登录时间")
		private java.util.Date loginDate;

		/** 备注*/
		@Column(name = "remark")
		@Excel(name ="备注")
		private java.lang.String remark;

		/** unionid*/
		@Column(name = "unionid")
		@Excel(name ="unionid")
		private java.lang.String unionid;

		/** 角色id*/
		@Transient
		private Long roleId;

		/** 角色组 */
		@Transient
		private Long[] roleIds;

		/** 岗位组 */
		@Transient
		private Long[] postIds;

}
