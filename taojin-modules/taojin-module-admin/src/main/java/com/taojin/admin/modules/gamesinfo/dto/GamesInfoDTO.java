package com.taojin.admin.modules.gamesinfo.dto;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.taojin.framework.config.JpaDto;
import io.swagger.annotations.ApiModelProperty;
import org.jeecgframework.poi.excel.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.format.annotation.DateTimeFormat;
import lombok.Data;

import javax.persistence.Column;


/**
 * 赛事信息 对象 games_info
 *
 * @author sujg
 * @date Mon Dec 18 13:44:04 CST 2023
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JpaDto
public class GamesInfoDTO
{
    private static final long serialVersionUID = 1L;


    /** ID 赛事ID */
    private Long id;

    /** 发布者ID 发布者ID ，对应 b_venue_authentication.id */
    @Excel(name = "发布者ID 发布者ID ，对应 b_venue_authentication.id")
    private Long bussId;

    /** 发布者名称 */
    @Excel(name = "发布者名称")
    private String bussNick;

    /** 赛事名称 */
    @Excel(name = "赛事名称")
    private String title;

    /** 运动类型 对应 games_sports_type.id */
    @Excel(name = "运动类型 对应 games_sports_type.id")
    private Integer sportsType;

    /** 赛事类型 10：校园联赛   20：企业联赛  30：民间联赛 40：官方联赛  50：机构联赛  60：业余联赛 */
    @Excel(name = "赛事类型 10：校园联赛   20：企业联赛  30：民间联赛 40：官方联赛  50：机构联赛  60：业余联赛")
    private Integer type;

    /** 赛制类型 赛制类型（10：单项赛   20：团体赛    30：单项+团体  40: 综合赛事） */
    @Excel(name = "赛制类型 赛制类型")
    private Integer ruleType;

    /** 是否公开 10: 公开    20： 不公开 */
    @Excel(name = "是否公开 10: 公开    20： 不公开")
    private Integer isPublic;

    /** 状态 状态（0：初始状态    10：待审核  15：审核失败  20：报名中   50：已结束    100:已经取消） */
    @Excel(name = "状态 状态")
    private Integer status;

    /** 主办单位 主办方，多个用英文逗号（,） 隔开 */
    @Excel(name = "主办单位 主办方，多个用英文逗号")
    private String hostOrg;

    /** 比赛地址 */
    @Excel(name = "比赛地址")
    private String address;

    /** 场馆名称 */
    @Excel(name = "场馆名称")
    private String venueName;

    /** 赛事头图 */
    @Excel(name = "赛事头图")
    private String headImgage;

    /** 地址经度 */
    @Excel(name = "地址经度")
    private BigDecimal addressLongitude;

    /** 地址纬度 */
    @Excel(name = "地址纬度")
    private BigDecimal addressLatitude;

    /** 比赛开始时间 */
    @Excel(name = "比赛开始时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /** 比赛结束时间 */
    @Excel(name = "比赛结束时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /** 报名开始时间 */
    @Excel(name = "报名开始时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date applyStartTime;

    /** 截止报名时间 */
    @Excel(name = "截止报名时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date stopTime;

    /** 名单公示开始时间 */
    @Excel(name = "名单公示开始时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date openListStartTime;

    /** 名单公示结束时间 */
    @Excel(name = "名单公示结束时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date openListStartEnd;

    /** 抽签公示开始时间 */
    @Excel(name = "抽签公示开始时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date openDrawTimeStart;

    /** 抽签公示结束时间 */
    @Excel(name = "抽签公示结束时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date openDrawTimeEnd;

    /** 单项赛兼项数量 */
    @Excel(name = "单项赛兼项数量")
    private Integer singleRepeat;

    /** 团体赛兼项数量 */
    @Excel(name = "团体赛兼项数量")
    private Integer groupRepeat;

    /** 是否允许退赛 10: 允许退赛    20：不允许退赛 */
    @Excel(name = "是否允许退赛 10: 允许退赛    20：不允许退赛")
    private Integer isExit;

    /** 退赛截止时间 */
    @Excel(name = "退赛截止时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date exitEndTime;

    /** 退赛处罚金额占比 百分制（50  =  50%） */
    @Excel(name = "退赛处罚金额占比 百分制")
    private Integer exitRatio;

    /** 单项赛参赛人员必填信息 对应报名信息字段表games_fields.id , 多个用逗号分开(eg: 1,34,23) */
    @Excel(name = "单项赛参赛人员必填信息 对应报名信息字段表games_fields.id , 多个用逗号分开(eg: 1,34,23)")
    private String singleFields;

    /** 团体赛参赛人员必填信息 对应报名信息字段表games_fields.id , 多个用逗号分开(eg: 1,34,23) */
    @Excel(name = "团体赛参赛人员必填信息 对应报名信息字段表games_fields.id , 多个用逗号分开(eg: 1,34,23)")
    private String groupFields;

    /** 0: 正常    1：删除 */
    @Excel(name = "0: 正常    1：删除")
    private Integer isDel;

    /** 创建时间 */
    private Date createTime;

    /** 更新时间 */
    private Date updateTime;


    /** 直播房间号 对应目睹直播的房间号 */
    private String playNumber;

    @ApiModelProperty(value = "赛事直播地址")
    private String palyUrl;


    /** 审核备注 */
    @ApiModelProperty(value = "审核备注 审核不通过时必填")
    private String auditNotes;



    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Map<String, Object> params = new HashMap<>();


    public GamesInfoDTO(Long id, String bussNick, String title, Integer ruleType, Integer status, String headImgage, Date startTime, Date endTime, String playNumber) {
        this.id = id;
        this.bussNick = bussNick;
        this.title = title;
        this.ruleType = ruleType;
        this.status = status;
        this.headImgage = headImgage;
        this.startTime = startTime;
        this.endTime = endTime;
        this.playNumber = playNumber;
    }
}
