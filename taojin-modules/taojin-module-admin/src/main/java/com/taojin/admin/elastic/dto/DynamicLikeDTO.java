package com.taojin.admin.elastic.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

public class DynamicLikeDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@ApiModelProperty(value = "动态Id集合，eg: [1,33,32,443]")
	private List<Long> dynamicsId;

	public List<Long> getDynamicsId() {
		return dynamicsId;
	}

	public void setDynamicsId(List<Long> dynamicsId) {
		this.dynamicsId = dynamicsId;
	}
}
