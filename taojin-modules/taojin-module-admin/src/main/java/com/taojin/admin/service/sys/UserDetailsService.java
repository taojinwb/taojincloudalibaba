package com.taojin.admin.service.sys;

import com.taojin.admin.dto.LoginUser;
import com.taojin.admin.dto.sys.SysUserDTO;
import com.taojin.admin.framework.constant.CacheConstant;
import com.taojin.admin.framework.util.JwtUtil;
import com.taojin.framework.enums.UserStatus;
import com.taojin.framework.exception.ServiceException;
import com.taojin.framework.redis.RedisUtil;
import com.taojin.framework.utils.BF;
import com.taojin.framework.utils.MessageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 用户验证处理
 *
 * @author sujg
 */
@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService
{
    private static final Logger log = LoggerFactory.getLogger(UserDetailsService.class);
    @Autowired
    private TokenService tokenService;

    @Autowired
    private SysUserService userService;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private SysPasswordService passwordService;

    @Autowired
    private SysPermissionService permissionService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
             SysUserDTO sysUserJPADTO = new SysUserDTO();
              sysUserJPADTO.setUserName(username);
            SysUserDTO user = userService.selectUserByUserName(sysUserJPADTO);
            if (user == null)
            {
                log.info("登录用户：{} 不存在.", username);
                throw new ServiceException(MessageUtils.message("user.not.exists"));
            }
            else if (BF.APPCompare(UserStatus.DELETED.getCode(),"=",user.getDelFlag()))
            {
                log.info("登录用户：{} 已被删除.", username);
                throw new ServiceException(MessageUtils.message("user.password.delete"));
            }
            else if (BF.APPCompare(UserStatus.DISABLE.getCode(),"=",user.getStatus()))
            {
                log.info("登录用户：{} 已被停用.", username);
                throw new ServiceException(MessageUtils.message("user.blocked"));
            }

        passwordService.validate(user);

        return createLoginUser(user);
    }

    public UserDetails createLoginUser(SysUserDTO user)
    {
        // 生成token,使用用户名进行签名和验证。方便后面解密。。。转到该方法会去调用UserDetailsService 生成。

        LoginUser loginUser = new LoginUser(user.getUserId(), user.getDeptId(),user.getUnionid(), user, permissionService.getMenuPermission(user));

        String token = tokenService.createToken(loginUser);// JwtUtil.sign(loginUser.getUsername(), loginUser.getUsername());
        redisUtil.set(CacheConstant.LOGIN_TOKEN_KEY+token,loginUser,JwtUtil.EXPIRE_TIME);
        redisUtil.set(token,loginUser.getUsername(),JwtUtil.EXPIRE_TIME);//仅仅为了调用C端使用，免登录
        redisUtil.set(CacheConstant.LOGIN_UNIONID_TOKEN_KEY+token,loginUser.getUnionid(),JwtUtil.EXPIRE_TIME);
        loginUser.setToken(token);
        return loginUser;
    }
}
