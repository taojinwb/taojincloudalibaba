package com.taojin.admin.service.sys;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import com.taojin.admin.dto.sys.SysUserDTO;
import com.taojin.admin.framework.common.BaseService;
import com.taojin.admin.entity.sys.SysDept;
import com.taojin.admin.entity.sys.SysRole;
import com.taojin.admin.dto.sys.SysDeptDTO;
import com.taojin.admin.framework.security.SecurityUtils;
import com.taojin.admin.framework.vo.TreeSelect;
import com.taojin.admin.repository.sys.SysDeptRepository;
import com.taojin.admin.repository.sys.SysRoleRepository;
import com.taojin.framework.utils.ConvertUtil;
import com.taojin.framework.utils.MF;
import com.taojin.framework.utils.SpringContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import com.taojin.framework.constant.UserConstants;
import com.taojin.framework.exception.ServiceException;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * 部门管理 服务实现
 *
 * @author sujg
 */
@Service
@Slf4j
public class SysDeptService extends BaseService<SysDept,Long>
{
    @Autowired
    private SysDeptRepository sysDeptRepository;

    @Autowired
    private SysRoleRepository sysRoleRepository;

    public SysDeptService(SysDeptRepository repository) {
        super(repository,repository);
    }

    /**
     * 查询部门管理数据
     *
     * @param dept 部门信息
     * @return 部门信息集合
     */
    public List<SysDeptDTO> selectDeptList(SysDept dept)
    {
        Example<SysDept> example = Example.of(dept);
        Sort.Order order1 = Sort.Order.asc("parentId"); // 第一个排序条件：按 order_num 升序排序
        Sort.Order order2 = Sort.Order.asc("orderNum"); // 第二个排序条件：按 createDate 降序排序
        Sort sort = Sort.by(order1, order2);

        List<SysDept> list= sysDeptRepository.findAll(filterByFields(dept),sort);

        List<SysDeptDTO> dtoList = new ArrayList<>();
        SysDeptDTO  sysDeptDTO ;
        for(SysDept sd :list){
            sysDeptDTO = new SysDeptDTO();
            BeanUtils.copyProperties(sd,sysDeptDTO);
            dtoList.add(sysDeptDTO);
        }
        return dtoList;
    }

    /**
     * 查询部门树结构信息
     *
     * @param dept 部门信息
     * @return 部门树信息集合
     */
    public List<TreeSelect> selectDeptTreeList(SysDept dept)
    {
        List<SysDeptDTO> depts = SpringContextUtils.getAopProxy(this).selectDeptList(dept);


        return buildDeptTreeSelect(depts);
    }

    /**
     * 构建前端所需要树结构
     *
     * @param depts 部门列表
     * @return 树结构列表
     */
    public List<SysDeptDTO> buildDeptTree(List<SysDeptDTO> depts)
    {

        List<SysDeptDTO> returnList = new ArrayList<>();
        List<Long> tempList = depts.stream().map(SysDeptDTO::getDeptId).collect(Collectors.toList());
        for (SysDeptDTO dept : depts)
        {
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(dept.getParentId()))
            {
                recursionFn(depts, dept);
                returnList.add(dept);
            }
        }
        if (returnList.isEmpty())
        {
            returnList = depts;
        }
        return returnList;
    }

    /**
     * 构建前端所需要下拉树结构
     *
     * @param depts 部门列表
     * @return 下拉树结构列表
     */
    public List<TreeSelect> buildDeptTreeSelect(List<SysDeptDTO> depts)
    {
        List<SysDeptDTO> deptTrees = buildDeptTree(depts);
        return deptTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
    }

    /**
     * 根据角色ID查询部门树信息
     *
     * @param roleId 角色ID
     * @return 选中部门列表
     */
    public List<Long> selectDeptListByRoleId(Long roleId)
    {
        SysRole role = this.sysRoleRepository.selectRoleById(roleId);

        return this.sysDeptRepository.selectDeptListByRoleId(roleId);//, role.isDeptCheckStrictly()?1:0
    }

    /**
     * 根据部门ID查询信息
     *
     * @param deptId 部门ID
     * @return 部门信息
     */
    public SysDept selectDeptById(Long deptId)
    {
        return this.sysDeptRepository.findById(deptId).orElse(null);
    }

    /**
     * 根据ID查询所有子部门（正常状态）
     *
     * @param deptId 部门ID
     * @return 子部门数
     */
    public int selectNormalChildrenDeptById(Long deptId)
    {
        return sysDeptRepository.selectNormalChildrenDeptById(deptId);
    }

    /**
     * 是否存在子节点
     *
     * @param deptId 部门ID
     * @return 结果
     */
    public boolean hasChildByDeptId(Long deptId)
    {
        int result = sysDeptRepository.hasChildByDeptId(deptId);
        return result > 0;
    }

    /**
     * 查询部门是否存在用户
     *
     * @param deptId 部门ID
     * @return 结果 true 存在 false 不存在
     */
    public boolean checkDeptExistUser(Long deptId)
    {
        String delflag="0";
        SysDept sd = sysDeptRepository.findAllByDeptIdAndDelFlag(deptId,delflag);

        return sd==null?false:true;
    }
    /**
     * 校验部门名称是否唯一
     *
     * @param dept 部门信息
     * @return 结果
     */
    public boolean checkDeptNameUnique(SysDept dept)
    {
        Long deptId = MF.isNull(dept.getDeptId()) ? -1L : dept.getDeptId();
        SysDept info = sysDeptRepository.findFirstByDeptNameAndParentIdAndDelFlag(dept.getDeptName(), dept.getParentId(),0L);
        if (MF.isNotNull(info) && info.getDeptId().longValue() != deptId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }


    /**
     * 校验部门是否有数据权限
     *
     * @param deptId 部门id
     */
    public void checkDeptDataScope(Long deptId)
    {
        if (!SysUserDTO.isAdmin(SecurityUtils.getUserId()))
        {
            SysDept dept = new SysDept();
            dept.setDeptId(deptId);
            List<SysDeptDTO> depts = SpringContextUtils.getAopProxy(this).selectDeptList(dept);
            if (MF.isEmpty(depts))
            {
                throw new ServiceException("没有权限访问部门数据！");
            }
        }
    }

    /**
     * 新增保存部门信息
     *
     * @param dept 部门信息
     */
    public void insertDept(SysDept dept)
    {
        SysDeptDTO info = this.sysDeptRepository.selectDeptById(dept.getParentId());
        // 如果父节点不为正常状态,则不允许新增子节点
        if (!UserConstants.DEPT_NORMAL.equals(info.getStatus()))
        {
            throw new ServiceException("部门停用，不允许新增");
        }
        dept.setAncestors(info.getAncestors() + "," + dept.getParentId());
        dept.setDelFlag("0");
        this.sysDeptRepository.save(dept);
    }

    /**
     * 修改保存部门信息
     *
     * @param dept 部门信息
     */
    @Transactional
    public void updateDept(SysDept dept)
    {
        SysDeptDTO newParentDept = this.sysDeptRepository.selectDeptById(dept.getParentId());
        SysDeptDTO oldDept = sysDeptRepository.selectDeptById(dept.getDeptId());
        if (MF.isNotNull(newParentDept) && MF.isNotNull(oldDept))
        {
            String newAncestors = newParentDept.getAncestors() + "," + newParentDept.getDeptId();
            String oldAncestors = oldDept.getAncestors();
            dept.setAncestors(newAncestors);
            updateDeptChildren(dept.getDeptId(), newAncestors, oldAncestors);
        }
         this.sysDeptRepository.save(dept);
        if (UserConstants.DEPT_NORMAL.equals(dept.getStatus()) && MF.isNotEmpty(dept.getAncestors())
                && !StringUtils.equals("0", dept.getAncestors()))
        {
            // 如果该部门是启用状态，则启用该部门的所有上级部门
            updateParentDeptStatusNormal(dept);
        }
    }

    /**
     * 修改该部门的父级部门状态
     *
     * @param dept 当前部门
     */
    private void updateParentDeptStatusNormal(SysDept dept)
    {
        String ancestors = dept.getAncestors();
        Long[] deptIds = ConvertUtil.toLongArray(ancestors);
        this.sysDeptRepository.updateStatusForDeptIds(deptIds);
    }

    /**
     * 修改子元素关系
     *
     * @param deptId 被修改的部门ID
     * @param newAncestors 新的父ID集合
     * @param oldAncestors 旧的父ID集合
     */
    public void updateDeptChildren(Long deptId, String newAncestors, String oldAncestors)
    {
        List<SysDept> children = this.sysDeptRepository.selectChildrenDeptById(deptId);
        for (SysDept child : children)
        {
            child.setAncestors(child.getAncestors().replaceFirst(oldAncestors, newAncestors));
        }
        if (children.size() > 0)
        {
            updateDeptChildren(children);
        }
    }
    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void updateDeptChildren(List<SysDept> depts) {
        for (SysDept dept : depts) {
            String ancestors = dept.getAncestors();
            String sql = "UPDATE sys_dept SET ancestors = (' " +ancestors+"  ')  WHERE dept_id = " + dept.getDeptId();
            entityManager.createNativeQuery(sql).executeUpdate();
        }
    }





    /**
     * 删除部门管理信息
     *
     * @param deptId 部门ID
     */
    public void deleteDeptById(Long deptId) throws Exception {
        SysDept sysDept = sysDeptRepository.findById(deptId).get();
        if("1".equals(sysDept.getStatus())){
            this.sysDeptRepository.deleteById(deptId);
        }else {
            throw new Exception("已启用的部门不能删除");
        }

    }

    /**
     * 递归列表
     */
    private void recursionFn(List<SysDeptDTO> list, SysDeptDTO t)
    {
        // 得到子节点列表
        List<SysDeptDTO> childList = getChildList(list, t);
        t.setChildren(childList);
        for (SysDeptDTO tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<SysDeptDTO> getChildList(List<SysDeptDTO> list, SysDeptDTO t)
    {
        List<SysDeptDTO> tlist = new ArrayList<SysDeptDTO>();
        Iterator<SysDeptDTO> it = list.iterator();
        while (it.hasNext())
        {
            SysDeptDTO n = (SysDeptDTO) it.next();
            if (MF.isNotNull(n.getParentId()) && n.getParentId().longValue() == t.getDeptId().longValue())
            {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<SysDeptDTO> list, SysDeptDTO t)
    {
        return getChildList(list, t).size() > 0;
    }


    /**
     * 查询部门下面是否有人
     * @param deptId
     * @return
     */
    public boolean selectDeptNumber(Long deptId)
    {
        return sysDeptRepository.selectDeptNumber(deptId) >0?true:false;
    }


    /**
     * 判断部门是否停用了
     * @param deptId
     * @return
     */
    public Boolean findByDeptId(Long deptId){
        SysDept sysDept = sysDeptRepository.findByDeptId(deptId);
        return sysDept.getStatus().equals("1")?true:false;
    }
}
