package com.taojin.admin.modules.gamesinfo.constant;

/**
 *  权限资源常量
 */
public interface PermissionConstant {


    /** 企业管理 权限组前缀 */
    String AUTH_GROUP = "AUTH";

    /** 企业管理 员工 审核 */
    String AUTH_USER_AUDIT = AUTH_GROUP + ":USER:AUDIT";
    /** 企业管理 员工 编辑 */
    String AUTH_USER_UPDATE = AUTH_GROUP + ":USER:UPDATE";
    /** 企业管理 员工 状态修改 */
    String AUTH_USER_UPDATE_STATE = AUTH_GROUP + ":USER:UPDATE_STATE";

    /** 企业管理 角色 新增 */
    String AUTH_ROLE_INSERT = AUTH_GROUP + ":ROLE:INSERT";
    /** 企业管理 角色 删除 */
    String AUTH_ROLE_DELETE = AUTH_GROUP + ":ROLE:DELETE";

    /** 企业管理 角色权限 修改 */
    String AUTH_ROLE_PERMISSION_UPDATE = AUTH_GROUP + ":ROLE_PERMISSION:UPDATE";



    /** 软件服务管理 权限组前缀 */
    String SAAS_SERVICE_GROUP = "SAAS";

    /** 软件服务管理 企业服务 修改信息 */
    String SAAS_AUTH_UPDATE = SAAS_SERVICE_GROUP + ":AUTH:UPDATE";
    /** 软件服务管理 企业服务 修改价格 */
    String SAAS_AUTH_UPDATE_PRICE = SAAS_SERVICE_GROUP + ":AUTH:UPDATE_PRICE";

    /** 软件服务管理 费用资源 新增 */
    String SAAS_SOURCE_INSERT = SAAS_SERVICE_GROUP + ":SOURCE:INSERT";
    /** 软件服务管理 费用资源 编辑 */
    String SAAS_SOURCE_UPDATE = SAAS_SERVICE_GROUP + ":SOURCE:UPDATE";
    /** 软件服务管理 费用资源 删除 */
    String SAAS_SOURCE_DELETE = SAAS_SERVICE_GROUP + ":SOURCE:DELETE";



    /** 硬件管理 权限组前缀 */
    String HDW_GROUP = "HDW";

    /** 硬件管理 硬件设备 新增 */
    String HDW_DEVICE_INSERT = HDW_GROUP + ":DEVICE:INSERT";
    /** 硬件管理 硬件设备 编辑 */
    String HDW_DEVICE_UPDATE = HDW_GROUP + ":DEVICE:UPDATE";
    /** 硬件管理 硬件设备 删除 */
    String HDW_DEVICE_DELETE = HDW_GROUP + ":DEVICE:DELETE";
    /** 硬件管理 硬件设备 生成应急码 */
    String HDW_DEVICE_URGENCY = HDW_GROUP + ":DEVICE:URGENCY";


    /** 硬件管理 硬件类型 新增 */
    String HDW_DEVICE_TYPE_INSERT = HDW_GROUP + ":DEVICE_TYPE:INSERT";
    /** 硬件管理 硬件类型 编辑 */
    String HDW_DEVICE_TYPE_UPDATE = HDW_GROUP + ":DEVICE_TYPE:UPDATE";
    /** 硬件管理 硬件类型 删除 */
    String HDW_DEVICE_TYPE_DELETE = HDW_GROUP + ":DEVICE_TYPE:DELETE";



    /** 商城管理 权限组前缀 */
    String SHOP_GROUP = "SHOP";

    /** 商城管理 店铺 修改状态 */
    String SHOP_STORE_UPDATE_STATE = SHOP_GROUP + ":STORE:UPDATE_STATE";
    /** 商城管理 店铺 绑定品牌 */
    String SHOP_STORE_BRAND_BIND = SHOP_GROUP + ":STORE:BRAND_BIND";
    /** 商城管理 店铺 解绑品牌 */
    String SHOP_STORE_BRAND_UNBIND = SHOP_GROUP + ":STORE:BRAND_UNBIND";

    /** 商城管理 类目 新增 */
    String SHOP_CATEGORY_INSERT = SHOP_GROUP + ":CATEGORY:INSERT";
    /** 商城管理 类目 修改 */
    String SHOP_CATEGORY_UPDATE = SHOP_GROUP + ":CATEGORY:UPDATE";
    /** 商城管理 类目 修改状态 */
    String SHOP_CATEGORY_UPDATE_STATE = SHOP_GROUP + ":CATEGORY:UPDATE_STATE";
    /** 商城管理 类目 删除 */
    String SHOP_CATEGORY_DELETE = SHOP_GROUP + ":CATEGORY:DELETE";

    /** 商城管理 规格 新增 */
    String SHOP_SPEC_INSERT = SHOP_GROUP + ":SPEC:INSERT";
    /** 商城管理 规格 修改 */
    String SHOP_SPEC_UPDATE = SHOP_GROUP + ":SPEC:UPDATE";
    /** 商城管理 规格 修改状态 */
    String SHOP_SPEC_UPDATE_STATE = SHOP_GROUP + ":SPEC:UPDATE_STATE";
    /** 商城管理 规格 删除 */
    String SHOP_SPEC_DELETE = SHOP_GROUP + ":SPEC:DELETE";

    /** 商城管理 品牌 新增 */
    String SHOP_BRAND_INSERT = SHOP_GROUP + ":BRAND:INSERT";
    /** 商城管理 品牌 修改 */
    String SHOP_BRAND_UPDATE = SHOP_GROUP + ":BRAND:UPDATE";
    /** 商城管理 品牌 修改状态 */
    String SHOP_BRAND_UPDATE_STATE = SHOP_GROUP + ":BRAND:UPDATE_STATE";
    /** 商城管理 品牌 删除 */
    String SHOP_BRAND_DELETE = SHOP_GROUP + ":BRAND:DELETE";

    /** 商城管理 banner 新增 */
    String SHOP_BANNER_INSERT = SHOP_GROUP + ":BANNER:INSERT";
    /** 商城管理 banner 修改 */
    String SHOP_BANNER_UPDATE = SHOP_GROUP + ":BANNER:UPDATE";
    /** 商城管理 banner 修改状态 */
    String SHOP_BANNER_UPDATE_STATE = SHOP_GROUP + ":BANNER:UPDATE_STATE";
    /** 商城管理 banner 修改权值 */
    String SHOP_BANNER_UPDATE_WEIGHT = SHOP_GROUP + ":BANNER:UPDATE_WEIGHT";
    /** 商城管理 banner 删除 */
    String SHOP_BANNER_DELETE = SHOP_GROUP + ":BANNER:DELETE";

    /** 商城管理 商品推荐 新增 */
    String SHOP_RECOMMEND_INSERT = SHOP_GROUP + ":RECOMMEND:INSERT";
    /** 商城管理 商品推荐 修改类型 */
    String SHOP_RECOMMEND_UPDATE = SHOP_GROUP + ":RECOMMEND:UPDATE";
    /** 商城管理 商品推荐 修改状态 */
    String SHOP_RECOMMEND_UPDATE_STATE = SHOP_GROUP + ":RECOMMEND:UPDATE_STATE";
    /** 商城管理 商品推荐 删除 */
    String SHOP_RECOMMEND_DELETE = SHOP_GROUP + ":RECOMMEND:DELETE";

    /** 商城管理 分销达人 修改 */
    String SHOP_SALE_UPDATE = SHOP_GROUP + ":SALE:UPDATE";



    /** 赛事管理 权限组前缀 */
    String GAME_GROUP = "GAME";

    /** 赛事管理 赛事 审核 */
    String GAME_AUDIT = GAME_GROUP + ":AUDIT";
    /** 赛事管理 赛事 删除 */
    String GAME_DELETE = GAME_GROUP + ":DELETE";

    /** 赛事管理 直播间 修改 */
    String GAME_LIVE_UPDATE = GAME_GROUP + ":LIVE:UPDATE";
    /** 赛事管理 直播间 审核 */
    String GAME_LIVE_AUDI = GAME_GROUP + ":LIVE:AUDI";
    /** 赛事管理 直播间 删除 */
    String GAME_LIVE_DELETE = GAME_GROUP + ":LIVE:DELETE";



    /** 小程序管理 权限组前缀 */
    String APP_GROUP = "APP";

    /** 小程序管理 banner 新增 */
    String APP_BANNER_INSERT = APP_GROUP + ":BANNER:INSERT";
    /** 小程序管理 banner 修改 */
    String APP_BANNER_UPDATE = APP_GROUP + ":BANNER:UPDATE";
    /** 小程序管理 banner 修改状态 */
    String APP_BANNER_UPDATE_STATE = APP_GROUP + ":BANNER:UPDATE_STATE";
    /** 小程序管理 banner 修改权值 */
    String APP_BANNER_UPDATE_WEIGHT = APP_GROUP + ":BANNER:UPDATE_WEIGHT";
    /** 小程序管理 banner 删除 */
    String APP_BANNER_DELETE = APP_GROUP + ":BANNER:DELETE";

    /** 小程序管理 运营位 新增 */
    String APP_OPERATION_INSERT = APP_GROUP + ":OPERATION:INSERT";
    /** 小程序管理 运营位 修改 */
    String APP_OPERATION_UPDATE = APP_GROUP + ":OPERATION:UPDATE";
    /** 小程序管理 运营位 修改状态 */
    String APP_OPERATION_UPDATE_STATE = APP_GROUP + ":OPERATION:UPDATE_STATE";
    /** 小程序管理 运营位 删除 */
    String APP_OPERATION_DELETE = APP_GROUP + ":OPERATION:DELETE";

    /** 小程序管理 优惠券 新增 */
    String APP_DISCOUNT_INSERT = APP_GROUP + ":DISCOUNT:INSERT";
    /** 小程序管理 优惠券 修改 */
    String APP_DISCOUNT_UPDATE = APP_GROUP + ":DISCOUNT:UPDATE";
    /** 小程序管理 优惠券 修改状态 */
    String APP_DISCOUNT_UPDATE_STATE = APP_GROUP + ":DISCOUNT:UPDATE_STATE";
    /** 小程序管理 优惠券 删除 */
    String APP_DISCOUNT_DELETE = APP_GROUP + ":DISCOUNT:DELETE";
    /** 小程序管理 优惠券 发送指定用户 */
    String APP_DISCOUNT_SEND_USER = APP_GROUP + ":DISCOUNT:SEND_USER";
    /** 小程序管理 优惠组合券 新增 */
    String APP_DISCOUNT_GROUP_INSERT = APP_GROUP + ":DISCOUNT_GROUP:INSERT";
    /** 小程序管理 优惠组合券 删除 */
    String APP_DISCOUNT_GROUP_DELETE = APP_GROUP + ":DISCOUNT_GROUP:DELETE";

    /** 小程序管理 会员权益 新增 */
    String APP_MEMBER_DISCOUNT_INSERT = APP_GROUP + ":MEMBER_DISCOUNT:INSERT";
    /** 小程序管理 会员权益 修改 */
    String APP_MEMBER_DISCOUNT_UPDATE = APP_GROUP + ":MEMBER_DISCOUNT:UPDATE";
    /** 小程序管理 会员权益 修改状态 */
    String APP_MEMBER_DISCOUNT_UPDATE_STATE = APP_GROUP + ":MEMBER_DISCOUNT:UPDATE_STATE";
    /** 小程序管理 会员权益 修改权值 */
    String APP_MEMBER_DISCOUNT_UPDATE_WEIGHT = APP_GROUP + ":MEMBER_DISCOUNT:UPDATE_WEIGHT";
    /** 小程序管理 会员权益 删除 */
    String APP_MEMBER_DISCOUNT_DELETE = APP_GROUP + ":MEMBER_DISCOUNT:DELETE";

    /** 小程序管理 会员卡 新增 */
    String APP_MEMBER_CARD_INSERT = APP_GROUP + ":MEMBER_CARD:INSERT";
    /** 小程序管理 会员卡 修改 */
    String APP_MEMBER_CARD_UPDATE = APP_GROUP + ":MEMBER_CARD:UPDATE";
    /** 小程序管理 会员卡 修改状态 */
    String APP_MEMBER_CARD_UPDATE_STATE = APP_GROUP + ":MEMBER_CARD:UPDATE_STATE";
    /** 小程序管理 会员卡 删除 */
    String APP_MEMBER_CARD_DELETE = APP_GROUP + ":MEMBER_CARD:DELETE";
    /** 小程序管理 会员卡 退卡 */
    String APP_MEMBER_CARD_REFUND = APP_GROUP + ":MEMBER_CARD:REFUND";

    /** 小程序管理 弹窗 新增 */
    String APP_POPUP_WINDOW_INSERT = APP_GROUP + ":POPUP_WINDOW:INSERT";
    /** 小程序管理 弹窗 修改 */
    String APP_POPUP_WINDOW_UPDATE = APP_GROUP + ":POPUP_WINDOW:UPDATE";
    /** 小程序管理 弹窗 删除 */
    String APP_POPUP_WINDOW_DELETE = APP_GROUP + ":POPUP_WINDOW:DELETE";


    /** 商户管理 权限组前缀 */
    String TENANT_GROUP = "TENANT";

    /** 商户管理 企业入驻 审核 */
    String TENANT_AUTH_ENTER_AUDIT = TENANT_GROUP + ":AUTH_ENTER:AUDIT";
    /** 商户管理 企业入驻 注销 */
    String TENANT_AUTH_ENTER_CANCEL = TENANT_GROUP + ":AUTH_ENTER:CANCEL";

    /** 商户管理 企业服务开通 审核 */
    String TENANT_AUTH_SERVICE_AUDIT = TENANT_GROUP + ":AUTH_SERVICE:AUDIT";
    /** 商户管理 企业账户 修改 */
    String TENANT_AUTH_ACCOUNT_UPDATE = TENANT_GROUP + ":AUTH_ACCOUNT:UPDATE";

    /** 商户管理 合作模式 修改 */
    String TENANT_MODEL_UPDATE = TENANT_GROUP + ":MODEL:AUDIT";

    /** 商户管理 直播间 修改 */
    String TENANT_LIVE_UPDATE = TENANT_GROUP + ":LIVE:AUDIT";

    /** 商户管理 场馆信息 修改 */
    String TENANT_VENUE_UPDATE = TENANT_GROUP + ":VENUE:UPDATE";
    /** 商户管理 场馆信息 审核 */
    String TENANT_VENUE_AUDIT = TENANT_GROUP + ":VENUE:AUDIT";
    /** 商户管理 场馆信息 修改支付宝入驻 */
    String TENANT_VENUE_UPDATE_ALIPAY = TENANT_GROUP + ":VENUE:AUDIT_ALIPAY";

    /** 商户管理 提现申请 审核 */
    String TENANT_WITHDRAW_AUDIT = TENANT_GROUP + ":WITHDRAW:AUDIT";



    // 其他的一些权限
    /** 合伙人 修改信息 */
    String PARTNER_UPDATE = "PARTNER:UPDATE";

    /** 合伙人 修改状态 */
    String PARTNER_UPDATE_STATE = "PARTNER:UPDATE_STATE";

    /** 活动 下架 */
    String ACTIVITY_UPDATE_DOWN_SHELF = "ACTIVITY:DOWN_SHELF";

    /** 俱乐部 下架 */
    String CLUB_UPDATE_DOWN_SHELF = "CLUB:DOWN_SHELF";
    /** 俱乐部 审核 */
    String CLUB_AUDI = "CLUB:AUDI";

    /** 订单 投诉处理 */
    String ORDER_COMPLAINT = "ORDER:COMPLAINT:UPDATE";
}
