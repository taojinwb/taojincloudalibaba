package com.taojin.admin.generator.service;

import java.util.List;

import cn.hutool.core.convert.Convert;
import com.taojin.admin.generator.entity.GenTableColumn;
import com.taojin.admin.generator.repository.GenTableColumnRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 业务字段 服务层实现
 *
 * @author taojin
 */
@Service
public class GenTableColumnService
{
	@Autowired
	private GenTableColumnRepository genTableColumnRepository;

	/**
     * 查询业务字段列表
     *
     * @param tableId 业务字段编号
     * @return 业务字段集合
     */
	public List<GenTableColumn> selectGenTableColumnListByTableId(Long tableId)
	{
	    return genTableColumnRepository.selectGenTableColumnListByTableId(tableId);
	}

    /**
     * 新增业务字段
     *
     * @param  业务字段信息
     * @return 结果
     */
	public void insertGenTableColumn(GenTableColumn genTableColumn)
	{
	     genTableColumnRepository.save(genTableColumn);
	}

	/**
     * 修改业务字段
     *
     * @param  业务字段信息
     * @return 结果
     */
	public void updateGenTableColumn(GenTableColumn genTableColumn)
	{
	     genTableColumnRepository.save(genTableColumn);
	}

	/**
     * 删除业务字段对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public void deleteGenTableColumnByIds(String ids)
	{
		for(String id :ids.split(",")) {

			genTableColumnRepository.deleteById(cn.hutool.core.convert.Convert.toLong(id));
		}
	}
}
