package com.taojin.admin.elastic.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SourceFilter;
import org.springframework.stereotype.Service;

import com.taojin.admin.elastic.entity.ClogsPro;
import com.taojin.admin.elastic.vo.activitesShowVo;

/**
 * 索引  c_logs-pro
 * @author boren
 * @doc  https://blog.csdn.net/YINJOER/article/details/108653911
 * https://blog.csdn.net/SimpleSimpleSimples/article/details/133420445?ops_request_misc=&request_id=&biz_id=102&utm_term=kibana%20%E8%AF%A6%E7%BB%86%E6%95%99%E7%A8%8B&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduweb~default-1-133420445.142^v100^pc_search_result_base2&spm=1018.2226.3001.4187
 *PUT /my_index
 * {
 *   "settings": {
 *     "index": {
 *       "refresh_interval": "1s",
 *       "number_of_shards": 1,
 *       "max_result_window": 10000,
 *       "number_of_replicas": 0
 *     }
 *   },
 *   "mappings": {
 *     "properties": {
 *       "title": { "type": "text" },
 *       "content": { "type": "text" },
 *       "timestamp": { "type": "date" }
 *     }
 *   }
 * }*
 *
 */

@Service
public class ClogProService {
	
	 private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
    private ElasticsearchRestTemplate elasticsearchTemplate;

	
	/**
	 * 查询活动浏览量
	 * @param activitesId 活动ID
	 * { 
		   "query" : {
		            "bool" : {
		              "must": [
		                 { "term" : {"path1.keyword": "api-c"}}, 
		                 { "term" : {"path2.keyword": "activities"}}, 
		                 { "term" : {"path3.keyword": "selectActivity"}}, 
		                 { "term" : {"path4.keyword": "7021"}} 
		              ]
		           }
		   }, 
		   "_source": [
		    "uri",
		    "path4"
		  	],
		  "size":1
		}
	 */
	public activitesShowVo getActivites(long activitesId)
	{
		NativeSearchQueryBuilder search = new NativeSearchQueryBuilder();
		//返回一条数据即可
		search.withPageable(PageRequest.of(0,1));
		search.withSourceFilter(new SourceFilter() {
            @Override
            public String[] getIncludes() {
            	//需要返回的字段
                return new String[]{"uri","path1","path2","path3","path4","user_id"};
            }

			@Override
			public String[] getExcludes() {
				// TODO Auto-generated method stub
				return null;
			}
        });

		
		BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
		
		boolQuery.must(QueryBuilders.termQuery("path1","api-c"));
//		boolQuery.must(QueryBuilders.termQuery("path2.keyword","activities"));
//		boolQuery.must(QueryBuilders.termQuery("path3.keyword","selectActivity"));;
//		boolQuery.must(QueryBuilders.termQuery("path4.keyword",activitesId+""));
//
		logger.info("=--"+boolQuery.toString());
		search.withQuery(boolQuery);
		
		
		
		activitesShowVo result = new activitesShowVo();
		
		try {
			
			SearchHits<ClogsPro> searchHits = elasticsearchTemplate.search(search.build(), ClogsPro.class);
			
			logger.info("searchHits = {}", searchHits);
			
			if(searchHits.getSearchHits().size() >0)
			{
				logger.info("search clog = {}",searchHits.getSearchHit(0).getContent().toString());
			}
			
			result.setId(activitesId);
			result.setTotal(searchHits.getTotalHits());
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//异常处理
			result.setId(activitesId);
			result.setTotal(0);
			
			logger.info("search error = {}",e.getMessage());
			//e.printStackTrace();
		}
		
		
		return result;
		
	}
	/**
	 * 批量查询活动浏览量
	 * @param activites
	 */
	public List<activitesShowVo> getActivitesForIds(List<Long> activites)
	{
		NativeSearchQueryBuilder search = new NativeSearchQueryBuilder();
		//返回一条数据即可
		search.withPageable(PageRequest.of(0,1));
		search.withSourceFilter(new SourceFilter() {
            @Override
            public String[] getIncludes() {
            	//需要返回的字段
                return new String[]{"uri","path1","path2","path3","path4","user_id"};
            }

			@Override
			public String[] getExcludes() {
				// TODO Auto-generated method stub
				return null;
			}
        });

		
		BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
		boolQuery.must(QueryBuilders.termQuery("path1.keyword","api-c"));
		boolQuery.must(QueryBuilders.termQuery("path2.keyword","activities"));
		boolQuery.must(QueryBuilders.termQuery("path3.keyword","selectActivity"));
		boolQuery.must(QueryBuilders.termsQuery("path4.keyword", activites));
		
		search.withQuery(boolQuery);
		//分组统计
		search.addAggregation(AggregationBuilders.terms("activites").field("path4.keyword"));
		
		//分组数据结果
		List<activitesShowVo> aggResults = new ArrayList<activitesShowVo>();
		
		
		try {
			//查询
			SearchHits<ClogsPro> searchHits =   elasticsearchTemplate.search(search.build(), ClogsPro.class);
			
			logger.info("searchHits = {}", searchHits);
			
			if(searchHits.getSearchHits().size() >0)
			{
				logger.info("search clog = {}",searchHits.getSearchHit(0).getContent().toString());
			}
			
			Map<String, Object> aggregationMap = new HashMap<>();
			Aggregations aggregations1 = (Aggregations) searchHits.getAggregations();
			parseGroup(aggregations1, aggregationMap);

			ParsedStringTerms parsedStringTerms = (ParsedStringTerms) aggregationMap.get("activites");

			parsedStringTerms.getBuckets().forEach(bucket ->{
				
				//logger.info(bucket.getKeyAsString() + "-" + bucket.getDocCount());
				
				activitesShowVo item = new activitesShowVo();
				item.setId(Long.parseLong(bucket.getKeyAsString()));
				item.setTotal(bucket.getDocCount());
				aggResults.add(item);
			});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("search error = {}",e.getMessage());
		}
		
		return aggResults;
	}
	//解析分组数据
	public void parseGroup(Aggregations aggregations, Map<String,Object> resultMap){
		if(aggregations!=null){
			for (Aggregation aggregation : aggregations) {
				ParsedStringTerms terms = (ParsedStringTerms) aggregation;
				String name = terms.getName();
				List<String> collect = terms.getBuckets().stream().map(i -> i.getKeyAsString()).collect(Collectors.toList());
				resultMap.put(name,collect);
			}
		}
	}
	

}
