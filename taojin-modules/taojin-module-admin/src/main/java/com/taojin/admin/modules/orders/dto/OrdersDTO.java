package com.taojin.admin.modules.orders.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.taojin.framework.config.JpaDto;
import io.swagger.annotations.ApiModelProperty;
import org.jeecgframework.poi.excel.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.format.annotation.DateTimeFormat;
import lombok.Data;

import javax.persistence.*;


/**
 * 订单主 对象 orders
 *
 * @author sujg
 * @date Thu Dec 14 16:22:23 CST 2023
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@JpaDto
public class OrdersDTO
{
    private static final long serialVersionUID = 1L;


    /** 订单ID */
    private String oid;

    /** 父订单ID */
    @Excel(name = "父订单ID")
    private String parentOid;




    /** 商品单价 单位（分） */
    @Excel(name = "商品单价 单位")
    private Integer itemsPrice;

    /** 商品类型 0：默认    5: 活动报名  10:场馆预约  12:会员卡   15: 会员卡充值   20: 赛事报名   30：商品 */
    @Excel(name = "商品类型 0：默认    5: 活动报名  10:场馆预约  12:会员卡   15: 会员卡充值   20: 赛事报名   30：商品")
    private Integer itemsType;



    /** skuId */
    @Excel(name = "skuId")
    private Long skuId;

    /** 订单价格 单位（分），单价*数量+邮费 */
    @Excel(name = "订单价格 单位")
    private Integer orderPrice;

    /** 实际支付价格 单位（分），单价*数量-优惠+邮费 */
    @Excel(name = "实际支付价格 单位")
    private Integer payPrice;

    /** 优惠价格 单位（分） */
    @Excel(name = "优惠价格 单位")
    private Integer discPrice;

    /** 支付方式 */
    @Excel(name = "支付方式")
    private String payWay;

    /** 订单状态 订单状态（0：订单创建  10: 待确认   20: 待支付    30: 待发货  35: 已发货   40: 待使用   50：已完成   60: 待结算  70：已结算 100: 系统取消  101：用户取消   120：订单删除） */
    @Excel(name = "订单状态 订单状态")
    private Integer status;

    /** 订单退款状态 0: 无退款   10：退款待审核/确认   20: 全额退款成功   30：部分退款成功  40：退款失败 */
    @Excel(name = "订单退款状态 0: 无退款   10：退款待审核/确认   20: 全额退款成功   30：部分退款成功  40：退款失败")
    private Integer refStatus;

    /** 评价状态 */
    @Excel(name = "评价状态")
    private Integer rateStatus;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;


    /** 用户昵称 */
    @Excel(name = "用户昵称")
    private String userNick;

    /** 商户ID 单位（分） */
    @Excel(name = "商户ID 单位")
    private Long bussId;

    /** 商户名称 */
    @Excel(name = "商户名称")
    private String bussName;



    /** 订单来源 */
    @Excel(name = "订单来源")
    private Integer source;

    /** 创建时间 记录创建时间(格式： yyyy-MM-dd HH:mm:ss) */
    private Date createTime;

    /** 更新时间 记录更新时间(格式： yyyy-MM-dd HH:mm:ss) */
    private Date updateTime;


    @ApiModelProperty(name = "search", value = "查询条件,用于模糊匹配 姓名、电话、订单号、商户名 ")
    private String search;


    @ApiModelProperty(name = "matchId", value = "运动类型的ID")
    private Long matchId;

    @ApiModelProperty(name = "downOrderStartDate", value = "下单区间 开始时间")
   @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date downOrderStartDate;

    @ApiModelProperty(name = "downOrderEndDate", value = "下单区间 结束时间")
   @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date downOrderEndDate;

    @ApiModelProperty(name = "sessionStartDate", value = "场次区间 开始时间, yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date sessionStartDate;

    @ApiModelProperty(name = "sessionEndDate", value = "场次区间 结束时间, yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date sessionEndDate;

    @ApiModelProperty(name = "cancelStartDate", value = "核销区间 开始时间, yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date cancelStartDate;

    @ApiModelProperty(name = "cancelEndDate", value = "核销区间 结束时间, yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date cancelEndDate;

    @ApiModelProperty(value = "限定支付方式，可以多选，作为list集合，wx：微信   ali: 支付宝  bank：银行卡  free: 无需支付   mcard: 会员充值卡  fcard: 会员次数卡  cash: 现金支付")
    private List<String> payTypes;
    @ApiModelProperty(name = "venueId", value = "场馆id")
    private Integer venueId;

    @ApiModelProperty(name = "venueName", value = "场馆名称")
    private String venueName;

    @ApiModelProperty(name = "orderCode", value = "订单号")
    private String orderCode;

    @ApiModelProperty(name = "parentOrderCode", value = "主订单号")
    private String parentOrderCode;



    @ApiModelProperty(name = "title", value = "商品名称")
    private String title;



    @ApiModelProperty(name = "discount", value = "优惠金额，单位分")
    private Integer discount;

    @ApiModelProperty(name = "downOrderPlace", value = "下单渠道; 10: 小程序 20：app 30: 线下收款 40: 扫码 50：商户后台下单")
    private Integer downOrderPlace;

    @ApiModelProperty(name = "orderType", value = "订单类型; 0：默认 5: 活动报名  10:场馆预约  12:会员卡   15: 会员卡充值  20: 赛事报名  30：商品")
    private Integer orderType;

    @ApiModelProperty(name = "payType", value = "付款方式;  cash 现金支付 mcard 会员卡支付")
    private String payType;

    @ApiModelProperty(name = "downOrderTime", value = "下单时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date downOrderTime;

    @ApiModelProperty(name = "userMessage", value = "用户留言")
    private String userMessage;

    @ApiModelProperty(name = "remark", value = "商家备注")
    private String remark;

    @ApiModelProperty(name = "name", value = "用户名称")
    private String name;


    @ApiModelProperty(name = "phone", value = "用户手机号")
    private String phone;

    @ApiModelProperty(name = "orderStartTime", value = "场地订单 开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date orderStartTime;

    @ApiModelProperty(name = "orderEndTime", value = "场地订单 结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date orderEndTime;

    @ApiModelProperty(name = "cancelTime", value = "核销时间,只有在状态为已核销的或者已结算(50 or 70)的情况下，这个时间才有效")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date cancelTime;


    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Map<String, Object> params = new HashMap<>();



    @ApiModelProperty(name = "sumMoney", value = "订单总金额，单位分")
    private Integer sumMoney;

    @ApiModelProperty(name = "payMoney", value = "实付总金额，单位分")
    private Integer payMoney;



    @ApiModelProperty(name = "childOrder", value = "子订单数据")
    private List<OrdersDTO> childOrder;

    public OrdersDTO(String orderCode) {
        this.orderCode = orderCode;
    }

    public OrdersDTO(String orderCode, Date orderStartTime, Date orderEndTime, Date cancelTime, String userNick,
                     String phone, String payType, Integer orderType, Integer downOrderPlace, Integer status,
                     String remark, Integer refStatus, Integer discPrice, Integer sumMoney, Integer discount, Integer payMoney) {
        this.orderCode = orderCode;
        this.orderStartTime = orderStartTime;
        this.orderEndTime = orderEndTime;
        this.cancelTime = cancelTime;
        this.userNick = userNick;
        this.phone = phone;
        this.payType = payType;
        this.orderType = orderType;
        this.downOrderPlace = downOrderPlace;
        this.status = status;
        this.remark = remark;
        this.refStatus = refStatus;
        this.discPrice = discPrice;
        this.sumMoney = sumMoney;
        this.discount = discount;
        this.payMoney = payMoney;
    }


}
