package com.taojin.admin.modules.gamesinfo.constant;

/**
 * 赛事消息 Tags
 * @author boren
 * queue name :  mq-games
 */
public class GamesTagsConfig {

    //赛事审核通过(需要发送赛事完成修改消息以及赛事结算消息)
    public final static String  GANMES_CHECK_SUCCESS  = "games_check_success";
}
