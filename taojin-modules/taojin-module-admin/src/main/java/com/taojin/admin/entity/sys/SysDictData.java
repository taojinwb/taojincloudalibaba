package com.taojin.admin.entity.sys;

import javax.persistence.*;
import java.sql.Timestamp;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import com.taojin.admin.entity.BaseEntity;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-26
*/
@Data
@Entity(name = "sys_dict_data")
public class SysDictData extends BaseEntity {

/** 字典编码*/
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "dict_code")
		private Long dictCode;

		/** 字典排序*/
		@Column(name = "dict_sort")
		@Excel(name ="字典排序")
		private java.lang.Integer dictSort;

		/** 字典标签*/
		@Column(name = "dict_label")
		@Excel(name ="字典标签")
		private java.lang.String dictLabel;

		/** 字典键值*/
		@Column(name = "dict_value")
		@Excel(name ="字典键值")
		private java.lang.String dictValue;

		/** 字典类型*/
		@Column(name = "dict_type")
		@Excel(name ="字典类型")
		private java.lang.String dictType;

		/** 样式属性（其他样式扩展）*/
		@Column(name = "css_class")
		@Excel(name ="样式属性（其他样式扩展）")
		private java.lang.String cssClass;

		/** 表格回显样式*/
		@Column(name = "list_class")
		@Excel(name ="表格回显样式")
		private java.lang.String listClass;

		/** 是否默认（Y是 N否）*/
		@Column(name = "is_default")
		@Excel(name ="是否默认（Y是 N否）")
		private java.lang.String isDefault;

		/** 状态（0正常 1停用）*/
		@Column(name = "status")
		@Excel(name ="状态（0正常 1停用）")
		private java.lang.String status;

//		/** 创建者*/
//		@Column(name = "create_by")
//		@Excel(name ="创建者")
//		private java.lang.String createBy;
//
//		/** 创建时间*/
//		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
//		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//		@Column(name = "create_time")
//		@Excel(name ="创建时间")
//		private java.util.Date createTime;
//
//		/** 更新者*/
//		@Column(name = "update_by")
//		@Excel(name ="更新者")
//		private java.lang.String updateBy;
//
//		/** 更新时间*/
//		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
//		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//		@Column(name = "update_time")
//		@Excel(name ="更新时间")
//		private java.util.Date updateTime;

		/** 备注*/
		@Column(name = "remark")
		@Excel(name ="备注")
		private java.lang.String remark;

}
