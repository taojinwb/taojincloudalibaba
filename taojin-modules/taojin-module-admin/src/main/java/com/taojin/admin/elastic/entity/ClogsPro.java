package com.taojin.admin.elastic.entity;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "c_logs-pro", shards = 5, replicas = 0)
public class ClogsPro implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 文档ID
	 */
	@Id
	private String _id;
	
	/**
	 * 访问路经
	 */
	@Field(type = FieldType.Keyword,fielddata=true)
	private String uri;
	
	/**
	 * 访问路经第一个值
	 */
	@Field(type = FieldType.Keyword,fielddata=true)
	private String path1;
	
	/**
	 * 访问路经第二个值
	 */
	@Field(type = FieldType.Keyword,fielddata=true)
	private String path2;
	
	/**
	 * 访问路经第三个值
	 */
	@Field(type = FieldType.Keyword,fielddata=true)
	private String path3;
	
	/**
	 * 访问路经第四个值
	 */
	@Field(type = FieldType.Keyword,fielddata=true)
	private String path4;
	
	/**
	 * 用户ID
	 */
	@Field(type = FieldType.Keyword,fielddata=true)
	private String user_id;

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getPath1() {
		return path1;
	}

	public void setPath1(String path1) {
		this.path1 = path1;
	}

	public String getPath2() {
		return path2;
	}

	public void setPath2(String path2) {
		this.path2 = path2;
	}

	public String getPath3() {
		return path3;
	}

	public void setPath3(String path3) {
		this.path3 = path3;
	}

	public String getPath4() {
		return path4;
	}

	public void setPath4(String path4) {
		this.path4 = path4;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	@Override
	public String toString() {
		return "ClogsPro [_id=" + _id + ", uri=" + uri + ", path1=" + path1 + ", path2=" + path2 + ", path3=" + path3
				+ ", path4=" + path4 + ", user_id=" + user_id + "]";
	}
	
}
