package com.taojin.admin.controller.sys;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.taojin.admin.entity.sys.SysGatewayRoute;
import com.taojin.admin.framework.common.BaseController;
import com.taojin.admin.service.sys.SysGatewayRouteService;
import com.taojin.framework.vo.ApiResult;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Description: gateway路由管理
 * @Author: jeecg-boot
 * @Date: 2020-05-26
 * @Version: V1.0
 */
@Api(tags = "gateway路由管理")
@RestController
@RequestMapping("/sys/gatewayRoute")
@Slf4j
public class SysGatewayRouteController extends BaseController {

    @Autowired
    private SysGatewayRouteService sysGatewayRouteService;

    @PostMapping(value = "/updateAll")
    public ApiResult<?> updateAll(@RequestBody JSONObject json) {
        sysGatewayRouteService.updateAll(json);
        return ApiResult.ok("操作成功！");
    }

    @GetMapping(value = "/list")
    public ApiResult<?> queryPageList(SysGatewayRoute sysGatewayRoute) {

        List<SysGatewayRoute> ls = sysGatewayRouteService.GetAllByFilter(new SysGatewayRoute());


        JSONArray array = new JSONArray();
        for(SysGatewayRoute rt: ls){
            JSONObject obj = (JSONObject) JSONObject.toJSON(rt);
            if(StringUtils.isNotEmpty(rt.getPredicates())){
                obj.put("predicates", JSONArray.parseArray(rt.getPredicates()));
            }
            if(StringUtils.isNotEmpty(rt.getFilters())){
                obj.put("filters", JSONArray.parseArray(rt.getFilters()));
            }
            array.add(obj);
        }
        return ApiResult.ok(array);
    }

    @GetMapping(value = "/clearRedis")
    public ApiResult<?> clearRedis() {
        sysGatewayRouteService.clearRedis();
        return ApiResult.ok("清除成功！");
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    //@RequiresPermissions("system:getway:delete")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ApiResult<?> delete(@RequestParam(name = "id", required = true) Long id) {
        sysGatewayRouteService.deleteById(id);
        return ApiResult.ok("删除路由成功");
    }

}
