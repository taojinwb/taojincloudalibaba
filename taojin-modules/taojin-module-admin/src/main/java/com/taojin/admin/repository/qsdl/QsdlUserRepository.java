//package com.taojin.admin.repository.qsdl;
//
//import com.taojin.admin.entity.sys.SysRole;
//import com.taojin.admin.entity.sys.SysUser;
//import com.taojin.admin.dto.sys.SysRoleDTO;
//import com.taojin.admin.dto.sys.SysUserDTO;
//
//import java.util.List;
//
///**
// * 不方便，暂时不启用
// */
//public interface QsdlUserRepository {
//
//
//    public List<SysRoleDTO> selectRoleList(SysRole sysRole, int page, int size, String sortProperty);
//
//    /**
//     * 根据用户实体类过滤 获取该用户部门、权限相关信息
//     * @param SysUser
//     * @param sortProperty
//     * @return
//     */
//    public List<SysUserDTO> getSysUserVoBySyUser(SysUser sysUser, String sortProperty);
//
//
//}
