package com.taojin.admin.elastic.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.taojin.admin.elastic.entity.ActivitiesCommentLike;

import java.util.List;

/**
 * 点赞数据
 * @author boren
 *
 */

public interface ActivitiesCommentLikeRepository extends ElasticsearchRepository<ActivitiesCommentLike, String> {
	/**
	 * 获取某条评论我点赞的记录
	 * @param commont_id
	 * @param liker_user_id
	 * @return
	 */
	List<ActivitiesCommentLike>  findByCommontIdInAndLikerUserId(List<Long> commont_id, Long liker_user_id);

	/**
	 * 获取某条评论我点赞的数据
	 * @param reply_id
	 * @param liker_user_id
	 * @return
	 */
	List<ActivitiesCommentLike>  findByReplyIdInAndLikerUserId(List<Long> reply_id,Long liker_user_id);
	
	/**
	 * 这个活动我所有的点赞数据
	 * @param activities_id
	 * @param liker_user_id
	 * @param pageRequest
	 * @return
	 */
	Page<ActivitiesCommentLike> findByActivitiesIdAndLikerUserId(Long activities_id,Long liker_user_id, PageRequest pageRequest);

	
}
