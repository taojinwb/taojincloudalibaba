package com.taojin.admin.controller.sys;


import javax.servlet.http.HttpServletResponse;

import com.taojin.admin.framework.common.BaseController;
import com.taojin.admin.entity.sys.SysDictData;
import com.taojin.admin.service.sys.SysDictDataService;
import com.taojin.admin.service.sys.SysDictTypeService;
import com.taojin.framework.utils.MF;
import com.taojin.framework.vo.ApiResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 数据字典信息
 *
 * @author sujg
 */
@RestController
@RequestMapping("/system/dict/data")
@Api(value="字典数据",tags = "字典数据")

public class SysDictDataController extends BaseController<SysDictData>
{
    @Autowired
    private SysDictDataService dictDataService;

    @Autowired
    private SysDictTypeService dictTypeService;

    @ApiOperation(value = "根据条件分页查询字典数据",tags = "字典数据")
    @PreAuthorize("@ss.hasPermi('system:dict:list')")
    @GetMapping("/list")
    public ApiResult list(SysDictData dictData, @RequestParam(name="pageNo", defaultValue="0") Integer pageNo,
                          @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        Page<SysDictData> list = dictDataService.selectDictDataList(dictData,pageable);
        return ApiResult.OK(list);
    }

    @PreAuthorize("@ss.hasPermi('system:dict:export')")
    @PostMapping("/export")
    public void export(HttpServletResponse response, SysDictData dictData)
    {

    }

    /**
     * 查询字典数据详细
     */
    @ApiOperation(value = "查询字典数据详细",tags = "字典数据")
    @PreAuthorize("@ss.hasPermi('system:dict:query')")
    @GetMapping(value = "/{dictCode}")
    public ApiResult<SysDictData> getInfo(@PathVariable Long dictCode)
    {
        return ApiResult.OK(dictDataService.selectDictDataById(dictCode));
    }

    /**
     * 根据字典类型查询字典数据信息
     */
    @ApiOperation(value = "根据字典类型查询字典数据信息",tags = "字典数据")
    @GetMapping(value = "/type/{dictType}")
    public ApiResult<List<SysDictData>> dictType(@PathVariable String dictType)
    {
        List<SysDictData> data = dictDataService.selectDictDataByType(dictType);
        if (MF.isNull(data))
        {
            data = new ArrayList<SysDictData>();
        }
        return ApiResult.OK(data);
    }

    /**
     * 新增字典类型
     */
    @ApiOperation(value = "新增字典类型",tags = "字典数据")
    @PreAuthorize("@ss.hasPermi('system:dict:add')")
    @PostMapping
    public ApiResult<String> add(@Validated @RequestBody SysDictData dict)
    {
      //  dict.setCreateBy(this.getLoginUser().getUsername());
        dictDataService.insertDictData(dict);
        return ApiResult.OK("新增成功");
    }

    /**
     * 修改保存字典类型
     */
    @ApiOperation(value = "修改保存字典类型",tags = "字典数据")
    @PreAuthorize("@ss.hasPermi('system:dict:edit')")
    @PutMapping
    public ApiResult<String> edit(@Validated @RequestBody SysDictData dict)
    {
      //  dict.setUpdateBy(this.getLoginUser().getUsername());
        dictDataService.updateDictData(dict);
        return ApiResult.OK("修改成功");
    }

    /**
     * 删除字典类型
     */
    @ApiOperation(value = "删除字典类型",tags = "字典数据")
    @PreAuthorize("@ss.hasPermi('system:dict:remove')")
    @DeleteMapping("/{dictCodes}")
    public ApiResult<String> remove(@PathVariable Long[] dictCodes)
    {
        dictDataService.deleteDictDataByIds(dictCodes);
        return ApiResult.OK("删除成功");
    }
}
