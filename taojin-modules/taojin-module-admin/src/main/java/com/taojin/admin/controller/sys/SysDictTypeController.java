package com.taojin.admin.controller.sys;


import java.util.List;

import com.taojin.admin.framework.common.BaseController;
import com.taojin.admin.entity.sys.SysDictType;
import com.taojin.admin.framework.annotation.Log;
import com.taojin.admin.service.sys.SysDictTypeService;
import com.taojin.framework.enums.BusinessType;
import com.taojin.framework.vo.ApiResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 数据字典信息
 *
 * @author sujg
 */
@RestController
@RequestMapping("/system/dict/type")
@Api(value="字典类型",tags = "字典类型")
public class SysDictTypeController extends BaseController<SysDictType>
{
    @Autowired
    private SysDictTypeService dictTypeService;

    @ApiOperation(value = "根据条件分页查询字典类型数据",tags = "字典数据")
    @PreAuthorize("@ss.hasPermi('system:dict:list')")
    @GetMapping("/list")
    public ApiResult<List<SysDictType>> list(SysDictType dictType, @RequestParam(name="pageNum", defaultValue="0") Integer pageNum,
                                             @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        if(pageNum<=1){
           pageNum=1;
        }
        pageNum=pageNum-1;
        Pageable pageable = PageRequest.of(pageNum, pageSize);
        Page<SysDictType> list = dictTypeService.selectDictTypeList(dictType,pageable);
        return ApiResult.OK(list);
    }



    /**
     * 查询字典类型详细
     */
    @ApiOperation(value = "查询字典类型详细",tags = "字典数据")
    @PreAuthorize("@ss.hasPermi('system:dict:query')")
    @GetMapping(value = "/{dictId}")
    public ApiResult getInfo(@PathVariable Long dictId)
    {
        return ApiResult.OK(dictTypeService.selectDictTypeById(dictId));
    }

    /**
     * 新增字典类型
     */
    @ApiOperation(value = "新增字典类型",tags = "字典数据")
    @PreAuthorize("@ss.hasPermi('system:dict:add')")
    @Log(title = "字典类型", businessType = BusinessType.INSERT)
    @PostMapping
    public ApiResult add(@Validated @RequestBody SysDictType dict)
    {
        if (!dictTypeService.checkDictTypeUnique(dict))
        {
            return ApiResult.error("新增字典'" + dict.getDictName() + "'失败，字典类型已存在");
        }
      //  dict.setCreateBy(this.getLoginUser().getUsername());
        dictTypeService.insertDictType(dict);
        return ApiResult.ok("新增成功");
    }

    /**
     * 修改字典类型
     */
    @ApiOperation(value = "修改字典类型",tags = "字典数据")
    @PreAuthorize("@ss.hasPermi('system:dict:edit')")
    @Log(title = "字典类型", businessType = BusinessType.UPDATE)
    @PutMapping
    public ApiResult edit(@Validated @RequestBody SysDictType dict)
    {
        if (!dictTypeService.checkDictTypeUnique(dict))
        {
            return ApiResult.error("修改字典'" + dict.getDictName() + "'失败，字典类型已存在");
        }
      //  dict.setUpdateBy(this.getLoginUser().getUsername());
        dictTypeService.updateDictType(dict);
        return ApiResult.OK("修改成功");
    }

    /**
     * 删除字典类型
     */
    @ApiOperation(value = "删除字典类型",tags = "字典数据")
    @PreAuthorize("@ss.hasPermi('system:dict:remove')")
    @Log(title = "字典类型", businessType = BusinessType.DELETE)
    @DeleteMapping("/{dictIds}")
    public ApiResult remove(@PathVariable Long[] dictIds)
    {
        dictTypeService.deleteDictTypeByIds(dictIds);
        return ApiResult.ok("删除成功");
    }

    /**
     * 刷新字典缓存
     */
    @ApiOperation(value = "刷新字典缓存",tags = "字典数据")
    @PreAuthorize("@ss.hasPermi('system:dict:remove')")
    @Log(title = "字典类型", businessType = BusinessType.CLEAN)
    @DeleteMapping("/refreshCache")
    public ApiResult refreshCache()
    {
        dictTypeService.resetDictCache();
        return ApiResult.OK("刷新成功");
    }

    /**
     * 获取字典选择框列表
     */
    @ApiOperation(value = "获取字典选择框列表",tags = "字典数据")
    @GetMapping("/optionselect")
    public ApiResult optionselect()
    {
        List<SysDictType> dictTypes = dictTypeService.selectDictTypeAll();
        return ApiResult.OK(dictTypes);
    }
}
