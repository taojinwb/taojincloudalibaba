package com.taojin.admin.modules.orders.constant;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author: BBWZ
 * @date: 2021/3/29 16:49
 * @description:
 *          对订单里面的一些状态数据聚合枚举，如支付类型的聚合 -线上支付(wx, ali, bank)
 */
public enum OrderPolyEnum {
    /** 空的聚合 */
    ORDER_POLY_EMNU_PAY_TYPE_NONE(-1, null, "空的聚合"),

    /** 支付类型，orders.payWay; 线上，线下，会员卡;
     * wx：微信   ali: 支付宝  bank：银行卡  free: 无需支付   mcard: 会员充值卡  fcard: 会员次数卡  cash: 现金支付 * */
    ORDER_POLY_EMNU_PAY_TYPE_ALL(10, Arrays.asList("ali", "bank", "wx", "free", "cash", "fcard", "mcard"), "全部支付类型"),
    ORDER_POLY_EMNU_PAY_TYPE_ON_LINE(11, Arrays.asList("ali", "bank", "wx"), "线上支付"),
    ORDER_POLY_EMNU_PAY_TYPE_OFF_LINE(12, Arrays.asList("free", "cash"), "线下支付"),
    ORDER_POLY_EMNU_PAY_TYPE_VIP_CARD(13, Arrays.asList("fcard", "mcard"), "会员卡支付"),

    /** 收支明细中的统计订单类型
     * 商品类型 0：默认    5: 活动报名  10:场馆预约  11:场馆商品  12:会员卡   15: 会员卡充值   20: 赛事报名   30：商品   40: 次数卡核销  50: 礼品 */
    ORDER_POLY_EMNU_COUNT_TYPE_All(20, Arrays.asList(5, 10, 11, 12, 15, 20, 30, 40, 50), "全部商品类型"),
    ORDER_POLY_EMNU_COUNT_TYPE_BOOKING(21, Collections.singletonList(10), "订场类商品"),
    ORDER_POLY_EMNU_COUNT_TYPE_GOODS(22, Collections.singletonList(11), "商品类商品"),
    ORDER_POLY_EMNU_COUNT_TYPE_VIP_CARD(23, Arrays.asList(12, 15), "会员卡类商品"),
    ORDER_POLY_EMNU_COUNT_TYPE_GAMES_APPLY(24, Collections.singletonList(20), "赛事报名"),
    ORDER_POLY_EMNU_COUNT_TYPE_SHOP_GOODS(25, Collections.singletonList(30), "商城商品"),
    ORDER_POLY_EMNU_COUNT_TYPE_VOTE(26, Collections.singletonList(50), "赛事投票，礼物"),


    /**
     * 商品类型， 按照各种场景可见的商品
     * 商品类型 0：默认    5: 活动报名  10:场馆预约  11:场馆商品  12:会员卡   15: 会员卡充值   20: 赛事报名   30：商品   40: 次数卡核销   50:赛事投票
     * */
    ORDER_POLY_EMNU_ITEM_TYPE_ALL(30, Arrays.asList(5, 10, 11, 12, 15, 20, 30, 40, 50), "全部商品类型"),
    ORDER_POLY_EMNU_ITEM_TYPE_VENUE_ORDER(31, Arrays.asList(5, 10, 11, 12, 15, 30, 40), "场馆订单中可见的商品类型"),
    ORDER_POLY_EMNU_ITEM_TYPE_WITHDRAWAL_COUNTER(32, Arrays.asList(10, 11, 12, 15), "场馆收支明细中可见的商品类型，列表和统计"),
    ORDER_POLY_EMNU_ITEM_TYPE_WITHDRAWAL_USER_INFO(33, Arrays.asList(10, 11, 12, 15, 20, 30, 50), "商户中心收支明细中可见的商品类型，列表和统计"),
//    ORDER_POLY_EMNU_ITEM_TYPE_WITHDRAWAL_USER(33, Arrays.asList(5, 10, 11, 12, 15, 20, 30, 40, 50), "商户中心收支明细中可见的商品类型，列表"),
//    ORDER_POLY_EMNU_ITEM_TYPE_VENUE_COUNTER_ORDER(32, Arrays.asList(5, 10, 11, 12, 15, 20, 30, 40), "场馆商品订单中可见的商品类型"),

    /**
     * 订单类型， 标记订单检索
     * 商品类型 0：默认    5: 活动报名  10:场馆预约  11:场馆商品  12:会员卡   15: 会员卡充值   20: 赛事报名   30：商品   40: 次数卡核销   50:赛事投票
     * */
    ORDER_POLY_EMNU_ITEM_SEARCH_VENUE(41, Arrays.asList(10, 11, 12, 15), "订单为场馆订单，orders中buss_id存储的是场馆id"),
    ORDER_POLY_EMNU_ITEM_SEARCH_SHOP(42, Collections.singletonList(30), "订单为商城订单，orders中buss_id存储的是商城id"),
    ORDER_POLY_EMNU_ITEM_SEARCH_AUTHEN(43, Arrays.asList(5, 20, 50), "订单为商户订单，orders中buss_id存储的是企业账户id"),


    /** 场馆数据看板，订单支付类型，会员卡支付 和 现金支付 */
    ORDER_POLY_EMNU_DATA_CASH(51, Arrays.asList("ali", "bank", "wx", "cash"), "场馆数据看板中，将其他非会员卡支付视为现金"),
    ORDER_POLY_EMNU_DATA_CARD(52, Arrays.asList("fcard", "mcard"), "场馆数据看板中，会员卡支付"),
    ORDER_POLY_EMNU_DATA_ALL_TYPE(55, Arrays.asList(10, 11), "场馆数据看板中，所有商品类型，就是场馆和场馆商品"),



    ;

    /** 收支明细-统计的所有类型 */
    public static OrderPolyEnum[] COUNT_TYPES = new OrderPolyEnum[]{ORDER_POLY_EMNU_COUNT_TYPE_BOOKING, ORDER_POLY_EMNU_COUNT_TYPE_GOODS, ORDER_POLY_EMNU_COUNT_TYPE_VIP_CARD,
            ORDER_POLY_EMNU_COUNT_TYPE_GAMES_APPLY, ORDER_POLY_EMNU_COUNT_TYPE_SHOP_GOODS, ORDER_POLY_EMNU_COUNT_TYPE_VOTE};

    OrderPolyEnum(Integer code, List polyList, String polyName) {
        this.code = code;
        this.polyList = polyList;
        this.polyName = polyName;
    }

    /** 按照code 映射 */
    private static Map<Integer, OrderPolyEnum> map;

    private Integer code;
    private List polyList;
    private String polyName;

    /** 按照code 查询映射的枚举 */
    public static OrderPolyEnum formCode(Integer code){
        if (map == null){
            map = Stream.of(OrderPolyEnum.values()).collect(Collectors.toMap(OrderPolyEnum::getCode, v -> v));
        }
        return map.get(code);
    }



    public Integer getCode() {
        return code;
    }
    public <T> List<T> getPolyList() {
        return polyList;
    }
    public String getPolyName() {
        return polyName;
    }
}
