package com.taojin.admin.service.sys;


import java.util.Arrays;

import com.taojin.admin.framework.common.BaseService;
import com.taojin.admin.entity.sys.SysNotice;
import com.taojin.admin.repository.sys.SysNoticeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * 公告 服务层实现
 *
 * @author sujg
 */
@Service
public class SysNoticeService extends BaseService<SysNotice,Long>
{
    @Autowired
    private SysNoticeRepository sysNoticeRepository;



    public SysNoticeService(SysNoticeRepository repository) {
        super(repository,repository);
    }


    /**
     * 查询公告信息
     *
     * @param noticeId 公告ID
     * @return 公告信息
     */
    public SysNotice selectNoticeById(Long noticeId)
    {
        return sysNoticeRepository.findById(noticeId).orElse(null);
    }

    /**
     * 查询公告列表
     *
     * @param notice 公告信息
     * @return 公告集合
     */
    public Page<SysNotice> selectNoticeList(SysNotice notice, Pageable pg)
    {
        return sysNoticeRepository.findAll(this.filterByFields(notice),pg);
    }




    /**
     * 新增公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    public void insertNotice(SysNotice notice)
    {
         sysNoticeRepository.save(notice);
    }

    /**
     * 修改公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    public void updateNotice(SysNotice notice)
    {
         this.sysNoticeRepository.save(notice);
    }

    /**
     * 删除公告对象
     *
     * @param noticeId 公告ID
     * @return 结果
     */
    public void deleteNoticeById(Long noticeId)
    {
         sysNoticeRepository.deleteById(noticeId);
    }

    /**
     * 批量删除公告信息
     *
     * @param noticeIds 需要删除的公告ID
     * @return 结果
     */
    public void deleteNoticeByIds(Long[] noticeIds)
    {
         sysNoticeRepository.deleteAllByIdInBatch(Arrays.asList(noticeIds));
    }
}
