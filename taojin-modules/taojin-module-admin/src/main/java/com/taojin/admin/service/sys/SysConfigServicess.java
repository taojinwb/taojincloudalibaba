package com.taojin.admin.service.sys;


import com.taojin.admin.framework.common.BaseService;
import com.taojin.admin.entity.sys.SysConfig;
import com.taojin.admin.repository.sys.SysConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * @Author : ydd
 * @Date : 2020/07/22
 **/
@Service
public class SysConfigServicess extends BaseService<SysConfig, Long> {

    @Autowired
    private SysConfigRepository sysConfigRepository;

    public SysConfigServicess(SysConfigRepository repository) {
        super(repository,repository);
    }

    public Page<SysConfig> getConfigList(SysConfig sysConfig, Pageable pageable){

        return sysConfigRepository.findAll(filterByFields(sysConfig),pageable);
    }

    // 创建实体
    public  SysConfig insert(SysConfig sysConfig) {
        return sysConfigRepository.save(sysConfig);
    }

//    // 根据ID读取实体
//    public  SysConfig findById(Long id) {
//        return  sysConfigRepository.findById(id).orElse(null);
//    }

    // 更新实体
    public  SysConfig update(SysConfig entity) {
        return sysConfigRepository.save(entity);
    }
    public void delete(Long id){
        sysConfigRepository.deleteById(id);
    }



}

