//package com.taojin.admin.modules.gamesinfo.controller;
//
//import java.util.*;
//import java.util.stream.Collectors;
//import com.taojin.admin.framework.common.BaseController;
//import com.taojin.admin.modules.gamesinfo.dto.GamesInfoDTO;
//import com.taojin.admin.modules.gamesinfo.entity.GamesInfo;
//import com.taojin.admin.modules.gamesinfo.service.GamesInfoService;
////import com.taojin.api.clientc.RemoteClientCAPI;
////import com.taojin.api.vo.ResultBean;
//import com.taojin.common.page.PageParam;
//import com.taojin.common.message.rocketmq.enums.MessageTag;
//import com.taojin.common.message.rocketmq.enums.MessageTopic;
//import com.taojin.common.message.rocketmq.producer.MessageProducer;
//import com.taojin.framework.vo.ApiResult;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiImplicitParam;
//import io.swagger.annotations.ApiImplicitParams;
//import org.apache.commons.lang.StringUtils;
//import org.jetbrains.annotations.NotNull;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.validation.annotation.Validated;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.data.domain.Page;
//import org.springframework.web.bind.annotation.*;
//
///**
// * 赛事信息 Controller
// * @author sujg
// * @date Mon Dec 18 13:44:04 CST 2023
// */
//@RestController
//@RequestMapping("/modules/gamesInfo")
//@Api(value="赛事信息 ",tags = "赛事信息 ")
//public class GamesInfoController extends BaseController<GamesInfo>
//{
//    @Autowired
//    private GamesInfoService gamesInfoService;
//    @Autowired
//    private MessageProducer messageProducer;
//
//    @Autowired
//    private RemoteClientCAPI remoteClientCAPI;
//
//    /**
//     * 查询赛事信息 列表
//     */
//    @PreAuthorize("@ss.hasPermi('modules:gamesInfo:list')")
//    @GetMapping("/list")
//    @ApiOperation(value = "获取赛事列表",notes = "返回数据为列表数据")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "pageSize", value = "每页展示条数",required = true,dataType = "int" ,paramType = "query"),
//            @ApiImplicitParam(name = "page", value = "当前页",required = true,dataType = "int" ,paramType = "query"),
//            @ApiImplicitParam(name = "search",value = "赛事名称",required = false,dataType = "String",paramType = "query"),
//            @ApiImplicitParam(name = "status",value = "状态（0：初始状态    10：待审核  15：审核失败  20：报名中   50：已结束    100:已经取消）, 多选状态使用,进行分割",required = false,dataType = "int",paramType = "query"),
//            @ApiImplicitParam(name = "sportsTypeId",value = "赛事类型主键",dataType = "int",paramType = "query"),
//            @ApiImplicitParam(name = "ruleType",value = "赛事类型",dataType = "int",paramType = "query")
//    })
//    public ApiResult list(@Validated @RequestParam(required = false) String search
//            ,@RequestParam Integer pageSize
//            ,@RequestParam Integer page
//            ,@RequestParam(required = false) String status
//            ,@RequestParam(required = false) Integer sportsTypeId
//            ,@RequestParam(required = false) Integer ruleType){
//        List<Integer> statusList = StringUtils.isBlank(status) ? Collections.emptyList(): (Arrays.stream(status.split(",")).map(Integer::valueOf).collect(Collectors.toList()));
//         messageProducer.sendMessage(MessageTopic.GAMES_TOPIC, MessageTag.GANMES_CHECK_SUCCESS,2222+"", "222222");
//        Map map=new HashMap();
//        map.put("create_time","desc");
//        Page<GamesInfoDTO> vo = gamesInfoService.list(this.getPageable(page,pageSize,map),statusList,ruleType,search,sportsTypeId);
//        return ApiResult.OK(vo);
//    }
//
//
//    @PreAuthorize("@ss.hasPermi('modules:gamesInfo:setPlayUrl')")
//    @PostMapping("/setPlayUrl")
//    @ApiOperation(value = "设置赛事直播地址")
//    public ApiResult setPlayUrl(@RequestBody @Validated GamesInfoDTO dto) throws Exception {
//        gamesInfoService.setPlayUrl(dto);
//        return ApiResult.OK();
//    }
//
//    @PreAuthorize("@ss.hasPermi('modules:gamesInfo:delete')")
//    @PostMapping("/delete/{gamesId}")
//    @ApiOperation(value = "删除赛事数据")
//    public ApiResult delete(@PathVariable(value = "gamesId") Long gamesId){
//        gamesInfoService.delete(gamesId);
//        return ApiResult.OK();
//    }
//
//    @PreAuthorize("@ss.hasPermi('modules:gamesInfo:checkGames')")
//    @PostMapping("/checkGames")
//    @ApiOperation(value = "赛事审核")
//    public ApiResult check(@RequestBody @Validated GamesInfoDTO dto) throws Exception {
//        ApiResult<Object> apiResult = new ApiResult<>();
//        GamesInfo info = gamesInfoService.check(dto);
//        if(info.getStatus().compareTo(10) != 0) {
//            apiResult.setMessage("文字内容不符合标准，请修改");
//        }
//        return apiResult;
//    }
//
//    @GetMapping("/detail/{gamesId}")
//    @ApiOperation(value = "获取赛事详情")
//    public ApiResult getDetail(@PathVariable(value = "gamesId") Long gamesId) throws Exception {
//        ResultBean resultBean = remoteClientCAPI.getOrderByOrderId(gamesId);
//        return getApiResult(resultBean);
//    }
//
//    @NotNull
//    private static ApiResult<Object> getApiResult(ResultBean resultBean) {
//        ApiResult<Object> apiResult = new ApiResult<>();
//        if(resultBean.getCode()!=200){
//            return ApiResult.error(resultBean.getCode(),resultBean.getMessage());
//        }else {
//            if(null!=resultBean.getMessage()){apiResult.setMessage(resultBean.getMessage());}
//            if(0!=resultBean.getCode()){apiResult.setCode(resultBean.getCode());}
//            if(null!=resultBean.getResponse()){apiResult.setData(resultBean.getResponse());}
//            if(0l!=resultBean.getTotalNum()){apiResult.setTotal(resultBean.getTotalNum());}
//            return apiResult;
//        }
//
//
//    }
//
//    @GetMapping("/vote/list/{id}")
//    @ApiOperation(value = "赛事投票列表")
//    @ApiImplicitParam(name = "id", value = "赛事id",required = true,dataType = "Long" ,paramType = "query")
//    public ApiResult voteList(@PathVariable("id") Long id) {
//        ResultBean resultBean = remoteClientCAPI.voteList(id);
//        return getApiResult(resultBean);
//    }
//
//    @GetMapping("/apply/list/single")
//    @ApiOperation(value = "获取单项赛报名列表",notes = "返回数据为列表数据")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "pageSize", value = "每页展示条数",required = true,dataType = "Integer" ,paramType = "query"),
//            @ApiImplicitParam(name = "pageNum", value = "当前页",required = true,dataType = "Integer" ,paramType = "query"),
//            @ApiImplicitParam(name = "search",value = "真实姓名/电话号码",required = true,dataType = "String",paramType = "query"),
//            @ApiImplicitParam(name = "projectId",value = "项目主键",required = true,dataType = "Long",paramType = "query"),
//            @ApiImplicitParam(name = "groupId",value = "项目组别主键 为空表示全部",required = true,dataType = "Long",paramType = "query"),
//            @ApiImplicitParam(name = "teamsId",value = "队伍主键 根据队伍查询队员时使用",required = true,dataType = "Long",paramType = "query"),
//            @ApiImplicitParam(name = "status",value = "报名状态， 30 报名成功， 40 退出比赛， 不传查全部", required = true,dataType = "Integer",paramType = "query")
//    })
//    public ApiResult listApply(String search, Long projectId, Long groupId, Long teamsId, Integer status, PageParam pageParam) {
//        ResultBean resultBean = remoteClientCAPI.listApply(pageParam.getPageSize(), pageParam.getPageNum(), search, projectId, groupId, teamsId, status);
//        return getApiResult(resultBean);
//    }
//
//    @GetMapping("/apply/list/team")
//    @ApiOperation(value = "获取团队赛报名列表", notes = "返回数据为列表数据")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "pageSize", value = "每页展示条数", required = true, dataType = "Integer", paramType = "query"),
//            @ApiImplicitParam(name = "pageNum", value = "当前页", required = true, dataType = "Integer", paramType = "query"),
//            @ApiImplicitParam(name = "search",value = "真实姓名/电话号码", required = true, dataType = "String", paramType = "query"),
//            @ApiImplicitParam(name = "projectId",value = "项目主键", required = true, dataType = "Long", paramType = "query"),
//            @ApiImplicitParam(name = "groupId",value = "项目组别主键 为空表示全部", required = true,dataType = "Long",paramType = "query"),
//            @ApiImplicitParam(name = "status",value = "状态， 10 报名成功， 110 退赛成功， 不传查全部", required = true, dataType = "Long", paramType = "query"),
//    })
//    public ApiResult listApplyTeams(String search, Long groupId, Long projectId, Integer status, PageParam pageParam) {
//        ResultBean resultBean = remoteClientCAPI.listApplyTeams(search, groupId, projectId, status, pageParam.getPageSize(), pageParam.getPageNum());
//        return getApiResult(resultBean);
//    }
//
//    @GetMapping("/getGroup/{projectId}")
//    @ApiOperation(value = "根据赛事赛事项目主键查询项目组别列表", notes = "返回数据为列表")
//    @ApiImplicitParam(name = "projectId",value = "项目id", required = true)
//    public ApiResult getGroupByProjectId(@PathVariable(value = "projectId") Long projectId) {
//        ResultBean resultBean = remoteClientCAPI.getGroupByProjectId(projectId);
//        return getApiResult(resultBean);
//    }
//
//    @GetMapping("/getFields")
//    @ApiOperation(value = "获取参赛人员必填信息列表",notes = "返回数据为列表")
//    public ApiResult getFields(){
//        ResultBean resultBean = remoteClientCAPI.getFields();
//        return getApiResult(resultBean);
//    }
//
//    @GetMapping("/ranking/run/{gameId}")
//    @ApiOperation(value = "获取线上跑赛事排名")
//    //@Result(factory = ResultBeanFactory.class, prototypeClass = com.lkd.basics.result.ResultBean.class)
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "gameId", value = "赛事id", required = true, dataType = "Long" , paramType = "query"),
//            @ApiImplicitParam(name = "projectId", value = "项目id", required = true, dataType = "Long" , paramType = "query"),
//            @ApiImplicitParam(name = "groupId", value = "分组id", required = true, dataType = "Long" , paramType = "query"),
//    })
//    public ApiResult ranking(@PathVariable("gameId") Long gameId, Integer projectId, Integer groupId, PageParam pageParam ) {
//        ResultBean resultBean = remoteClientCAPI.ranking(gameId, projectId, groupId, pageParam);
//        return getApiResult(resultBean);
//    }
//
//    @GetMapping("/run/sign/{userId}")
//    @ApiOperation(value = "用户打卡记录")
//    //@Result(factory = ResultBeanFactory.class, prototypeClass = com.lkd.basics.result.ResultBean.class)
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "gameId", value = "赛事id", required = true, dataType = "Long" , paramType = "query"),
//            @ApiImplicitParam(name = "userId", value = "用户id", required = true, dataType = "Long" , paramType = "query"),
//    })
//    public ApiResult getUserRunGameSignList(Long gameId, @PathVariable("userId") Long userId, PageParam pageParam) {
//        ResultBean resultBean = remoteClientCAPI.getUserRunGameSignList(gameId, userId, (pageParam));
//        return getApiResult(resultBean);
//    }
//
//}
