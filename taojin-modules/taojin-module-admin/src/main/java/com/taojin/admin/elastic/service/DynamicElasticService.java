package com.taojin.admin.elastic.service;

import cn.hutool.json.JSONUtil;
import com.taojin.admin.elastic.entity.InCommentLike;
import com.taojin.admin.elastic.repository.InCommentLikeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * ES 中 动态相关数据
 * @author boren
 *
 */
@Service
public class DynamicElasticService {
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Resource
	private InCommentLikeRepository inCommentLikeRepository;

	/**
	 * 保存用户点赞记录
	 * 
	 */
	public InCommentLike  addLike(InCommentLike params)
	{

		params.setCreateTime(new Date());
		params.set_id(null);

		logger.info("[点赞] param : {} ", JSONUtil.toJsonStr(params));
		InCommentLike  likeEntity = inCommentLikeRepository.save(params);
		
		return likeEntity;
	}

	/**
	 * 删除用户点赞记录
	 *
	 */
	public void unLike(String id)
	{
		inCommentLikeRepository.deleteById(id);
	}
	
//	/**
//	 * 当前用对某个动态评论的点赞数据记录
//	 * @param dynamicId
//	 * @param token
//	 * @return
//	 */
//	public List<InCommentLike>  getMyLikesForDynamicId(List<Long> dynamicId,String token)
//	{
//		PageRequest  request = PageRequest.of(0,100);
//
//		Long userId = userService.getUserIdForToken(token);
//
//		try {
//			Page<InCommentLike>  page = inCommentLikeRepository.findByDynamicIdInAndLikerUserId(dynamicId, userId,request);
//
//			return page.getContent();
//
//		} catch (Exception e) {
//
//			logger.info("serach err = {}",e.getMessage());
//			// TODO Auto-generated catch block
//			return new ArrayList<InCommentLike>();
//		}
//	}
//
//
//	/**
//	 * 当前用对某个活动评论的点赞数据记录
//	 * @param param
//	 * @return
//	 */
//	public List<InCommentLike> getMyLikesForIds(MyLikesDTO param) {
//		return inCommentLikeRepository.findByCommontIdInAndLikerUserId(param.getCommentIds(), param.getUid());
//	}
//
	
	

}
