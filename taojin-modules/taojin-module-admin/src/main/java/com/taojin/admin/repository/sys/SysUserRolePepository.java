package com.taojin.admin.repository.sys;

import com.taojin.admin.entity.sys.SysConfig;
import com.taojin.admin.entity.sys.SysUserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SysUserRolePepository extends  JpaRepository<SysUserRole, Long>, JpaSpecificationExecutor<SysUserRole> {
    int countByRoleId(Long roleId);

    int deleteSysUserRoleByRoleIdAndUserId(Long roleId,Long userId);
    int deleteSysUserRolesByRoleIdAndUserIdIn(Long roleId, Long[] userIds);

    int deleteSysUserRolesByUserIdIn( Long[] userIds);

    int deleteSysUserRoleByUserId(Long userId);
}
