package com.taojin.admin.modules.orders.service;

import java.util.*;
import java.util.List;
import com.taojin.admin.framework.common.BaseService;
import com.taojin.admin.modules.orders.constant.OrderStatusPolyEnum;
import com.taojin.admin.modules.orders.dto.OrdersDTO;
import com.taojin.common.helper.BeanHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.taojin.admin.modules.orders.entity.Orders;
import com.taojin.admin.modules.orders.repository.OrdersRepository;

/**
 * 订单主 Service业务层处理
 *
 * @author sujg
 * @date Thu Dec 14 16:22:23 CST 2023
 */
@Service
public class OrdersService  extends BaseService<Orders, Long>
{
    @Autowired
    private OrdersRepository ordersRepository;

    public OrdersService(OrdersRepository repository) {
        super(repository,repository);
    }

    /**
     * 获取订单列表，
     * @param ordersDTO
     * @param pageable
     * @return
     */
    public List<OrdersDTO> getOrderParentList(OrdersDTO ordersDTO, Pageable pageable){
        OrderStatusPolyEnum statusEnum = OrderStatusPolyEnum.forCode(ordersDTO.getStatus());
        if (ordersDTO.getPayTypes() == null) {
            ordersDTO.setPayTypes(Collections.emptyList());
        }
        //查出主订单，子订单
        List<OrdersDTO> order_main = ordersRepository.getOrderParentList(ordersDTO,statusEnum.getStatusCodes(),
                statusEnum.getRefStatusCodes(),pageable);

        List<OrdersDTO> order_child = ordersRepository.getOrderChildList(ordersDTO,statusEnum.getStatusCodes(),
                statusEnum.getRefStatusCodes(),pageable);


        //绑定子订单
        List<OrdersDTO> lst = new ArrayList<>();
        for(OrdersDTO dto : order_main){
            String parentId = dto.getOrderCode(); // 假设orderId即为parentid
            List<OrdersDTO> matchingChildOrders = findMatchingChildOrders(order_child, parentId);
            dto.setChildOrder(matchingChildOrders);
            lst.add(dto);
        }
        return lst;

    }
    private static List<OrdersDTO> findMatchingChildOrders(List<OrdersDTO> childOrders, String parentId) {
        List<OrdersDTO> matchingChildOrders = new ArrayList<>();
        for (OrdersDTO childOrder : childOrders) {
            if (parentId.equals(childOrder.getOrderCode())) {
                matchingChildOrders.add(childOrder);
            }
        }
        return matchingChildOrders;
    }

}
