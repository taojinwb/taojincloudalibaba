package com.taojin.admin.entity.sys;

import javax.persistence.*;
import java.sql.Timestamp;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import com.taojin.admin.entity.BaseEntity;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-26
*/
@Data
@Entity(name = "sys_job")
public class SysJob extends BaseEntity {

/** 任务ID*/
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "job_id")
		private Long jobId;

/** 任务名称*/
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "job_name")
		private java.lang.String jobName;

/** 任务组名*/
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "job_group")
		private java.lang.String jobGroup;

		/** 调用目标字符串*/
		@Column(name = "invoke_target")
		@Excel(name ="调用目标字符串")
		private java.lang.String invokeTarget;

		/** cron执行表达式*/
		@Column(name = "cron_expression")
		@Excel(name ="cron执行表达式")
		private java.lang.String cronExpression;

		/** 计划执行错误策略（1立即执行 2执行一次 3放弃执行）*/
		@Column(name = "misfire_policy")
		@Excel(name ="计划执行错误策略（1立即执行 2执行一次 3放弃执行）")
		private java.lang.String misfirePolicy;

		/** 是否并发执行（0允许 1禁止）*/
		@Column(name = "concurrent")
		@Excel(name ="是否并发执行（0允许 1禁止）")
		private java.lang.String concurrent;

		/** 状态（0正常 1暂停）*/
		@Column(name = "status")
		@Excel(name ="状态（0正常 1暂停）")
		private java.lang.String status;

//		/** 创建者*/
//		@Column(name = "create_by")
//		@Excel(name ="创建者")
//		private java.lang.String createBy;
//
//		/** 创建时间*/
//		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
//		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//		@Column(name = "create_time")
//		@Excel(name ="创建时间")
//		private java.util.Date createTime;
//
//		/** 更新者*/
//		@Column(name = "update_by")
//		@Excel(name ="更新者")
//		private java.lang.String updateBy;
//
//		/** 更新时间*/
//		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
//		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//		@Column(name = "update_time")
//		@Excel(name ="更新时间")
//		private java.util.Date updateTime;

		/** 备注信息*/
		@Column(name = "remark")
		@Excel(name ="备注信息")
		private java.lang.String remark;

}
