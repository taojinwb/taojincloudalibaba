package com.taojin.admin.modules.orders.repository;

import com.taojin.admin.modules.orders.dto.OrdersDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.taojin.admin.modules.orders.entity.Orders;
import java.util.List;
import java.util.Map;

/**
 * 订单主 repository
 *
 * @author sujg
 * @date Thu Dec 14 16:22:23 CST 2023
 */
public interface OrdersRepository extends  JpaRepository<Orders, Long>, JpaSpecificationExecutor<Orders> {



    /**
     * 按照参数查询主订单编号
     * @param detailId 场馆主键
     * @param selectDto 查询参数
     * @param statusList 订单状态
     * @param refStatusList 退款状态
     * @param pageable 开始时间
     * @return
     */
    @Query(value = " select  max( o.buss_id) as venueId, max(o.buss_name )as venueName,   o.parent_oid as orderCode " +
            " ,max(extd.start_time) as orderStartTime, max(extd.end_time) as orderEndTime, max(o.update_time) as cancelTime ,\n" +
             " max(user_nick) as userNick,max(extd.buyer_phone) as phone, max( o.pay_way) as payType, max(o.items_type) as orderType,  max(o.source) as downOrderPlace,\n" +
             " max(o.status),max(buss_notes) as remark, max(ref_status) as refStatus, sum(items_price) as sumMoney, sum(disc_price) as discount," +
            " sum(items_price - disc_price) as payMoney,count(o.oid) as num " +
            "  from orders o " +
            " left join b_venue_detail_member m on m.id = o.buss_user_id " +
            " left join orders_extd extd on extd.oid = o.oid " +
            " left join c_user u on u.id = o.user_id "+
            " where (COALESCE(:statusList, null) is null or o.status in :statusList) "+
            " and (COALESCE(:refStatusList, null) is null or o.ref_status in :refStatusList) "+
//            " and (COALESCE(:refStatusList, null) is null or o.ref_status in :refStatusList) "+
            // 分类不为空时查询分类箱等+ 购买会员卡/充值会员卡且为全部类型的数据
            " and (COALESCE(:#{#selectDto.matchId}, null) is null or (o.items_category = :#{#selectDto.matchId}" +
            " or ((o.items_type = 12 or o.items_type = 15) and o.items_category is null))) "+
            " and (COALESCE(:#{#selectDto.source}, null) is null or o.source = :#{#selectDto.source}) " +
            " and (COALESCE(:#{#selectDto.search}, null) is null " +
            " or (extd.buyer_phone like %:#{#selectDto.search}% " +
            " or o.user_nick like %:#{#selectDto.search}% " +
            " or o.buss_name like %:#{#selectDto.search}% " +
            " or o.items_title like %:#{#selectDto.search}% " +
            " or o.oid like %:#{#selectDto.search}%)) " +
            " and (COALESCE(:#{#selectDto.itemsType}, null) is null or o.items_type = :#{#selectDto.itemsType}) " +
            " and (COALESCE(:#{#selectDto.downOrderStartDate}, null) is null or o.create_time >= :#{#selectDto.downOrderStartDate}) " +
            " and (COALESCE(:#{#selectDto.downOrderEndDate}, null) is null or o.create_time <= :#{#selectDto.downOrderEndDate}) "+
            " and (COALESCE(:#{#selectDto.sessionStartDate}, null) is null or extd.start_time >= :#{#selectDto.sessionStartDate}) " +
            " and (COALESCE(:#{#selectDto.sessionEndDate}, null) is null or extd.end_time <= :#{#selectDto.sessionEndDate}) " +
            " and (COALESCE(:#{#selectDto.cancelStartDate}, null) is null or (o.status in (50, 70) and o.update_time >= :#{#selectDto.cancelStartDate})) " +
            " and (COALESCE(:#{#selectDto.cancelEndDate}, null) is null or(o.status in (50, 70) and o.update_time <= :#{#selectDto.cancelEndDate})) " +
            // 作为收支明细时查询的不要支付方式为会员卡的订单数据
            " and (COALESCE(:#{#selectDto.payTypes}, null) is null or o.pay_way in :#{#selectDto.payTypes}) " +
           " group by o.parent_oid ORDER by max(o.id) desc",nativeQuery = true )
    List<OrdersDTO> getOrderParentList(@Param("selectDto") OrdersDTO selectDto,
                                                @Param("statusList") List<Integer> statusList,
                                                @Param("refStatusList") List<Integer> refStatusList,
                                                Pageable pageable);
    /**
     * 按照参数查询主订单编号
     * @param detailId 场馆主键
     * @param selectDto 查询参数
     * @param statusList 订单状态
     * @param refStatusList 退款状态
     * @param pageable 开始时间
     * @return
     */
    @Query(value = " select  o.buss_id as venueId, o.buss_name as venueName,  o.parent_oid as orderCode " +
            " ,extd.start_time as orderStartTime, extd.end_time as orderEndTime, o.update_time as cancelTime ,\n" +
            " user_nick as userNick,extd.buyer_phone as phone,  o.pay_way as payType, o.items_type as orderType,  o.source as downOrderPlace,\n" +
            " o.status,buss_notes as remark, ref_status as refStatus, items_price as summoney, disc_price as discount," +
            " items_price  as paymoney " +
            "  from orders o " +
            " left join b_venue_detail_member m on m.id = o.buss_user_id " +
            " left join orders_extd extd on extd.oid = o.oid " +
            " left join c_user u on u.id = o.user_id "+
            " where (COALESCE(:statusList, null) is null or o.status in :statusList) "+
            " and (COALESCE(:refStatusList, null) is null or o.ref_status in :refStatusList) "+
//            " and (COALESCE(:refStatusList, null) is null or o.ref_status in :refStatusList) "+
            // 分类不为空时查询分类箱等+ 购买会员卡/充值会员卡且为全部类型的数据
            " and (COALESCE(:#{#selectDto.matchId}, null) is null or (o.items_category = :#{#selectDto.matchId}" +
            " or ((o.items_type = 12 or o.items_type = 15) and o.items_category is null))) "+
            " and (COALESCE(:#{#selectDto.source}, null) is null or o.source = :#{#selectDto.source}) " +
            " and (COALESCE(:#{#selectDto.search}, null) is null " +
            " or (extd.buyer_phone like %:#{#selectDto.search}% " +
            " or o.user_nick like %:#{#selectDto.search}% " +
            " or o.buss_name like %:#{#selectDto.search}% " +
            " or o.items_title like %:#{#selectDto.search}% " +
            " or o.oid like %:#{#selectDto.search}%)) " +
            " and (COALESCE(:#{#selectDto.itemsType}, null) is null or o.items_type = :#{#selectDto.itemsType}) " +
            " and (COALESCE(:#{#selectDto.downOrderStartDate}, null) is null or o.create_time >= :#{#selectDto.downOrderStartDate}) " +
            " and (COALESCE(:#{#selectDto.downOrderEndDate}, null) is null or o.create_time <= :#{#selectDto.downOrderEndDate}) "+
            " and (COALESCE(:#{#selectDto.sessionStartDate}, null) is null or extd.start_time >= :#{#selectDto.sessionStartDate}) " +
            " and (COALESCE(:#{#selectDto.sessionEndDate}, null) is null or extd.end_time <= :#{#selectDto.sessionEndDate}) " +
            " and (COALESCE(:#{#selectDto.cancelStartDate}, null) is null or (o.status in (50, 70) and o.update_time >= :#{#selectDto.cancelStartDate})) " +
            " and (COALESCE(:#{#selectDto.cancelEndDate}, null) is null or(o.status in (50, 70) and o.update_time <= :#{#selectDto.cancelEndDate})) " +
            // 作为收支明细时查询的不要支付方式为会员卡的订单数据
            " and (COALESCE(:#{#selectDto.payTypes}, null) is null or o.pay_way in :#{#selectDto.payTypes}) " +
            " ORDER by o.id desc",nativeQuery = true )
    List<OrdersDTO> getOrderChildList(@Param("selectDto") OrdersDTO selectDto,
                                                @Param("statusList") List<Integer> statusList,
                                                @Param("refStatusList") List<Integer> refStatusList,
                                                Pageable pageable);

}
