package com.taojin.admin.entity.sys;

import javax.persistence.*;
import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.taojin.admin.entity.BaseEntity;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-11-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Entity(name = "sys_config")
public class SysConfig extends BaseEntity {

	/** 主键*/
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	/** 银行结算失败通知，1：短信通知 0：不通知，2:微信通知*/
	@Column(name = "yhjssbtz")
	@Excel(name ="银行结算失败通知，1：短信通知 0：不通知，2:微信通知")
	private java.lang.String yhjssbtz;

	/** 通知手机号码/微信号，多个用逗号分隔,*/
	@Column(name = "tzsjhm")
	@Excel(name ="通知手机号码/微信号，多个用逗号分隔,")
	private java.lang.String tzsjhm;

	/** 小程序商家入驻1：开启 0：关闭*/
	@Column(name = "xcxsjrz")
	@Excel(name ="小程序商家入驻1：开启 0：关闭")
	private java.lang.String xcxsjrz;

	/** 公司付款结算账户*/
	@Column(name = "gsjsfkzh")
	@Excel(name ="公司付款结算账户")
	private java.lang.String gsjsfkzh;

	/** 公司付款结算银行*/
	@Column(name = "gsjsfkyh")
	@Excel(name ="公司付款结算银行")
	private java.lang.String gsjsfkyh;

	/** 赛事步数限制 健步跑打卡限制步数*/
	@Column(name = "ssbsxz")
	@Excel(name ="赛事步数限制 健步跑打卡限制步数")
	private java.lang.String ssbsxz;

	/** 是否自动生成身份证1:生成 0:不生成*/
	@Column(name = "sfscsfz")
	@Excel(name ="是否自动生成身份证1:生成 0:不生成")
	private java.lang.String sfscsfz;

	/** 赛事id，该ssid维护了的，且ssscsfz为1的则生成*/
	@Column(name = "ssid")
	@Excel(name ="赛事id，该ssid维护了的，且ssscsfz为1的则生成")
	private java.lang.String ssid;

	/** 优惠券是否指定企业1：指定 0：不指定*/
	@Column(name = "sfzdqy")
	@Excel(name ="优惠券是否指定企业1：指定 0：不指定")
	private java.lang.String sfzdqy;

	/** 配置名称*/
	@Column(name = "pzmc")
	@Excel(name ="配置名称")
	private java.lang.String pzmc;

	/** 配置健名*/
	@Column(name = "pzkey")
	@Excel(name ="配置健名")
	private java.lang.String pzkey;

	/** 配置健值*/
	@Column(name = "pzvalue")
	@Excel(name ="配置健值")
	private java.lang.String pzvalue;

	/** 说明*/
	@Column(name = "sm")
	@Excel(name ="说明")
	private java.lang.String sm;

	/** 是否可用，1：启用 0：禁用*/
	@Column(name = "sfky")
	@Excel(name ="是否可用，1：启用 0：禁用")
	private java.lang.String sfky;

	/** 类型Y：内置不能删除*/
	@Column(name = "pztype")
	@Excel(name ="类型Y：内置不能删除")
	private java.lang.String pztype;

}
