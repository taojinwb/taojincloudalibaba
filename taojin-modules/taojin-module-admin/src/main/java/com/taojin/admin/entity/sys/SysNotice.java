package com.taojin.admin.entity.sys;

import javax.persistence.*;
import java.sql.Timestamp;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import com.taojin.admin.entity.BaseEntity;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-26
*/
@Data
@Entity(name = "sys_notice")
public class SysNotice extends BaseEntity {

/** 公告ID*/
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "notice_id")
		private java.lang.Long noticeId;

		/** 公告标题*/
		@Column(name = "notice_title")
		@Excel(name ="公告标题")
		private java.lang.String noticeTitle;

		/** 公告类型（1通知 2公告）*/
		@Column(name = "notice_type")
		@Excel(name ="公告类型（1通知 2公告）")
		private java.lang.String noticeType;

		/** 公告内容*/
		@Column(name = "notice_content")
		@Excel(name ="公告内容")
		private java.lang.String noticeContent;

		/** 公告状态（0正常 1关闭）*/
		@Column(name = "status")
		@Excel(name ="公告状态（0正常 1关闭）")
		private java.lang.String status;

//		/** 创建者*/
//		@Column(name = "create_by")
//		@Excel(name ="创建者")
//		private java.lang.String createBy;
//
//		/** 创建时间*/
//		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
//		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//		@Column(name = "create_time")
//		@Excel(name ="创建时间")
//		private java.util.Date createTime;
//
//		/** 更新者*/
//		@Column(name = "update_by")
//		@Excel(name ="更新者")
//		private java.lang.String updateBy;
//
//		/** 更新时间*/
//		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
//		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//		@Column(name = "update_time")
//		@Excel(name ="更新时间")
//		private java.util.Date updateTime;

		/** 备注*/
		@Column(name = "remark")
		@Excel(name ="备注")
		private java.lang.String remark;

}
