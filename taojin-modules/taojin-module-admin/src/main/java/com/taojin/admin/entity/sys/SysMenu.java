package com.taojin.admin.entity.sys;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.taojin.admin.entity.BaseEntity;

import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;

/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-26
*/
@Entity(name = "sys_menu")
@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
public class SysMenu extends BaseEntity  implements Serializable {

/** 菜单ID*/
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "menu_id")
		private Long menuId;

		/** 菜单名称*/
		@Column(name = "menu_name")
		@Excel(name ="菜单名称")
		private java.lang.String menuName;

		/** 父菜单ID*/
		@JsonSerialize(using = ToStringSerializer.class)
		@Column(name = "parent_id")
		@Excel(name ="父菜单ID")
		private Long parentId;

		/** 显示顺序*/
		@Column(name = "order_num")
		@Excel(name ="显示顺序")
		private java.lang.Integer orderNum;

		/** 路由地址*/
		@Column(name = "path")
		@Excel(name ="路由地址")
		private java.lang.String path;

		/** 组件路径*/
		@Column(name = "component")
		@Excel(name ="组件路径")
		private java.lang.String component;

		/** 路由参数*/
		@Column(name = "query")
		@Excel(name ="路由参数")
		private java.lang.String query;

		/** 是否为外链（0是 1否）*/
		@Column(name = "is_frame")
		@Excel(name ="是否为外链（0是 1否）")
		private java.lang.String isFrame;

		/** 是否缓存（0缓存 1不缓存）*/
		@Column(name = "is_cache")
		@Excel(name ="是否缓存（0缓存 1不缓存）")
		private java.lang.String isCache;

		/** 菜单类型（M目录 C菜单 F按钮）*/
		@Column(name = "menu_type")
		@Excel(name ="菜单类型（M目录 C菜单 F按钮）")
		private java.lang.String menuType;

		/** 菜单状态（0显示 1隐藏）*/
		@Column(name = "visible")
		@Excel(name ="菜单状态（0显示 1隐藏）")
		private java.lang.String visible;

		/** 菜单状态（0正常 1停用）*/
		@Column(name = "status")
		@Excel(name ="菜单状态（0正常 1停用）")
		private java.lang.String status;

		/** 权限标识*/
		@Column(name = "perms")
		@Excel(name ="权限标识")
		private java.lang.String perms;

		/** 菜单图标*/
		@Column(name = "icon")
		@Excel(name ="菜单图标")
		private java.lang.String icon;

//		/** 创建者*/
//		@Column(name = "create_by")
//		@Excel(name ="创建者")
//		private java.lang.String createBy;
//
//		/** 创建时间*/
//		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
//		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//		@Column(name = "create_time")
//		@Excel(name ="创建时间")
//		private java.util.Date createTime;
//
//		/** 更新者*/
//		@Column(name = "update_by")
//		@Excel(name ="更新者")
//		private java.lang.String updateBy;
//
//		/** 更新时间*/
//		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
//		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//		@Column(name = "update_time")
//		@Excel(name ="更新时间")
//		private java.util.Date updateTime;

		/** 备注*/
		@Column(name = "remark")
		@Excel(name ="备注")
		private java.lang.String remark;

}
