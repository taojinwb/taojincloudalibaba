package com.taojin.admin.repository.sys;

import com.taojin.admin.entity.sys.SysConfig;
import com.taojin.admin.entity.sys.SysDictData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SysDictDataRepository extends  JpaRepository<SysDictData, Long>, JpaSpecificationExecutor<SysDictData> {
    SysDictData findAllByDictCodeAndDictType(String dictcode,String dicttype);

    SysDictData findSysDictDataByDictCode(Long dictCode);

    List<SysDictData> findAllByDictType(String dictType);

    int countByDictType(String dictType);
    List<SysDictData> findAllByDictTypeAndStatusOrderByDictSort(String dicttype,String status);

    @Modifying
    @Query(value="update sys_dict_data set dict_type = :newdicttype where dict_type = :olddicttype",nativeQuery = true)
    void updateDictDataType(@Param("olddicttype") String olddicttype,@Param("newdicttype") String newdicttype);

}
