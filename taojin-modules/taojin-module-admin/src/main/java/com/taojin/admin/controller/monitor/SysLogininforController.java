package com.taojin.admin.controller.monitor;


import com.taojin.admin.entity.sys.SysLogininfor;
import com.taojin.admin.framework.common.BaseController;
import com.taojin.admin.framework.annotation.Log;
import com.taojin.admin.service.sys.SysLogininforService;
import com.taojin.admin.service.sys.SysPasswordService;
import com.taojin.admin.util.SortUtil;
import com.taojin.framework.enums.BusinessType;
import com.taojin.framework.vo.ApiResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 系统访问记录
 *
 * @author sujg
 */
@RestController
@RequestMapping("/monitor/logininfor")
@Slf4j
public class SysLogininforController extends BaseController
{
    @Autowired
    private SysLogininforService logininforService;

    @Autowired
    private SysPasswordService passwordService;

    @PreAuthorize("@ss.hasPermi('monitor:logininfor:list')")
    @GetMapping("/list")
    public ApiResult list(SysLogininfor logininfor)
    {
           Map m = SortUtil.SortParam();
            m.put("info_id",SortUtil.Desc);
        Page<SysLogininfor> list=logininforService.selectLogininforList(logininfor,this.getPageable(logininfor.getParams(),m));

        return ApiResult.OK(list);
    }



    @PreAuthorize("@ss.hasPermi('monitor:logininfor:remove')")
    @Log(title = "登录日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/{infoIds}")
    public ApiResult remove(@PathVariable Long[] infoIds)
    {
        logininforService.deleteLogininforByIds(infoIds);
        return ApiResult.OK();
    }

    @PreAuthorize("@ss.hasPermi('monitor:logininfor:remove')")
    @Log(title = "登录日志", businessType = BusinessType.CLEAN)
    @DeleteMapping("/clean")
    public ApiResult clean()
    {
        logininforService.cleanLogininfor();
        return ApiResult.OK();
    }

    @PreAuthorize("@ss.hasPermi('monitor:logininfor:unlock')")
    @Log(title = "账户解锁", businessType = BusinessType.OTHER)
    @GetMapping("/unlock/{userName}")
    public ApiResult unlock(@PathVariable("userName") String userName)
    {
        passwordService.clearLoginRecordCache(userName);
        return ApiResult.OK();
    }
}
