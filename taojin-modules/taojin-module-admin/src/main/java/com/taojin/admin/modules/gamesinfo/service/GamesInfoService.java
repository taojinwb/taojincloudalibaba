package com.taojin.admin.modules.gamesinfo.service;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Optional;

import com.taojin.admin.framework.common.BaseService;
import com.taojin.admin.modules.gamesinfo.constant.GamesTagsConfig;
import com.taojin.admin.modules.gamesinfo.dto.GamesInfoDTO;
import com.taojin.admin.modules.gamesinfo.entity.GamesInfo;
import com.taojin.admin.modules.gamesinfo.entity.GamesInfoExtd;
import com.taojin.admin.modules.gamesinfo.repository.GamesInfoExtdRepository;
import com.taojin.admin.modules.gamesinfo.repository.GamesInfoRepository;
import com.taojin.common.message.rocketmq.enums.MessageTag;
import com.taojin.common.message.rocketmq.enums.MessageTopic;
import com.taojin.common.message.rocketmq.producer.MessageProducer;
import org.apache.commons.lang.StringUtils;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.common.RemotingHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;


/**
 * 赛事信息 Service业务层处理
 *
 * @author sujg
 * @date Mon Dec 18 13:44:04 CST 2023
 */
@Service
public class GamesInfoService  extends BaseService<GamesInfo, Long> {

    /** 审核状态：15 不通过 20 通过 */
    private final Integer status=15;

    @Autowired
    private GamesInfoRepository gamesInfoRepository;

    @Autowired
    private GamesInfoExtdRepository gamesInfoExtdRepository;
    @Autowired
    private MessageProducer messageProducer;

    public GamesInfoService(GamesInfoRepository repository) {
        super(repository, repository);
    }

    /**
     * 赛事主键
     * @param status       状态（0：初始状态 10：待审核 15：审核失败 20：报名中 50：已结束 100:已经取消）
     * @param ruleType     赛事类型（10：单项赛 20：团体赛 30：单项+团体）
     * @param search       搜索字段(赛事名称)
     * @param sportsTypeId 赛事运动项目
     * @return
     */
    public Page<GamesInfoDTO> list(Pageable pageable, List<Integer> status, Integer ruleType, String search,
                                   Integer sportsTypeId) {
        if (StringUtils.isNotBlank(search)) {
            search = "%" + search + "%";
        } else {
            search = null;
        }
        Page<GamesInfoDTO> pageVO = gamesInfoRepository.findAllByParams(status, ruleType, search, sportsTypeId, pageable);
        return pageVO;
    }



    /**
     * 赛制赛事直播地址
     * @param dto
     * @throws Exception
     */
    @Transactional(rollbackFor = {Error.class, Exception.class , RuntimeException.class})
    public void setPlayUrl(GamesInfoDTO dto) throws Exception {
        GamesInfoExtd byGamesId = gamesInfoExtdRepository.findByGamesId(dto.getId());
        if(byGamesId == null) {
            throw new Exception("非法请求");
        }
        gamesInfoExtdRepository.updatePlayUrlByGamesId(dto.getPalyUrl(),byGamesId.getId());
    }


    @Transactional(rollbackFor = Exception.class)
    public void delete(Long gamesId) {
        GamesInfo info = gamesInfoRepository.findById(gamesId).orElse(null);
        Assert.isTrue(info != null && info.getIsDel() == 0, "未查询到赛事信息");
        gamesInfoRepository.updateIsDelById(gamesId);

    }

    /**
     * 赛事审核
     * @param dto 审核数据
     * @return
     * @throws Exception
     */
    @Transactional(rollbackFor = {RuntimeException.class, Error.class, Exception.class})
    public GamesInfo check(GamesInfoDTO dto) throws Exception {
        if(dto.getStatus()==null){
            dto.setStatus(15);
        }
        Optional<GamesInfo> gamesInfoOptional = gamesInfoRepository.findById(dto.getId());
        if (gamesInfoOptional == null || !gamesInfoOptional.isPresent()) {
            throw new Exception("非法请求");
        }
        GamesInfo info = gamesInfoOptional.get();
        // 不是待审核
        if (info.getStatus().intValue() != 10) {
            throw new Exception("赛事状态异常");
        }
        //修改赛事状态
        int row = gamesInfoRepository.updateStatusById(status, dto.getId());
        if (row != 1) {
            throw new Exception("审核失败，请重试！");
        }
        //审核不通过时原因不能为空
        if (dto.getStatus() == 15 && StringUtils.isBlank(dto.getAuditNotes())) {
            throw new Exception("请输入审核不通过原因！");
        }
        row = gamesInfoRepository.updateStatusById(status, dto.getId());
        if (row != 1) {
            throw new Exception("审核失败，请重试！");
        }
        //原因不为空修改审核原因
        if (StringUtils.isNotBlank(dto.getAuditNotes())) {
            row = gamesInfoExtdRepository.updateFailReasonByGamesId(dto.getAuditNotes(), info.getId());
            if (row < 1) {
                throw new Exception("审核失败，请重试！");
            }
        }
        //审核通过发送审核通过的消息
        if (dto.getStatus() == 20) {
            //修改人工审核状态为已通过
            gamesInfoExtdRepository.updateManualAuditStatusByGamesId(1, info.getId());
            messageProducer.sendMessage(MessageTopic.GAMES_TOPIC,MessageTag.GANMES_CHECK_SUCCESS,info.getId()+"", info.getTitle());

        }
        return info;
    }



}
