package com.taojin.admin.controller.sys;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.taojin.admin.entity.sys.SysUser;
import com.taojin.admin.framework.util.CasServiceUtil;
import com.taojin.admin.framework.util.JwtUtil;
import com.taojin.admin.framework.util.XmlUtils;
import com.taojin.admin.service.sys.SysDeptService;
import com.taojin.admin.service.sys.SysUserService;
import com.taojin.framework.constant.FrameworkConstant;
import com.taojin.framework.redis.RedisUtil;
import com.taojin.framework.vo.ApiResult;
import io.swagger.annotations.Api;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson2.JSONObject;

import lombok.extern.slf4j.Slf4j;

/**
 * <p>
 * CAS单点登录客户端登录认证
 * </p>
 *
 * @Author zhoujf
 * @since 2018-12-20
 */
@Slf4j
@RestController
@RequestMapping("/sys/cas/client")
@Api(tags = "单点登录",value = "单点登录")
public class CasClientController {

    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysDeptService sysDeptService;
    @Autowired
    private RedisUtil redisUtil;

    //@Value("${cas.prefixUrl}")
    private String prefixUrl="http://localhost:8443/cas";


    @GetMapping("/validateLogin")
    public Object validateLogin(@RequestParam(name="ticket") String ticket,
                                @RequestParam(name="service") String service,
                                HttpServletRequest request,
                                HttpServletResponse response) throws Exception {
        ApiResult<JSONObject> result = new ApiResult<JSONObject>();
        log.info("Rest api login.");
        try {
            String validateUrl = prefixUrl+"/p3/serviceValidate";
            String res = CasServiceUtil.getStValidate(validateUrl, ticket, service);
            log.info("res."+res);
            final String error = XmlUtils.getTextForElement(res, "authenticationFailure");
            if(StringUtils.isNotEmpty(error)) {
                throw new Exception(error);
            }
            final String principal = XmlUtils.getTextForElement(res, "user");
            if (StringUtils.isEmpty(principal)) {
                throw new Exception("No principal was found in the response from the CAS server.");
            }
            log.info("-------token----username---"+principal);
            //1. 校验用户是否有效
            SysUser sysUser = sysUserService.getSysUserByUserName(principal);
            result = sysUserService.checkUserIsEffective(sysUser);
            if(!result.isSuccess()) {
                return result;
            }
            String token = JwtUtil.sign(sysUser.getUserName(), sysUser.getPassword());
            // 设置超时时间
            redisUtil.set(FrameworkConstant.PREFIX_USER_TOKEN + token, token);
            redisUtil.expire(FrameworkConstant.PREFIX_USER_TOKEN + token, JwtUtil.EXPIRE_TIME*2 / 1000);

            //获取用户部门信息
            JSONObject obj = new JSONObject();
//            List<SysDepart> departs = sysDepartService.queryUserDeparts(sysUser.getId());
//            obj.put("departs", departs);
//            if (departs == null || departs.size() == 0) {
//                obj.put("multi_depart", 0);
//            } else if (departs.size() == 1) {
//                sysUserService.updateUserDepart(principal, departs.get(0).getOrgCode(),null);
//                obj.put("multi_depart", 1);
//            } else {
//                obj.put("multi_depart", 2);
//            }
            obj.put("token", token);
            obj.put("userInfo", sysUser);
            result.setData(obj);
            result.success("登录成功");

        } catch (Exception e) {
            //e.printStackTrace();
            result.error500(e.getMessage());
        }
        return new HttpEntity<>(result);
    }


}
