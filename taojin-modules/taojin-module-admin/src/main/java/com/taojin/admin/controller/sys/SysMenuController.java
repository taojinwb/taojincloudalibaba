package com.taojin.admin.controller.sys;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.taojin.admin.dto.sys.SysMenuDTO;
import com.taojin.admin.framework.common.BaseController;
import com.taojin.admin.entity.sys.SysMenu;
import com.taojin.admin.framework.annotation.Log;
import com.taojin.admin.service.sys.SysMenuService;
import com.taojin.framework.constant.UserConstants;
import com.taojin.framework.enums.BusinessType;
import com.taojin.framework.utils.MF;
import com.taojin.framework.vo.ApiResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 菜单信息
 *
 * @author sujg
 */
@RestController
@RequestMapping("/system/menu")
@Slf4j
@Api(tags = "菜单信息相关接口" ,value = "菜单信息相关接口" )
public class SysMenuController extends BaseController
{
    @Autowired
    private SysMenuService menuService;



    /**
     * 获取菜单列表
     */
    @ApiOperation(value = "获取菜单列表",tags = "菜单信息相关接口")
    @PreAuthorize("@ss.hasPermi('system:menu:list')")
    @GetMapping("/list")
    public ApiResult list(SysMenuDTO menu)
    {
        List<SysMenuDTO> menus = menuService.selectMenuList(menu, this.getLoginUser().getUserId());
        return ApiResult.OK(menus);
    }

    /**
     * 根据菜单编号获取详细信息
     */
    @ApiOperation(value = "根据菜单编号获取详细信息",tags = "菜单信息相关接口")
    @PreAuthorize("@ss.hasPermi('system:menu:query')")
    @GetMapping("/getInfo/{menuId}")
    public ApiResult<SysMenu> getInfo(@PathVariable Long menuId)
    {
        SysMenu sm = menuService.selectMenuById(menuId);
        return ApiResult.ok(sm);
    }

    /**
     * 获取菜单下拉树列表
     */
    @ApiOperation(value = "获取菜单下拉树列表",tags = "菜单信息相关接口")
    @GetMapping("/treeselect")
    public ApiResult treeselect(SysMenuDTO menu)
    {
        List<SysMenuDTO> menus = menuService.selectMenuList(menu, this.getLoginUser().getUserId());
        return ApiResult.ok(menuService.buildMenuTreeSelect(menus));
    }

    /**
     * 加载对应角色菜单列表树
     */
    @ApiOperation(value = "加载对应角色菜单列表树",tags = "菜单信息相关接口")
    @GetMapping(value = "/roleMenuTreeselect/{roleId}")
    public ApiResult roleMenuTreeselect(@PathVariable("roleId") Long roleId)
    {
        List<SysMenuDTO> menus = menuService.selectMenuList(this.getLoginUser().getUserId());
        Map ajax = new HashMap();
        ajax.put("checkedKeys", menuService.selectMenuListByRoleId(roleId));
        ajax.put("menus", menuService.buildMenuTreeSelect(menus));

        return ApiResult.ok(ajax);
    }

    /**
     * 新增菜单
     */
    @ApiOperation(value = "新增菜单",tags = "菜单信息相关接口")
    @PreAuthorize("@ss.hasPermi('system:menu:add')")
    @Log(title = "菜单管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public ApiResult add(@Validated @RequestBody SysMenu menu)
    {
        if (!menuService.checkMenuNameUnique(menu))
        {
            return ApiResult.error("新增菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
        }
        else if (UserConstants.YES_FRAME.equals(menu.getIsFrame()) && !MF.ishttp(menu.getPath()))
        {
            return ApiResult.error("新增菜单'" + menu.getMenuName() + "'失败，地址必须以http(s)://开头");
        }else if("1".equals(menuService.selectMenuById(menu.getParentId()).getStatus())){

            return ApiResult.error("新增菜单'" + menu.getMenuName() + "菜单已禁用，不可以添加到当前菜单下");
        }
        //menu.setCreateBy(this.getLoginUser().getUsername());
        menuService.Insert(menu);
        return ApiResult.ok("添加成功");
    }

    /**
     * 修改菜单
     */
    @ApiOperation(value = "修改菜单",tags = "菜单信息相关接口")
    @PreAuthorize("@ss.hasPermi('system:menu:edit')")
    @Log(title = "菜单管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public ApiResult edit(@Validated @RequestBody SysMenu menu)
    {
        if (!menuService.checkMenuNameUnique(menu))
        {
            return ApiResult.error("修改菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
        }
        else if (UserConstants.YES_FRAME.equals(menu.getIsFrame()) && !MF.ishttp(menu.getPath()))
        {
            return ApiResult.error("修改菜单'" + menu.getMenuName() + "'失败，地址必须以http(s)://开头");
        }
        else if (menu.getMenuId().equals(menu.getParentId()))
        {
            return ApiResult.error("修改菜单'" + menu.getMenuName() + "'失败，上级菜单不能选择自己");
        }
        else if("1".equals(menuService.selectMenuById(menu.getParentId()).getStatus())){

            return ApiResult.error("修改菜单'" + menu.getMenuName() + "菜单已禁用，不可以修改到当前菜单下");
        }
      //  menu.setUpdateBy(this.getLoginUser().getUsername());
        menuService.Insert(menu);
        return ApiResult.ok("修改成功");
    }

    /**
     * 删除菜单
     */
    @ApiOperation(value = "删除菜单",tags = "菜单信息相关接口")
    @PreAuthorize("@ss.hasPermi('system:menu:remove')")
    @Log(title = "菜单管理", businessType = BusinessType.DELETE)
    @GetMapping("/del/{menuId}")
    public ApiResult remove(@PathVariable("menuId") Long menuId)
    {
        if (menuService.hasChildByMenuId(menuId))
        {
            return ApiResult.error("存在子菜单,不允许删除");
        }
        if (menuService.checkMenuExistRole(menuId))
        {
            return ApiResult.error("菜单已分配,不允许删除");
        }
        menuService.RemoveById(menuId);
        return ApiResult.ok("删除成功");
    }
}
