package ${packageName}.${classname}.dto;

#foreach ($import in $importList)
import ${import};
#end
import javax.persistence.*;
import java.sql.Timestamp;

import com.taojin.framework.config.JpaDto;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;


/**
 * ${functionName}对象 ${tableName}
 *
 * @author ${author}
 * @date ${datetime}
 */
#if($table.crud || $table.sub)
#set($Entity="BaseEntity")
#elseif($table.tree)
#set($Entity="TreeEntity")
#end
@Data
@AllArgsConstructor
@NoArgsConstructor
@JpaDto
public class ${ClassName}DTO
{
    private static final long serialVersionUID = 1L;

#foreach ($column in $columns)

    /** $column.columnComment */
#if($column.list)
#set($parentheseIndex=$column.columnComment.indexOf("（"))
#if($parentheseIndex != -1)
    #set($comment=$column.columnComment.substring(0, $parentheseIndex))
#else
    #set($comment=$column.columnComment)
#end
    #set($comment = $comment.replaceAll('"', ''))
#if($parentheseIndex != -1)
    @Excel(name = "${comment}")
#elseif($column.javaType == 'Date')
    @Excel(name = "${comment}", width = 30, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
#else
    @Excel(name = "${comment}")
#end
#end
#if($column.columnName == $table.pkColumn.columnName)
#end
    private $column.javaType $column.javaField;
#end
#if($table.sub)

    /** $table.subTable.functionName信息 */
    private List<${subClassName}> ${subclassName}List;

#end



#if($table.sub)
    public List<${subClassName}> get${subClassName}List(){return ${subclassName}List;}

    public void set${subClassName}List(List<${subClassName}> ${subclassName}List){this.${subclassName}List = ${subclassName}List;}

#end
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Map<String, Object> params = new HashMap<>();


}
