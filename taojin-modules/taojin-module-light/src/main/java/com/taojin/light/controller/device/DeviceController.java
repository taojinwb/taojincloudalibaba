package com.taojin.light.controller.device;

import com.taojin.light.client.bean.ResultBean;
import com.taojin.light.dto.DeviceDTO;
import com.taojin.light.vo.DeviceVO;
import com.taojin.light.vo.LightLoopVO;
import com.taojin.light.service.device.DeviceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Api(
        value = "API - DeviceController",
        tags = {"设备信息接口"}
)
@RestController
@RequestMapping({"/light"})
public class DeviceController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    private DeviceService deviceService;

    public DeviceController() {
    }

    @ApiOperation(
            value = "设备信息列表",
            response = ResultBean.class
    )
    @RequestMapping(
            value = {"/list"},
            method = {RequestMethod.POST}
    )
    public ResultBean list(@RequestBody DeviceDTO deviceDTO, HttpServletRequest request) {
        ResultBean resultBean = ResultBean.getInstance();
        String token = request.getHeader("token");
        List<DeviceVO> devices = this.deviceService.list(token, deviceDTO);
        resultBean.setData(devices);
        return resultBean;
    }

    @ApiOperation(
            value = "设备回路接口",
            response = ResultBean.class
    )
    @RequestMapping(
            value = {"/getLoop/{deviceId}"},
            method = {RequestMethod.GET}
    )
    public ResultBean getLoop(@PathVariable Long deviceId) {
        ResultBean resultBean = ResultBean.getInstance();
        List<LightLoopVO> loop = this.deviceService.getLoop(deviceId);
        resultBean.setData(loop);
        return resultBean;
    }
}
