package com.taojin.light.entity.sys;

import javax.persistence.*;
import java.sql.Timestamp;

import lombok.NoArgsConstructor;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import com.taojin.light.entity.BaseEntity;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-26
*/
@Entity
@Data
@Table(name = "sys_role", schema = "", catalog = "")
public class SysRole extends BaseEntity {

/** 角色ID*/
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "role_id")
		private Long roleId;

		/** 角色名称*/
		@Column(name = "role_name")
		@Excel(name ="角色名称")
		private java.lang.String roleName;

		/** 角色权限字符串*/
		@Column(name = "role_key")
		@Excel(name ="角色权限字符串")
		private java.lang.String roleKey;

		/** 显示顺序*/
		@Column(name = "role_sort")
		@Excel(name ="显示顺序")
		private java.lang.Integer roleSort;

		/** 数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）*/
		@Column(name = "data_scope")
		@Excel(name ="数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）")
		private java.lang.String dataScope;

		/** 菜单树选择项是否关联显示*/
		@Column(name = "menu_check_strictly")
		@Excel(name ="菜单树选择项是否关联显示")
		private boolean menuCheckStrictly;

		/** 部门树选择项是否关联显示*/
		@Column(name = "dept_check_strictly")
		@Excel(name ="部门树选择项是否关联显示")
		private boolean deptCheckStrictly;

		/** 角色状态（0正常 1停用）*/
		@Column(name = "status")
		@Excel(name ="角色状态（0正常 1停用）")
		private java.lang.String status;

		/** 删除标志（0代表存在 2代表删除）*/
		@Column(name = "del_flag")
		@Excel(name ="删除标志（0代表存在 2代表删除）")
		private java.lang.String delFlag;



		/** 备注*/
		@Column(name = "remark")
		@Excel(name ="备注")
		private java.lang.String remark;
		public boolean isAdmin()
		{
			return isAdmin(this.roleId);
		}

		public static boolean isAdmin(Long roleId)
		{
			return roleId != null && 1L == roleId;
		}

}