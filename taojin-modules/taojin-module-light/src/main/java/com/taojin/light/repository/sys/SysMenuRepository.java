package com.taojin.light.repository.sys;

import com.taojin.light.entity.sys.SysMenu;
import com.taojin.light.dto.jpadto.SysMenuJPADTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SysMenuRepository extends  JpaRepository<SysMenu, Long>, JpaSpecificationExecutor<SysMenu> {
    @Query(value="select  distinct m.menu_id, m.parent_id, m.menu_name, m.path, m.component, m.`query`, m.visible, m.status, ifnull(m.perms,'') as perms, m.is_frame, " +
            " m.is_cache, m.menu_type, m.icon, m.order_num, m.create_time,m.create_by,m.update_time,m.update_by,m.remark,m.tenant_id from sys_menu m  " +
            " left join sys_role_menu rm on m.menu_id = rm.menu_id " +
            " left join sys_user_role ur on rm.role_id = ur.role_id " +
            " left join sys_role ro on ur.role_id = ro.role_id " +
            " where 1=1 " +
            " AND (:#{#sysMenuVo.params.userId} IS NULL OR ur.user_id = :#{#sysMenuVo.params.userId}) " +
            " AND (:#{#sysMenuVo.menuName} IS NULL OR m.menu_name like %:#{#sysMenuVo.menuName}%)" +
            " AND (:#{#sysMenuVo.visible} IS NULL OR m.visible = :#{#sysMenuVo.visible}) " +
            " AND (:#{#sysMenuVo.status} IS NULL OR m.status = :#{#sysMenuVo.status}) " +
            " order by m.parent_id, m.order_num ",nativeQuery = true)
    List<SysMenu> selectMenuListByUserId(SysMenuJPADTO sysMenuVo);
    @Query(value = "SELECT * FROM sys_menu " +
            "WHERE (:#{#sysMenuVo.menuName} IS NULL OR menu_name LIKE %:#{#sysMenuVo.menuName}%) " +
            "AND (:#{#sysMenuVo.visible} IS NULL OR visible = :#{#sysMenuVo.visible}) " +
            "AND (:#{#sysMenuVo.status} IS NULL OR status = :#{#sysMenuVo.status}) " +
            "AND (:#{#sysMenuVo.menuType} IS NULL OR menu_type = :#{#sysMenuVo.menuType}) " +
            "AND (:#{#sysMenuVo.parentId} IS NULL OR parent_id = :#{#sysMenuVo.parentId}) " +
            "ORDER BY parent_id, order_num", nativeQuery = true)
    List<SysMenu> findallList(@Param("sysMenuVo") SysMenuJPADTO sysMenuVo);

    @Query(value=" select distinct m.perms from sys_menu m left join sys_role_menu rm on m.menu_id = rm.menu_id" +
            " left join sys_user_role ur on rm.role_id = ur.role_id" +
            " left join sys_role r on r.role_id = ur.role_id" +
            " where m.status = '0' and r.status = '0' " +
            " and ur.user_id = :userId " +
            " ",nativeQuery = true)
    List<String>selectMenuPermsByUserId(@Param("userId") Long userId);


    @Query(value="select distinct m.perms from sys_menu m left join sys_role_menu rm on m.menu_id = rm.menu_id where m.status = '0' and rm.role_id = :roleId",nativeQuery = true)
    List<String> selectMenuPermsByRoleId(@Param("roleId") Long roleId);

    @Query(value=" select  distinct m.menu_id, m.parent_id, m.menu_name, m.path, m.component, m.`query` as query, m.visible, m.status , ifnull(m.perms,'') as perms, m.is_frame, m.is_cache, m.menu_type, m.icon, m.order_num,  m.create_time,m.create_by,m.update_time,m.update_by,m.remark,m.tenant_id " +
            " from sys_menu m where m.menu_type in ('M', 'C')" +
            " and m.status = 0 order by m.parent_id, m.order_num",nativeQuery = true)
    List<SysMenuJPADTO>selectMenuTreeAll();

    @Query(value="select  distinct m.menu_id, m.parent_id, m.menu_name, m.path, m.component, m.`query` as query, m.visible, m.status, ifnull(m.perms,'') as perms, m.is_frame, m.is_cache, m.menu_type, m.icon, m.order_num, m.create_time,m.create_by,m.update_time,m.update_by,m.remark,m.tenant_id " +
            " from sys_menu m" +
            " left join sys_role_menu rm on m.menu_id = rm.menu_id" +
            " left join sys_user_role ur on rm.role_id = ur.role_id" +
            " left join sys_role ro on ur.role_id = ro.role_id" +
            " left join sys_user u on ur.user_id = u.user_id" +
            " where u.user_id = :userId and m.menu_type in ('M', 'C') and m.status = 0  AND ro.status = 0" +
            " order by m.parent_id, m.order_num",nativeQuery = true)
    List<SysMenuJPADTO>selectMenuTreeByUserId(@Param("userId") Long userId);


    @Query(value="select m.menu_id " +
            " from sys_menu m " +
            "     left join sys_role_menu rm on m.menu_id = rm.menu_id " +
            "     where rm.role_id = :roleId " +
            " order by m.parent_id, m.order_num",nativeQuery = true)
    List<Long>selectMenuListByRoleId(@Param("roleId") Long roleId);
    /**
     * 父子联动，关联和不关连的，写了两段，下次改用queryDsl
     */
    @Query(value="select m.menu_id " +
            " from sys_menu m " +
            "            left join sys_role_menu rm on m.menu_id = rm.menu_id " +
            "        where rm.role_id = :roleId and" +
            "               m.menu_id not in (select m.parent_id from sys_menu m inner join sys_role_menu rm on m.menu_id = rm.menu_id and rm.role_id = :roleId) " +
            " order by m.parent_id, m.order_num",nativeQuery = true)
    List<Long>selectMenuListByRoleIdgl(@Param("roleId") Long roleId);

    int countByParentId(Long parentId);

   @Query(value=" select  menu_id, menu_name, parent_id, order_num, path, component, query, is_frame, is_cache, menu_type, visible, status, ifnull(perms,'') as perms, " +
           " icon, create_time,create_by,update_time,update_by,remark,tenant_id from sys_menu where menu_name=:menuName and parent_id = :parentId limit 1",nativeQuery = true )
   SysMenu checkMenuNameUnique(@Param("menuName") String menuName, @Param("parentId") Long parentId);

}
