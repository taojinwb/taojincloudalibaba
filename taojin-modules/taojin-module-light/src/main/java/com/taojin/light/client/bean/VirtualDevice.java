
package com.taojin.light.client.bean;

import com.taojin.light.dto.AtSetDTO;
import java.io.Serializable;
import java.net.Socket;

public class VirtualDevice implements Serializable {
    private static final long serialVersionUID = 1L;
    private String lightId;
    private Long deviceId;
    private Socket socket;
    private int loopNum = 0;
    private int lockStatus = 0;
    private long lastExeComdTime;
    public int sendComNum = 0;
    private AtSetDTO comdOpt;
    private ReCodeBean recode;

    public VirtualDevice() {
    }

    public String getLightId() {
        return this.lightId;
    }

    public void setLightId(String lightId) {
        this.lightId = lightId;
    }

    public int getLockStatus() {
        return this.lockStatus;
    }

    public void setLockStatus(int lockStatus) {
        this.lockStatus = lockStatus;
    }

    public long getLastExeComdTime() {
        return this.lastExeComdTime;
    }

    public void setLastExeComdTime(long lastExeComdTime) {
        this.lastExeComdTime = lastExeComdTime;
    }

    public AtSetDTO getComdOpt() {
        return this.comdOpt;
    }

    public void setComdOpt(AtSetDTO comdOpt) {
        this.comdOpt = comdOpt;
    }

    public Socket getSocket() {
        return this.socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public ReCodeBean getRecode() {
        return this.recode;
    }

    public void setRecode(ReCodeBean recode) {
        this.recode = recode;
    }

    public int getLoopNum() {
        return this.loopNum;
    }

    public void setLoopNum(int loopNum) {
        this.loopNum = loopNum;
    }

    public Long getDeviceId() {
        return this.deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String toString() {
        return "VirtualDevice [lightId=" + this.lightId + ", socket=" + this.socket + ", loopNum=" + this.loopNum + ", lockStatus=" + this.lockStatus + ", lastExeComdTime=" + this.lastExeComdTime + ", comdOpt=" + this.comdOpt.toString() + ", recode=" + this.recode.toString() + "]";
    }
}
