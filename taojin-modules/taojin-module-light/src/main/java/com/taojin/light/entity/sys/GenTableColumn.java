package com.taojin.light.entity.sys;

import javax.persistence.*;
import java.sql.Timestamp;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import com.taojin.light.entity.BaseEntity;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-26
*/
@Entity
@Data
@Table(name = "gen_table_column", schema = "taojin", catalog = "")
public class GenTableColumn extends BaseEntity {

/** 编号*/
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "column_id")
		private Long columnId;

		/** 归属表编号*/
		@JsonSerialize(using = ToStringSerializer.class)
		@Column(name = "table_id")
		@Excel(name ="归属表编号")
		private Long tableId;

		/** 列名称*/
		@Column(name = "column_name")
		@Excel(name ="列名称")
		private java.lang.String columnName;

		/** 列描述*/
		@Column(name = "column_comment")
		@Excel(name ="列描述")
		private java.lang.String columnComment;

		/** 列类型*/
		@Column(name = "column_type")
		@Excel(name ="列类型")
		private java.lang.String columnType;

		/** JAVA类型*/
		@Column(name = "java_type")
		@Excel(name ="JAVA类型")
		private java.lang.String javaType;

		/** JAVA字段名*/
		@Column(name = "java_field")
		@Excel(name ="JAVA字段名")
		private java.lang.String javaField;

		/** 是否主键（1是）*/
		@Column(name = "is_pk")
		@Excel(name ="是否主键（1是）")
		private java.lang.String isPk;

		/** 是否自增（1是）*/
		@Column(name = "is_increment")
		@Excel(name ="是否自增（1是）")
		private java.lang.String isIncrement;

		/** 是否必填（1是）*/
		@Column(name = "is_required")
		@Excel(name ="是否必填（1是）")
		private java.lang.String isRequired;

		/** 是否为插入字段（1是）*/
		@Column(name = "is_insert")
		@Excel(name ="是否为插入字段（1是）")
		private java.lang.String isInsert;

		/** 是否编辑字段（1是）*/
		@Column(name = "is_edit")
		@Excel(name ="是否编辑字段（1是）")
		private java.lang.String isEdit;

		/** 是否列表字段（1是）*/
		@Column(name = "is_list")
		@Excel(name ="是否列表字段（1是）")
		private java.lang.String isList;

		/** 是否查询字段（1是）*/
		@Column(name = "is_query")
		@Excel(name ="是否查询字段（1是）")
		private java.lang.String isQuery;

		/** 查询方式（等于、不等于、大于、小于、范围）*/
		@Column(name = "query_type")
		@Excel(name ="查询方式（等于、不等于、大于、小于、范围）")
		private java.lang.String queryType;

		/** 显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）*/
		@Column(name = "html_type")
		@Excel(name ="显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）")
		private java.lang.String htmlType;

		/** 字典类型*/
		@Column(name = "dict_type")
		@Excel(name ="字典类型")
		private java.lang.String dictType;

		/** 排序*/
		@Column(name = "sort")
		@Excel(name ="排序")
		private java.lang.Integer sort;
//
//		/** 创建者*/
//		@Column(name = "create_by")
//		@Excel(name ="创建者")
//		private java.lang.String createBy;
//
//		/** 创建时间*/
//		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
//		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//		@Column(name = "create_time")
//		@Excel(name ="创建时间")
//		private java.util.Date createTime;
//
//		/** 更新者*/
//		@Column(name = "update_by")
//		@Excel(name ="更新者")
//		private java.lang.String updateBy;
//
//		/** 更新时间*/
//		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
//		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//		@Column(name = "update_time")
//		@Excel(name ="更新时间")
//		private java.util.Date updateTime;

}