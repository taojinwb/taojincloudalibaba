
package com.taojin.light.entity.device;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@EntityListeners({AuditingEntityListener.class})
@Entity(
        name = "b_ith_device"
)
@ApiModel(
        description = "设备登记表"
)
public class BIthDevice implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(
            strategy = GenerationType.IDENTITY
    )
    private Long id;
    private String title;
    private Integer type;
    private Long authId;
    private Long detailid;
    private String serial;
    private String status;
    private String ver;
    private String ip;
    private String mac;
    private String now;
    private String t1;
    private String h1;
    private String t2;
    private String h2;
    private String emergencyCode;
    private Integer isDelete;
    @JsonFormat(
            pattern = "yyyy-MM-dd",
            timezone = "GMT+8"
    )
    private Date createTime;
    @JsonFormat(
            pattern = "yyyy-MM-dd",
            timezone = "GMT+8"
    )
    @LastModifiedDate
    private Date updateTime;

    public BIthDevice() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getAuthId() {
        return this.authId;
    }

    public void setAuthId(Long authId) {
        this.authId = authId;
    }

    public Long getDetailid() {
        return this.detailid;
    }

    public void setDetailid(Long detailid) {
        this.detailid = detailid;
    }

    public String getSerial() {
        return this.serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVer() {
        return this.ver;
    }

    public void setVer(String ver) {
        this.ver = ver;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMac() {
        return this.mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getNow() {
        return this.now;
    }

    public void setNow(String now) {
        this.now = now;
    }

    public String getT1() {
        return this.t1;
    }

    public void setT1(String t1) {
        this.t1 = t1;
    }

    public String getH1() {
        return this.h1;
    }

    public void setH1(String h1) {
        this.h1 = h1;
    }

    public String getT2() {
        return this.t2;
    }

    public void setT2(String t2) {
        this.t2 = t2;
    }

    public String getH2() {
        return this.h2;
    }

    public void setH2(String h2) {
        this.h2 = h2;
    }

    public Date getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getEmergencyCode() {
        return this.emergencyCode;
    }

    public void setEmergencyCode(String emergencyCode) {
        this.emergencyCode = emergencyCode;
    }

    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getIsDelete() {
        return this.isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String toString() {
        return "BIthDevice [id=" + this.id + ", title=" + this.title + ", type=" + this.type + ", authId=" + this.authId + ", detailid=" + this.detailid + ", serial=" + this.serial + ", status=" + this.status + ", ver=" + this.ver + ", ip=" + this.ip + ", mac=" + this.mac + ", now=" + this.now + ", t1=" + this.t1 + ", h1=" + this.h1 + ", t2=" + this.t2 + ", h2=" + this.h2 + ", emergencyCode=" + this.emergencyCode + ", isDelete=" + this.isDelete + ", createTime=" + this.createTime + ", updateTime=" + this.updateTime + "]";
    }
}
