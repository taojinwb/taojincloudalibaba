package com.taojin.light.framework.common;

import com.taojin.framework.exception.BaseException;
import com.taojin.light.dto.LoginUser;
import com.taojin.light.entity.sys.SysConfig;
import com.taojin.light.framework.config.TaojinConfig;
import com.taojin.light.framework.security.SecurityUtils;
import com.taojin.light.repository.sys.SysConfigRepository;
import com.taojin.framework.redis.RedisUtil;
import com.taojin.light.framework.util.JwtUtil;
import com.taojin.framework.utils.BF;
import com.taojin.framework.utils.ConvertUtil;
import com.taojin.framework.utils.MF;
import com.taojin.light.framework.util.TokenUtils;
import com.taojin.framework.utils.ServletUtils;
import lombok.extern.slf4j.Slf4j;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class BaseController<T> {


    @Autowired
    private SysConfigRepository sysConfigRepository;

    @Autowired
    private RedisUtil redisUtil;

    /**
     * 取前端分页参数
     * jpa是第0页开始，前端还是保留第一页开始固减1
     * @Map pageParam
     * @return
     */
    public Pageable getPageable(Map pageParam){
        return  getPageable(pageParam,Sort.by(new ArrayList<>()));
    }

    /**
     * 取前端分页参数
     * jpa是第0页开始，前端还是保留第一页开始固减1
     * @Map pageParam
     * @map sortParam   排序参数，key为字段，value为desc/asc
     * @return
     */
    public Pageable getPageable(Map pageParam,Map sortParam){
        List<Sort.Order> orderList = new ArrayList<>();
        // 假设 sortParam 是一个 Map<String, String> 对象
        for (Object key : sortParam.keySet()) {
            String value = BF.getThisString(sortParam.get(key)); // 获取排序条件
            if (value != null) {
                // 如果条件满足，将对应的字段和排序方式添加到排序列表
                if ("desc".equalsIgnoreCase(value)) {
                    orderList.add(Sort.Order.desc(BF.getThisString(key)));
                } else if ("asc".equalsIgnoreCase(value)) {
                    orderList.add(Sort.Order.asc(BF.getThisString(key)));
                }
                // 其他情况可根据需要添加额外逻辑
            }
        }
        // 将排序列表转换为 Sort 对象
        Sort sort = Sort.by(orderList);
        return  getPageable(pageParam,sort);
    }

    /**
     * 先从实体类的 param里面取分页参数，取不到，就直接取请求参数
     * Sort sort = Sort.by(
     *     Sort.Order.by("fieldName1").descending(),
     *     Sort.Order.by("fieldName2").ascending()
     * );
     * @param pageParam
     * @param sort
     * @return
     */
    public Pageable getPageable(Map pageParam,Sort sort){
        int pageNo = 1;
        int pageSize =20;

        String r_pageNo =ServletUtils.getRequest().getParameter("pageNum")==null?ServletUtils.getRequest().getParameter("pageIndex"):ServletUtils.getRequest().getParameter("pageNum");

         if(BF.APPCompare(r_pageNo,"<>","")){
             pageNo = ConvertUtil.toInt(r_pageNo);
             pageSize = ConvertUtil.toInt(ServletUtils.getRequest().getParameter("pageSize"), 20);
         }else {
             pageNo = (pageParam.get("pageNum") == null) ? ConvertUtil.toInt(pageParam.get("pageIndex"), 1) : ConvertUtil.toInt(pageParam.get("pageNum"), 1);
             pageSize = ConvertUtil.toInt(pageParam.get("pageSize"), 20);
         }
         pageNo = pageNo-1;//第0页开始

        if(sort==null) {
            return PageRequest.of(pageNo, pageSize);
        }else{
            return PageRequest.of(pageNo, pageSize, sort);
        }
    }


    /**
     * 根据配置健名查配置表配置值，可用的，最新一个。
     * @param pzkey 配置健名
     * @return 配置值
     */
    public  String getConfig(String pzkey){
        if(MF.isEmpty(pzkey)){
            throw new BaseException("配置健值不能为空！");
        }

        SysConfig sc = sysConfigRepository.findSysConfigByPzkeyAndSfky(pzkey,"1");
        if(sc == null){
            throw new BaseException("没有找到健值为【\"+pzkey+\"】的配置！");
        }

        return sc.getPzvalue()==null?"":sc.getPzvalue();
    }

    /**
     * 返回配置表某个字段的值，仅返回最早的一条记录。
     * @param zdvalue
     * @return
     */
    public  String getConfigZd(String zdvalue){
        String V = "";
//        if (MF.isEmpty(zdvalue)) {
//            throw new BaseException("需返回的字段名不能为空！");
//        }
//        Map<String ,Object> sc = sysConfigRepository.getSysConfigRow();
//        if (sc == null) {
//            throw new BaseException("没有配置表记录！");
//        }
//
//        V = sc.get(zdvalue)==null?"":sc.get(zdvalue).toString();
        return  V;
    }
    /**
     * 获取用户缓存信息
     */
    public LoginUser getLoginUser()
    {
        return SecurityUtils.getLoginUser();
    }
    /**
     * 根据token，获取adminuser phone
     * @param token
     * @return
     */
    public Long getUsernameByToken(String token){
        try {
            TokenUtils.verifyToken(token,redisUtil);
        }catch (Exception e){//校验失败的，则该token可能是其他服务生成的token
            return (Long)redisUtil.get(token);  //缓存没有返回null
        }
        return MF.stringToLong(JwtUtil.getUsername(token));
    }
    /**
     * 导出excel
     *
     * @param
     */
    protected ModelAndView exportXls(List<T> exportList , T object, Class<T> clazz, String title) {
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());

        //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.FILE_NAME, title);
        mv.addObject(NormalExcelConstants.CLASS, clazz);
        //update-begin--Author:liusq  Date:20210126 for：图片导出报错，ImageBasePath未设置--------------------
        ExportParams exportParams = new ExportParams(title + "报表", "导出人:" + this.getLoginUser().getUsername(), title);
        exportParams.setImageBasePath(TaojinConfig.getProfile());
        //update-end--Author:liusq  Date:20210126 for：图片导出报错，ImageBasePath未设置----------------------
        mv.addObject(NormalExcelConstants.PARAMS, exportParams);
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);

         return mv;
    }
    /**
     * 根据每页sheet数量导出多sheet
     *
     * @param request
     * @param object 实体类
     * @param clazz 实体类class
     * @param title 标题
     * @param exportFields 导出字段自定义
     * @param pageNum 每个sheet的数据条数
     * @param request
     */
    protected ModelAndView exportXlsSheet(HttpServletRequest request, T object, Class<T> clazz, String title,String exportFields,Integer pageNum) {
//        // Step.1 组装查询条件
//        QueryWrapper<T> queryWrapper = QueryGenerator.initQueryWrapper(object, request.getParameterMap());
//        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//        // Step.2 计算分页sheet数据
//        double total = service.count();
//        int count = (int)Math.ceil(total/pageNum);
//        //update-begin-author:liusq---date:20220629--for: 多sheet导出根据选择导出写法调整 ---
//        // Step.3  过滤选中数据
//        String selections = request.getParameter("selections");
//        if (oConvertUtils.isNotEmpty(selections)) {
//            List<String> selectionList = Arrays.asList(selections.split(","));
//            queryWrapper.in("id",selectionList);
//        }
//        //update-end-author:liusq---date:20220629--for: 多sheet导出根据选择导出写法调整 ---
//        // Step.4 多sheet处理
//        List<Map<String, Object>> listMap = new ArrayList<Map<String, Object>>();
//        for (int i = 1; i <=count ; i++) {
//            Page<T> page = new Page<T>(i, pageNum);
//            IPage<T> pageList = service.page(page, queryWrapper);
//            List<T> exportList = pageList.getRecords();
//            Map<String, Object> map = new HashMap<>(5);
//            ExportParams  exportParams=new ExportParams(title + "报表", "导出人:" + sysUser.getRealname(), title+i, lkdlcBaseConfig.getPath().getUpload());
//            exportParams.setType(ExcelType.XSSF);
//            //map.put("title",exportParams);
//            //表格Title
//            map.put(NormalExcelConstants.PARAMS,exportParams);
//            //表格对应实体
//            map.put(NormalExcelConstants.CLASS,clazz);
//            //数据集合
//            map.put(NormalExcelConstants.DATA_LIST, exportList);
//            listMap.add(map);
//        }
//        // Step.4 AutoPoi 导出Excel
//        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
//        //此处设置的filename无效 ,前端会重更新设置一下
//        mv.addObject(NormalExcelConstants.FILE_NAME, title);
//        mv.addObject(NormalExcelConstants.MAP_LIST, listMap);
        return null;
    }


}
