package com.taojin.light.repository.sys;

import com.taojin.light.entity.sys.SysConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * 系统配置表
 * @author sujg
 *
 */


public interface SysConfigRepository extends  JpaRepository<SysConfig, Long>, JpaSpecificationExecutor<SysConfig>{
    SysConfig findSysConfigByPzkeyAndSfky(String configkey,String sfky);


}
