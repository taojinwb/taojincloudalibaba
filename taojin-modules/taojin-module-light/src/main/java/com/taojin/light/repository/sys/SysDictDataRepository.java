package com.taojin.light.repository.sys;

import com.taojin.light.entity.sys.SysConfig;
import com.taojin.light.entity.sys.SysDictData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SysDictDataRepository extends  JpaRepository<SysDictData, Long>, JpaSpecificationExecutor<SysDictData> {
    SysDictData findAllByDictCodeAndDictType(String dictcode,String dicttype);

    SysDictData findSysDictDataByDictCode(Long dictCode);

    List<SysDictData> findAllByDictType(String dictType);

    int countByDictType(String dictType);
    List<SysDictData> findAllByDictTypeAndStatusOrderByDictSort(String dicttype,String status);

    @Modifying
    @Query(value="update sys_dict_data set dict_type = :newdicttype where dict_type = :olddicttype",nativeQuery = true)
    void updateDictDataType(String olddicttype,String newdicttype);

}
