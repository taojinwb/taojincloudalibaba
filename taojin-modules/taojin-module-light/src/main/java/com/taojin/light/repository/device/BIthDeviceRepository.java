package com.taojin.light.repository.device;

import com.taojin.light.dto.LightOrderDTO;
import com.taojin.light.entity.device.BIthDevice;
import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface BIthDeviceRepository extends JpaRepository<BIthDevice, Long>, JpaSpecificationExecutor<BIthDevice> {
    @Transactional
    @Modifying
    @Query("update b_ith_device set mac=?2 where id=?1 ")
    int updateMacForId(Long id, String macAddress);

    @Transactional
    @Modifying
    @Query("update b_ith_device set ip=?2 where id=?1 ")
    int updateIpForId(Long id, String ip);

    @Transactional
    @Modifying
    @Query("update b_ith_device set ip=?2,now=?3 where id=?1 ")
    int updateIpForId(Long id, String ip, String time);

    @Transactional
    @Modifying
    @Query("update b_ith_device set serial=?2,ip=?3,now=?4 where id=?1 ")
    int regDeviceForId(Long id, String serial, String ip, String time);

    @Transactional
    @Modifying
    @Query("update b_ith_device set now=?2 where id=?1 ")
    int updateTimeForId(Long id, String time);

    @Transactional
    @Modifying
    @Query("update b_ith_device set serial=?2 where id=?1 ")
    int updateNameForId(Long id, String serial);

    @Transactional
    @Modifying
    @Query("update b_ith_device set status=?2 where id=?1 ")
    int updateStatusForId(Long id, String status);

    @Query(
            value = "select d.* from b_ith_device d where d.auth_id = ?1  and  if (?2 is not null,d.type = ?2,1=1) and  if (?3 is not null,d.pro_type = ?3,1=1) and  d.is_delete = 0 order by d.create_time desc ",
            nativeQuery = true
    )
    List<BIthDevice> list(Long authId, Integer type, Long proType);

    @Query(
            value = "select auth_id from b_ith_device where id = ?1",
            nativeQuery = true
    )
    Long deviceOwner(Long id);

    @Query(
            value = "select authenticationid from b_venue_detail_user where username = ?1 and status = 0 and is_delete = 0",
            nativeQuery = true
    )
    Long getAuthIdByPhone(String phone);

   @Query(value=" select d.oid,d.start_time,d.end_time,e.id as deviceid from orders  A\n"+
    "  left join b_venue_field b on a.items_id = b.id\n"+
   "  left join pay_record c on a.oid = c.oid\n"+
   "  left join orders_extd d on d.oid = a.oid\n"+
   "  left join b_ith_device e on e.venuefiled_id = b.id\n"+
   "  where c.pay_status=10 and a.status in(40,50,70)" +
   "  and b.id in(?1) and  (start_time > NOW() - INTERVAL 20 MINUTE\n" +
           "  or end_time > NOW() - INTERVAL 20 MINUTE)",nativeQuery = true)
    List<LightOrderDTO> getVenueFieldByorder(String venuefieldid);
}
