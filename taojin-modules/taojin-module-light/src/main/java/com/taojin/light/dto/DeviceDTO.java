package com.taojin.light.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(
        value = "设备信息DTO",
        description = "设备信息DTO"
)
public class DeviceDTO {
    @ApiModelProperty(
            value = "设备类型(test: 1闸机,2灯控   pro: 4闸机,5灯控  )",
            name = "type"
    )
    private Integer type;
    @ApiModelProperty(
            value = "运行类型",
            name = "proType"
    )
    private Long proType;

    public DeviceDTO() {
    }

    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getProType() {
        return this.proType;
    }

    public void setProType(Long proType) {
        this.proType = proType;
    }

    public String toString() {
        return "DeviceDTO [type=" + this.type + ", proType=" + this.proType + "]";
    }
}
