package com.taojin.light.service.device;

import com.taojin.light.client.common.BeanConvertUtil;
import com.taojin.light.client.common.DateUtils;
import com.taojin.light.client.common.UserUtil;
import com.taojin.light.dto.DeviceDTO;
import com.taojin.light.vo.DeviceVO;
import com.taojin.light.vo.LightLoopVO;
import com.taojin.light.entity.device.BIthDevice;
import com.taojin.light.entity.device.BIthLightLoop;

import java.util.List;
import javax.annotation.Resource;

import com.taojin.light.repository.device.BIthDeviceRepository;
import com.taojin.light.repository.device.BIthLightLoopRepository;
import com.taojin.light.repository.device.BIthLoopFieldRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class DeviceService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Resource
    private BIthDeviceRepository bIthDeviceRepository;
    @Resource
    private BIthLightLoopRepository bIthLightLoopRepository;
    @Resource
    private BIthLoopFieldRepository bIthLoopFieldRepository;
    @Resource
    private UserUtil userUtil;

    public DeviceService() {
    }

    public List<DeviceVO> list(String token, DeviceDTO deviceDTO) {
        Long authId = this.userUtil.getAuthIdForToken(token);
        this.logger.info("企业ID ={}, diviceDto = {}", authId, deviceDTO.toString());
        List<BIthDevice> deviceList = this.bIthDeviceRepository.list(authId, deviceDTO.getType(), deviceDTO.getProType());
        String currentTime = DateUtils.getDeviceHitTime();
        List<DeviceVO> deviceVOS = BeanConvertUtil.converListTo(deviceList, DeviceVO::new, (s, t) -> {
            t.setCurrentTime(currentTime);
        });
        this.logger.info("deivce list ={}", deviceVOS.size());
        return deviceVOS;
    }

    public List<LightLoopVO> getLoop(Long deviceId) {
        List<BIthLightLoop> loop = this.bIthLightLoopRepository.getLoop(deviceId);
        List<LightLoopVO> loopVOS = BeanConvertUtil.converListTo(loop, LightLoopVO::new);
        loopVOS.stream().forEach((lightLoopVO) -> {
            lightLoopVO.setVenueTitle(this.bIthLoopFieldRepository.getVenueTitle(lightLoopVO.getId()));
        });
        return loopVOS;
    }
}
