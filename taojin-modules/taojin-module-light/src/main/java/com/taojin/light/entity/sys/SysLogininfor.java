package com.taojin.light.entity.sys;

import javax.persistence.*;
import java.sql.Timestamp;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import com.taojin.light.entity.BaseEntity;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-26
*/
@Entity
@Data
@Table(name = "sys_logininfor", schema = "", catalog = "")
public class SysLogininfor extends BaseEntity {

/** 访问ID*/
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "info_id")
		private Long infoId;

		/** 用户账号*/
		@Column(name = "user_name")
		@Excel(name ="用户账号")
		private java.lang.String userName;

		/** 登录IP地址*/
		@Column(name = "ipaddr")
		@Excel(name ="登录IP地址")
		private java.lang.String ipaddr;

		/** 登录地点*/
		@Column(name = "login_location")
		@Excel(name ="登录地点")
		private java.lang.String loginLocation;

		/** 浏览器类型*/
		@Column(name = "browser")
		@Excel(name ="浏览器类型")
		private java.lang.String browser;

		/** 操作系统*/
		@Column(name = "os")
		@Excel(name ="操作系统")
		private java.lang.String os;

		/** 登录状态（0成功 1失败）*/
		@Column(name = "status")
		@Excel(name ="登录状态（0成功 1失败）")
		private java.lang.String status;

		/** 提示消息*/
		@Column(name = "msg")
		@Excel(name ="提示消息")
		private java.lang.String msg;

		/** 访问时间*/
		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
		@Column(name = "login_time")
		@Excel(name ="访问时间")
		private java.util.Date loginTime;

}