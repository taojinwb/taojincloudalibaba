package com.taojin.light.dto.sys;

import javax.persistence.*;
import java.sql.Timestamp;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-30
*/
@Data
public class SysUserDTO {
		private Long userId;

		@Excel(name ="部门ID")
		private java.lang.String deptId;

		@Excel(name ="用户账号")
		private java.lang.String userName;

		@Excel(name ="用户昵称")
		private java.lang.String nickName;

		@Excel(name ="用户类型（00系统用户）")
		private java.lang.String userType;

		@Excel(name ="用户邮箱")
		private java.lang.String email;

		@Excel(name ="手机号码")
		private java.lang.String phonenumber;

		@Excel(name ="用户性别（0男 1女 2未知）")
		private java.lang.String sex;

		@Excel(name ="头像地址")
		private java.lang.String avatar;

		@Excel(name ="密码")
		private java.lang.String password;

		@Excel(name ="帐号状态（0正常 1停用）")
		private java.lang.String status;

		@Excel(name ="删除标志（0代表存在 2代表删除）")
		private java.lang.String delFlag;

		@Excel(name ="最后登录IP")
		private java.lang.String loginIp;

		@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
		@Excel(name ="最后登录时间")
		private java.util.Date loginDate;

		@Excel(name ="创建者")
		private java.lang.String createBy;

		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
		@Excel(name ="创建时间")
		private java.util.Date createTime;

		@Excel(name ="更新者")
		private java.lang.String updateBy;

		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
		@Excel(name ="更新时间")
		private java.util.Date updateTime;

		@Excel(name ="备注")
		private java.lang.String remark;

}