package com.taojin.light.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(
        value = "设备VO",
        description = "设备VO"
)
public class DeviceVO {
    @ApiModelProperty(
            value = "设备ID",
            name = "id"
    )
    private Long id;
    @ApiModelProperty(
            value = "设备名称",
            name = "title"
    )
    private String title;
    @ApiModelProperty(
            value = "设备类型",
            name = "type"
    )
    private Integer type;
    @ApiModelProperty(
            value = "当前状态",
            name = "status"
    )
    private String status;
    @ApiModelProperty(
            value = "设备IP",
            name = "ip"
    )
    private String ip;
    @ApiModelProperty(
            value = "设备的序列号",
            name = "serial"
    )
    private String serial;
    @ApiModelProperty(
            value = "应急码;用于应急的时刻开门用",
            name = "emergencyCode"
    )
    private String emergencyCode;
    private String now;
    private String currentTime;

    public DeviceVO() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getType() {
        return this.type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIp() {
        return this.ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getSerial() {
        return this.serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getEmergencyCode() {
        return this.emergencyCode;
    }

    public void setEmergencyCode(String emergencyCode) {
        this.emergencyCode = emergencyCode;
    }

    public String getNow() {
        return this.now;
    }

    public void setNow(String now) {
        this.now = now;
    }

    public String getCurrentTime() {
        return this.currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public String toString() {
        return "DeviceVO [id=" + this.id + ", title=" + this.title + ", type=" + this.type + ", status=" + this.status + ", ip=" + this.ip + ", serial=" + this.serial + ", emergencyCode=" + this.emergencyCode + ", now=" + this.now + ", currentTime=" + this.currentTime + "]";
    }
}
