package com.taojin.light.entity.sys;

import javax.persistence.*;
import java.sql.Timestamp;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import com.taojin.light.entity.BaseEntity;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-26
*/
@Entity
@Data
@Table(name = "sys_dept", schema = "taojin", catalog = "")
public class SysDept extends BaseEntity {

/** 部门id*/
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "dept_id")
		private Long deptId;

		/** 父部门id*/
		@JsonSerialize(using = ToStringSerializer.class)
		@Column(name = "parent_id" )
		@Excel(name ="父部门id")
		private Long parentId;

		/** 祖级列表*/
		@Column(name = "ancestors")
		@Excel(name ="祖级列表")
		private java.lang.String ancestors;

		/** 部门名称*/
		@Column(name = "dept_name")
		@Excel(name ="部门名称")
		private java.lang.String deptName;

		/** 显示顺序*/
		@Column(name = "order_num")
		@Excel(name ="显示顺序")
		private java.lang.Integer orderNum;

		/** 负责人*/
		@Column(name = "leader")
		@Excel(name ="负责人")
		private java.lang.String leader;

		/** 联系电话*/
		@Column(name = "phone")
		@Excel(name ="联系电话")
		private java.lang.String phone;

		/** 邮箱*/
		@Column(name = "email")
		@Excel(name ="邮箱")
		private java.lang.String email;

		/** 部门状态（0正常 1停用）*/
		@Column(name = "status")
		@Excel(name ="部门状态（0正常 1停用）")
		private java.lang.String status;

		/** 删除标志（0代表存在 2代表删除）*/
		@Column(name = "del_flag")
		@Excel(name ="删除标志（0代表存在 2代表删除）")
		private java.lang.String delFlag;

//		/** 创建者*/
//		@Column(name = "create_by")
//		@Excel(name ="创建者")
//		private java.lang.String createBy;
//
//		/** 创建时间*/
//		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
//		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//		@Column(name = "create_time")
//		@Excel(name ="创建时间")
//		private java.util.Date createTime;
//
//		/** 更新者*/
//		@Column(name = "update_by")
//		@Excel(name ="更新者")
//		private java.lang.String updateBy;
//
//		/** 更新时间*/
//		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
//		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//		@Column(name = "update_time")
//		@Excel(name ="更新时间")
//		private java.util.Date updateTime;

}