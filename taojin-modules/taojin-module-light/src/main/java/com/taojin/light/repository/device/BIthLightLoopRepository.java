package com.taojin.light.repository.device;
import com.taojin.light.entity.device.BIthLightLoop;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface BIthLightLoopRepository extends JpaRepository<BIthLightLoop, Long>, JpaSpecificationExecutor<BIthLightLoop> {
    @Transactional
    @Modifying
    @Query("update b_ith_light_loop set status=?3 where deviceId=?1  and loopNumber=?2")
    int updateLoopStatus(Long deviceId, int loopNumber, int status);

    @Query(
            value = "select l.* from b_ith_light_loop l where l.device_id = ?1 and l.is_delete = 0 order by loop_number",
            nativeQuery = true
    )
    List<BIthLightLoop> getLoop(Long deviceId);
}
