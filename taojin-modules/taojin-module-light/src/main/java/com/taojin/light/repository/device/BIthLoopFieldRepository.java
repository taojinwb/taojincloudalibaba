package com.taojin.light.repository.device;
import com.taojin.light.entity.device.BIthLoopField;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface BIthLoopFieldRepository extends JpaRepository<BIthLoopField, Long>, JpaSpecificationExecutor<BIthLoopField> {
    @Query(
            value = "select name from b_venue_field where id = (select field_id from b_ith_loop_field where loop_id = ?1)",
            nativeQuery = true
    )
    String getVenueTitle(Long loopId);
}
