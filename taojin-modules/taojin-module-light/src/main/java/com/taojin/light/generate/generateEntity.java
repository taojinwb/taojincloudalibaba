package com.taojin.light.generate;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class generateEntity {
    private static String jdbcUrl = "jdbc:mysql://192.168.0.200:3306/likedong?useUnicode=true&characterEncoding=UTF-8&useSSL=false&autoReconnect=true&failOverReadOnly=false&serverTimezone=Asia/Shanghai";
    private static String username = "root";
    private static String password = "lkd@123456";//lkd@123456
    private static  String tbname="sys_config";
    private static  String filepath="E:\\栎刻动\\generate\\entity\\";
    private static String packagestr="com.taojin.admin.entity.sys;";
    private static String packagedtostr="com.taojin.admin.dto.sys;";
    private static  String catalog="likedong";
    private static String classname="";
        public static void main(String[] args) {
            String[] tlst = "VIEW_ORDER_LIGHT".split(",");
            for(String t:tlst ) {
                tbname = t;
                classname = tbname.substring(0, 1).toUpperCase() + convertToCamelCase(tbname).substring(1);
                String entity_str = getEntityContent();
                createfile(entity_str);
            }
        }


    /**
     * 创建实体类，联合主键的，另外手写
     * @return
     */
    public static String  getEntityContent(){
            StringBuilder sa = new StringBuilder();

            StringBuilder sa_dto = new StringBuilder();
            try (
                Connection connection = DriverManager.getConnection(jdbcUrl, username, password)) {
                DatabaseMetaData metaData = connection.getMetaData();
                // 获取表的中文注释
                ResultSet tables = metaData.getTables(catalog, catalog, tbname, new String[]{"TABLE"});
                String tb_remarks ="";
                while (tables.next()) {
                    String tableName = tables.getString("TABLE_NAME");
                    tb_remarks = tables.getString("REMARKS");
                    System.out.println("Table Name: " + tableName);
                    System.out.println("Table Comment: " + tb_remarks);
                }
                // 获取主键信息
                ResultSet primaryKeys = metaData.getPrimaryKeys(catalog, catalog, tbname);
                Set<String> primaryKeyColumns = new HashSet<>();
                while (primaryKeys.next()) {
                    String primaryKeyColumnName = primaryKeys.getString("COLUMN_NAME");

                    primaryKeyColumns.add(primaryKeyColumnName);
                }
                // 添加包名和导入语句
                StringBuilder entityStringBuilder = new StringBuilder();
                sa.append("package "+packagestr+"\n\n");
                sa.append("import javax.persistence.*;\n");
                sa.append("import java.sql.Timestamp;\n");
                sa.append("import org.jeecgframework.poi.excel.annotation.Excel;\n");
                sa.append("import com.fasterxml.jackson.databind.annotation.JsonSerialize;\n");
                sa.append("import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;\n");
                sa.append("import lombok.AllArgsConstructor;\n");
                sa.append("import lombok.NoArgsConstructor;\n");
                sa.append("import org.hibernate.annotations.DynamicInsert;\n");
                sa.append("import org.hibernate.annotations.DynamicUpdate;\n");
                sa.append("import java.util.Date;\n");
                sa.append("import org.springframework.format.annotation.DateTimeFormat;\n");
                sa.append("import com.fasterxml.jackson.annotation.JsonFormat;\n");
                sa.append("import com.taojin.light.entity.BaseEntity;\n");
                sa.append("import lombok.Data;\n");
                sa.append("/**\n");

                LocalDate currentDate = LocalDate.now();
                sa.append(" *"+tb_remarks+"实体类\n");
                sa.append(" *\n");
                sa.append(" * @author sujg\n");
                sa.append(" * @data  "+currentDate);
                sa.append("\n*/\n");
                sa_dto.append(sa.toString().replace(packagestr,packagedtostr));
                // 添加类的注解
                entityStringBuilder.append("@Entity\n");
                entityStringBuilder.append("@Data\n");
                entityStringBuilder.append("@AllArgsConstructor\n");
                entityStringBuilder.append("@NoArgsConstructor\n");
                entityStringBuilder.append("@DynamicInsert\n");
                entityStringBuilder.append("@DynamicUpdate\n");
                entityStringBuilder.append("@Table(name = \""+tbname+"\", schema = \"\", catalog = \""+tb_remarks+"\")\n");
                entityStringBuilder.append("public class "+classname+" extends BaseEntity {\n\n");
                sa_dto.append("public class "+classname+"DTO"+" {\n");
                ResultSet resultSet = metaData.getColumns(catalog, catalog, tbname, null);
                while (resultSet.next()) {
                    String columnName = resultSet.getString("COLUMN_NAME");
                    String columnType = resultSet.getString("TYPE_NAME");
                    String tableComment = resultSet.getString("REMARKS");
                    // 判断是否为主键列
                    boolean isPrimaryKey = primaryKeyColumns.contains(columnName);
                    if(isPrimaryKey){
                        // 将数据库字段名转换为驼峰命名规则
                        String fieldName = convertToCamelCase(columnName);
                        // 生成JPA实体类的字段声明，并添加相关注解
                        entityStringBuilder.append("/** "+tableComment+"*/\n");
                        entityStringBuilder.append("\t\t@Id\n");
                        entityStringBuilder.append("\t\t@GeneratedValue(strategy = GenerationType.IDENTITY)\n");
                        entityStringBuilder.append("\t\t@Column(name = \"" + columnName + "\")\n");
                       // entityStringBuilder.append("@ApiModelProperty(\""+tableComment+"\")\n");
                        entityStringBuilder.append( "\t\tprivate " + getJavaTypeFromDbType(columnType) + " " + fieldName + ";\n\n");
                        sa_dto.append("\t\tprivate " + getJavaTypeFromDbType(columnType) + " " + fieldName + ";\n\n");

                    }else{
                        // 将数据库字段名转换为驼峰命名规则
                        String fieldName = convertToCamelCase(columnName);
                        // 生成JPA实体类的字段声明，并添加相关注解
                        entityStringBuilder.append("\t\t/** "+tableComment+"*/\n");
                        if(columnType.equals("BIGINT")){//当实体类中的字段为Long类型，且值超过前端js显示的长度范围时会导致前端回显错误
                            entityStringBuilder.append("\t\t@JsonSerialize(using = ToStringSerializer.class)\n");
                            sa_dto.append("\t\t@JsonSerialize(using = ToStringSerializer.class)\n");
                        }
                        if(columnType.equals("DATE") || columnType.equals("DATETIME")){
                            entityStringBuilder.append("\t\t@JsonFormat(timezone = \"GMT+8\", pattern = \"yyyy-MM-dd HH:mm:ss\")\n");
                            entityStringBuilder.append("\t\t@DateTimeFormat(pattern = \"yyyy-MM-dd HH:mm:ss\")\n");
                            sa_dto.append("\t\t@JsonFormat(timezone = \"GMT+8\", pattern = \"yyyy-MM-dd HH:mm:ss\")\n");
                            sa_dto.append("\t\t@DateTimeFormat(pattern = \"yyyy-MM-dd HH:mm:ss\")\n");
                        }
                        entityStringBuilder.append("\t\t@Column(name = \"" + columnName + "\")\n");
                        entityStringBuilder.append("\t\t@Excel(name =\""+tableComment+"\")\n");
                        //entityStringBuilder.append("@ApiModelProperty(\""+tableComment+"\")\n");
                        entityStringBuilder.append( "\t\tprivate " + getJavaTypeFromDbType(columnType) + " " + fieldName + ";\n\n");
                        sa_dto.append("\t\t@Excel(name =\""+tableComment+"\")\n");
                        sa_dto.append("\t\tprivate " + getJavaTypeFromDbType(columnType) + " " + fieldName + ";\n\n");

                    }
                }
                sa.append(entityStringBuilder);
                sa.append("}");
                sa_dto.append("}");
                //System.out.println(sa.toString());
                createDTOFile(sa_dto.toString());
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return sa.toString();
        }
        private static void createDTOFile(String content){
            String fileName = classname+"DTO.java"; // 指定文件名
            File file = new File(filepath+"\\DTO\\", fileName);
            try (
                    BufferedWriter writer = new BufferedWriter(new FileWriter(file,false))) {
                // 将生成的内容写入文件
                writer.write(content);
                System.out.println("File generated  DTO successfully at: " + file.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        private static void createfile(String content){
                String fileName = classname+".java"; // 指定文件名
                File file = new File(filepath, fileName);
                try (
                        BufferedWriter writer = new BufferedWriter(new FileWriter(file,false))) {
                    // 将生成的内容写入文件
                    writer.write(content);
                    System.out.println("File generated successfully at: " + file.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        // 根据数据库字段类型获取Java对应类型
        private static String getJavaTypeFromDbType(String dbType) {
            // 这里可以根据需要添加更多的数据库类型映射
            switch (dbType.toUpperCase()) {
                case "VARCHAR":
                    return "java.lang.String";
                case "NVARCHAR":
                    return "java.lang.String";
                case "INT":
                    return "java.lang.Integer";
                case "BIGINT":
                    return "Long";
                case "TEXT":
                    return "java.lang.String";
                case "DATE":
                    return "java.util.Date";
                case "DATETIME":
                    return "java.util.Date";
                case "INTEGER":
                    return "java.lang.Integer";
                default:
                    return "java.lang.String";
            }
        }

        // 将数据库字段名转换为驼峰命名规则
        private static String convertToCamelCase(String columnName) {
            String[] parts = columnName.split("_");
            StringBuilder camelCaseName = new StringBuilder(parts[0]);

            for (int i = 1; i < parts.length; i++) {
                camelCaseName.append(parts[i].substring(0, 1).toUpperCase()).append(parts[i].substring(1));
            }

            return camelCaseName.toString();
        }
    }
