package com.taojin.light.entity.sys;

import javax.persistence.*;
import java.sql.Timestamp;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import com.taojin.light.entity.BaseEntity;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-26
*/
@Entity
@Data
@Table(name = "sys_dict_type", schema = "taojin", catalog = "")
public class SysDictType extends BaseEntity {

/** 字典主键*/
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "dict_id")
		private Long dictId;

		/** 字典名称*/
		@Column(name = "dict_name")
		@Excel(name ="字典名称")
		private java.lang.String dictName;

		/** 字典类型*/
		@Column(name = "dict_type")
		@Excel(name ="字典类型")
		private java.lang.String dictType;

		/** 状态（0正常 1停用）*/
		@Column(name = "status")
		@Excel(name ="状态（0正常 1停用）")
		private java.lang.String status;

//		/** 创建者*/
//		@Column(name = "create_by")
//		@Excel(name ="创建者")
//		private java.lang.String createBy;
//
//		/** 创建时间*/
//		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
//		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//		@Column(name = "create_time")
//		@Excel(name ="创建时间")
//		private java.util.Date createTime;
//
//		/** 更新者*/
//		@Column(name = "update_by")
//		@Excel(name ="更新者")
//		private java.lang.String updateBy;
//
//		/** 更新时间*/
//		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
//		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//		@Column(name = "update_time")
//		@Excel(name ="更新时间")
//		private java.util.Date updateTime;

		/** 备注*/
		@Column(name = "remark")
		@Excel(name ="备注")
		private java.lang.String remark;

}