package com.taojin.light.entity.sys;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import com.taojin.light.entity.BaseEntity;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-26
*/
@Entity
@Data
@Table(name = "sys_user_role", schema = "taojin", catalog = "")
@IdClass(SysUserRolePK.class)
public class SysUserRole extends BaseEntity {
			@Id
			@Column(name = "user_id")
			private Long userId;
			@Id
			@Column(name = "role_id")
			private Long roleId;

}