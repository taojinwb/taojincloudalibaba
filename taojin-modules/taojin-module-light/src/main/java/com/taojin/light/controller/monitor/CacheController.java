package com.taojin.light.controller.monitor;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.taojin.framework.vo.ApiResult;
import com.taojin.light.framework.constant.CacheConstant;
import com.taojin.light.vo.SysCache;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * 缓存监控
 *
 * @author sujg
 */
@RestController
@RequestMapping("/monitor/cache")
@Slf4j
@Api(tags = "缓存监控相关接口" ,value = "缓存监控相关接口" )
public class CacheController
{
    @Autowired
    private RedisTemplate<String, String> redisTemplate;


    private final static List<SysCache> caches = new ArrayList<SysCache>();
    {
        caches.add(new SysCache(CacheConstant.LOGIN_TOKEN_KEY, "用户信息"));
        caches.add(new SysCache(CacheConstant.SYS_CONFIG_KEY, "配置信息"));
        caches.add(new SysCache(CacheConstant.SYS_DICT_KEY, "数据字典"));
        caches.add(new SysCache(CacheConstant.CAPTCHA_CODE_KEY, "验证码"));
        caches.add(new SysCache(CacheConstant.REPEAT_SUBMIT_KEY, "防重提交"));
        caches.add(new SysCache(CacheConstant.RATE_LIMIT_KEY, "限流处理"));
        caches.add(new SysCache(CacheConstant.PWD_ERR_CNT_KEY, "密码错误次数"));
    }

    @PreAuthorize("@ss.hasPermi('monitor:cache:list')")
    @GetMapping("/list")
    @ApiOperation(value = "",tags = "")
    public ApiResult getInfo() throws Exception
    {
        Properties info = (Properties) redisTemplate.execute((RedisCallback<Object>) connection -> connection.info());
        Properties commandStats = (Properties) redisTemplate.execute((RedisCallback<Object>) connection -> connection.info("commandstats"));
        Object dbSize = redisTemplate.execute((RedisCallback<Object>) connection -> connection.dbSize());

        Map<String, Object> result = new HashMap<>(3);
        result.put("info", info);
        result.put("dbSize", dbSize);

        List<Map<String, String>> pieList = new ArrayList<>();
        commandStats.stringPropertyNames().forEach(key -> {
            Map<String, String> data = new HashMap<>(2);
            String property = commandStats.getProperty(key);
            data.put("name", StringUtils.removeStart(key, "cmdstat_"));
            data.put("value", StringUtils.substringBetween(property, "calls=", ",usec"));
            pieList.add(data);
        });
        result.put("commandStats", pieList);
        return ApiResult.ok(result);
    }

    @ApiOperation(value = "")
    @PreAuthorize("@ss.hasPermi('monitor:cache:list')")
    @GetMapping("/getNames")
    public ApiResult cache()
    {
        return ApiResult.ok(caches);
    }

    @PreAuthorize("@ss.hasPermi('monitor:cache:list')")
    @GetMapping("/getKeys/{cacheName}")
    public ApiResult getCacheKeys(@PathVariable String cacheName)
    {
        Set<String> cacheKeys = redisTemplate.keys(cacheName + "*");
        return ApiResult.ok(cacheKeys);
    }

    @PreAuthorize("@ss.hasPermi('monitor:cache:list')")
    @GetMapping("/getValue")
    public ApiResult getCacheValue(@RequestParam("cacheName") String cacheName, @RequestParam("cacheKey") String cacheKey)
    {
        String cacheValue = redisTemplate.opsForValue().get(cacheKey);
        SysCache sysCache = new SysCache(cacheName, cacheKey, cacheValue);
        return ApiResult.ok(sysCache);
    }

    @PreAuthorize("@ss.hasPermi('monitor:cache:list')")
    @DeleteMapping("/clearCacheName")
    public ApiResult clearCacheName(@RequestParam("cacheName") String cacheName)
    {
        Collection<String> cacheKeys = redisTemplate.keys(cacheName + "*");
        redisTemplate.delete(cacheKeys);
        return ApiResult.ok();
    }

    @PreAuthorize("@ss.hasPermi('monitor:cache:list')")
    @PostMapping("/clearCacheKey")
    public ApiResult clearCacheKey( @RequestBody Map<String, Object> requestData)
    {
        String cacheKey= (String)requestData.get("cacheKey");
        log.info(cacheKey);
        redisTemplate.delete(cacheKey);
        return ApiResult.ok();
    }

    @PreAuthorize("@ss.hasPermi('monitor:cache:list')")
    @PostMapping("/clearCacheAll")
    public ApiResult clearCacheAll()
    {
        Collection<String> cacheKeys = redisTemplate.keys("*");
        redisTemplate.delete(cacheKeys);
        return ApiResult.ok();
    }
}
