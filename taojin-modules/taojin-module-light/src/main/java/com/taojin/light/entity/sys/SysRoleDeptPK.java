package com.taojin.light.entity.sys;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
@Data
public class SysRoleDeptPK implements Serializable {

    private Long roleId;

    private Long deptId;
}
