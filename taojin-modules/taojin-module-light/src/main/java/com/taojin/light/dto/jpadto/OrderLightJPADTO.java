package com.taojin.light.dto.jpadto;


import com.taojin.framework.config.JpaDto;
import lombok.NoArgsConstructor;
import lombok.Data;

import java.util.Date;

/**
 *实体类
 *
 * @author sujg
 * @data  2023-11-29
 */

@Data
@JpaDto
@NoArgsConstructor
public class OrderLightJPADTO {


    private java.lang.Long id;
    private java.lang.String oid;

    private Date startTime;

    private Date  endTime;

    private Long lightLoopId;

    private Long deviceId;

    private int loopNumber;
    private Long detailid;

}