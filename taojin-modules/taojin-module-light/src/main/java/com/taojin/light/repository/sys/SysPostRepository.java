package com.taojin.light.repository.sys;

import com.taojin.light.entity.sys.SysConfig;
import com.taojin.light.entity.sys.SysPost;
import com.taojin.light.service.sys.SysPostService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface SysPostRepository extends  JpaRepository<SysPost, Long>, JpaSpecificationExecutor<SysPost>{

    @Query(value="select p.post_id\n" +
            "        from sys_post p\n" +
            "\t        left join sys_user_post up on up.post_id = p.post_id\n" +
            "\t        left join sys_user u on u.user_id = up.user_id\n" +
            "\t    where u.user_id = :userId",nativeQuery = true)
    List<Long> selectPostListByUserId(Long userId);

    @Query(value="select p.post_id, p.post_name, p.post_code\n" +
            "\t\t from sys_post p\n" +
            "\t\t\t left join sys_user_post up on up.post_id = p.post_id\n" +
            "\t\t\t left join sys_user u on u.user_id = up.user_id\n" +
            "\t\t where u.user_name = :userName ",nativeQuery = true)
    List<SysPost>selectPostsByUserName(String userName);

    SysPost  findFirstByPostName(String postName);
    SysPost findFirstByPostCode(String postCode);
}
