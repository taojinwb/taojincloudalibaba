package com.taojin.light.entity;


import java.io.Serializable;
import java.util.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import springfox.documentation.annotations.ApiIgnore;

import javax.persistence.*;

/**
 * Entity基类
 *
 * @author sujg
 */
@MappedSuperclass
@ApiIgnore
@EntityListeners(AuditingEntityListener.class)
public class BaseEntity implements Serializable
{
    private static final long serialVersionUID = 1L;


    @CreatedBy
    @ApiModelProperty(value = "创建人", hidden = true)
    @Column(name = "create_by", updatable = false)
    public String createBy;

    @CreatedDate
    @ApiModelProperty(value = "创建时间", hidden = true)
    @Column(name = "create_time", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date createTime;

    @LastModifiedBy
    @ApiModelProperty(value = "更新人", hidden = true)
    @Column(name = "update_by",insertable = false)
    public String updateBy;

    @LastModifiedDate
    @ApiModelProperty(value = "更新时间", hidden = true)
    @Column(name = "update_time",insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date updateTime;

    @ApiModelProperty(value = "租户ID", hidden = true)
    @Column(name = "Tenant_id",updatable = false)
    public Long TenantId = 0L;

    /** 搜索值 */
    @Transient
    @JsonIgnore
    private String searchValue;

    @Transient
    @JsonIgnore
    private boolean flag;

    /** 请求参数,要初始化 */
    @Transient
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Map<String, Object> params = new HashMap<>();

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getTenantId() {
        return TenantId;
    }

    public void setTenantId(Long tenantId) {
        TenantId = tenantId;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    @Override
    public String toString() {
        return "BaseEntity{" +
                "createBy='" + createBy + '\'' +
                ", createTime=" + createTime +
                ", updateBy='" + updateBy + '\'' +
                ", updateTime=" + updateTime +
                ", TenantId=" + TenantId +
                ", searchValue='" + searchValue + '\'' +
                ", flag=" + flag +
                ", params=" + params +
                '}';
    }
}
