package com.taojin.light.framework.factory;

import com.taojin.light.entity.sys.SysLogininfor;
import com.taojin.light.entity.sys.SysOperLog;
import com.taojin.light.service.sys.SysLogininforService;
import com.taojin.light.service.sys.SysOperLogService;
import com.taojin.framework.constant.SysConstants;
import com.taojin.framework.utils.*;
import eu.bitwalker.useragentutils.UserAgent;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.TimerTask;
/**
 * 异步工厂（产生任务用）
 *
 * @author taojin
 */

@Slf4j
public class AsyncFactory
{

    /**
     * 记录登录信息
     *
     * @param username 用户名
     * @param status 状态
     * @param message 消息
     * @param args 列表
     * @return 任务task
     */
    public static synchronized  TimerTask recordLogininfor(final String username, final String status, final String message,
                                             final Object... args)
    {

        final UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));

        final String ip = IpUtils.getIpAddr();

                return new TimerTask()
                {
                    @Override
                    public void run()
                    {


                                    String address = AddressUtils.getRealAddressByIP(ip);
                                    StringBuilder s = new StringBuilder();
                                    s.append(getBlock(ip));
                                    s.append(address);
                                    s.append(getBlock(username));
                                    s.append(getBlock(status));
                                    s.append(getBlock(message));
                                    // 打印信息到日志

                                    // 获取客户端操作系统
                                    String os = "";
                                    // 获取客户端浏览器
                                    String browser = userAgent.getBrowser().getName();
                                    // 封装对象
                                    SysLogininfor logininfor = new SysLogininfor();
                                    logininfor.setUserName(username);
                                    logininfor.setIpaddr(ip);
                                    logininfor.setLoginLocation(address);
                                    logininfor.setBrowser(browser);
                                    logininfor.setOs(os);
                                    logininfor.setMsg(message);
                                    logininfor.setLoginTime(DateUtils.getNowDate());
                                    // 日志状态
                                    if (StringUtils.equalsAny(status, SysConstants.LOGIN_SUCCESS, SysConstants.LOGOUT, SysConstants.REGISTER)) {
                                        logininfor.setStatus(SysConstants.SUCCESS);
                                    } else if (SysConstants.LOGIN_FAIL.equals(status)) {
                                        logininfor.setStatus(SysConstants.FAIL);
                                    }
                                    // 插入数据
                                    SpringContextUtils.getBean(SysLogininforService.class).Insert(logininfor);

                        }
        };
    }

    /**
     * 操作日志记录
     *
     * @param operLog 操作日志信息
     * @return 任务task
     */
    public static TimerTask recordOper(final SysOperLog operLog)
    {
        return new TimerTask()
        {
            @Override
            public void run()
            {
                // 远程查询操作地点
                operLog.setOperLocation(AddressUtils.getRealAddressByIP(operLog.getOperIp()));
                SpringContextUtils.getBean(SysOperLogService.class).Insert(operLog);

            }
        };
    }
    public static String getBlock(Object msg)
    {
        if (msg == null)
        {
            msg = "";
        }
        return "[" + msg.toString() + "]";
    }
}
