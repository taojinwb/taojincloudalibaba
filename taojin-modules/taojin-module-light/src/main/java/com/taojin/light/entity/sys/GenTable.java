package com.taojin.light.entity.sys;

import javax.persistence.*;
import java.sql.Timestamp;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import com.taojin.light.entity.BaseEntity;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-26
*/
@Entity
@Data
@Table(name = "gen_table", schema = "taojin", catalog = "")
public class GenTable extends BaseEntity {

/** 编号*/
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "table_id")
		private Long tableId;

		/** 表名称*/
		@Column(name = "table_name")
		@Excel(name ="表名称")
		private java.lang.String tableName;

		/** 表描述*/
		@Column(name = "table_comment")
		@Excel(name ="表描述")
		private java.lang.String tableComment;

		/** 关联子表的表名*/
		@Column(name = "sub_table_name")
		@Excel(name ="关联子表的表名")
		private java.lang.String subTableName;

		/** 子表关联的外键名*/
		@Column(name = "sub_table_fk_name")
		@Excel(name ="子表关联的外键名")
		private java.lang.String subTableFkName;

		/** 实体类名称*/
		@Column(name = "class_name")
		@Excel(name ="实体类名称")
		private java.lang.String className;

		/** 使用的模板（crud单表操作 tree树表操作）*/
		@Column(name = "tpl_category")
		@Excel(name ="使用的模板（crud单表操作 tree树表操作）")
		private java.lang.String tplCategory;

		/** 生成包路径*/
		@Column(name = "package_name")
		@Excel(name ="生成包路径")
		private java.lang.String packageName;

		/** 生成模块名*/
		@Column(name = "module_name")
		@Excel(name ="生成模块名")
		private java.lang.String moduleName;

		/** 生成业务名*/
		@Column(name = "business_name")
		@Excel(name ="生成业务名")
		private java.lang.String businessName;

		/** 生成功能名*/
		@Column(name = "function_name")
		@Excel(name ="生成功能名")
		private java.lang.String functionName;

		/** 生成功能作者*/
		@Column(name = "function_author")
		@Excel(name ="生成功能作者")
		private java.lang.String functionAuthor;

		/** 生成代码方式（0zip压缩包 1自定义路径）*/
		@Column(name = "gen_type")
		@Excel(name ="生成代码方式（0zip压缩包 1自定义路径）")
		private java.lang.String genType;

		/** 生成路径（不填默认项目路径）*/
		@Column(name = "gen_path")
		@Excel(name ="生成路径（不填默认项目路径）")
		private java.lang.String genPath;

		/** 其它生成选项*/
		@Column(name = "options")
		@Excel(name ="其它生成选项")
		private java.lang.String options;

//		/** 创建者*/
//		@Column(name = "create_by")
//		@Excel(name ="创建者")
//		private java.lang.String createBy;
//
//		/** 创建时间*/
//		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
//		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//		@Column(name = "create_time")
//		@Excel(name ="创建时间")
//		private java.util.Date createTime;
//
//		/** 更新者*/
//		@Column(name = "update_by")
//		@Excel(name ="更新者")
//		private java.lang.String updateBy;
//
//		/** 更新时间*/
//		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
//		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//		@Column(name = "update_time")
//		@Excel(name ="更新时间")
//		private java.util.Date updateTime;

		/** 备注*/
		@Column(name = "remark")
		@Excel(name ="备注")
		private java.lang.String remark;

}