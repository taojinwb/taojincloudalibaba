package com.taojin.light.repository.sys;

import com.taojin.light.entity.sys.SysRole;
import com.taojin.light.dto.jpadto.SysRoleJPADTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SysRoleRepository extends  JpaRepository<SysRole, Long>, JpaSpecificationExecutor<SysRole> {
@Query(value="  select distinct r.* \n" +
        "        from sys_role r\n" +
        "        left join sys_user_role ur on ur.role_id = r.role_id\n" +
        "        left join sys_user u on u.user_id = ur.user_id\n" +
        "        left join sys_dept d on u.dept_id = d.dept_id" +
        " where r.role_id = :roleId",nativeQuery = true)
SysRole selectRoleById(@Param("roleId") Long roleId);


@Query(value="  select  distinct r.* " +
        "        from sys_role r " +
        "        left join sys_user_role ur on ur.role_id = r.role_id" +
        "        left join sys_user u on u.user_id = ur.user_id" +
        "        left join sys_dept d on u.dept_id = d.dept_id" +
        " where r.del_flag = '0' " +
        " AND (:#{#sysRole.roleId} IS NULL OR  r.role_id = :#{#sysRole.roleId}) " +
        " AND (:#{#sysRole.roleName} IS NULL OR  r.role_name like concat('%',:#{#sysRole.roleName}, '%')) " +
        " AND (:#{#sysRole.status} IS NULL OR  r.status = :#{#sysRole.status}) " +
        " AND (:#{#sysRole.roleKey} IS NULL OR  r.role_key like concat('%', :#{#sysRole.roleKey}, '%')) " +
        " AND (:#{#sysRole.params['beginTime']} IS NULL OR   date_format(r.create_time,'%Y-%m-%d') >= date_format(:#{#sysRole.params['beginTime']},'%Y-%m-%d')) " +
        " AND (:#{#sysRole.params['endTime']} IS NULL OR     date_format(r.create_time,'%Y-%m-%d') >= date_format(:#{#sysRole.params['endTime']},'%Y-%m-%d') )" +
        "  order by r.role_sort ",nativeQuery = true)
    Page<SysRoleJPADTO> selectRoleList(SysRoleJPADTO sysRole, Pageable pageable);
    @Query(value="  select distinct r.role_name, r.role_key, r.role_id, r.role_sort, r.data_scope, r.menu_check_strictly, r.dept_check_strictly, r.`status`, r.del_flag, r.create_by,r.create_time, r.update_by, r.update_time, r.remark " +
            "        from sys_role r\n" +
            "        left join sys_user_role ur on ur.role_id = r.role_id" +
            "        left join sys_user u on u.user_id = ur.user_id" +
            "       left join sys_dept d on u.dept_id = d.dept_id" +
            "  WHERE r.del_flag = '0' and r.status='0' and ur.user_id = :userId",nativeQuery = true)
    List<SysRoleJPADTO>  selectRolePermissionByUserId(@Param("userId") Long userId);

    @Query(value="select r.role_id        from sys_role r" +
            "       left join sys_user_role ur on ur.role_id = r.role_id      " +
            " left join sys_user u on u.user_id = ur.user_id   where u.user_id = :userId",nativeQuery = true)
    List<Long>selectRoleListByUserId(@Param("userId") Long userId);

    @Query(value="  select  distinct r.* \n" +
            "        from sys_role r\n" +
            "        left join sys_user_role ur on ur.role_id = r.role_id\n" +
            "        left join sys_user u on u.user_id = ur.user_id\n" +
            "        left join sys_dept d on u.dept_id = d.dept_id" +
            " where r.role_name=:roleName and r.del_flag = '0' limit 1",nativeQuery = true)
    SysRole checkRoleNameUnique(@Param("roleName") String roleName);

    @Query(value="  select distinct r.*\n" +
            "        from sys_role r\n" +
            "        left join sys_user_role ur on ur.role_id = r.role_id\n" +
            "        left join sys_user u on u.user_id = ur.user_id\n" +
            "        left join sys_dept d on u.dept_id = d.dept_id" +
            " where r.role_key=:roleKey and r.del_flag = '0' limit 1",nativeQuery = true)
    SysRole checkRoleKeyUnique(@Param("roleKey") String roleKey);

    @Query(value="update sys_role set del_flag = '2' where role_id = :roleId",nativeQuery = true)
    int deleteRoleById(@Param("roleId") Long roleId);

    int deleteSysRolesByRoleIdIn(Long[] roleIds);

    @Query(value=" select distinct r.* \n" +
            "        from sys_role r\n" +
            "\t        left join sys_user_role ur on ur.role_id = r.role_id\n" +
            "\t        left join sys_user u on u.user_id = ur.user_id\n" +
            "\t        left join sys_dept d on u.dept_id = d.dept_id " +
            " where r.del_flag = '0' and u.user_name = :userName ",nativeQuery = true)
   List<SysRole>selectRolesByUserName(String userName);

}
