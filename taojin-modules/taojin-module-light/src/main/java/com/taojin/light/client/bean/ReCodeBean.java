
package com.taojin.light.client.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class ReCodeBean implements Serializable {
    private static final long serialVersionUID = 1L;
    private String reCodeStr;
    private int type;
    private int readType;
    private Map<String, String> codeMaps;
    private String crcCode;
    public VirtualDevice virDevice;
    public final Map<String, Integer> loopMap = new HashMap<String, Integer>() {
        {
            this.put("03", 1);
            this.put("04", 2);
            this.put("05", 3);
            this.put("06", 4);
            this.put("07", 5);
            this.put("08", 6);
            this.put("09", 7);
            this.put("0A", 8);
            this.put("0B", 9);
            this.put("0C", 10);
            this.put("0D", 11);
            this.put("0E", 12);
        }
    };

    public ReCodeBean(String recodeStr, VirtualDevice virDevice) {
        this.reCodeStr = recodeStr;
        this.virDevice = virDevice;
        this.codeMaps = new HashMap();
        this.virDevice.sendComNum = 0;
        this.reCodeAnalysis();
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getReadType() {
        return this.readType;
    }

    public void setReadType(int readType) {
        this.readType = readType;
    }

    public Map<String, String> getCodeMaps() {
        return this.codeMaps;
    }

    public void setCodeMaps(Map<String, String> codeMaps) {
        this.codeMaps = codeMaps;
    }

    public String getCrcCode() {
        return this.crcCode;
    }

    public void setCrcCode(String crcCode) {
        this.crcCode = crcCode;
    }

    public void reCodeAnalysis() {
        System.out.println(this.reCodeStr+"=======================");
        String[] codes = this.reCodeStr.split(" ");
        String codeType = codes[1];
        int i;
        if ("03".equals(codeType)) {
            this.type = 3;
            if ("18".equals(codes[2])) {
                this.readType = 0;
                i = 4;

                for(i = 0; i < this.virDevice.getLoopNum(); ++i) {
                    this.getCodeMaps().put(i + 1 + "", codes[i]);
                    i += 2;
                }
            } else if ("02".equals(codes[2])) {
                this.readType = 1;
                this.getCodeMaps().put(this.virDevice.getComdOpt().getRouteid() + "", codes[4]);
            }
        } else if ("06".equals(codeType)) {
            this.type = 6;
            if ("01".equals(codes[3])) {
                for(i = 0; i < this.virDevice.getLoopNum(); ++i) {
                    this.getCodeMaps().put(i + 1 + "", codes[5]);
                }
            } else if ("02".equals(codes[3])) {
                for(i = 0; i < this.virDevice.getLoopNum(); ++i) {
                    this.getCodeMaps().put(i + 1 + "", codes[5]);
                }
            } else {
                this.getCodeMaps().put(this.loopMap.get(codes[3]) + "", codes[5]);
            }
        }
        System.out.println(this.getCodeMaps()+"=--------------");
    }

    public String toString() {
        return "ReCodeBean [reCodeStr=" + this.reCodeStr + ", type=" + this.type + ", readType=" + this.readType + ", codeMaps=" + this.codeMaps + ", crcCode=" + this.crcCode + ", virDevice lockstatus=" + this.virDevice.getLockStatus() + "]";
    }
}
