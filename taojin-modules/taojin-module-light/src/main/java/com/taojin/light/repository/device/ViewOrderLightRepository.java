package com.taojin.light.repository.device;

import com.taojin.light.entity.device.BIthLightLoop;
import com.taojin.light.dto.jpadto.OrderLightJPADTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface ViewOrderLightRepository extends JpaRepository<BIthLightLoop, Long>, JpaSpecificationExecutor<BIthLightLoop> {

    @Query(value="select   \n" +
            " COALESCE(d.id, 0 ) AS id,\n" +
            " COALESCE ( a.oid, '0' ) AS oid,\n" +
            " COALESCE ( a.start_time, '1999-11-29 14:00:00' ) AS start_time,\n" +
            " COALESCE ( a.end_time, '1999-11-29 14:00:00' ) AS end_time,\n" +
            " COALESCE ( f.id, 0 ) AS light_loop_id,\n" +
            " COALESCE ( f.device_id, 0 ) AS device_id,\n" +
            " COALESCE ( f.loop_number, 0 ) AS loop_number,\n" +
            " d.detailid AS detailid \t\n" +
            " from orders_extd a\n" +
            " left join orders b on a.oid = b.oid\n" +
            " left join b_venue_field_source_lock c on c.id = b.items_id\n" +
            " left join b_venue_field d on d.id = c.visit_field_id \n" +
            " left join b_ith_light_loop f on f.id = d.light_loop_id \n" +
            " \n" +
            " WHERE  \n" +
            " b.status IN ( 40, 50, 70 )\n" +
            " AND  b.ref_status <> 20  \t\t\n" +
            " AND COALESCE ( a.start_time, '1999-11-29 14:00:00' ) >NOW()\n" +
            " AND COALESCE ( a.end_time, '1999-11-29 14:00:00' ) > NOW()\n" +
            " AND (\n" +
            " \n" +
            " COALESCE ( a.start_time, '1999-11-29 14:00:00' ) < NOW() + INTERVAL 60 MINUTE\n" +
            " OR \n" +
            " COALESCE ( a.end_time, '1999-11-29 14:00:00' ) < NOW() + INTERVAL 60 MINUTE\n" +
            " )  and detailid in (:detailid) \n" +
            " order by f.device_id,a.start_time " ,nativeQuery = true)
    List<OrderLightJPADTO> selectallbydetailids(@Param("detailid") String detailid);

    @Query(value="select   \n" +
            " count(d.id) AS id\n" +
            " from orders_extd a\n" +
            " left join orders b on a.oid = b.oid\n" +
            " left join b_venue_field_source_lock c on c.id = b.items_id\n" +
            " left join b_venue_field d on d.id = c.visit_field_id \n" +
            " where b.status IN ( 40, 50, 70 ) and b.ref_status <> 20" +
            " and a.start_time>=:starttime and a.start_time<=:endtime  and d.id = :fieldid   " +
            "",
            nativeQuery = true)
    int getCount(@Param("starttime")String starttime,@Param("endtime")String endtime,@Param("fieldid")Long fieldid);

}
