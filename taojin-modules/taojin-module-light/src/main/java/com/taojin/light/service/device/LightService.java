
package com.taojin.light.service.device;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.taojin.framework.redis.RedisUtil;
import com.taojin.light.client.bean.ReCodeBean;
import com.taojin.light.client.bean.ResultBean;
import com.taojin.light.client.bean.VirtualDevice;
import com.taojin.light.client.common.BeiFenCtrlCmds;
import com.taojin.light.client.common.ClientMap;
import com.taojin.light.client.common.DateUtils;
import com.taojin.light.client.common.ModBusUtils;
import com.taojin.light.client.common.UserUtil;
import com.taojin.light.dto.AtSetDTO;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import javax.annotation.Resource;

import com.taojin.light.entity.device.BIthDevice;
import com.taojin.light.repository.device.BIthDeviceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

@Service
public class LightService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private RedisUtil redisClient;
    @Resource
    private UserUtil userUtil;
    @Resource
    private BIthDeviceRepository bIthDeviceRepository;

    private String root_token;

    public LightService() {
    }

    public ResultBean executeBeifenAction(AtSetDTO set) {
        ResultBean resultBean = new ResultBean();
        if (ClientMap.clients.containsKey(set.getDeviceId())) {
            VirtualDevice virDevice = (VirtualDevice)ClientMap.clients.get(set.getDeviceId());
            long isTimeOut = DateUtils.getSystemTimestamp() - virDevice.getLastExeComdTime();
            if (isTimeOut > 5000L) {
                virDevice.setLockStatus(0);
            }

            if (set.getRouteid() < 0 || set.getRouteid() > virDevice.getLoopNum()) {
                resultBean.setCode(400);
                resultBean.setMessage("设备路不在可以用范围内,当前设备回路数量为" + virDevice.getLoopNum());
                return resultBean;
            }

            this.logger.info("次数 = {}", virDevice.sendComNum);
            if (virDevice.sendComNum >= 3) {
                this.logger.info("设备或已下线 = {}", virDevice.sendComNum);
                ClientMap.clients.remove(set.getDeviceId());
                this.redisClient.hdel("dk", new Object[]{set.getDeviceId()});
            }

            if (virDevice.getLockStatus() == 0) {
                try {
                    virDevice.setLockStatus(1);
                    ++virDevice.sendComNum;
                    long timesteamp = DateUtils.getSystemTimestamp();
                    virDevice.setLastExeComdTime(timesteamp);
                    virDevice.setComdOpt(set);
                    virDevice.setRecode((ReCodeBean)null);
                    OutputStream mOutputStream = virDevice.getSocket().getOutputStream();
                    String comd = null;
                    if (set.getExeaction().indexOf("?") == -1) {
                        comd = BeiFenCtrlCmds.getComdForloopNoId(set.getRouteid(), Integer.parseInt(set.getExeaction()));
                        this.logger.info("操作指令 = {}", comd);
                    } else {
                        if (set.getRouteid() < 10) {
                            comd = "0" + set.getRouteid();
                        } else {
                            comd = set.getRouteid() + "";
                        }

                        comd = BeiFenCtrlCmds.getStatusComd(comd);
                        this.logger.info("查询指令 = {}", comd);
                    }

                    this.logger.info("发送指令 = {}", comd);
                    mOutputStream.write(ModBusUtils.hexStringToByteArray(comd));
                    mOutputStream.flush();
                    boolean wait = true;
                    long waittime = 0L;

                    while(wait) {
                        waittime = DateUtils.getSystemTimestamp() - timesteamp;
                        if (virDevice.getRecode() != null && timesteamp == virDevice.getLastExeComdTime()) {
                            wait = false;
                        } else if (waittime > 3000L) {
                            resultBean.setCode(500);
                            resultBean.setMessage("指令执行超时");
                            wait = false;
                        }

                        try {
                            Thread.sleep(200L);
                        } catch (InterruptedException var14) {
                            var14.printStackTrace();
                        }
                    }

                    this.logger.info("deviceId = {} , recodeis = {},wait time = {}", new Object[]{virDevice.getLightId(), virDevice.getRecode(), waittime});
                    if (resultBean.getCode() == 200) {
                        resultBean.setMessage(virDevice.getRecode().toString());
                        if (0 == set.getRouteid()) {
                            this.bIthDeviceRepository.updateStatusForId(Long.valueOf(set.getDeviceId().replaceAll("dk", "")), set.getExeaction());
                        }
                    }

                    virDevice.setLockStatus(0);
                } catch (IOException var15) {
                    resultBean.setCode(500);
                    resultBean.setMessage(var15.getMessage());
                    virDevice.setLockStatus(0);
                }
            } else {
                resultBean.setCode(500);
                resultBean.setMessage("设备正在执行上一条指令，请稍后再试！");
            }
        } else {
            resultBean.setCode(500);
            resultBean.setMessage("该设备可能不在线，请检查...");
            this.logger.info("该设备({}) 可能不在线，请检查...", set.getDeviceId());
        }

        return resultBean;
    }

    private void deviceOwnerCheck(String deviceId, String token) {
        Long userId = this.userUtil.getAuthIdForToken(token);
        if (StringUtils.isEmpty(deviceId)) {
            throw new NullPointerException("设备ID不能为空");
        } else {
          //  logger.info("==2222=="+userId);
            Long bIthDevice = this.bIthDeviceRepository.deviceOwner(Long.valueOf(deviceId.replaceAll("dk", "")));
            logger.info(userId.getClass()+"--------"+bIthDevice.getClass());
            logger.info(bIthDevice+"===="+userId+"==="+(cn.hutool.core.util.ObjectUtil.equal(bIthDevice,userId)));
            if (!cn.hutool.core.util.ObjectUtil.equal(bIthDevice,userId)) {
                throw new RuntimeException("非法操作");
            }
        }
    }
    public ResultBean findDeviceSocket(AtSetDTO set){
        InetAddress localHost = null;

        try {
            localHost = Inet4Address.getLocalHost();
        } catch (UnknownHostException var14) {
        }

        String serviceIp = localHost.getHostAddress();
        if (null != set.getSocketIp() && serviceIp.equals(set.getSocketIp())) {
            this.logger.info("socket 在当前服务器");
            return this.executeBeifenAction(set);
        } else {
            String deviceSocketIp = (String)this.redisClient.hget("dk", set.getDeviceId());
            this.logger.info("deviceId = {},ip = {}", set.getDeviceId(), deviceSocketIp);
            ResultBean resultBean;
            if (deviceSocketIp != null) {
                if (serviceIp.equals(deviceSocketIp)) {
                    this.logger.info("socket 在当前服务器（自检）");
                    return this.executeBeifenAction(set);
                }

                this.logger.info("socket 在 {} 服务器", deviceSocketIp);
                RestTemplate restTemplate = new RestTemplate();
                String url = "http://" + deviceSocketIp + ":9001/light/ctl/comd";
                HttpHeaders headers = new HttpHeaders();
                MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
                headers.setContentType(type);
                headers.add("Accept", MediaType.APPLICATION_JSON.toString());
                JSONObject param = new JSONObject();
                param.put("deviceId", set.getDeviceId());
                param.put("routeid", set.getRouteid() + "");
                param.put("exeaction", set.getExeaction());
                param.put("socketIp", deviceSocketIp);
                HttpEntity<JSONObject> request = new HttpEntity(param, headers);
                String result = (String)restTemplate.postForObject(url, request, String.class, new Object[0]);
                this.logger.info(result);
                resultBean = (ResultBean)JSON.parseObject(result, ResultBean.class);
            } else {
                resultBean = new ResultBean();
                resultBean.setCode(500);
                resultBean.setMessage("设备不在线，请检查设备网络是否正常");
            }

            return resultBean;
        }
    }


    public ResultBean findDeviceSocket(AtSetDTO set, String token) {
            this.deviceOwnerCheck(set.getDeviceId(), token);
            return findDeviceSocket(set);

    }
}
