package com.taojin.light.framework.config;

import com.taojin.light.service.device.DataService;
import com.taojin.light.client.thrand.UsrTcpSocketThrad;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Order(10)
@Transactional(
        rollbackFor = {Exception.class, RuntimeException.class, Error.class}
)
public class ServiceSocketConfig implements CommandLineRunner {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    DataService dataService;
    public static ServerSocket serviceSocket = null;
    private static final ThreadPoolExecutor threadPool;

    public ServiceSocketConfig() {
    }

    public void createSocket() {
        try {
            serviceSocket = new ServerSocket(8888);
            InetAddress localHost = null;

            try {
                localHost = Inet4Address.getLocalHost();
            } catch (UnknownHostException var4) {
            }

            String serviceIp = localHost.getHostAddress();
            this.dataService.setServiceIp(serviceIp);
            this.logger.info("Socket Service Start... ");
            this.logger.info("Service address: " + localHost.getHostAddress());

            while(true) {
                Socket socket = serviceSocket.accept();
                this.logger.info("接收到 client Socket  = {}", socket.getInetAddress());
                threadPool.execute(new UsrTcpSocketThrad(socket, this.dataService));
                this.logger.info("Thread ActiveCount  = {},poolSize = {}", threadPool.getActiveCount(), threadPool.getPoolSize());
            }
        } catch (IOException var5) {
            var5.printStackTrace();
        }
    }

    public void run(String... args) throws Exception {
        this.createSocket();
    }

    static {
        threadPool = new ThreadPoolExecutor(20, 50, 10L, TimeUnit.SECONDS, new LinkedBlockingQueue());
    }
}
