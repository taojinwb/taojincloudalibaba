package com.taojin.light.controller.sys;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;

import com.taojin.framework.vo.ApiResult;
import com.taojin.light.framework.common.BaseController;
import com.taojin.light.dto.jpadto.SysRoleJPADTO;
import com.taojin.light.entity.sys.SysDept;
import com.taojin.light.entity.sys.SysUser;
import com.taojin.light.framework.annotation.Log;
import com.taojin.light.framework.security.SecurityUtils;
import com.taojin.light.service.sys.SysDeptService;
import com.taojin.light.service.sys.SysPostService;
import com.taojin.light.service.sys.SysRoleService;
import com.taojin.light.service.sys.SysUserService;
import com.taojin.light.dto.jpadto.SysUserJPADTO;
import com.taojin.framework.enums.BusinessType;
import com.taojin.framework.utils.MF;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * 用户信息
 *
 * @author sujg
 */
@RestController
@RequestMapping("/system/user")
@Slf4j
@Api(value="用户管理",tags = {"用户管理","角色相关接口"})
public class SysUserController extends BaseController<SysUser>
{
    @Autowired
    private SysUserService userService;

    @Autowired
    private SysRoleService roleService;

    @Autowired
    private SysDeptService deptService;

    @Autowired
    private SysPostService postService;

    /**
     * 获取用户列表
     */
    @ApiOperation(value = "获取用户列表", tags = "用户管理")
    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @GetMapping("/list")
    public ApiResult list(SysUser sysUser)
    {
         sysUser.setDelFlag("1,0");
         Page<SysUser> listt=userService.selectUserList(sysUser,this.getPageable(sysUser.getParams()));
        return ApiResult.OK(listt);
    }


    /**
     * 根据用户编号获取详细信息
     */
    @ApiOperation(value = "根据用户编号获取详细信息", tags = "用户管理")
    @PreAuthorize("@ss.hasPermi('system:user:query')")
    @GetMapping("/getinfo")
    public ApiResult getInfo(@RequestParam(value = "userId", required = false) Long userId)
    {
        Map ajax = new HashMap<>();
        List<SysRoleJPADTO> roles = roleService.selectRoleAll();

        ajax.put("roles", SysUserJPADTO.isAdmin(userId) ? roles : roles.stream().filter(r -> !r.isAdmin()).collect(Collectors.toList()));

        ajax.put("posts", postService.selectPostAll());
        if (MF.isNotNull(userId))
        {
            SysUserJPADTO sysUservo = userService.selectUserById(userId);
            ajax.put("uservo", sysUservo);
            ajax.put("postIds", postService.selectPostListByUserId(userId));
            ajax.put("roleIds",roleService.selectRoleListByUserId(userId) );

        }
        return ApiResult.OK(ajax);
    }

    /**
     * 新增用户
     */
    @ApiOperation(value = "新增用户", tags = "用户管理")
    @PreAuthorize("@ss.hasPermi('system:user:add')")
    @Log(title = "用户管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public ApiResult add(@Validated @RequestBody SysUser user)
    {
        if (!userService.checkUserNameUnique(user))
        {
            return ApiResult.error("新增用户'" + user.getUserName() + "'失败，登录账号已存在");
        }
        else if (MF.isNotEmpty(user.getPhonenumber()) && !userService.checkPhoneUnique(user))
        {
            return ApiResult.error("新增用户'" + user.getUserName() + "'失败，手机号码已存在");
        }
        else if (MF.isNotEmpty(user.getEmail()) && !userService.checkEmailUnique(user))
        {
            return ApiResult.error("新增用户'" + user.getUserName() + "'失败，邮箱账号已存在");
        }
        //user.setCreateBy(this.getLoginUser().getUsername());
        user.setPassword(SecurityUtils.encryptPassword(user.getPassword()));
        user.setDelFlag("0");
        userService.insertUser(user);
        return ApiResult.OK("添加成功");
    }

    /**
     * 修改用户
     */
    @ApiOperation(value = "修改用户", tags = "用户管理")
    @PreAuthorize("@ss.hasPermi('system:user:edit')")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public ApiResult edit(@Validated @RequestBody SysUser user)
    {
        userService.checkUserAllowed(user);
       // userService.checkUserDataScope(user.getUserId());
        if (!userService.checkUserNameUnique(user))
        {
            return ApiResult.error("修改用户'" + user.getUserName() + "'失败，登录账号已存在");
        }
        else if (MF.isNotEmpty(user.getPhonenumber()) && !userService.checkPhoneUnique(user))
        {
            return ApiResult.error("修改用户'" + user.getUserName() + "'失败，手机号码已存在");
        }
        else if (MF.isNotEmpty(user.getEmail()) && !userService.checkEmailUnique(user))
        {
            return ApiResult.error("修改用户'" + user.getUserName() + "'失败，邮箱账号已存在");
        }
        //user.setUpdateBy(this.getLoginUser().getUsername());
        userService.updateUser(user);

        return ApiResult.OK("更新成功！");
    }

    /**
     * 删除用户
     */
    @ApiOperation(value = "删除用户", tags = "用户管理")
    @PreAuthorize("@ss.hasPermi('system:user:remove')")
    @Log(title = "用户管理", businessType = BusinessType.DELETE)
    @PostMapping("/del/{userIds}")
    public ApiResult remove(@PathVariable Long[] userIds)
    {
        if (ArrayUtils.contains(userIds, this.getLoginUser().getUserId()))
        {
            return ApiResult.error("当前用户不能删除");
        }
        userService.deleteUserByIds(userIds);
        return ApiResult.OK("删除成功");
    }

    /**
     * 重置密码
     */
    @ApiOperation(value = "重置密码", tags = "用户管理")
    @PreAuthorize("@ss.hasPermi('system:user:resetPwd')")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PutMapping("/resetPwd")
    public ApiResult resetPwd(@RequestBody SysUser user)
    {
        userService.checkUserAllowed(user);
        userService.checkUserDataScope(user.getUserId());
        user.setPassword(SecurityUtils.encryptPassword(user.getPassword()));
        user.setUpdateBy(this.getLoginUser().getUsername());
        SysUserJPADTO sj = new SysUserJPADTO();
        BeanUtils.copyProperties(user,sj);
       userService.resetPwd(sj);
        return ApiResult.OK("修改密码成功");
    }

    /**
     * 状态修改
     */
    @ApiOperation(value = "状态修改", tags = "用户管理")
    @PreAuthorize("@ss.hasPermi('system:user:edit')")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public ApiResult changeStatus(@RequestBody SysUser user)
    {
        userService.checkUserAllowed(user);
        userService.checkUserDataScope(user.getUserId());
       // user.setUpdateBy(this.getLoginUser().getUsername());
        userService.updateUserStatus(user);
        return ApiResult.OK("修改状态成功");
    }

    /**
     * 根据用户编号获取授权角色
     */
    @ApiOperation(value = "根据用户编号获取授权角色", tags = "用户管理")
    @PreAuthorize("@ss.hasPermi('system:user:query')")
    @GetMapping("/authRole/{userId}")
    public ApiResult authRole(@PathVariable("userId") Long userId)
    {
        Map ajax = new HashMap();
        SysUserJPADTO user = userService.selectUserById(userId);
        List<SysRoleJPADTO> roles = roleService.selectRolesByUserId(userId);
        ajax.put("user", user);
        ajax.put("roles", SysUserJPADTO.isAdmin(userId) ? roles : roles.stream().filter(r -> !r.isAdmin()).collect(Collectors.toList()));
        return ApiResult.OK(ajax);
    }

    /**
     * 用户授权角色
     */
    @ApiOperation(value = "用户授权角色", tags = "用户管理")
    @PreAuthorize("@ss.hasPermi('system:user:edit')")
    @Log(title = "用户管理", businessType = BusinessType.GRANT)
    @PutMapping("/authRole")
    public ApiResult insertAuthRole(Long userId, Long[] roleIds)
    {
        userService.checkUserDataScope(userId);
        userService.insertUserAuth(userId, roleIds);
        return ApiResult.OK("授权成功");
    }

    /**
     * 获取部门树列表
     */
    @ApiOperation(value = "获取部门树列表", tags = "用户管理")
    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @GetMapping("/deptTree")
    public ApiResult deptTree(SysDept dept)
    {
        return ApiResult.OK( deptService.selectDeptTreeList(dept));
    }

    /**
     * 导出excel
     *
     * @param
     * @param
     */

    @PostMapping(value = "/exportXls")
    @ApiOperation(value="导出用户信息",notes="导出用户信息",tags = "用户管理")
    public ModelAndView exportXls(HttpServletRequest request, SysUser sysUser) {
        return super.exportXls( userService.selectUserList(sysUser), sysUser, SysUser.class, "用户信息");
    }

}
