package com.taojin.light.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@ApiModel(
        value = "订单灯控DTO",
        description = "订单灯控DTO"
)
@AllArgsConstructor
@Data
public class LightOrderDTO {
    public long oid;
    public Date startTime;
    public Date endTime;
    public long deviceid;
}
