package com.taojin.light.repository.sys;

import com.taojin.light.entity.sys.SysConfig;
import com.taojin.light.entity.sys.SysDictType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface SysDictTypeRepository extends  JpaRepository<SysDictType, Long>, JpaSpecificationExecutor<SysDictType> {
    SysDictType findFirstByDictType(String dictType);
    List<SysDictType> findAllByDictType(String dictType);
}
