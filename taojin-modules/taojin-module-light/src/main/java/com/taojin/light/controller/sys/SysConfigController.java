package com.taojin.light.controller.sys;

import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import com.taojin.framework.vo.ApiResult;
import com.taojin.light.framework.common.BaseController;
import com.taojin.light.entity.sys.SysConfig;
import com.taojin.light.framework.annotation.Log;
import com.taojin.light.service.sys.SysConfigService;
import com.taojin.api.admin.ServerAdminAPI;
import com.taojin.framework.enums.BusinessType;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 参数配置 信息操作处理
 *
 * @author sujg
 */
@RestController
@RequestMapping("/system/config")
@Tag(name = "系统管理")
public class SysConfigController extends BaseController
{
    @Autowired
    private SysConfigService configService;
    @Resource
    private ServerAdminAPI accountClient;

    /**
     * 获取参数配置列表 ..
     */
    @PreAuthorize("@ss.hasPermi('system:config:list')")
    @GetMapping("/list")
    @Operation(summary = "配置列表", description = "查询配置列表信息",tags = "系统管理")
    public ApiResult list(SysConfig config)
    {

        List<SysConfig> list = configService.selectConfigList(config);
        return ApiResult.ok(list);
    }

    @Log(title = "参数管理", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('system:config:export')")
    @PostMapping("/export")
    public ModelAndView export(HttpServletResponse response, SysConfig config)
    {
        return super.exportXls(configService.selectConfigList(config), config, SysConfig.class, "参数管理");
    }

    /**
     * 根据参数编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:config:query')")
    @GetMapping(value = "/getInfo/{configId}")
    public ApiResult getInfo(@PathVariable Long configId)
    {
        return ApiResult.ok(configService.selectConfigById(configId));
    }

    /**
     * 根据参数键名查询参数值
     */
    @GetMapping(value = "/configKey/{configKey}")
    public ApiResult getConfigKey(@PathVariable String configKey)
    {
        return ApiResult.ok(configService.selectConfigByKey(configKey));
    }

    /**
     * 新增参数配置
     */
    @PreAuthorize("@ss.hasPermi('system:config:add')")
    @Log(title = "参数管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public ApiResult add(@Validated @RequestBody SysConfig config)
    {
        if (!configService.checkConfigKeyUnique(config))
        {
            return ApiResult.error("新增参数'" + config.getPzmc() + "'失败，参数键名已存在");
        }
        //config.setCreateBy(this.getLoginUser().getUsername());
        configService.insertConfig(config);
        return ApiResult.ok("添加成功");
    }

    /**
     * 修改参数配置
     */
    @PreAuthorize("@ss.hasPermi('system:config:edit')")
    @Log(title = "参数管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public ApiResult edit(@Validated @RequestBody SysConfig config)
    {

        if (!configService.checkConfigKeyUnique(config))
        {
            return ApiResult.error("修改参数'" + config.getPzkey() + "'失败，参数键名已存在");
        }
       // config.setUpdateBy(this.getLoginUser().getUsername());
        configService.updateConfig(config);
        return ApiResult.ok("修改成功");
    }

    /**
     * 删除参数配置
     */
    @PreAuthorize("@ss.hasPermi('system:config:remove')")
    @Log(title = "参数管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{configIds}")
    public ApiResult remove(@PathVariable Long[] configIds)
    {
        configService.deleteConfigByIds(configIds);
        return ApiResult.ok("删除成功");
    }

    /**
     * 刷新参数缓存
     */
    @PreAuthorize("@ss.hasPermi('system:config:remove')")
    @Log(title = "参数管理", businessType = BusinessType.CLEAN)
    @DeleteMapping("/refreshCache")
    public ApiResult refreshCache()
    {
        configService.resetConfigCache();
        return ApiResult.ok();
    }
}
