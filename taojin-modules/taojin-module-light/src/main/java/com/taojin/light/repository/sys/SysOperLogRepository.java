package com.taojin.light.repository.sys;

import com.taojin.light.entity.sys.SysConfig;
import com.taojin.light.entity.sys.SysOperLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface SysOperLogRepository extends  JpaRepository<SysOperLog, Long>, JpaSpecificationExecutor<SysOperLog>
{
    @Query(value=" truncate table sys_oper_log" ,nativeQuery = true)
    void cleanOperLog();
}
