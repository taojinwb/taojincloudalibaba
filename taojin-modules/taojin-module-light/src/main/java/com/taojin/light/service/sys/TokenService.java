package com.taojin.light.service.sys;

import javax.servlet.http.HttpServletRequest;

import com.taojin.light.dto.LoginUser;
import com.taojin.light.framework.util.JwtUtil;
import com.taojin.light.framework.util.TokenUtils;
import com.taojin.light.framework.constant.CacheConstant;
import com.taojin.framework.constant.SysConstants;
import com.taojin.framework.redis.RedisUtil;
import com.taojin.framework.utils.*;
import eu.bitwalker.useragentutils.UserAgent;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * token验证处理
 *
 * @author sujg
 */
@Component
@Slf4j
public class TokenService
{

    @Autowired
    private RedisUtil redisUtil;

    private static final Long MILLIS_MINUTE_TEN = 20 * 60 * 1000L;
    protected static final long MILLIS_SECOND = 1000;

    protected static final long MILLIS_MINUTE = 60 * MILLIS_SECOND;

    private int expireTime=30;

    /**
     * 获取用户身份信息
     *
     * @return 用户信息
     */
    public LoginUser getLoginUser(HttpServletRequest request)
    {

        // 获取请求携带的令牌
        String token = getToken(request);
        if (BF.APPCompare(token,"<>","") && BF.APPCompare(token,"<>","null"))
        {
            try
            {

               if(TokenUtils.verifyToken(token,redisUtil)) {
                   log.info("----------"+redisUtil.get(CacheConstant.LOGIN_TOKEN_KEY +token));
                   LoginUser user = (LoginUser) redisUtil.get(CacheConstant.LOGIN_TOKEN_KEY +token);
                   return user;
               }
            }
            catch (Exception e)
            {
                log.error("获取用户信息异常'{}'", e.getMessage());
            }
        }
        return null;
    }
    /**
     * 设置用户身份信息
     */
    public void setLoginUser(LoginUser loginUser)
    {
        if (MF.isNotNull(loginUser) && StringUtils.isNotEmpty(loginUser.getToken()))
        {
            refreshToken(loginUser);
        }
    }
    /**
     * 删除用户身份信息
     */
    public void delLoginUser(String token)
    {
        if (StringUtils.isNotEmpty(token))
        {
            redisUtil.del(token);
        }
    }
    /**
     * 创建令牌
     *
     * @param loginUser 用户信息
     * @return 令牌
     */
    public String createToken(LoginUser loginUser)
    {
      String  token = JwtUtil.sign(loginUser.getUsername(), loginUser.getUsername());;
        loginUser.setToken(token);
        setUserAgent(loginUser);
        refreshToken(loginUser);

        return  token;
    }


    /**
     * 验证令牌有效期，相差不足20分钟，自动刷新缓存
     *
     * @param loginUser
     * @return 令牌
     */
    public void verifyToken(LoginUser loginUser)
    {
        long expireTime = loginUser.getExpireTime();
        long currentTime = System.currentTimeMillis();
        if (expireTime - currentTime <= MILLIS_MINUTE_TEN)
        {
            refreshToken(loginUser);
        }
    }

    /**
     * 刷新令牌有效期
     *
     * @param loginUser 登录信息
     */
    public void refreshToken(LoginUser loginUser)
    {
        loginUser.setLoginTime(System.currentTimeMillis());
        loginUser.setExpireTime(loginUser.getLoginTime() + expireTime * MILLIS_MINUTE);
        redisUtil.set(CacheConstant.LOGIN_TOKEN_KEY+loginUser.getToken(), loginUser, expireTime);
    }
    /**
     * 获取请求token
     *
     * @param request
     * @return token
     */
    private String getToken(HttpServletRequest request)
    {
        String token = request.getHeader(CacheConstant.HEAD_AUTHORIZATION);
        if (StringUtils.isNotEmpty(token) && token.startsWith(SysConstants.TOKEN_PREFIX))
        {
            token = token.replace(SysConstants.TOKEN_PREFIX, "");
            token = token.replaceAll("\\s","");
            if(BF.APPCompare(token,"=","null")){
                token = "";
            }
        }else{
            token="";
        }
        return token;
    }

    /**
     * 设置用户代理信息
     *
     * @param loginUser 登录信息
     */
    public void setUserAgent(LoginUser loginUser)
    {
        UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
        String ip = IpUtils.getIpAddr();
        loginUser.setIpaddr(ip);
        loginUser.setLoginLocation(AddressUtils.getRealAddressByIP(ip));
        loginUser.setBrowser(userAgent.getBrowser().getName());
        loginUser.setOs(userAgent.getOperatingSystem().getName());
        loginUser.setLoginTime(System.currentTimeMillis());
    }

}
