package com.taojin.light.repository.sys;

import com.taojin.light.entity.sys.SysConfig;
import com.taojin.light.entity.sys.SysNotice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SysNoticeRepository  extends  JpaRepository<SysNotice, Long>, JpaSpecificationExecutor<SysNotice>{

}
