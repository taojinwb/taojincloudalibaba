package com.taojin.light.framework.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.taojin.framework.vo.ApiResult;
import com.taojin.light.framework.constant.CacheConstant;
import com.taojin.framework.constant.SysConstants;
import com.taojin.framework.utils.BF;
import com.taojin.framework.utils.MF;
import com.taojin.framework.utils.ServletUtils;
import com.taojin.framework.constant.SymbolConstant;
import com.taojin.framework.exception.BaseException;
import org.apache.commons.lang3.StringUtils;

/**
 * @Author Scott
 * @Date 2018-07-12 14:23
 * @Desc JWT工具类
 **/
public class JwtUtil {

    /**Token有效期为7天（Token在reids中缓存时间为两倍）*/
    //public static final long EXPIRE_TIME = (7 * 12) * 60 * 60 * 1000;
    public static final long EXPIRE_TIME = 60*60 * 1000;
    static final String WELL_NUMBER = SymbolConstant.WELL_NUMBER + SymbolConstant.LEFT_CURLY_BRACKET;

    /**
     *
     * @param response
     * @param code
     * @param errorMsg
     */
    public static void responseError(ServletResponse response, Integer code, String errorMsg) {
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        // issues/I4YH95浏览器显示乱码问题
        httpServletResponse.setHeader("Content-type", "text/html;charset=UTF-8");
        ApiResult jsonResult = new ApiResult(code, errorMsg);
        jsonResult.setSuccess(false);
        OutputStream os = null;
        try {
            os = httpServletResponse.getOutputStream();
            httpServletResponse.setCharacterEncoding("UTF-8");
            httpServletResponse.setStatus(code);
            os.write(new ObjectMapper().writeValueAsString(jsonResult).getBytes("UTF-8"));
            os.flush();
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 校验token是否正确
     *
     * @param token  密钥
     * @param secret 用户的密码
     * @return 是否正确
     */
    public static boolean verify(String token, String username, String secret) {
        try {
            // 根据密码生成JWT效验器
            Algorithm algorithm = Algorithm.HMAC256(secret);
            JWTVerifier verifier = JWT.require(algorithm).withClaim("username", username).build();
            // 效验TOKEN
            DecodedJWT jwt = verifier.verify(token);
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    /**
     * 获得token中的信息无需secret解密也能获得
     *
     * @return token中包含的用户名
     */
    public static String getUsername(String token) {
        try {
            if(BF.APPCompare(token,"<>","")){

                 DecodedJWT jwt = JWT.decode(token);
                return jwt.getClaim("username").asString();
            }
            return null;
        } catch (JWTDecodeException e) {
            return null;
        }
    }

    /**
     * 生成签名,5min后过期
     *
     * @param username 用户名
     * @param secret   用户的密码
     * @return 加密的token
     */
    public static String sign(String username, String secret) {
        Date date = new Date(System.currentTimeMillis() + EXPIRE_TIME);
        Algorithm algorithm = Algorithm.HMAC256(secret);
        // 附带username信息
        return JWT.create().withClaim("username", username).withExpiresAt(date).sign(algorithm);

    }

    /**
     * 根据request中的token获取用户账号
     *
     * @param request
     * @return
     * @throws BaseException
     */
    public static String getUserNameByToken(HttpServletRequest request) throws BaseException {
        String accessToken = getToken(request);
        String username ="";
             username = getUsername(accessToken);
            if (MF.isEmpty(username)) {
                throw new BaseException("未获取到用户");
            }
        return username;
    }
    /**
     * 获取请求token
     *
     * @param request
     * @return token
     */
    private static String getToken(HttpServletRequest request)
    {
        String token = request.getHeader(CacheConstant.HEAD_AUTHORIZATION);
        if (StringUtils.isNotEmpty(token) && token.startsWith(SysConstants.TOKEN_PREFIX))
        {
            //前端送的token 拼接了Constants.TOKEN_PREFIX
            token = token.replace(SysConstants.TOKEN_PREFIX, "");
            token = token.replaceAll("\\s","");
            if(BF.APPCompare(token,"=","null")){
                token = "";
            }
        }else{
            token="";
        }
        return token;
    }
    /**
     *  从session中获取变量
     * @param key
     * @return
     */
    public static String getSessionData(String key) {
        //${myVar}%
        //得到${} 后面的值
        String moshi = "";
        String wellNumber = WELL_NUMBER;

        if(key.indexOf(SymbolConstant.RIGHT_CURLY_BRACKET)!=-1){
            moshi = key.substring(key.indexOf("}")+1);
        }
        String returnValue = null;
        if (key.contains(wellNumber)) {
            key = key.substring(2,key.indexOf("}"));
        }
        if (MF.isNotEmpty(key)) {
            HttpSession session = ServletUtils.getRequest().getSession();
            returnValue = (String) session.getAttribute(key);
        }
        //结果加上${} 后面的值
        if(returnValue!=null){returnValue = returnValue + moshi;}
        return returnValue;
    }
 
	public static void main(String[] args) {
        Date date = new Date(System.currentTimeMillis() + EXPIRE_TIME);
        Algorithm algorithm = Algorithm.HMAC256("123456");
        // 附带username信息
        String ss= JWT.create().withClaim("username", "admin").withExpiresAt(date).sign(algorithm);
        System.out.println(ss);

        System.out.printf("==="+getUsername(ss));
	}
}