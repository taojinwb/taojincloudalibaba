package com.taojin.light.repository.sys;

import com.taojin.light.entity.sys.SysConfig;
import com.taojin.light.entity.sys.SysDept;
import com.taojin.light.dto.sys.SysDeptDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SysDeptRepository extends  JpaRepository<SysDept, Long>, JpaSpecificationExecutor<SysDept> {
    @Query(value="select d.dept_id" +
            " from sys_dept d" +
            "            left join sys_role_dept rd on d.dept_id = rd.dept_id" +
            "        where rd.role_id = #{roleId}" +
            "              and d.dept_id not in (select d.parent_id from sys_dept d inner join sys_role_dept rd on d.dept_id = rd.dept_id and rd.role_id = #{roleId})"+
            " order by d.parent_id, d.order_num" ,nativeQuery = true)
   List<Long> selectDeptListByRoleId(@Param("roleId") Long roleId,@Param("deptCheckStrictly")  Integer deptCheckStrictly);



    @Query(value="select d.dept_id" +
            " from sys_dept d" +
            "            left join sys_role_dept rd on d.dept_id = rd.dept_id" +
            "        where rd.role_id = #{roleId} " +
            " order by d.parent_id, d.order_num" ,nativeQuery = true)
    List<Long> selectDeptListByRoleId(@Param("roleId") Long roleId);

    @Query(value="select count(*) from sys_dept where status = 0 and del_flag = '0' and find_in_set(#{deptId}, ancestors)",nativeQuery = true)
    int selectNormalChildrenDeptById(@Param("deptId") Long deptId);
    @Query(value="select count(1) from sys_dept " +
            " where del_flag = '0' and parent_id = #{deptId} limit 1",nativeQuery = true)
    int hasChildByDeptId(@Param("deptId") Long deptId);

    int findAllByDeptIdAndDelFlag(Long deptId, Long delflag);

    @Query(value="select   * from sys_dept where dept_name=:deptName and parent_id=:parentId and del_flag=:delFlag",nativeQuery = true)
    SysDept findFirstByDeptNameAndParentIdAndDelFlag(String deptName, Long parentId, Long delFlag);


    @Query(value="select distinct new com.taojin.light.dto.sys.SysDeptDTO(d.deptId, d.parentId, d.ancestors, d.deptName, d.orderNum, d.leader, d.phone, d.email, d.status,e.deptName as parentName ) "+
            " from SysDept d\n" +
            " left join SysDept e on e.deptId = d.parentId\n"+
            " where d.deptId = :deptId")
    SysDeptDTO selectDeptById(@Param("deptId") Long deptId);

    @Modifying
    @Query("update SysDept d set d.status = '0' where d.deptId in :deptIds")
    void updateStatusForDeptIds(@Param("deptIds") Long[] deptIds);

    @Query(value="select *  from sys_dept where find_in_set(:deptId, ancestors)",nativeQuery = true)
    List<SysDeptDTO> selectChildrenDeptById(@Param("deptId") Long deptId);


}
