package com.taojin.light.service.device;

import com.taojin.framework.redis.RedisUtil;
import com.taojin.light.client.bean.ReCodeBean;
import com.taojin.light.client.common.DateUtils;
import com.taojin.light.entity.device.BIthDevice;

import java.util.Optional;

import com.taojin.light.repository.device.BIthDeviceRepository;
import com.taojin.light.repository.device.BIthLightLoopRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    BIthDeviceRepository deviceDao;
    @Autowired
    BIthLightLoopRepository lightLoopDao;
    @Autowired
    private RedisUtil redisClient;
    private String serviceIp;

    public DataService() {
    }

    public void regDeviceToCache(String deviceId) {
        this.redisClient.hset("dk", deviceId, this.serviceIp);
    }

    public void removeDeviceToCache(String deviceId) {
        this.redisClient.hdel("dk", new Object[]{deviceId});
    }

    public BIthDevice getDeviceInfo(Long deviceId) {
        Optional<BIthDevice> opt = this.deviceDao.findById(deviceId);
        return opt.isPresent() ? (BIthDevice)opt.get() : null;
    }

    public void RegDevice(Long deviceId, String ip) {
        int row = this.deviceDao.regDeviceForId(deviceId, "LKD-304/G780-BF-XLXAD", ip, DateUtils.getDeviceHitTime());
        this.logger.info("{} 写入 id = {},time = {}, row = {}", new Object[]{"dk" + deviceId, ip, DateUtils.getDeviceHitTime(), row});
    }

    public void updateDeviceLocalIP(Long deviceId, String ip) {
        int row = this.deviceDao.updateIpForId(deviceId, ip, DateUtils.getDeviceHitTime());
        this.logger.info("{} 写入 id = {},time = {}, row = {}", new Object[]{"dk" + deviceId, ip, DateUtils.getDeviceHitTime(), row});
    }

    public void updateDeviceTime(Long deviceId) {
        int row = this.deviceDao.updateTimeForId(deviceId, DateUtils.getDeviceHitTime());
        this.logger.info("{} 写入 id = dk{}, time = {},row = {}", new Object[]{deviceId, DateUtils.getDeviceHitTime(), row});
    }

    public void updateLoopStatus(ReCodeBean recode) {
        Long deviceId = recode.virDevice.getDeviceId();
        if (recode.getCodeMaps().size() > 0) {
            recode.getCodeMaps().forEach((key, value) -> {
                int loopNumber = Integer.parseInt(key);
                int status = Integer.parseInt(value);
                int rows = this.lightLoopDao.updateLoopStatus(deviceId, loopNumber, status);
                this.logger.info("update = {}, 设备ID = {}， 回路 = {}, 状态 = {}", new Object[]{rows, deviceId, loopNumber, status});
            });
        }

    }

    public String getServiceIp() {
        return this.serviceIp;
    }

    public void setServiceIp(String serviceIp) {
        this.serviceIp = serviceIp;
    }
}
