package com.taojin.light;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan("com.taojin")
@EnableJpaRepositories(basePackages = "com.taojin")
@EnableFeignClients(basePackages = "com.taojin.api")
//@EnableScheduling
@Slf4j
public class TaojinLightApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(TaojinLightApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  灯控系统后台  taojin-module-light启动成   ლ(´ڡ`ლ)ﾞ  \n" );
        Environment env = applicationContext.getEnvironment();
        printActiveProfiles(env);
    }
    private static void printActiveProfiles(Environment env) {
        String[] activeProfiles = env.getActiveProfiles();
        log.info("\n****************************************************************\n"+
                "*                                                              *\n"+
                "**********************启动了配置模式:【" + String.join(", ", activeProfiles)+"】成功********************\n"+
                "*                                                              *\n"+
                "****************************************************************");
    }

}
