package com.taojin.light.service.sys;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.taojin.light.framework.common.BaseService;
import com.taojin.light.entity.sys.SysMenu;
import com.taojin.light.entity.sys.SysRole;
import com.taojin.light.dto.jpadto.SysMenuJPADTO;
import com.taojin.light.dto.jpadto.SysUserJPADTO;
import com.taojin.light.entity.sys.vo.MetaVo;
import com.taojin.light.entity.sys.vo.RouterVo;
import com.taojin.light.framework.security.SecurityUtils;
import com.taojin.light.framework.vo.TreeSelect;
import com.taojin.light.repository.sys.SysMenuRepository;
import com.taojin.light.repository.sys.SysRoleMenuRepository;
import com.taojin.light.repository.sys.SysRoleRepository;
import com.taojin.framework.utils.BF;
import com.taojin.framework.utils.MF;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.taojin.framework.constant.SysConstants;
import com.taojin.framework.constant.UserConstants;

/**
 * 菜单 业务层处理
 * 
 * @author sujg
 */
@Service
@Slf4j
public class SysMenuService extends BaseService<SysMenu,Long>
{
    @Autowired
    private SysMenuRepository sysMenuRepository;
    @Autowired
    private SysRoleRepository sysRoleRepository;
    @Autowired
    private SysRoleMenuRepository sysRoleMenuRepository;
    public SysMenuService(SysMenuRepository repository) {
        super(repository,repository);
    }




    public static final String PREMISSION_STRING = "perms[\"{0}\"]";


    /**
     * 根据用户查询系统菜单列表
     * 
     * @param userId 用户ID
     * @return 菜单列表
     */
    public List<SysMenuJPADTO> selectMenuList(Long userId)
    {
        return selectMenuList(new SysMenuJPADTO(), userId);
    }

    /**
     * 查询系统菜单列表
     * 
     * @param menu 菜单信息
     * @return 菜单列表
     */
    public List<SysMenuJPADTO> selectMenuList(SysMenuJPADTO menu, Long userId)
    {

        List<SysMenuJPADTO> menuList = null;
        // 管理员显示所有菜单信息
        if (SysUserJPADTO.isAdmin(userId))
        {
            List<SysMenu> m = sysMenuRepository.findallList(menu);
            menuList = new ArrayList<>();
            for (SysMenu sm : m) {
                SysMenuJPADTO menuVo = new SysMenuJPADTO();
                BeanUtils.copyProperties(sm,menuVo);
                menuList.add(menuVo);
            }
        }
        else
        {
            menu.getParams().put("userId", userId);
            menuList = selectMenuListByUserId(menu);
        }
        return menuList;
    }

    /**
     * 类转换
     * @param menu
     * @return
     */
    public List<SysMenuJPADTO> selectMenuListByUserId(SysMenuJPADTO menu){
        List<SysMenuJPADTO> menuList = new ArrayList<>();
       List<SysMenu>  m = sysMenuRepository.selectMenuListByUserId(menu);
        for (SysMenu sm : m) {
            SysMenuJPADTO menuVo = new SysMenuJPADTO();
            BeanUtils.copyProperties(sm,menuVo);
            menuList.add(menuVo);
        }

        return menuList;
    }
    /**
     * 根据用户ID查询权限
     * 
     * @param userId 用户ID
     * @return 权限列表
     */
    public Set<String> selectMenuPermsByUserId(Long userId)
    {
        List<String> perms = sysMenuRepository.selectMenuPermsByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        for (String perm : perms)
        {
            if (StringUtils.isNotEmpty(perm))
            {
                permsSet.addAll(Arrays.asList(perm.trim().split(",")));
            }
        }
        return permsSet;
    }

    /**
     * 根据角色ID查询权限
     * 
     * @param roleId 角色ID
     * @return 权限列表
     */
    public Set<String> selectMenuPermsByRoleId(Long roleId)
    {
        List<String> perms = sysMenuRepository.selectMenuPermsByRoleId(roleId);
        Set<String> permsSet = new HashSet<>();
        for (String perm : perms)
        {
            if (StringUtils.isNotEmpty(perm))
            {
                permsSet.addAll(Arrays.asList(perm.trim().split(",")));
            }
        }
        return permsSet;
    }

    /**
     * 根据用户ID查询菜单
     * 
     * @param userId 用户名称
     * @return 菜单列表
     */
    public List<SysMenuJPADTO> selectMenuTreeByUserId(Long userId)
    {
        List<SysMenuJPADTO> menus ;
        if (SecurityUtils.isAdmin(userId))
        {
            
            menus = sysMenuRepository.selectMenuTreeAll();

        }
        else
        {
            menus = sysMenuRepository.selectMenuTreeByUserId(userId);
        }
        return getChildPerms(menus, 0);
    }

    /**
     * 根据角色ID查询菜单树信息
     * 
     * @param roleId 角色ID
     * @return 选中菜单列表
     */
    public List<Long> selectMenuListByRoleId(Long roleId)
    {
        SysRole role = sysRoleRepository.selectRoleById(roleId);
        if(role.isDeptCheckStrictly()){
            return sysMenuRepository.selectMenuListByRoleIdgl(roleId);
        }else{
            return sysMenuRepository.selectMenuListByRoleId(roleId);
        }
    }

    /**
     * 构建前端路由所需要的菜单
     * 
     * @param menus 菜单列表
     * @return 路由列表
     */
    public List<RouterVo> buildMenus(List<SysMenuJPADTO> menus)
    {
        List<RouterVo> routers = new LinkedList<RouterVo>();
        for (SysMenuJPADTO menu : menus)
        {
            RouterVo router = new RouterVo();
            router.setHidden("1".equals(menu.getVisible()));
            router.setName(getRouteName(menu));
            router.setPath(getRouterPath(menu));
            router.setComponent(getComponent(menu));

            router.setQuery(menu.getQuery());
            router.setMeta(new MetaVo(menu.getMenuName(), menu.getIcon(), StringUtils.equals("1", menu.getIsCache()), menu.getPath()));
            List<SysMenuJPADTO> cMenus = menu.getChildren();
            if (MF.isNotEmpty(cMenus) && UserConstants.TYPE_DIR.equals(menu.getMenuType()))
            {
                router.setAlwaysShow(true);
                router.setRedirect("noRedirect");
                router.setChildren(buildMenus(cMenus));
            }
            else if (isMenuFrame(menu))
            {
                router.setMeta(null);
                List<RouterVo> childrenList = new ArrayList<RouterVo>();
                RouterVo children = new RouterVo();
                children.setPath(menu.getPath());
                children.setComponent(menu.getComponent());
                children.setName(StringUtils.capitalize(menu.getPath()));
                children.setMeta(new MetaVo(menu.getMenuName(), menu.getIcon(), StringUtils.equals("1", menu.getIsCache()), menu.getPath()));
                children.setQuery(menu.getQuery());
                childrenList.add(children);
                router.setChildren(childrenList);
            }
            else if (menu.getParentId().intValue() == 0 && isInnerLink(menu))
            {
                router.setMeta(new MetaVo(menu.getMenuName(), menu.getIcon()));
                router.setPath("/");
                List<RouterVo> childrenList = new ArrayList<RouterVo>();
                RouterVo children = new RouterVo();
                String routerPath = innerLinkReplaceEach(menu.getPath());
                children.setPath(routerPath);
                children.setComponent(UserConstants.INNER_LINK);
                children.setName(StringUtils.capitalize(routerPath));
                children.setMeta(new MetaVo(menu.getMenuName(), menu.getIcon(), menu.getPath()));
                childrenList.add(children);
                router.setChildren(childrenList);
            }
            routers.add(router);
        }
        return routers;
    }

    /**
     * 构建前端所需要树结构
     * 
     * @param menus 菜单列表
     * @return 树结构列表
     */
    public List<SysMenuJPADTO> buildMenuTree(List<SysMenuJPADTO> menus)
    {
        List<SysMenuJPADTO> returnList = new ArrayList<SysMenuJPADTO>();
        List<Long> tempList = menus.stream().map(SysMenuJPADTO::getMenuId).collect(Collectors.toList());
        for (Iterator<SysMenuJPADTO> iterator = menus.iterator(); iterator.hasNext();)
        {
            SysMenuJPADTO menu = (SysMenuJPADTO) iterator.next();
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(menu.getParentId()))
            {
                recursionFn(menus, menu);
                returnList.add(menu);
            }
        }
        if (returnList.isEmpty())
        {
            returnList = menus;
        }
        return returnList;
    }

    /**
     * 构建前端所需要下拉树结构
     * 
     * @param menus 菜单列表
     * @return 下拉树结构列表
     */
    public List<TreeSelect> buildMenuTreeSelect(List<SysMenuJPADTO> menus)
    {
        List<SysMenuJPADTO> menuTrees = buildMenuTree(menus);
        return menuTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
    }

    /**
     * 根据菜单ID查询信息
     * 
     * @param menuId 菜单ID
     * @return 菜单信息
     */
    public SysMenu selectMenuById(Long menuId)
    {
        return sysMenuRepository.findById(menuId).orElse(null);
    }

    /**
     * 是否存在菜单子节点
     * 
     * @param menuId 菜单ID
     * @return 结果
     */
    public boolean hasChildByMenuId(Long menuId)
    {
        int result = sysMenuRepository.countByParentId(menuId);
        return result > 0;
    }

    /**
     * 查询菜单使用数量
     * 
     * @param menuId 菜单ID
     * @return 结果
     */
    public boolean checkMenuExistRole(Long menuId)
    {
        int result = sysRoleMenuRepository.countByMenuId(menuId);
        return result > 0;
    }



    /**
     * 校验菜单名称是否唯一
     * 
     * @param menu 菜单信息
     * @return 结果
     */
    public boolean checkMenuNameUnique(SysMenu menu)
    {
        Long menuId = MF.isNull(menu.getMenuId()) ? -1L : menu.getMenuId();

        SysMenu info = sysMenuRepository.checkMenuNameUnique(menu.getMenuName(), menu.getParentId());

        if (MF.isNotNull(info) && info.getMenuId().longValue() != menuId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 获取路由名称
     * 
     * @param menu 菜单信息
     * @return 路由名称
     */
    public String getRouteName(SysMenuJPADTO menu)
    {
        String routerName = StringUtils.capitalize(menu.getPath());
        // 非外链并且是一级目录（类型为目录）
        if (isMenuFrame(menu))
        {
            routerName = StringUtils.EMPTY;
        }
        return routerName;
    }

    /**
     * 获取路由地址
     * 
     * @param menu 菜单信息
     * @return 路由地址
     */
    public String getRouterPath(SysMenuJPADTO menu)
    {
        String routerPath = menu.getPath();
        // 内链打开外网方式
        if (menu.getParentId().intValue() != 0 && isInnerLink(menu))
        {
            routerPath = innerLinkReplaceEach(routerPath);
        }
        // 非外链并且是一级目录（类型为目录）
        if (0 == menu.getParentId().intValue() && UserConstants.TYPE_DIR.equals(menu.getMenuType())
                && UserConstants.NO_FRAME.equals(menu.getIsFrame()))
        {
            routerPath = "/" + menu.getPath();
        }
        // 非外链并且是一级目录（类型为菜单）
        else if (isMenuFrame(menu))
        {
            routerPath = "/";
        }
        return routerPath;
    }

    /**
     * 获取组件信息
     * 
     * @param menu 菜单信息
     * @return 组件信息
     */
    public String getComponent(SysMenuJPADTO menu)
    {
        String component = UserConstants.LAYOUT;
        if (StringUtils.isNotEmpty(menu.getComponent()) && !isMenuFrame(menu))
        {
            component = menu.getComponent();
        }
        else if (StringUtils.isEmpty(menu.getComponent()) && menu.getParentId().intValue() != 0 && isInnerLink(menu))
        {
            component = UserConstants.INNER_LINK;
        }
        else if (StringUtils.isEmpty(menu.getComponent()) && isParentView(menu))
        {
            component = UserConstants.PARENT_VIEW;
        }
        return component;
    }

    /**
     * 是否为菜单内部跳转
     * 
     * @param menu 菜单信息
     * @return 结果
     */
    public boolean isMenuFrame(SysMenuJPADTO menu)
    {
        return menu.getParentId().intValue() == 0 && UserConstants.TYPE_MENU.equals(menu.getMenuType())
                && menu.getIsFrame().equals(UserConstants.NO_FRAME);
    }

    /**
     * 是否为内链组件
     * 
     * @param menu 菜单信息
     * @return 结果
     */
    public boolean isInnerLink(SysMenuJPADTO menu)
    {
        return BF.APPCompare(menu.getIsFrame(),"=",UserConstants.NO_FRAME) && MF.ishttp(menu.getPath());
    }

    /**
     * 是否为parent_view组件
     * 
     * @param menu 菜单信息
     * @return 结果
     */
    public boolean isParentView(SysMenuJPADTO menu)
    {
        return menu.getParentId().intValue() != 0 && UserConstants.TYPE_DIR.equals(menu.getMenuType());
    }

    /**
     * 根据父节点的ID获取所有子节点
     * 
     * @param list 分类表
     * @param parentId 传入的父节点ID
     * @return String
     */
    public List<SysMenuJPADTO> getChildPerms(List<SysMenuJPADTO> list, int parentId)
    {
        List<SysMenuJPADTO> returnList = new ArrayList<SysMenuJPADTO>();
        for (Iterator<SysMenuJPADTO> iterator = list.iterator(); iterator.hasNext();)
        {
            SysMenuJPADTO t = (SysMenuJPADTO) iterator.next();
            // 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
            if (t.getParentId() == parentId)
            {
                recursionFn(list, t);
                returnList.add(t);
            }
        }
        return returnList;
    }

    /**
     * 递归列表
     * 
     * @param list 分类表
     * @param t 子节点
     */
    private void recursionFn(List<SysMenuJPADTO> list, SysMenuJPADTO t)
    {
        // 得到子节点列表
        List<SysMenuJPADTO> childList = getChildList(list, t);
        t.setChildren(childList);
        for (SysMenuJPADTO tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<SysMenuJPADTO> getChildList(List<SysMenuJPADTO> list, SysMenuJPADTO t)
    {
        List<SysMenuJPADTO> tlist = new ArrayList<SysMenuJPADTO>();
        Iterator<SysMenuJPADTO> it = list.iterator();
        while (it.hasNext())
        {
            SysMenuJPADTO n = (SysMenuJPADTO) it.next();
            if (n.getParentId().longValue() == t.getMenuId().longValue())
            {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<SysMenuJPADTO> list, SysMenuJPADTO t)
    {
        return getChildList(list, t).size() > 0;
    }

    /**
     * 内链域名特殊字符替换
     * 
     * @return 替换后的内链域名
     */
    public String innerLinkReplaceEach(String path)
    {
        return StringUtils.replaceEach(path, new String[] { SysConstants.HTTP, SysConstants.HTTPS, SysConstants.WWW, "." },
                new String[] { "", "", "", "/" });
    }
}
