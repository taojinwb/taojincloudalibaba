package com.taojin.light.service.sys;

import com.taojin.light.dto.LoginUser;
import com.taojin.light.dto.jpadto.SysUserJPADTO;
import com.taojin.framework.enums.UserStatus;
import com.taojin.framework.exception.ServiceException;
import com.taojin.framework.utils.BF;
import com.taojin.framework.utils.MessageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 用户验证处理
 *
 * @author sujg
 */
@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService
{
    private static final Logger log = LoggerFactory.getLogger(UserDetailsService.class);

    @Autowired
    private SysUserService userService;
    
    @Autowired
    private SysPasswordService passwordService;

    @Autowired
    private SysPermissionService permissionService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
             SysUserJPADTO sysUserJPADTO = new SysUserJPADTO();
              sysUserJPADTO.setUserName(username);
            SysUserJPADTO user = userService.selectUserByUserName(sysUserJPADTO);
            if (user == null)
            {
                log.info("登录用户：{} 不存在.", username);
                throw new ServiceException(MessageUtils.message("user.not.exists"));
            }
            else if (BF.APPCompare(UserStatus.DELETED.getCode(),"=",user.getDelFlag()))
            {
                log.info("登录用户：{} 已被删除.", username);
                throw new ServiceException(MessageUtils.message("user.password.delete"));
            }
            else if (BF.APPCompare(UserStatus.DISABLE.getCode(),"=",user.getStatus()))
            {
                log.info("登录用户：{} 已被停用.", username);
                throw new ServiceException(MessageUtils.message("user.blocked"));
            }

        passwordService.validate(user);

        return createLoginUser(user);
    }

    public UserDetails createLoginUser(SysUserJPADTO user)
    {
        LoginUser loginUser = new LoginUser(user.getUserId(), user.getDeptId(),user.getUnionid(), user, permissionService.getMenuPermission(user));
        return loginUser;
    }
}
