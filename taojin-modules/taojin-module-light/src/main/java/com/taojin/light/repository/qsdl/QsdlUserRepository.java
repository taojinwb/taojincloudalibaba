//package com.taojin.light.repository.qsdl;
//
//import com.taojin.light.entity.sys.SysRole;
//import com.taojin.light.entity.sys.SysUser;
//import com.taojin.light.dto.jpadto.SysRoleJPADTO;
//import com.taojin.light.dto.jpadto.SysUserJPADTO;
//
//import java.util.List;
//
///**
// * 不方便，暂时不启用
// */
//public interface QsdlUserRepository {
//
//
//    public List<SysRoleJPADTO> selectRoleList(SysRole sysRole, int page, int size, String sortProperty);
//
//    /**
//     * 根据用户实体类过滤 获取该用户部门、权限相关信息
//     * @param SysUser
//     * @param sortProperty
//     * @return
//     */
//    public List<SysUserJPADTO> getSysUserVoBySyUser(SysUser sysUser, String sortProperty);
//
//
//}
