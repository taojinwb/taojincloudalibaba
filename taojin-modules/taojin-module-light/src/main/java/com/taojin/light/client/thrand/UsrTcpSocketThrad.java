package com.taojin.light.client.thrand;

import com.taojin.light.client.bean.ReCodeBean;
import com.taojin.light.client.bean.VirtualDevice;
import com.taojin.light.client.common.BeiFenCtrlCmds;
import com.taojin.light.client.common.ClientMap;
import com.taojin.light.client.common.ModBusUtils;
import com.taojin.light.dto.AtSetDTO;
import com.taojin.light.service.device.DataService;
import com.taojin.light.entity.device.BIthDevice;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UsrTcpSocketThrad implements Runnable {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    DataService dataService = null;
    private String clientHost = null;
    private Socket socket;
    private String lightId = "";
    private Long deviceId = 0L;
    private int loopNum = 0;
    private OutputStream mOutputStream = null;
    boolean state;

    public UsrTcpSocketThrad(Socket socket, DataService dataService) {
        this.socket = socket;
        this.dataService = dataService;
        this.clientHost = socket.getRemoteSocketAddress().toString().replace("/", "");
    }

    public void run() {
        this.state = true;
        this.logger.info("socket is connect ip:port = {} ", this.clientHost);

        try {
            if (!this.socket.getKeepAlive()) {
                this.socket.setKeepAlive(true);
            }

            this.socket.setSoTimeout(60000);
            String clientMsg = "";

            for(this.mOutputStream = this.socket.getOutputStream(); this.state; Thread.sleep(3000L)) {
                InputStream mInputStream = this.socket.getInputStream();
                byte[] bytes = ModBusUtils.readInputStream(mInputStream);
                clientMsg="";
                for(int i = 0; i < bytes.length; ++i) {
                    clientMsg = clientMsg + ModBusUtils.byteToASCLL(bytes[i]);
                }

                this.logger.info("收到信息={}", clientMsg);
                String crc = ModBusUtils.getCRC3(bytes);
                logger.error(crc+"=================");
                if (clientMsg.indexOf("dk") != -1) {
                    this.logger.info("注册信息：{}", clientMsg);
                    this.lightId = clientMsg.trim().replaceAll("dk", "").replaceAll("ok","");
                    this.deviceId = Long.parseLong(this.lightId.trim().replaceAll("dk", "").replaceAll("ok",""));
                    this.logger.info("device = {} ,crc ={}", this.deviceId, crc);
                    if (!"".equals(this.lightId)) {
                        if (!this.isRegDevice()) {
                            this.dataService.RegDevice(this.deviceId, this.clientHost);
                        } else {
                            this.dataService.updateDeviceLocalIP(this.deviceId, this.clientHost);
                        }

                        if (!ClientMap.clients.containsKey(this.lightId)) {
                            VirtualDevice virDevice = new VirtualDevice();
                            virDevice.setLightId(this.lightId);
                            virDevice.setSocket(this.socket);
                            virDevice.setLoopNum(this.loopNum);
                            virDevice.setDeviceId(this.deviceId);
                            ClientMap.clients.put(this.lightId, virDevice);
                            logger.info("--------设备注册成功"+this.lightId);
                            this.dataService.regDeviceToCache(this.lightId);
                        } else {
                            ((VirtualDevice)ClientMap.clients.get(this.lightId)).setSocket(this.socket);
                            this.dataService.regDeviceToCache(this.lightId);
                            logger.info("设备注册成功！"+this.lightId);
                        }

                        AtSetDTO set = new AtSetDTO();
                        set.setDeviceId(this.lightId);
                        set.setRouteid(0);
                        set.setExeaction("?");
                        this.sendComd(set);
                    } else {
                        this.dataService.updateDeviceTime(this.deviceId);
                    }
                } else if ("0000".equals(crc)) {
                    String sexadecimal = ModBusUtils.bytes2HexString(bytes);
                    if (this.reCodeVerification(sexadecimal)) {
                        ReCodeBean code = new ReCodeBean(ModBusUtils.bytes2HexString(bytes), (VirtualDevice)ClientMap.clients.get(this.lightId));
                        this.dataService.updateLoopStatus(code);
                        ((VirtualDevice)ClientMap.clients.get(this.lightId)).setRecode(code);
                        this.logger.info("回码解析：{},code = {}", this.lightId, code.toString());
                    } else {
                        this.logger.info("回码不合法！");
                    }
                } else if ("75B7".equals(crc) || clientMsg.contains("ok")) {
                    this.dataService.updateDeviceTime(this.deviceId);
                    this.logger.info("心跳信息：{}，msg = {}", this.lightId, clientMsg);
                } else if ("".equals(clientMsg.trim())) {
                    this.logger.info("已断开链接：{}，msg = {}", this.lightId, clientMsg);
                    this.socket.close();
                    this.mOutputStream.close();
                    mInputStream.close();
                    ClientMap.clients.remove(this.lightId);
                    this.dataService.removeDeviceToCache(this.lightId);
                    this.state = false;
                } else {
                    this.logger.info("未知信息：{}，msg = {}", this.lightId, clientMsg);
                }
            }
        } catch (Exception var7) {
            this.state = false;
        }

    }

    private boolean isRegDevice() {
        boolean reg = false;
        BIthDevice device = this.dataService.getDeviceInfo(this.deviceId);
        if (null != device) {
            this.logger.info("device info  = {}", device.toString());
            if (null != device.getT1()) {
                this.loopNum = Integer.parseInt(device.getT1());
            }

            if (null != device.getSerial() && !"".equals(device.getSerial())) {
                reg = true;
            }
        }

        return reg;
    }

    private void sendComd(AtSetDTO set) throws IOException {
        String comd = null;
        if (set.getExeaction().indexOf("?") == -1) {
            comd = BeiFenCtrlCmds.getComdForloopNoId(set.getRouteid(), Integer.parseInt(set.getExeaction()));
            this.logger.info("操作指令 = {}", comd);
        } else {
            if (set.getRouteid() < 10) {
                comd = "0" + set.getRouteid();
            } else {
                comd = set.getRouteid() + "";
            }

            comd = BeiFenCtrlCmds.getStatusComd(comd);
        }

        this.logger.info("发送指令 = {}", comd);
        this.mOutputStream.write(ModBusUtils.hexStringToByteArray(comd));
        this.mOutputStream.flush();
    }

    private boolean isSocketColse() {
        try {
            this.socket.sendUrgentData(255);
            return false;
        } catch (IOException var2) {
            return true;
        }
    }

    private boolean reCodeVerification(String codestr) {
        String clientCrc = codestr.substring(codestr.length() - 6, codestr.length());
        clientCrc = clientCrc.replace(" ", "");
        String data = codestr.substring(0, codestr.length() - 6);
        String checkCrc = ModBusUtils.getCRC3(ModBusUtils.hexStringToByteArray(data));
        this.logger.info("客户端校验码 = {},计算校验码 = {},回码：{}", new Object[]{clientCrc, checkCrc, codestr});
        return checkCrc.equals(clientCrc);
    }
}
