package com.taojin.light.controller.sys;


import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

import com.taojin.framework.vo.ApiResult;
import com.taojin.light.framework.common.BaseController;
import com.taojin.light.dto.LoginUser;
import com.taojin.light.dto.jpadto.SysUserJPADTO;
import com.taojin.light.dto.jpadto.SysRoleJPADTO;
import com.taojin.light.entity.sys.*;
import com.taojin.light.framework.annotation.Log;
import com.taojin.light.service.sys.*;
import com.taojin.framework.enums.BusinessType;
import com.taojin.framework.utils.MF;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 角色信息
 *
 * @author sujg
 */
@RestController
@RequestMapping("/system/role")
/*@Api(tags = {"角色相关接口" ,"用户管理"},value = "角色相关接口" )*/
@Api(tags = "sys")
public class SysRoleController extends BaseController
{
    @Autowired
    private SysRoleService roleService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private SysUserService userService;

    @Autowired
    private SysDeptService deptService;

    @ApiOperation(value = "获取角色列表数据",notes = "返回数据为列表 需分页", tags = "角色相关接口")
    @PreAuthorize("@ss.hasPermi('system:role:list')")
    @GetMapping("/list")
    public ApiResult list(SysRoleJPADTO role)
    {
        Page<SysRoleJPADTO> list=roleService.selectRoleList(role,this.getPageable(role.getParams()));

        return ApiResult.OK(list);
    }

    @ApiOperation(value = "导出excel", tags = "角色相关接口")
    @Log(title = "角色管理", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('system:role:export')")
    @PostMapping("/export")
    public ModelAndView export(HttpServletResponse response, SysRoleJPADTO role)
    {
        return super.exportXls(roleService.selectRoleList(role), role, SysRole.class, "角色管理");
    }

    /**
     * 根据角色编号获取详细信息
     */
    @ApiOperation(value = "根据id获取用户详情",tags = "角色相关接口")
    @PreAuthorize("@ss.hasPermi('system:role:query')")
    @GetMapping(value = "/{roleId}")
    public ApiResult getInfo(@PathVariable Long roleId)
    {
        //roleService.checkRoleDataScope(roleId);
        return ApiResult.OK(roleService.selectRoleById(roleId));
    }

    /**
     * 新增角色
     */
    @ApiOperation(value = "新增角色", tags = "角色相关接口")
    @PreAuthorize("@ss.hasPermi('system:role:add')")
    @Log(title = "角色管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public ApiResult add(@Validated @RequestBody SysRoleJPADTO role)
    {
        if (!roleService.checkRoleNameUnique(role))
        {
            return ApiResult.error("新增角色'" + role.getRoleName() + "'失败，角色名称已存在");
        }
        else if (!roleService.checkRoleKeyUnique(role))
        {
            return ApiResult.error("新增角色'" + role.getRoleName() + "'失败，角色权限已存在");
        }
        //role.setCreateBy(this.getLoginUser().getUsername());
        return ApiResult.OK(roleService.insertRole(role));

    }

    /**
     * 修改保存角色
     */
    @ApiOperation(value = "修改保存角色", tags = "角色相关接口")
    @PreAuthorize("@ss.hasPermi('system:role:edit')")
    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public ApiResult edit(@Validated @RequestBody SysRoleJPADTO role)
    {
        roleService.checkRoleAllowed(role);
        roleService.checkRoleDataScope(role.getRoleId());
        if (!roleService.checkRoleNameUnique(role))
        {
            return ApiResult.error("修改角色'" + role.getRoleName() + "'失败，角色名称已存在");
        }
        else if (!roleService.checkRoleKeyUnique(role))
        {
            return ApiResult.error("修改角色'" + role.getRoleName() + "'失败，角色权限已存在");
        }
       // role.setUpdateBy(this.getLoginUser().getUsername());

        if (roleService.updateRole(role) > 0)
        {
            // 更新缓存用户权限
            LoginUser loginUser = getLoginUser();
            if (MF.isNotNull(loginUser.getUser()) && !loginUser.getUser().isAdmin())
            {
                loginUser.setPermissions(permissionService.getMenuPermission(loginUser.getUser()));
                SysUserJPADTO sysUserJPADTO = new SysUserJPADTO();
                sysUserJPADTO.setUserName(loginUser.getUser().getUserName());
                loginUser.setUser(userService.selectUserByUserName(sysUserJPADTO));
                tokenService.setLoginUser(loginUser);
            }
            return ApiResult.OK();
        }
        return ApiResult.error("修改角色'" + role.getRoleName() + "'失败，请联系管理员");
    }

    /**
     * 修改保存数据权限
     */
    @ApiOperation(value = "修改保存数据权限", tags = "角色相关接口")
    @PreAuthorize("@ss.hasPermi('system:role:edit')")
    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @PutMapping("/dataScope")
    public ApiResult dataScope(@RequestBody SysRoleJPADTO role)
    {
        roleService.checkRoleAllowed(role);
        roleService.checkRoleDataScope(role.getRoleId());
        return ApiResult.OK(roleService.authDataScope(role));
    }

    /**
     * 状态修改
     */
    @ApiOperation(value = "状态修改", tags = "角色相关接口")
    @PreAuthorize("@ss.hasPermi('system:role:edit')")
    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public ApiResult changeStatus(@RequestBody SysRoleJPADTO role)
    {
        roleService.checkRoleAllowed(role);
        roleService.checkRoleDataScope(role.getRoleId());
        //role.setUpdateBy(this.getLoginUser().getUsername());
        roleService.updateRoleStatus(role);
        return ApiResult.ok("更新成功");
    }

    /**
     * 删除角色
     */
    @ApiOperation(value = "删除角色", tags = "角色相关接口")
    @PreAuthorize("@ss.hasPermi('system:role:remove')")
    @Log(title = "角色管理", businessType = BusinessType.DELETE)
    @PostMapping("/del/{roleIds}")
    public ApiResult remove(@PathVariable String roleIds)
    {
        String[] stringArray = roleIds.split(","); // 将传递的字符串根据逗号分隔成字符串数组
        Long[] longArray = new Long[stringArray.length];

        for (int i = 0; i < stringArray.length; i++) {
            longArray[i] = Long.parseLong(stringArray[i]); // 将字符串数组中的每个元素转换为 Long 类型
        }

        roleService.deleteRoleByIds(longArray);
        return ApiResult.ok("删除成功");
    }

    /**
     * 获取角色选择框列表
     */
    @ApiOperation(value = "获取角色选择框列表", tags = "角色相关接口")
    @PreAuthorize("@ss.hasPermi('system:role:query')")
    @GetMapping("/optionselect")
    public ApiResult optionselect()
    {
        return ApiResult.ok(roleService.selectRoleAll());
    }

    /**
     * 查询已分配用户角色列表
     */
    @ApiOperation(value = "查询已分配用户角色列表", tags = "角色相关接口")
    @PreAuthorize("@ss.hasPermi('system:role:list')")
    @GetMapping("/authUser/allocatedList")
    public ApiResult allocatedList(SysUser user)
    {
        Page<SysUser> list=userService.selectAllocatedList(user,this.getPageable(user.getParams()));

        return ApiResult.ok(list);
    }

    /**
     * 查询未分配用户角色列表
     */
    @ApiOperation(value = "查询未分配用户角色列表", tags = "角色相关接口")
    @PreAuthorize("@ss.hasPermi('system:role:list')")
    @GetMapping("/authUser/unallocatedList")
    public ApiResult<Page<SysUser>> unallocatedList(SysUser user)
    {

        Page<SysUser> list = userService.selectUnallocatedList(user,this.getPageable(user.getParams()));
        return ApiResult.ok(list);
    }

    /**
     * 取消授权用户
     */
    @ApiOperation(value = "取消授权用户", tags = "角色相关接口")
    @PreAuthorize("@ss.hasPermi('system:role:edit')")
    @Log(title = "角色管理", businessType = BusinessType.GRANT)
    @PutMapping("/authUser/cancel")
    public ApiResult cancelAuthUser(@RequestBody SysUserRole userRole)
    {
        return ApiResult.ok(roleService.deleteAuthUser(userRole));
    }

    /**
     * 批量取消授权用户
     */
    @ApiOperation(value = "批量取消授权用户", tags = "角色相关接口")
    @PreAuthorize("@ss.hasPermi('system:role:edit')")
    @Log(title = "角色管理", businessType = BusinessType.GRANT)
    @PutMapping("/authUser/cancelAll")
    public ApiResult cancelAuthUserAll(Long roleId, Long[] userIds)
    {
        return ApiResult.ok(roleService.deleteAuthUsers(roleId, userIds));
    }

    /**
     * 批量选择用户授权
     */
    @ApiOperation(value = "批量选择用户授权", tags = "角色相关接口")
    @PreAuthorize("@ss.hasPermi('system:role:edit')")
    @Log(title = "角色管理", businessType = BusinessType.GRANT)
    @PutMapping("/authUser/selectAll")
    public ApiResult selectAuthUserAll(Long roleId, Long[] userIds)
    {
        roleService.checkRoleDataScope(roleId);
        return ApiResult.ok(roleService.insertAuthUsers(roleId, userIds));
    }

    /**
     * 获取对应角色部门树列表
     */
    @ApiOperation(value = "获取对应角色部门树列表", tags = "角色相关接口")
    @PreAuthorize("@ss.hasPermi('system:role:query')")
    @GetMapping(value = "/deptTree/{roleId}")
    public ApiResult deptTree(@PathVariable("roleId") Long roleId)
    {
        Map ajax = new HashMap<>();
        ajax.put("checkedKeys", deptService.selectDeptListByRoleId(roleId));
        ajax.put("depts", deptService.selectDeptTreeList(new SysDept()));
        return ApiResult.ok(ajax);
    }
}
