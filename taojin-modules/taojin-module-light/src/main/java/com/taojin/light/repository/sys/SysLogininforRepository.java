package com.taojin.light.repository.sys;

import com.taojin.light.entity.sys.SysConfig;
import com.taojin.light.entity.sys.SysLogininfor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

public interface SysLogininforRepository extends  JpaRepository<SysLogininfor, Long>, JpaSpecificationExecutor<SysLogininfor> {
    @Query(value=" truncate table sys_logininfor" ,nativeQuery = true)
    void cleanLogininfor();


}

