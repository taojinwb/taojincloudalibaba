package com.taojin.light.client.common;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class BeiFenCtrlCmds implements Serializable {
    private static final long serialVersionUID = 1L;
    public static String Switch_00_On = "03 06 00 02 00 01 E8 28";
    public static String Switch_00_Off = "03 06 00 01 00 00 D9 E8";
    public static String Switch_01_On = "03 06 00 03 00 01 B9 E8";
    public static String Switch_01_Off = "03 06 00 03 00 00 78 28";
    public static String Switch_02_On = "03 06 00 04 00 01 08 29";
    public static String Switch_02_Off = "03 06 00 04 00 00 C9 E9";
    public static String Switch_03_On = "03 06 00 05 00 01 59 E9";
    public static String Switch_03_Off = "03 06 00 05 00 00 98 29";
    public static String Switch_04_On = "03 06 00 06 00 01 A9 E9";
    public static String Switch_04_Off = "03 06 00 06 00 00 68 29";
    public static String Switch_05_On = "03 06 00 07 00 01 F8 29";
    public static String Switch_05_Off = "03 06 00 07 00 00 39 E9";
    public static String Switch_06_On = "03 06 00 08 00 01 C8 2A";
    public static String Switch_06_Off = "03 06 00 08 00 00 09 EA";
    public static String Switch_07_On = "03 06 00 09 00 01 99 EA";
    public static String Switch_07_Off = "03 06 00 09 00 00 58 2A";
    public static String Switch_08_On = "03 06 00 0A 00 01 69 EA";
    public static String Switch_08_Off = "03 06 00 0A 00 00 A8 2A";
    public static String Switch_09_On = "03 06 00 0B 00 01 38 2A";
    public static String Switch_09_Off = "03 06 00 0B 00 00 F9 EA";
    public static String Switch_10_On = "03 06 00 0C 00 01 89 EB";
    public static String Switch_10_Off = "03 06 00 0C 00 00 48 2B";
    public static String Switch_11_On = "03 06 00 0D 00 01 D8 2B";
    public static String Switch_11_Off = "03 06 00 0D 00 00 19 EB";
    public static String Switch_12_On = "03 06 00 0E 00 01 28 2B";
    public static String Switch_12_Off = "03 06 00 0E 00 00 E9 EB";
    public static final Map<String, String> comdMap = new HashMap<String, String>() {
        {
            this.put("Switch_00_On", "03 06 00 02 00 01 E8 28");
            this.put("Switch_00_Off", "03 06 00 01 00 00 D9 E8");
            this.put("Switch_01_On", "03 06 00 03 00 01 B9 E8");
            this.put("Switch_01_Off", "03 06 00 03 00 00 78 28");
            this.put("Switch_02_On", "03 06 00 04 00 01 08 29");
            this.put("Switch_02_Off", "03 06 00 04 00 00 C9 E9");
            this.put("Switch_03_On", "03 06 00 05 00 01 59 E9");
            this.put("Switch_03_Off", "03 06 00 05 00 00 98 29");
            this.put("Switch_04_On", "03 06 00 06 00 01 A9 E9");
            this.put("Switch_04_Off", "03 06 00 06 00 00 68 29");
            this.put("Switch_05_On", "03 06 00 07 00 01 F8 29");
            this.put("Switch_05_Off", "03 06 00 07 00 00 39 E9");
            this.put("Switch_06_On", "03 06 00 08 00 01 C8 2A");
            this.put("Switch_06_Off", "03 06 00 08 00 00 09 EA");
            this.put("Switch_07_On", "03 06 00 09 00 01 99 EA");
            this.put("Switch_07_Off", "03 06 00 09 00 00 58 2A");
            this.put("Switch_08_On", "03 06 00 0A 00 01 69 EA");
            this.put("Switch_08_Off", "03 06 00 0A 00 00 A8 2A");
            this.put("Switch_09_On", "03 06 00 0B 00 01 38 2A");
            this.put("Switch_09_Off", "03 06 00 0B 00 00 F9 EA");
            this.put("Switch_10_On", "03 06 00 0C 00 01 89 EB");
            this.put("Switch_10_Off", "03 06 00 0C 00 00 48 2B");
            this.put("Switch_11_On", "03 06 00 0D 00 01 D8 2B");
            this.put("Switch_11_Off", "03 06 00 0D 00 00 19 EB");
            this.put("Switch_12_On", "03 06 00 0E 00 01 28 2B");
            this.put("Switch_12_Off", "03 06 00 0E 00 00 E9 EB");
        }
    };

    public BeiFenCtrlCmds() {
    }

    public static String getStatusComd(String loopCode) {
        String codePre = "03 03 00 ";
        String codeEnd = " 00 01";
        String codeComd = "";
        byte var5 = -1;
        switch(loopCode.hashCode()) {
            case 1536:
                if (loopCode.equals("00")) {
                    var5 = 0;
                }
                break;
            case 1537:
                if (loopCode.equals("01")) {
                    var5 = 1;
                }
                break;
            case 1538:
                if (loopCode.equals("02")) {
                    var5 = 2;
                }
                break;
            case 1539:
                if (loopCode.equals("03")) {
                    var5 = 3;
                }
                break;
            case 1540:
                if (loopCode.equals("04")) {
                    var5 = 4;
                }
                break;
            case 1541:
                if (loopCode.equals("05")) {
                    var5 = 5;
                }
                break;
            case 1542:
                if (loopCode.equals("06")) {
                    var5 = 6;
                }
                break;
            case 1543:
                if (loopCode.equals("07")) {
                    var5 = 7;
                }
                break;
            case 1544:
                if (loopCode.equals("08")) {
                    var5 = 8;
                }
                break;
            case 1545:
                if (loopCode.equals("09")) {
                    var5 = 9;
                }
            case 1546:
            case 1547:
            case 1548:
            case 1549:
            case 1550:
            case 1551:
            case 1552:
            case 1553:
            case 1554:
            case 1555:
            case 1556:
            case 1557:
            case 1558:
            case 1559:
            case 1560:
            case 1561:
            case 1562:
            case 1563:
            case 1564:
            case 1565:
            case 1566:
            default:
                break;
            case 1567:
                if (loopCode.equals("10")) {
                    var5 = 10;
                }
                break;
            case 1568:
                if (loopCode.equals("11")) {
                    var5 = 11;
                }
                break;
            case 1569:
                if (loopCode.equals("12")) {
                    var5 = 12;
                }
        }

        switch(var5) {
            case 0:
                codeComd = "03 03 00 03 00 0C";
                break;
            case 1:
                codeComd = codePre + "03" + codeEnd;
                break;
            case 2:
                codeComd = codePre + "04" + codeEnd;
                break;
            case 3:
                codeComd = codePre + "05" + codeEnd;
                break;
            case 4:
                codeComd = codePre + "06" + codeEnd;
                break;
            case 5:
                codeComd = codePre + "07" + codeEnd;
                break;
            case 6:
                codeComd = codePre + "08" + codeEnd;
                break;
            case 7:
                codeComd = codePre + "09" + codeEnd;
                break;
            case 8:
                codeComd = codePre + "0A" + codeEnd;
                break;
            case 9:
                codeComd = codePre + "0B" + codeEnd;
                break;
            case 10:
                codeComd = codePre + "0C" + codeEnd;
                break;
            case 11:
                codeComd = codePre + "0D" + codeEnd;
                break;
            case 12:
                codeComd = codePre + "0E" + codeEnd;
        }

        if (null != codeComd) {
            String crc = ModBusUtils.getCRC3(ModBusUtils.hexStringToByteArray(codeComd));
            codeComd = codeComd + " " + crc;
        }

        return codeComd;
    }

    public static String getComdForloopNoId(int loopNoId, int type) {
        String comd = null;
        System.out.println("获取命令 loopNoId = " + loopNoId + " type = " + type);
        if (loopNoId >= 0 && loopNoId <= 12 && (type == 0 || type == 1)) {
            System.out.println("获取命令");
            if (loopNoId < 10) {
                comd = "0" + loopNoId;
            } else {
                comd = loopNoId + "";
            }

            if (type == 0) {
                comd = "Switch_" + comd + "_Off";
            } else if (type == 1) {
                comd = "Switch_" + comd + "_On";
            }

            System.out.println("命令值：" + comd);
            comd = (String)comdMap.get(comd);
        }

        return comd;
    }
}
