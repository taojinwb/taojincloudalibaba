package com.taojin.light.service.sys;

import java.util.Arrays;
import java.util.List;

import com.taojin.light.framework.common.BaseService;
import com.taojin.light.entity.sys.SysLogininfor;
import com.taojin.light.repository.sys.SysLogininforRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
/**
 * 系统访问日志情况信息 服务层处理
 * 
 * @author sujg
 */
@Service
public class SysLogininforService extends BaseService<SysLogininfor, Long> {

@Autowired
private SysLogininforRepository sysLogininforRepository;


        public SysLogininforService(SysLogininforRepository repository) {
                super(repository,repository);
        }

        /**
         * 新增系统登录日志
         *
         * @param logininfor 访问日志对象
         */
        public void insertLogininfor(SysLogininfor logininfor)
        {
                sysLogininforRepository.save(logininfor);
        }

        /**
         * 查询系统登录日志集合
         *
         * @param logininfor 访问日志对象
         * @return 登录记录集合
         */
        public Page<SysLogininfor> selectLogininforList(SysLogininfor logininfor, Pageable pageable)
        {
                return sysLogininforRepository.findAll(filterByFields(logininfor),pageable);
        }
        public List<SysLogininfor> selectLogininforList(SysLogininfor logininfor)
        {
                return sysLogininforRepository.findAll(filterByFields(logininfor));
        }

        /**
         * 批量删除系统登录日志
         *
         * @param infoIds 需要删除的登录日志ID
         * @return 结果
         */
        public void deleteLogininforByIds(Long[] infoIds)
        {

                Iterable<Long> iterable = Arrays.asList(infoIds);
                 sysLogininforRepository.deleteAllByIdInBatch(iterable);
        }

        /**
         * 清空系统登录日志
         */
        public void cleanLogininfor()
        {
                sysLogininforRepository.cleanLogininfor();
        }

}
