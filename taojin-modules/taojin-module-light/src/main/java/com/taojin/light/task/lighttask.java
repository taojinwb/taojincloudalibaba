package com.taojin.light.task;

import cn.hutool.core.date.DateUtil;
import com.taojin.framework.redis.RedisUtil;
import com.taojin.light.dto.AtSetDTO;
import com.taojin.light.dto.jpadto.OrderLightJPADTO;
import com.taojin.light.framework.common.BaseController;
import com.taojin.light.repository.device.ViewOrderLightRepository;
import com.taojin.light.constant.LigthConstant;
import com.taojin.light.service.device.LightService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.*;
@Slf4j
@Component
public class lighttask extends BaseController {
    @Autowired
    ViewOrderLightRepository viewOrderLightRepository;
    @Autowired
     LightService lightSerice;
    @Autowired
    RedisUtil redisUtil;

    /**
     * 取出60分钟以后的，需要控制的场地，存入缓存
     * @param param
     */
   // @Scheduled(cron = "0/10 * * * * ?")
    @XxlJob("order_light")
    public ReturnT<String> order_light(String param){
        try {
            String pzvalue = this.getConfig("order.light");
            //逗号分开，设备id。需要根据订单控制的ID。
            //115,225  115这个设备，统一设置为提前5分钟开关。
            Map<String, List<OrderLightJPADTO>> cacheMap = new HashMap<>();
            Map<String, List<OrderLightJPADTO>> close_cacheMap = new HashMap<>();
            if (pzvalue != "") {
                List<OrderLightJPADTO> ViewOrderLights = viewOrderLightRepository.selectallbydetailids((pzvalue));
              //  for (ViewOrderLight ldt : ViewOrderLights) {
                for(int i=0;i<ViewOrderLights.size();i++){
                    OrderLightJPADTO ldt = new OrderLightJPADTO();
                    ldt = (OrderLightJPADTO)ViewOrderLights.get(i);
                    String starttime = DateUtil.format(ldt.getStartTime(), "yyyyMMddHHmm");
                    String endtime = DateUtil.format(ldt.getEndTime(), "yyyyMMddHHmm");
                    if (cacheMap.containsKey(starttime)) {
                        // 如果存在，将当前oid与缓存中的oid拼接，用#号分隔
                        List<OrderLightJPADTO> list =  cacheMap.get(starttime);
                        list.add(ldt);
                        cacheMap.put(starttime, list);
                    } else {
                        // 如果不存在，直接将当前oid放入缓存
                        List<OrderLightJPADTO> list = new ArrayList<>();
                        list.add(ldt);
                        cacheMap.put(starttime, list);
                    }
                    if (close_cacheMap.containsKey(endtime)) {
                        // 如果存在，将当前oid与缓存中的oid拼接，用#号分隔
                        List<OrderLightJPADTO> list = close_cacheMap.get(endtime);
                        list.add(ldt);
                        close_cacheMap.put(endtime, list);
                    } else {
                        // 如果不存在，直接将当前oid放入缓存
                        List<OrderLightJPADTO> list = new ArrayList<>();
                        list.add(ldt);
                        close_cacheMap.put(endtime, list);
                    }
                }


                // 遍历 cacheMap，将值存入 Redis
                for (Map.Entry<String, List<OrderLightJPADTO>> entry : cacheMap.entrySet()) {
                   // redisUtil.del(LigthConstant.token_light_open+entry.getKey());
                    redisUtil.set(LigthConstant.token_light_open + entry.getKey(), entry.getValue(),LigthConstant.token_light_time);
                }
                // 遍历 cacheMap，将值存入 Redis
                for (Map.Entry<String, List<OrderLightJPADTO>> entry : close_cacheMap.entrySet()) {
                    //redisUtil.del(LigthConstant.token_light_close+entry.getKey());
                    redisUtil.set(LigthConstant.token_light_close + entry.getKey(), entry.getValue(),LigthConstant.token_light_time);
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }
      return  ReturnT.SUCCESS;
    }

    /***
     * 开灯 ,五分钟内开始的订单，开灯
     * @param param
     * @return
     */
    @XxlJob("open_light")
    public ReturnT<String> open_light(String param){//应该取loop id
            String now_date = DateUtil.format(DateUtil.offsetMinute(DateUtil.date(), 5), "yyyyMMddHHmm");
            List<OrderLightJPADTO> viewOrderLights = redisUtil.get(LigthConstant.token_light_open + now_date);
            if(viewOrderLights!=null) {
                for (OrderLightJPADTO vol : viewOrderLights) {
                    AtSetDTO set = new AtSetDTO();
                    set.setDeviceId(vol.getDeviceId() + "");
                    set.setExeaction("1");
                    set.setRouteid(vol.getLoopNumber());
                    set.setSocketIp("127.0.0.1");
                    this.lightSerice.findDeviceSocket(set);
                    log.info("设备{},路由{},根据订单{}，开启灯控，订单开始时间{}", vol.getDetailid(), vol.getLoopNumber(), vol.getOid(), vol.getStartTime());
                }
                //删除开灯缓存
                redisUtil.del(LigthConstant.token_light_open + now_date);
            }
        return  ReturnT.SUCCESS;

    }

    /***
     *关灯 延迟5分钟关闭
     * @param param
     * @return
     */
    @XxlJob("close_light")
    public ReturnT<String> close_light(String param){//应该取loop id
        String now_date = DateUtil.format( DateUtil.offsetMinute(DateUtil.date(),-5),"yyyyMMddHHmm");


        List<OrderLightJPADTO> viewOrderLights =  redisUtil.get(LigthConstant.token_light_close+now_date);
         if(viewOrderLights!=null) {
                for (OrderLightJPADTO vol : viewOrderLights) {
                    int  ord_num = this.viewOrderLightRepository.getCount(DateUtil.format(DateUtil.offsetMinute(DateUtil.date(), -6),"yyyy-MM-dd HH:mm:ss"), DateUtil.format(DateUtil.offsetMinute(DateUtil.date(), 1),"yyyy-MM-dd HH:mm:ss"), vol.getId());
                    //判断当前时间到延后5分钟内，该场地是否有新的订单，有则会开灯，不应该在做关灯。
                    log.info(ord_num + "===========");
                    if (ord_num <= 0) {
                        AtSetDTO set = new AtSetDTO();
                        set.setDeviceId(vol.getDeviceId() + "");
                        set.setExeaction("0");
                        set.setRouteid(vol.getLoopNumber());
                        set.setSocketIp("127.0.0.1");
                        this.lightSerice.findDeviceSocket(set);
                        log.info("设备{},路由{},根据订单{}，关闭灯控，订单结束时间{}", vol.getDetailid(), vol.getLoopNumber(), vol.getOid(), vol.getEndTime());
                    }
                }
                //删除开灯缓存
                redisUtil.del(LigthConstant.token_light_close + now_date);
            }
        return  ReturnT.SUCCESS;
    }

    public static void main(String[] str){
        System.out.println( DateUtil.format( DateUtil.offsetMinute(DateUtil.date(),1),"yyyyMMddHHmm"));
    }
}
