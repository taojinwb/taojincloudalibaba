package com.taojin.light.controller.sys;


import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.taojin.framework.vo.ApiResult;
import com.taojin.light.framework.common.BaseController;
import com.taojin.light.entity.sys.SysPost;
import com.taojin.light.framework.annotation.Log;
import com.taojin.light.service.sys.SysPostService;
import com.taojin.framework.enums.BusinessType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * 岗位信息操作处理
 *
 * @author sujg
 */
@RestController
@RequestMapping("/system/post")
/*@Api(tags = "岗位信息操作处理相关接口" ,value = "岗位信息操作处理相关接口" )*/
@Api(tags = "sys")
public class SysPostController extends BaseController
{
    @Autowired
    private SysPostService postService;

    /**
     * 获取岗位列表
     */
    @ApiOperation(value = "获取岗位列表", tags = "岗位信息操作处理相关接口")
    @PreAuthorize("@ss.hasPermi('system:post:list')")
    @GetMapping("/list")
    public ApiResult list(SysPost post)
    {

        return ApiResult.ok(postService.selectPostList(post));
    }

    @ApiOperation(value = "导出excel", tags = "岗位信息操作处理相关接口")
    @Log(title = "岗位管理", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('system:post:export')")
    @PostMapping("/export")
    public ModelAndView export(HttpServletResponse response, SysPost post)
    {
        return super.exportXls(postService.selectPostList(post), post, SysPost.class, "岗位管理");
    }

    /**
     * 根据岗位编号获取详细信息
     */
    @ApiOperation(value = "根据岗位编号获取详细信息", tags = "岗位信息操作处理相关接口")
    @PreAuthorize("@ss.hasPermi('system:post:query')")
    @GetMapping(value = "/getInfo/{postId}")
    public ApiResult getInfo(@PathVariable Long postId)
    {
        return ApiResult.ok(postService.selectPostById(postId));
    }

    /**
     * 新增岗位
     */
    @ApiOperation(value = "新增岗位", tags = "岗位信息操作处理相关接口")
    @PreAuthorize("@ss.hasPermi('system:post:add')")
    @Log(title = "岗位管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public ApiResult add(@Validated @RequestBody SysPost post)
    {
        if (!postService.checkPostNameUnique(post))
        {
            return ApiResult.error("新增岗位'" + post.getPostName() + "'失败，岗位名称已存在");
        }
        else if (!postService.checkPostCodeUnique(post))
        {
            return ApiResult.error("新增岗位'" + post.getPostName() + "'失败，岗位编码已存在");
        }
        //post.setCreateBy(this.getLoginUser().getUsername());
        postService.insertPost(post);
        return ApiResult.ok("添加成功");
    }

    /**
     * 修改岗位
     */
    @ApiOperation(value = "修改岗位", tags = "岗位信息操作处理相关接口")
    @PreAuthorize("@ss.hasPermi('system:post:edit')")
    @Log(title = "岗位管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public ApiResult edit(@Validated @RequestBody SysPost post)
    {
        if (!postService.checkPostNameUnique(post))
        {
            return ApiResult.error("修改岗位'" + post.getPostName() + "'失败，岗位名称已存在");
        }
        else if (!postService.checkPostCodeUnique(post))
        {
            return ApiResult.error("修改岗位'" + post.getPostName() + "'失败，岗位编码已存在");
        }
       // post.setUpdateBy(this.getLoginUser().getUsername());
        postService.updatePost(post);
        return ApiResult.ok("修改成功");
    }

    /**
     * 删除岗位
     */
    @ApiOperation(value = "删除岗位", tags = "岗位信息操作处理相关接口")
    @PreAuthorize("@ss.hasPermi('system:post:remove')")
    @Log(title = "岗位管理", businessType = BusinessType.DELETE)
    @PostMapping("/del/{postIds}")
    public ApiResult remove(@PathVariable Long[] postIds)
    {
        postService.deletePostByIds(postIds);
        return ApiResult.ok("删除成功");
    }

    /**
     * 获取岗位选择框列表
     */
    @ApiOperation(value = "获取岗位选择框列表", tags = "岗位信息操作处理相关接口")
    @GetMapping("/optionselect")
    public ApiResult optionselect()
    {
        List<SysPost> posts = postService.selectPostAll();
        return ApiResult.ok(posts);
    }
}
