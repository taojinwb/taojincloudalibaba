package com.taojin.light.controller.sys;


import java.util.List;

import com.taojin.framework.vo.ApiResult;
import com.taojin.light.framework.common.BaseController;
import com.taojin.light.dto.sys.SysDeptDTO;
import com.taojin.light.entity.sys.SysDept;
import com.taojin.light.framework.annotation.Log;
import com.taojin.light.service.sys.SysDeptService;
import com.taojin.framework.constant.UserConstants;
import com.taojin.framework.enums.BusinessType;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 部门信息
 *
 * @author sujg
 */
@RestController
@RequestMapping("/system/dept")
@Tag(name = "系统管理")
public class SysDeptController extends BaseController
{
    @Autowired
    private SysDeptService deptService;

    /**
     * 获取部门列表
     */
    @Operation(summary = "获取部门列表", description = "部门信息相关接口",tags = "系统管理")
    @PreAuthorize("@ss.hasPermi('system:dept:list')")
    @GetMapping("/list")
    public ApiResult list(SysDept dept)
    {
        List<SysDeptDTO> depts = deptService.selectDeptList(dept);
        return ApiResult.ok(depts);
    }

    /**
     * 查询部门列表（排除节点）
     */
    @ApiOperation(value = "查询部门列表（排除节点）",tags = "部门信息相关接口")
    @PreAuthorize("@ss.hasPermi('system:dept:list')")
    @GetMapping("/list/exclude/{deptId}")
    public ApiResult excludeChild(@PathVariable(value = "deptId", required = false) Long deptId)
    {
        List<SysDeptDTO> depts = deptService.selectDeptList(new SysDept());
        depts.removeIf(d -> d.getDeptId().intValue() == deptId || ArrayUtils.contains(StringUtils.split(d.getAncestors(), ","), deptId + ""));
        return ApiResult.ok(depts);
    }

    /**
     * 根据部门编号获取详细信息
     */
    @ApiOperation(value = "根据部门编号获取详细信息",tags = "部门信息相关接口")
    @PreAuthorize("@ss.hasPermi('system:dept:query')")
    @GetMapping(value = "/{deptId}")
    public ApiResult getInfo(@PathVariable Long deptId)
    {
        deptService.checkDeptDataScope(deptId);
        return ApiResult.ok(deptService.selectDeptById(deptId));
    }

    /**
     * 新增部门
     */
    @ApiOperation(value = "新增部门",tags = "部门信息相关接口")
    @PreAuthorize("@ss.hasPermi('system:dept:add')")
    @Log(title = "部门管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public ApiResult add(@Validated @RequestBody SysDept dept)
    {
        if (!deptService.checkDeptNameUnique(dept))
        {
            return ApiResult.error("新增部门'" + dept.getDeptName() + "'失败，部门名称已存在");
        }
        SysDept sd = new SysDept();
        BeanUtils.copyProperties(dept,sd);
       // sd.setCreateBy(this.getLoginUser().getUsername());
        deptService.insertDept(sd);
        return ApiResult.ok("添加成功");
    }

    /**
     * 修改部门
     */
    @ApiOperation(value = "修改部门",tags = "部门信息相关接口")
    @PreAuthorize("@ss.hasPermi('system:dept:edit')")
    @Log(title = "部门管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public ApiResult edit(@Validated @RequestBody SysDept dept)
    {
        Long deptId = dept.getDeptId();
       // deptService.checkDeptDataScope(deptId);
        if (!deptService.checkDeptNameUnique(dept))
        {
            return ApiResult.error("修改部门'" + dept.getDeptName() + "'失败，部门名称已存在");
        }
        else if (dept.getParentId().equals(deptId))
        {
            return ApiResult.error("修改部门'" + dept.getDeptName() + "'失败，上级部门不能是自己");
        }
        else if (StringUtils.equals(UserConstants.DEPT_DISABLE, dept.getStatus()) && deptService.selectNormalChildrenDeptById(deptId) > 0)
        {
            return ApiResult.error("该部门包含未停用的子部门！");
        }

        //dept.setUpdateBy(this.getLoginUser().getUsername());
        deptService.updateDept(dept);
        return ApiResult.ok("更新成功");
    }

    /**
     * 删除部门
     */
    @ApiOperation(value = "删除部门",tags = "部门信息相关接口")
    @PreAuthorize("@ss.hasPermi('system:dept:remove')")
    @Log(title = "部门管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{deptId}")
    public ApiResult remove(@PathVariable Long deptId)
    {
        if (deptService.hasChildByDeptId(deptId))
        {
            return ApiResult.error("存在下级部门,不允许删除");
        }
        if (deptService.checkDeptExistUser(deptId))
        {
            return ApiResult.error("部门存在用户,不允许删除");
        }
      //  deptService.checkDeptDataScope(deptId);
        deptService.deleteDeptById(deptId);
        return ApiResult.ok("删除成功！");
    }
}
