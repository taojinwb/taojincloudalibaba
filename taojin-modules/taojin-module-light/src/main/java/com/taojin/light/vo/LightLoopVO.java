package com.taojin.light.vo;

public class LightLoopVO {
    private Long id;
    private Long deviceId;
    private Integer loopNumber;
    private Integer status;
    private String venueTitle;

    public LightLoopVO() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDeviceId() {
        return this.deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getLoopNumber() {
        return this.loopNumber;
    }

    public void setLoopNumber(Integer loopNumber) {
        this.loopNumber = loopNumber;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getVenueTitle() {
        return this.venueTitle;
    }

    public void setVenueTitle(String venueTitle) {
        if (venueTitle == null) {
            this.venueTitle = "";
        } else {
            this.venueTitle = venueTitle;
        }

    }
}
