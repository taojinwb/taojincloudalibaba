package com.taojin.light.controller.device;

import com.taojin.light.client.bean.ResultBean;
import com.taojin.light.dto.AtSetDTO;
import com.taojin.light.service.device.LightService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Api(
        value = "API - LightController",
        tags = {"灯光控制接口"}
)
@RestController
@RequestMapping({"/light/ctl"})
public class LightController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private LightService lightSerice;

    public LightController() {
    }

    @ApiOperation(
            value = "设备指令执行接口",
            notes = "",
            response = ResultBean.class
    )
    @RequestMapping(
            value = {"/comd"},
            method = {RequestMethod.POST}
    )
    public ResultBean apply(@Validated @RequestBody AtSetDTO set, BindingResult result, HttpServletRequest request) {
        this.logger.info( request.getHeader("token")+"==comd = {}", set.toString());
        String token = request.getHeader("token");
        ResultBean resultBean = this.lightSerice.findDeviceSocket(set, token);
        return resultBean;
    }
}
