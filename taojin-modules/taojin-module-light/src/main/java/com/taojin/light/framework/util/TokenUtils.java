package com.taojin.light.framework.util;


import com.taojin.light.framework.constant.CacheConstant;
import com.taojin.framework.utils.MF;
import org.apache.commons.lang3.StringUtils;
import com.taojin.framework.constant.FrameworkConstant;
import com.taojin.framework.constant.TenantConstant;
import com.taojin.framework.exception.BaseException;
import com.taojin.framework.redis.RedisUtil;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author sujg
 * @Date 2019/9/23 14:12
 * @Description: 编程校验token有效性
 */
@Slf4j
public class TokenUtils {



    /**
     * 获取 request 里传递的 token
     *
     * @param request
     * @return
     */
    public static String getTokenByRequest(HttpServletRequest request) {
        String token = request.getParameter("token");
        if (token == null) {
            token = request.getHeader("X-Access-Token");
        }
        return token;
    }

    /**
     * 获取 request 里传递的 tenantId (租户ID)
     *
     * @param request
     * @return
     */
    public static String getTenantIdByRequest(HttpServletRequest request) {
        String tenantId = request.getParameter(TenantConstant.TENANT_ID);
        if (tenantId == null) {
            tenantId = MF.getString(request.getHeader(FrameworkConstant.TENANT_ID));
        }
        return tenantId;
    }

    /**
     * 获取 request 里传递的 lowAppId (低代码应用ID)
     *
     * @param request
     * @return
     */
    public static String getLowAppIdByRequest(HttpServletRequest request) {
        String lowAppId = request.getParameter(TenantConstant.FIELD_LOW_APP_ID);
        if (lowAppId == null) {
            lowAppId = MF.getString(request.getHeader(TenantConstant.X_LOW_APP_ID));
        }
        return lowAppId;
    }

    /**
     * 验证Token
     */
    public static boolean verifyToken(HttpServletRequest request, RedisUtil redisUtil) {
        
        String token = getTokenByRequest(request);
        return TokenUtils.verifyToken(token, redisUtil);
    }

    /**
     * 验证Token
     */
    public static boolean verifyToken(String token,  RedisUtil redisUtil) {
        if (StringUtils.isBlank(token)) {
            throw new BaseException("token不能为空!");
        }
        // 解密获得username，用于和数据库进行对比
        String username = JwtUtil.getUsername(token);
        log.info(username+"-------------");
        if (username == null) {
            throw new BaseException("token非法无效!");
        }
//        LoginUser user = commonApi.getUserByName(username);
//        if (user == null) {
//            throw new BaseException("用户不存在!");
//        }
//        // 判断用户状态
//        if (user.getStatus() != 1) {
//            throw new BaseException("账号已被锁定,请联系管理员!");
//        }
        // 校验token成功后，延长有效时间,每次都
        redisUtil.expire(CacheConstant.LOGIN_TOKEN_KEY + token, JwtUtil.EXPIRE_TIME * 2 / 1000);
        return true;
    }

    /**
     * 刷新token（保证用户在线操作不掉线）
     * @param token
     * @param userName
     * @param passWord
     * @param redisUtil
     * @return
     */
    private static void jwtTokenRefresh(String token, String userName, String passWord, RedisUtil redisUtil) {
            // 校验token有效性
            if (JwtUtil.verify(token, userName, passWord)) {
                // 设置Toekn缓存有效时间
                redisUtil.expire(CacheConstant.LOGIN_TOKEN_KEY + token, JwtUtil.EXPIRE_TIME * 2 / 1000);
            }
    }

    /**
     * 获取登录用户，各模块用户信息可能不一致，个模块自行实现
     *
     * @param commonApi
     * @param username
     * @return

    public static LoginUser getLoginUser(String username, CommonAPI commonApi, RedisUtil redisUtil) {
        LoginUser loginUser = null;
        String loginUserKey = CacheConstant.SYS_USERS_CACHE + "::" + username;
        //【重要】此处通过redis原生获取缓存用户，是为了解决微服务下system服务挂了，其他服务互调不通问题---
        if (redisUtil.hasKey(loginUserKey)) {
            try {
                loginUser = (LoginUser) redisUtil.get(loginUserKey);
                //解密用户
                SensitiveInfoUtil.handlerObject(loginUser, false);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else {
            // 查询用户信息
            loginUser = commonApi.getUserByName(username);
        }
        return loginUser;
    } */
}
