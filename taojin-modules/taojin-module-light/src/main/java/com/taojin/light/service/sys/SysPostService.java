package com.taojin.light.service.sys;

import com.taojin.light.framework.common.BaseService;
import com.taojin.light.entity.sys.SysPost;
import com.taojin.light.repository.sys.SysPostRepository;
import com.taojin.light.repository.sys.SysUserPostRepository;
import com.taojin.light.repository.sys.SysUserRepository;
import com.taojin.framework.constant.UserConstants;
import com.taojin.framework.exception.ServiceException;
import com.taojin.framework.utils.MF;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * 岗位信息 服务层处理
 * 
 * @author sujg
 */
@Service
public class SysPostService extends BaseService<SysPost,Long>
{
    @Autowired
    private SysPostRepository sysPostRepository;

    @Autowired
    private SysUserRepository sysUserRepository;

    @Autowired
    private SysUserPostRepository sysUserPostRepository;

    public SysPostService(SysPostRepository repository) {
        super(repository,repository);
    }

    /**
     * 查询岗位信息集合
     *
     * @param post 岗位信息
     * @return 岗位信息集合
     */
    public List<SysPost> selectPostList(SysPost post)
    {
        return sysPostRepository.findAll(this.filterByFields(post));
    }

    /**
     * 查询所有岗位
     *
     * @return 岗位列表
     */
    public List<SysPost> selectPostAll()
    {
        return sysPostRepository.findAll();
    }

    /**
     * 通过岗位ID查询岗位信息
     *
     * @param postId 岗位ID
     * @return 角色对象信息
     */
    public SysPost selectPostById(Long postId)
    {
        return sysPostRepository.findById(postId).orElse(null);
    }

    /**
     * 根据用户ID获取岗位选择框列表
     *
     * @param userId 用户ID
     * @return 选中岗位ID列表
     */
    public List<Long> selectPostListByUserId(Long userId)
    {
        return sysPostRepository.selectPostListByUserId(userId);
    }

    /**
     * 校验岗位名称是否唯一
     *
     * @param post 岗位信息
     * @return 结果
     */
    public boolean checkPostNameUnique(SysPost post)
    {
        Long postId = MF.isNull(post.getPostId()) ? -1L : post.getPostId();
        SysPost info = sysPostRepository.findFirstByPostName(post.getPostName());
        if (MF.isNotNull(info) && info.getPostId().longValue() != postId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验岗位编码是否唯一
     *
     * @param post 岗位信息
     * @return 结果
     */
    public boolean checkPostCodeUnique(SysPost post)
    {
        Long postId = MF.isNull(post.getPostId()) ? -1L : post.getPostId();
        SysPost info = sysPostRepository.findFirstByPostCode(post.getPostCode());
        if (MF.isNotNull(info) && info.getPostId().longValue() != postId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 通过岗位ID查询岗位使用数量
     *
     * @param postId 岗位ID
     * @return 结果
     */
    public int countUserPostById(Long postId)
    {
        return sysUserPostRepository.countAllByPostId(postId);
    }

    /**
     * 删除岗位信息
     *
     * @param postId 岗位ID
     * @return 结果
     */
    public void deletePostById(Long postId)
    {
         sysPostRepository.deleteById(postId);
    }

    /**
     * 批量删除岗位信息
     *
     * @param postIds 需要删除的岗位ID
     * @return 结果
     */
    public void deletePostByIds(Long[] postIds)
    {
        // 使用 Arrays.asList() 将 Long 数组转换为 List
        List<Long> postIdList = Arrays.asList(postIds);

        // 将 List 转换为 Iterable<? extends ID>
        Iterable<? extends Long> iterableIds = postIdList;
        for (Long postId : postIds)
        {
            SysPost post = selectPostById(postId);
            if (countUserPostById(postId) > 0)
            {
                throw new ServiceException(String.format("%1$s已分配,不能删除", post.getPostName()));
            }
        }
         sysPostRepository.deleteAllById(iterableIds);
    }

    /**
     * 新增保存岗位信息
     *
     * @param post 岗位信息
     * @return 结果
     */
    public void insertPost(SysPost post)
    {
         sysPostRepository.save(post);
    }

    /**
     * 修改保存岗位信息
     *
     * @param post 岗位信息
     * @return 结果
     */
    public void updatePost(SysPost post)
    {
        sysPostRepository.save(post);
    }
}
