package com.taojin.light.client.common;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Supplier;
import org.springframework.beans.BeanUtils;

public class BeanConvertUtil extends BeanUtils {
    public BeanConvertUtil() {
    }

    public static <S, T> T converTo(S source, Supplier<T> target) {
        return converTo(source, target, (MyConverCallBack<S, T>) null);
    }

    public static <S, T> T converTo(S source, Supplier<T> targetSupplier, BeanConvertUtil.MyConverCallBack<S, T> callBack) {
        if (source != null && targetSupplier != null) {
            T target = targetSupplier.get();
            copyProperties(source, target);
            if (callBack != null) {
                callBack.callBack(source, target);
            }

            return target;
        } else {
            return null;
        }
    }

    public static <S, T> List<T> converListTo(List<S> source, Supplier<T> targetSupplier) {
        return converListTo(source, targetSupplier, (BeanConvertUtil.MyConverCallBack)null);
    }

    public static <S, T> List<T> converListTo(List<S> sources, Supplier<T> targetSupplier, BeanConvertUtil.MyConverCallBack<S, T> callBack) {
        if (sources != null && targetSupplier != null) {
            List<T> list = new ArrayList(sources.size());

            Object target;
            for(Iterator var4 = sources.iterator(); var4.hasNext(); list.add((T) target)) {
                S source = (S) var4.next();
                target = targetSupplier.get();
                copyProperties(source, target);
                if (callBack != null) {
                    callBack.callBack(source, (T) target);
                }
            }

            return list;
        } else {
            return null;
        }
    }

    @FunctionalInterface
    public interface MyConverCallBack<S, T> {
        void callBack(S t, T s);
    }
}
