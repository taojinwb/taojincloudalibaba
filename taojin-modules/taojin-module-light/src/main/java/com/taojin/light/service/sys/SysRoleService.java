package com.taojin.light.service.sys;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.taojin.light.framework.common.BaseService;
import com.taojin.light.entity.sys.*;
import com.taojin.light.framework.security.SecurityUtils;
import com.taojin.light.repository.sys.*;
import com.taojin.light.dto.jpadto.SysRoleJPADTO;
import com.taojin.light.dto.jpadto.SysUserJPADTO;
import com.taojin.framework.utils.MF;
import com.taojin.framework.utils.SpringContextUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.taojin.framework.constant.UserConstants;
import com.taojin.framework.exception.ServiceException;

/**
 * 角色 业务层处理
 * 
 * @author sujg
 */
@Service
@Slf4j
public class SysRoleService extends BaseService<SysRole,Long>
{
    @Autowired
    private SysRoleRepository sysRoleRepository;
    @Autowired
    private SysRoleMenuRepository sysRoleMenuRepository;
    @Autowired
    private SysUserRolePepository sysUserRolePepository;
    @Autowired
    private SysRoleDeptRepository sysRoleDeptRepository;

    public SysRoleService(SysRoleRepository repository) {
        super(repository,repository);
    }


    /**
     * 根据条件分页查询角色数据
     * 
     * @param role 角色信息
     * @return 角色数据集合信息
     */
    public List<SysRoleJPADTO> selectRoleList(SysRoleJPADTO role)
    {
        return sysRoleRepository.selectRoleList(role,Pageable.unpaged()).getContent();
    }
    public Page<SysRoleJPADTO> selectRoleList(SysRoleJPADTO role, Pageable pageable)
    {
        return sysRoleRepository.selectRoleList(role,pageable);
    }

    /**
     * 根据用户ID查询角色
     * 
     * @param userId 用户ID
     * @return 角色列表
     */
    public List<SysRoleJPADTO> selectRolesByUserId(Long userId)
    {
        List<SysRoleJPADTO> userRoles = sysRoleRepository.selectRolePermissionByUserId(userId);
        SysRoleJPADTO sj = new SysRoleJPADTO();
        sj.setStatus("0");
        List<SysRoleJPADTO> roles = selectRoleList(sj);
        for (SysRoleJPADTO role : roles)
        {
            for (SysRoleJPADTO userRole : userRoles)
            {
                if (role.getRoleId().longValue() == userRole.getRoleId().longValue())
                {
                    role.setFlag(true);
                    break;
                }
            }
        }
        return roles;
    }

    /**
     * 根据用户ID查询权限
     * 
     * @param userId 用户ID
     * @return 权限列表
     */
    public Set<String> selectRolePermissionByUserId(Long userId)
    {
        List<SysRoleJPADTO> perms = sysRoleRepository.selectRolePermissionByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        for (SysRoleJPADTO perm : perms)
        {
            if (MF.isNotNull(perm))
            {
                permsSet.addAll(Arrays.asList(perm.getRoleKey().trim().split(",")));
            }
        }
        return permsSet;
    }

    /**
     * 查询所有角色
     * 
     * @return 角色列表
     */
    public List<SysRoleJPADTO> selectRoleAll()
    {
        return SpringContextUtils.getAopProxy(this).selectRoleList(new SysRoleJPADTO());
    }

    /**
     * 根据用户ID获取角色选择框列表
     * 
     * @param userId 用户ID
     * @return 选中角色ID列表
     */
    public List<Long> selectRoleListByUserId(Long userId)
    {
        return sysRoleRepository.selectRoleListByUserId(userId);
    }

    /**
     * 通过角色ID查询角色
     * 
     * @param roleId 角色ID
     * @return 角色对象信息
     */
    public SysRole selectRoleById(Long roleId)
    {
        return sysRoleRepository.selectRoleById(roleId);
    }

    /**
     * 校验角色名称是否唯一
     * 
     * @param role 角色信息
     * @return 结果
     */
    public boolean checkRoleNameUnique(SysRoleJPADTO role)
    {
        Long roleId = MF.isNull(role.getRoleId()) ? -1L : role.getRoleId();
        SysRole info = sysRoleRepository.checkRoleNameUnique(role.getRoleName());
        if (MF.isNotNull(info) && info.getRoleId().longValue() != roleId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验角色权限是否唯一
     * 
     * @param role 角色信息
     * @return 结果
     */
    public boolean checkRoleKeyUnique(SysRoleJPADTO role)
    {
        Long roleId = MF.isNull(role.getRoleId()) ? -1L : role.getRoleId();
        SysRole info = sysRoleRepository.checkRoleKeyUnique(role.getRoleKey());
        if (MF.isNotNull(info) && info.getRoleId().longValue() != roleId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验角色是否允许操作
     * 
     * @param role 角色信息
     */
    public void checkRoleAllowed(SysRoleJPADTO role)
    {
        if (MF.isNotNull(role.getRoleId()) && role.isAdmin())
        {
            throw new ServiceException("不允许操作超级管理员角色");
        }
    }

    /**
     * 校验角色是否有数据权限
     * 
     * @param roleId 角色id
     */
    public void checkRoleDataScope(Long roleId)
    {
        if (!SysUserJPADTO.isAdmin(SecurityUtils.getUserId()))
        {
            SysRoleJPADTO role = new SysRoleJPADTO();
            role.setRoleId(roleId);
            List<SysRoleJPADTO> roles = SpringContextUtils.getAopProxy(this).selectRoleList(role);
            if (MF.isEmpty(roles))
            {
                throw new ServiceException("没有权限访问角色数据！");
            }
        }
    }

    /**
     * 通过角色ID查询角色使用数量
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    public int countUserRoleByRoleId(Long roleId)
    {
        return sysUserRolePepository.countByRoleId(roleId);
    }

    /**
     * 新增保存角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Transactional
    public int insertRole(SysRoleJPADTO role)
    {
        // 新增角色信息
        SysRole sysRole = new SysRole();
        sysRole.setRoleName(role.getRoleName());
        sysRole.setRoleKey(role.getRoleKey());
        sysRole.setRoleSort(role.getRoleSort());
        sysRole.setRemark(role.getRemark());
        sysRole.setStatus("0");
        sysRole.setDelFlag("0");
        sysRole =  this.sysRoleRepository.save(sysRole);
        role.setRoleId(sysRole.getRoleId());
        return insertRoleMenu(role);
    }

    /**
     * 修改保存角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Transactional
    public int updateRole(SysRoleJPADTO role)
    {
        // 修改角色信息
        SysRole sysRole = this.sysRoleRepository.findById(role.getRoleId()).orElse(null);
        sysRole.setRoleName(role.getRoleName());
        sysRole.setRoleKey(role.getRoleKey());
        sysRole.setRoleSort(role.getRoleSort());
        sysRole.setRemark(role.getRemark());

       sysRole =   this.sysRoleRepository.save(sysRole);

        // 删除角色与菜单关联
        this.sysRoleMenuRepository.deleteSysRoleMenuByRoleId(sysRole.getRoleId());
        role.setRoleId(sysRole.getRoleId());

        return insertRoleMenu(role);
    }
    /**
     * 新增角色菜单信息
     *
     * @param role 角色对象
     */
    public int insertRoleMenu(SysRoleJPADTO role)
    {
        int rows = 1;
        // 新增用户与角色管理
        List<SysRoleMenu> list = new ArrayList<SysRoleMenu>();
        for (Long menuId : role.getMenuIds())
        {
            SysRoleMenu rm = new SysRoleMenu();
            rm.setRoleId(role.getRoleId());
            rm.setMenuId(menuId);
            list.add(rm);
        }
        if (list.size() > 0)
        {
            Iterable<SysRoleMenu> myIterable = list;
            rows = this.sysRoleMenuRepository.saveAll(myIterable).size();
        }
        return rows;
    }
    /**
     * 修改角色状态
     * 
     * @param role 角色信息
     * @return 结果
     */
    public void updateRoleStatus(SysRoleJPADTO role)
    {
       SysRole  sysRole =  this.sysRoleRepository.findById(role.getRoleId()).orElse(null);
       sysRole.setStatus(role.getStatus());
         this.sysRoleRepository.save(sysRole);
    }

    /**
     * 修改数据权限信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    @Transactional
    public int authDataScope(SysRoleJPADTO role)
    {
        // 修改角色信息
        SysRole sysRole = this.sysRoleRepository.findById(role.getRoleId()).orElse(null);
        sysRole.setRoleName(role.getRoleName());
        sysRole.setRoleKey(role.getRoleKey());
        sysRole.setRoleSort(role.getRoleSort());
        sysRole.setRemark(role.getRemark());
        this.sysRoleRepository.save(sysRole);
        // 删除角色与部门关联
        this.sysRoleDeptRepository.deleteSysRoleDeptByRoleId(role.getRoleId());
        // 新增角色和部门信息（数据权限）
        return insertRoleDept(role);
    }



    /**
     * 新增角色部门信息(数据权限)
     *
     * @param role 角色对象
     */
    public int insertRoleDept(SysRoleJPADTO role)
    {
        int rows = 1;
        // 新增角色与部门（数据权限）管理
        List<SysRoleDept> list = new ArrayList<SysRoleDept>();
        for (Long deptId : role.getDeptIds())
        {
            SysRoleDept rd = new SysRoleDept();
            rd.setRoleId(role.getRoleId());
            rd.setDeptId(deptId);
            list.add(rd);
        }
        if (list.size() > 0)
        {
            rows = this.sysRoleDeptRepository.saveAll(list).size();
        }
        return rows;
    }

    /**
     * 通过角色ID删除角色
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    @Transactional
    public int deleteRoleById(Long roleId)
    {
        // 删除角色与菜单关联
        this.sysRoleMenuRepository.deleteSysRoleMenuByRoleId(roleId);
        // 删除角色与部门关联
        this.sysRoleDeptRepository.deleteSysRoleDeptByRoleId(roleId);
        return this.sysRoleRepository.deleteRoleById(roleId);
    }

    /**
     * 批量删除角色信息
     * 
     * @param roleIds 需要删除的角色ID
     * @return 结果
     */
    @Transactional
    public int deleteRoleByIds(Long[] roleIds)
    {
        for (Long roleId : roleIds)
        {
            SysRoleJPADTO sysRole = new SysRoleJPADTO();
            sysRole.setRoleId(roleId);
            checkRoleAllowed(sysRole);
            checkRoleDataScope(roleId);
            SysRole role = selectRoleById(roleId);
            if (countUserRoleByRoleId(roleId) > 0)
            {
                throw new ServiceException(String.format("%1$s已分配,不能删除", role.getRoleName()));
            }
        }
        // 删除角色与菜单关联
        this.sysRoleMenuRepository.deleteSysRoleMenusByRoleIdIn(roleIds);
        // 删除角色与部门关联
        this.sysRoleDeptRepository.deleteSysRoleDeptsByRoleIdIn(roleIds);
        return this.sysRoleRepository.deleteSysRolesByRoleIdIn(roleIds);
    }

    /**
     * 取消授权用户角色
     * 
     * @param userRole 用户和角色关联信息
     * @return 结果
     */
    public int deleteAuthUser(SysUserRole userRole)
    {

        return this.sysUserRolePepository.deleteSysUserRoleByRoleIdAndUserId(userRole.getRoleId(), userRole.getUserId());
    }

    /**
     * 批量取消授权用户角色
     * 
     * @param roleId 角色ID
     * @param userIds 需要取消授权的用户数据ID
     * @return 结果
     */
    public int deleteAuthUsers(Long roleId, Long[] userIds)
    {

        return this.sysUserRolePepository.deleteSysUserRolesByRoleIdAndUserIdIn(roleId, userIds);
    }

    /**
     * 批量选择授权用户角色
     * 
     * @param roleId 角色ID
     * @param userIds 需要授权的用户数据ID
     * @return 结果
     */
    public int insertAuthUsers(Long roleId, Long[] userIds)
    {
        // 新增用户与角色管理
        List<SysUserRole> list = new ArrayList<SysUserRole>();
        for (Long userId : userIds)
        {
            SysUserRole ur = new SysUserRole();
            ur.setUserId(userId);
            ur.setRoleId(roleId);
            list.add(ur);
        }
        return this.sysUserRolePepository.saveAll(list).size();
    }
}
