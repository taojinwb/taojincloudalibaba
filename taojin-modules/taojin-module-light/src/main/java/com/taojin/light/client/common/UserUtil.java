
package com.taojin.light.client.common;


import javax.annotation.Resource;

import com.taojin.framework.redis.RedisUtil;
import com.taojin.light.repository.device.BIthDeviceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class UserUtil {
    @Resource
    private RedisUtil redisUtil;
    @Resource
    private BIthDeviceRepository bIthDeviceRepository;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public UserUtil() {
    }

    public Long getAuthIdForToken(String token) {
        if (StringUtils.isEmpty(token)) {
            throw new IllegalArgumentException("token不能为空");
        } else {
            String phone = this.redisUtil.get(token).toString();
            Long authId = this.bIthDeviceRepository.getAuthIdByPhone(phone);
            this.logger.info("Token = {}, phone = {},authid = {}", token, phone,authId);
            return authId;
        }
    }
}
