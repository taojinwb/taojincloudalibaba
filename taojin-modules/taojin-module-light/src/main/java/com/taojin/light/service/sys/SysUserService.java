package com.taojin.light.service.sys;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson2.JSONObject;
import com.taojin.framework.vo.ApiResult;
import com.taojin.light.framework.common.BaseService;
import com.taojin.light.dto.sys.SysUserDTO;
import com.taojin.light.entity.sys.*;
import com.taojin.light.framework.security.SecurityUtils;
import com.taojin.light.repository.sys.*;
import com.taojin.light.dto.jpadto.SysUserJPADTO;
import com.taojin.framework.constant.FrameworkConstant;
import com.taojin.framework.constant.UserConstants;
import com.taojin.framework.exception.ServiceException;
import com.taojin.framework.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.validation.Validator;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 用户 业务层处理
 * 
 * @author sujg
 */
@Service
@Slf4j
public class SysUserService extends BaseService<SysUser, Long>
{
    @Autowired
    SysUserRepository sysUserRepository;
    @Autowired
    SysRoleRepository sysRoleRepository;
    @Autowired
    SysPostRepository sysPostRepository;
    @Autowired
    SysUserRolePepository sysUserRolePepository;

    @Autowired
    SysUserPostRepository sysUserPostRepository;

    public SysUserService(SysUserRepository repository) {
        super(repository,repository);
    }


    public SysUserJPADTO findByUsername(String username){
         return sysUserRepository.findByUserName(username);
    }

    public SysUser getSysUserByUserName(String username){
        return sysUserRepository.findFirstByUserNameAndDelFlag(username,"0");
    }

    public Set<String> selectRolesByUserName(String username){
        return sysUserRepository.selectRolesByUserName(username);
    }


    public Page<SysUser> selectUserList(SysUser sysUser, Pageable pageable){

        return sysUserRepository.findAll(filterByFields(sysUser),pageable);
    }



    @Autowired
    protected Validator validator;

    /**
     * 根据条件分页查询用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectUserList(SysUser user)
    {
        return this.sysUserRepository.findAll(filterByFields(user));
    }

    /**
     * 根据条件分页查询已分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectAllocatedList(SysUser user)
    {
        return sysUserRepository.selectAllocatedList(user,Pageable.unpaged()).getContent();
    }
    /**
     * 根据条件分页查询已分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public Page<SysUser> selectAllocatedList(SysUser user, Pageable pg)
    {
        return sysUserRepository.selectAllocatedList(user,pg);
    }


    /**
     * 根据条件分页查询未分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectUnallocatedList(SysUser user)
    {
        return sysUserRepository.selectUnallocatedList(user,Pageable.unpaged()).getContent();
    }
    /**
     * 根据条件分页查询未分配用户角色列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public Page<SysUser> selectUnallocatedList(SysUser user,Pageable pg)
    {
        return sysUserRepository.selectUnallocatedList(user,pg);
    }


    /**
     * 通过用户名查询用户
     *
     * @param sysUserJPADTO 用户名
     * @return 用户对象信息
     */
    public SysUserJPADTO selectUserByUserName(SysUserJPADTO sysUserJPADTO)
    {
        List<SysUserJPADTO>  lst = sysUserRepository.selectUserDTOBySysUser(sysUserJPADTO);


          if(lst.size()>0){
              return lst.get(0);
          }else{
             return  null;
          }
    }

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    public SysUserJPADTO selectUserById(Long userId)
    {
        return sysUserRepository.selectUserByUserId(userId);
    }

    /**
     * 查询用户所属角色组
     *
     * @param userName 用户名
     * @return 结果
     */
    public String selectUserRoleGroup(String userName)
    {
        List<SysRole> list = sysRoleRepository.selectRolesByUserName(userName);
        if (CollectionUtils.isEmpty(list))
        {
            return null;
        }
        return list.stream().map(SysRole::getRoleName).collect(Collectors.joining(","));
    }

    /**
     * 查询用户所属岗位组
     *
     * @param userName 用户名
     * @return 结果
     */
    public String selectUserPostGroup(String userName)
    {
        List<SysPost> list = this.sysPostRepository.selectPostsByUserName(userName);
        if (CollectionUtils.isEmpty(list))
        {
            return null;
        }
        return list.stream().map(SysPost::getPostName).collect(Collectors.joining(","));
    }

    /**
     * 校验用户名称是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    public boolean checkUserNameUnique(SysUser user)
    {
        Long userId = MF.isNull(user.getUserId()) ? -1L : user.getUserId();
        SysUser info = this.sysUserRepository.findFirstByUserNameAndDelFlag(user.getUserName(),"0");
        if (MF.isNotNull(info) && info.getUserId().longValue() != userId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验手机号码是否唯一
     *
     * @param user 用户信息
     * @return
     */
    public boolean checkPhoneUnique(SysUser user)
    {
        Long userId = MF.isNull(user.getUserId()) ? -1L : user.getUserId();
        SysUser info = this.sysUserRepository.findFirstByPhonenumberAndDelFlag(user.getPhonenumber(),"0");
        if (MF.isNotNull(info) && info.getUserId().longValue() != userId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     * @return
     */
    public boolean checkEmailUnique(SysUser user)
    {
        Long userId = MF.isNull(user.getUserId()) ? -1L : user.getUserId();
        SysUser info = this.sysUserRepository.findFirstByEmailAndDelFlag(user.getEmail(),"0");
        if (MF.isNotNull(info) && info.getUserId().longValue() != userId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验用户是否允许操作
     *
     * @param user 用户信息
     */
    public void checkUserAllowed(SysUser user)
    {
        if (MF.isNotNull(user.getUserId()) &&  isAdmin(user.getUserId()))
        {
            throw new ServiceException("不允许操作超级管理员用户");
        }
    }

    public static boolean isAdmin(Long userId)
    {
        return userId != null && 1L == userId;
    }


    /**
     * 校验用户是否有数据权限
     *
     * @param userId 用户id
     */
    public void checkUserDataScope(Long userId)
    {
        if (!SysUserJPADTO.isAdmin(SecurityUtils.getUserId()))
        {
            SysUser user = new SysUser();
            user.setUserId(userId);
            List<SysUser> users = SpringContextUtils.getAopProxy(this).selectUserList(user);
            if (MF.isEmpty(users))
            {
                throw new ServiceException("没有权限访问用户数据！");
            }
        }
    }

    /**
     * 新增保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Transactional
    public void insertUser(SysUser user)
    {
        // 新增用户信息
        this.sysUserRepository.save(user);
        user.setPostIds((Long[])user.getParams().get("postIds"));
        // 新增用户岗位关联
        insertUserPost(user);
        // 新增用户与角色管理
        insertUserRole(user);
    }

    /**
     * 注册用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    public void registerUser(SysUser user)
    {
         sysUserRepository.save(user) ;
    }

    /**
     * 修改保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Transactional
    public void updateUser(SysUser user)
    {
        Long userId = user.getUserId();
        // 删除用户与角色关联
        sysUserRolePepository.deleteSysUserRoleByUserId(userId);

        SysUserDTO sysUserDTO = new SysUserDTO();
        // 新增用户与角色管理
        insertUserRole(user);
        // 删除用户与岗位关联
        sysUserPostRepository.deleteSysUserPostsByUserId(userId);
        // 新增用户与岗位管理
        insertUserPost(user);
        user.setUpdateBy(this.getUserName());
        user.setUpdateTime(DateUtils.getNowDate());
        this.sysUserRepository.updateFields(user);
    }

    /**
     * 用户授权角色
     *
     * @param userId 用户ID
     * @param roleIds 角色组
     */
    @Transactional
    public void insertUserAuth(Long userId, Long[] roleIds)
    {
        sysUserRolePepository.deleteSysUserRoleByUserId(userId);
        insertUserRole(userId, roleIds);
    }

    /**
     * 修改用户状态
     *
     * @param user 用户信息
     * @return 结果
     */
    public void updateUserStatus(SysUser user)
    {
        SysUser sysUser = this.sysUserRepository.findById(user.getUserId()).orElse(null);
        sysUser.setStatus(user.getStatus());
        sysUser.setUpdateBy(user.getUpdateBy());
        sysUser.setUpdateTime(DateUtils.getTimestamp());
         sysUserRepository.save(sysUser);
    }

    /**
     * 修改用户基本信息
     *
     * @param user 用户信息
     * @return 结果
     */
    public void updateUserProfile(SysUser user)
    {
        sysUserRepository.save(user);
    }

    /**
     * 修改用户头像
     *
     * @param userName 用户名
     * @param avatar 头像地址
     * @return 结果
     */
    public void updateUserAvatar(String userName, String avatar)
    {
        SysUser  sysUser = sysUserRepository.findFirstByUserNameAndDelFlag(userName,"0");
        sysUser.setAvatar(avatar);
        sysUserRepository.save(sysUser);
    }

    /**
     * 重置用户密码
     *
     * @param user 用户信息
     * @return 结果
     */
    public void resetPwd(SysUserJPADTO user)
    {
        SysUser  sysUser = sysUserRepository.findById(user.getUserId()).orElse(null);
        sysUser.setPassword(user.getPassword());
        sysUserRepository.save(sysUser);
    }

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    public void resetUserPwd(String userName, String password)
    {
        SysUser  sysUser = sysUserRepository.findFirstByUserNameAndDelFlag(userName,"0");
        sysUser.setPassword(password);
        sysUserRepository.save(sysUser);
    }

    /**
     * 新增用户角色信息
     *
     * @param user 用户对象
     */
    public void insertUserRole(SysUser user)
    {
        this.insertUserRole(user.getUserId(), user.getRoleIds());
    }

    /**
     * 新增用户岗位信息
     *
     * @param user 用户对象
     */
    public void insertUserPost(SysUser user)
    {
        Long[] posts = user.getPostIds();
        if (MF.isNotEmpty(posts))
        {
            // 新增用户与岗位管理
            List<SysUserPost> list = new ArrayList<SysUserPost>(posts.length);
            for (Long postId : posts)
            {
                SysUserPost up = new SysUserPost();
                up.setUserId(user.getUserId());
                up.setPostId(postId);

                list.add(up);
            }
            // 将 List 转换为 Iterable<? extends ID>
            Iterable<? extends SysUserPost> iterableIds = list;
            sysUserPostRepository.saveAll(iterableIds);
        }
    }

    /**
     * 新增用户角色信息
     *
     * @param userId 用户ID
     * @param roleIds 角色组
     */
    public void insertUserRole(Long userId, Long[] roleIds)
    {
        if (MF.isNotEmpty(roleIds))
        {
            // 新增用户与角色管理
            List<SysUserRole> list = new ArrayList<SysUserRole>(roleIds.length);
            for (Long roleId : roleIds)
            {
                SysUserRole ur = new SysUserRole();
                ur.setUserId(userId);
                ur.setRoleId(roleId);
                list.add(ur);
            }

            Iterable<? extends SysUserRole> iterableIds = list;
            sysUserRolePepository.saveAll(iterableIds);
        }
    }

    /**
     * 通过用户ID删除用户
     *
     * @param userId 用户ID
     * @return 结果
     */
    @Transactional
    public void deleteUserById(Long userId)
    {
        // 删除用户与角色关联
        sysUserRolePepository.deleteSysUserRoleByUserId(userId);
        // 删除用户与岗位表
        sysUserPostRepository.deleteSysUserPostsByUserId(userId);
        sysUserRepository.deleteById(userId);
    }

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户ID
     * @return 结果
     */
    @Transactional
    public void deleteUserByIds(Long[] userIds)
    {
        for (Long userId : userIds)
        {
            SysUser sv = new SysUser();
            sv.setUserId(userId);
            checkUserAllowed(sv);
            checkUserDataScope(userId);
        }
        // 删除用户与角色关联
        sysUserRolePepository.deleteSysUserRolesByUserIdIn(userIds);
        // 删除用户与岗位关联
        sysUserPostRepository.deleteSysUserPostsByUserIdIn(userIds);
       for(Long userId:userIds){
            sysUserRepository.UpdateDelFlag(userId);
        }
    }
    public static String getBlock(Object msg)
    {
        if (msg == null)
        {
            msg = "";
        }
        return "[" + msg.toString() + "]";
    }
    /**
     * 校验用户是否有效
     * @param sysUser
     * @return
     */
    public ApiResult<JSONObject> checkUserIsEffective(SysUser sysUser) {
        ApiResult<JSONObject> result = new ApiResult<JSONObject>();
         String ip = IpUtils.getIpAddr();
        String address = AddressUtils.getRealAddressByIP(ip);


        // 获取客户端操作系统
        String os = "";
        // 获取客户端浏览器
        // 封装对象
        SysLogininfor logininfor = new SysLogininfor();
        logininfor.setUserName("");
        logininfor.setIpaddr(ip);
        logininfor.setLoginLocation(address);
        logininfor.setBrowser("");
        logininfor.setOs(os);
        logininfor.setLoginTime(DateUtils.getNowDate());

        //情况1：根据用户信息查询，该用户不存在
        if (sysUser == null) {
            result.error500("该用户不存在，请注册");

            logininfor.setMsg("用户登录失败，用户不存在！");
            return result;
        }
        //情况2：根据用户信息查询，该用户已注销
        //update-begin---author:王帅   Date:20200601  for：if条件永远为falsebug------------
        if (FrameworkConstant.DEL_FLAG_1.equals(sysUser.getDelFlag())) {
            //update-end---author:王帅   Date:20200601  for：if条件永远为falsebug------------

            logininfor.setUserName( sysUser.getUserName());
            logininfor.setMsg("用户登录失败，用户名:" + sysUser.getUserName() + "已注销！");
            result.error500("该用户已注销");
            return result;
        }
        //情况3：根据用户信息查询，该用户已冻结
        if (FrameworkConstant.USER_FREEZE.equals(sysUser.getStatus())) {
            logininfor.setUserName( sysUser.getUserName());
            logininfor.setMsg("用户登录失败，用户名:" + sysUser.getUserName() + "已冻结！");
            result.error500("该用户已冻结");
            return result;
        }
        return result;
    }

    public static void main(String[] args) {
        log.info(DateUtil.date().toJdkDate()+"===");
    }

}
