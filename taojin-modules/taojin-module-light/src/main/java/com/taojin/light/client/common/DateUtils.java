
package com.taojin.light.client.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class DateUtils {
    private static SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static SimpleDateFormat sf2 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private static SimpleDateFormat sf1 = new SimpleDateFormat("yyyyMMddHHmmss");
    private static SimpleDateFormat sf3 = new SimpleDateFormat("yyyyMMddHHmmssSSSSSS");

    public DateUtils() {
    }

    public static void main(String[] args) {
        System.out.println(getDeviceHitTime());
    }

    public static void setTimeZone() {
        sf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        sf2.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
    }

    public static String deviceTimeToSqlTime(String deviceTime) {
        String resultTime = null;

        try {
            Date format1 = sf2.parse(deviceTime);
            resultTime = sf1.format(format1);
        } catch (ParseException var3) {
            var3.printStackTrace();
        }

        return resultTime;
    }

    public static String getSystemTime() {
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        calendar.setTime(new Date());
        return sf2.format(calendar.getTime());
    }

    public static long getSystemTimestamp() {
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        return calendar.getTimeInMillis();
    }

    public static String getDeviceHitTime() {
        sf1.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
        calendar.setTime(new Date());
        return sf1.format(calendar.getTime());
    }
}
