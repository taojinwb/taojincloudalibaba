package com.taojin.light.service.sys;

import com.taojin.light.dto.jpadto.SysUserJPADTO;
import com.taojin.light.framework.factory.AsyncFactory;
import com.taojin.light.framework.security.SecurityUtils;
import com.taojin.light.framework.constant.CacheConstant;
import com.taojin.framework.exception.BaseException;
import com.taojin.framework.redis.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import com.taojin.framework.constant.SysConstants;
import com.taojin.framework.utils.MessageUtils;
import com.taojin.framework.manager.AsyncManager;
import com.taojin.light.framework.security.context.AuthenticationContextHolder;

/**
 * 登录密码方法
 * 
 * @author sujg
 */
@Component
@Slf4j
public class SysPasswordService
{
    @Autowired
    private RedisUtil redisUtil;

    @Value(value = "${user.password.maxRetryCount}")
    private int maxRetryCount;

    @Value(value = "${user.password.lockTime}")
    private int lockTime;

    /**
     * 登录账户密码错误次数缓存键名
     * 
     * @param username 用户名
     * @return 缓存键key
     */
    private String getCacheKey(String username)
    {
        return CacheConstant.PWD_ERR_CNT_KEY + username;
    }

    public void validate(SysUserJPADTO user)
    {
        Authentication usernamePasswordAuthenticationToken = AuthenticationContextHolder.getContext();
        String username = usernamePasswordAuthenticationToken.getName();
        String password = usernamePasswordAuthenticationToken.getCredentials().toString();
        Integer retryCount = (Integer) redisUtil.get(getCacheKey(username));

        if (retryCount == null)
        {
            retryCount = 0;
        }

        if (retryCount >= Integer.valueOf(maxRetryCount).intValue())
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, SysConstants.LOGIN_FAIL,
                    MessageUtils.message("user.password.retry.limit.exceed", maxRetryCount, lockTime)));
            throw new BaseException("密码错误，重试次数【"+maxRetryCount+"】，锁定时间："+lockTime+"分钟");
        }

        if (!matches(user, password))
        {
            retryCount = retryCount + 1;
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, SysConstants.LOGIN_FAIL,
                    MessageUtils.message("user.password.retry.limit.count", retryCount)));
            redisUtil.set(getCacheKey(username), retryCount, lockTime*60);
            throw new BaseException("密码输入错误"+retryCount+"次");
        }
        else
        {
            clearLoginRecordCache(username);
        }
    }

    public boolean matches(SysUserJPADTO user, String rawPassword)
    {
        return SecurityUtils.matchesPassword(rawPassword, user.getPassword());
    }

    public void clearLoginRecordCache(String loginName)
    {
        if (redisUtil.hasKey(getCacheKey(loginName)))
        {
            redisUtil.del(getCacheKey(loginName));
        }
    }
}
