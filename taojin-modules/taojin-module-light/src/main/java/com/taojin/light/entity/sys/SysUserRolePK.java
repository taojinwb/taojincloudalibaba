package com.taojin.light.entity.sys;

import lombok.Data;

import javax.persistence.Column;
import java.io.Serializable;
@Data
public class SysUserRolePK implements Serializable {

    private Long userId;

    private Long roleId;
}
