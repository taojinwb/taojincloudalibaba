package com.taojin.light.repository.sys;

import com.taojin.light.entity.sys.SysUser;
import com.taojin.light.dto.jpadto.SysUserJPADTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface SysUserRepository extends  JpaRepository<SysUser, Long>, JpaSpecificationExecutor<SysUser>{
    @Query(value="select  new com.taojin.admin.vo.SysUserVo(*) from sys_user where user_name=?1",nativeQuery = true)
    SysUserJPADTO findByUserName(String username);



    @Query(value = " select role_key from sys_role where role_id in (select role_id from sys_user_role where user_id = (select user_id from sys_user where user_name=:username))  ", nativeQuery = true)
    Set<String> selectRolesByUserName(@Param("username")  String username);


    @Query(value="select u.user_id, u.dept_id, u.user_name, u.nick_name, u.email, u.avatar, u.phonenumber, u.password, u.sex, u.status, u.del_flag, u.login_ip, u.login_date, u.create_by, u.create_time, u.remark,u.unionid,\n" +
            "     d.parent_id, d.ancestors, d.dept_name, d.order_num, d.leader, d.status as dept_status,\n" +
            "    r.role_id, r.role_name, r.role_key, r.role_sort, r.data_scope, r.status as role_status,u.user_type,u.update_by ,u.update_time\n" +
            "    from sys_user u\n" +
            "    left join sys_dept d on u.dept_id = d.dept_id\n" +
            "    left join sys_user_role ur on u.user_id = ur.user_id\n" +
            "    left join sys_role r on r.role_id = ur.role_id\n" +
            "    where  u.del_flag = '0' \n"+
            " AND (:#{#sysUserJPADTO.userName} IS NULL OR  u.user_name = :#{#sysUserJPADTO.userName}) " +
            " AND (:#{#sysUserJPADTO.userId} IS NULL OR  u.user_id = :#{#sysUserJPADTO.userId}) "
               ,nativeQuery = true)
    List<SysUserJPADTO> selectUserDTOBySysUser(SysUserJPADTO sysUserJPADTO);


    @Query(value="select u.user_id, u.dept_id, u.user_name, u.nick_name, u.email, u.avatar, u.phonenumber, u.password, u.sex, u.status, u.del_flag, u.login_ip, u.login_date, u.create_by, u.create_time, u.remark,\n" +
            "     d.parent_id, d.ancestors, d.dept_name, d.order_num, d.leader, d.status as dept_status,\n" +
            "    r.role_id, r.role_name, r.role_key, r.role_sort, r.data_scope, r.status as role_status,u.user_type\n" +
            "    from sys_user u\n" +
            "    left join sys_dept d on u.dept_id = d.dept_id\n" +
            "    left join sys_user_role ur on u.user_id = ur.user_id\n" +
            "    left join sys_role r on r.role_id = ur.role_id\n" +
            "    where u.user_id = :userId and u.del_flag = '0'",nativeQuery = true)
    SysUserJPADTO selectUserByUserId(@Param("userId") Long userId);
    @Query(value=" select distinct u.*" +
            "   from sys_user u\n" +
            "  left join sys_dept d on u.dept_id = d.dept_id\n" +
            "  left join sys_user_role ur on u.user_id = ur.user_id\n" +
            "  left join sys_role r on r.role_id = ur.role_id\n" +
            "    where u.del_flag = '0' and r.role_id = :#{#sysUser.roleId} " +
            " AND (:#{#sysUser.userName} IS NULL OR  u.userName = :#{#sysUser.userName}) " +
            " AND (:#{#sysUser.phonenumber} IS NULL OR  u.phonenumber = :#{#sysUser.phonenumber}) " +
            " ",nativeQuery = true)
    Page<SysUser> selectAllocatedList(SysUser sysUser, Pageable pageable);

    @Query(value = " select distinct u.*\n" +
            "    from sys_user u\n" +
            " left join sys_dept d on u.dept_id = d.dept_id\n" +
            " left join sys_user_role ur on u.user_id = ur.user_id\n" +
            " left join sys_role r on r.role_id = ur.role_id\n" +
            "    where u.del_flag = '0' and (r.role_id != :#{#sysUser.roleId} or r.role_id IS NULL)\n" +
            "    and u.user_id not in (select u.user_id from sys_user u inner join sys_user_role ur on u.user_id = ur.user_id and ur.role_id = :#{#sysUser.roleId} )" +
            " AND (:#{#sysUser.userName} IS NULL OR  u.userName = :#{#sysUser.userName}) " +
            " AND (:#{#sysUser.phonenumber} IS NULL OR  u.phonenumber = :#{#sysUser.phonenumber}) ",nativeQuery = true)
    Page<SysUser>selectUnallocatedList(SysUser sysUser,Pageable pg);

    @Modifying
    @Query("UPDATE SysUser u SET login_ip=?1,login_date=?2 "+
            " WHERE u.id = ?3")
    void UpdateUserLogin(String loginip, Date logindate, Long uid);

    @Modifying
    @Query("UPDATE SysUser u SET u.userName= COALESCE(:#{#sysUser.userName}, u.userName) " +
            ",u.deptId= COALESCE(:#{#sysUser.deptId}, u.deptId) " +
            ",u.phonenumber= COALESCE(:#{#sysUser.phonenumber}, u.phonenumber) " +
            ",u.email= COALESCE(:#{#sysUser.email}, u.email) " +
            ",u.sex= COALESCE(:#{#sysUser.sex}, u.sex) " +
            ",u.status= COALESCE(:#{#sysUser.status}, u.status) " +
            ",u.remark= COALESCE(:#{#sysUser.remark}, u.remark) " +
            ",u.updateBy= COALESCE(:#{#sysUser.updateBy}, u.updateBy) " +
            ",u.updateTime= COALESCE(:#{#sysUser.updateTime}, u.updateTime) " +
            "WHERE u.userId = :#{#sysUser.userId}")
    void updateFields(SysUser sysUser);



    SysUser findFirstByUserNameAndDelFlag(String userName,String delFlag);
    SysUser findFirstByPhonenumberAndDelFlag(String phoneNumber,String delFlag);

    SysUser findFirstByEmailAndDelFlag(String email,String delFlag);

    @Modifying
    @Query("UPDATE SysUser u SET  del_flag = '2' WHERE u.id = ?1")
    void UpdateDelFlag(Long userId);

}
