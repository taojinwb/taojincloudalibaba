
package com.taojin.light.client.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ApiModel(
        description = " 数据结果"
)
public class ResultBean implements Serializable {
    private static final long serialVersionUID = 1L;
    public static final int CODE_SERVICE_ERROR = 500;
    public static final int CODE_CLIENT_ERROR = 400;
    public static final int CODE_SUCCESS = 200;
    public static final int CODE_NOTSOURCE = 404;
    public static final int CODE_LOGIN_INVALID = 401;
    @ApiModelProperty(
            value = "状态码（500: 服务端错误  400: 客户端错误/请求参数错误 200: 请求成功  404: 请求的资源不存在  401: 登录失效）",
            required = true
    )
    private int code = 200;
    @ApiModelProperty("数据结果集: [{}]")
    private List response = new ArrayList();
    @ApiModelProperty("返回提示信息")
    private String message = "";

    public ResultBean() {
    }

    public static ResultBean getInstance() {
        return new ResultBean();
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setData(Object data) {
        this.response.add(data);
    }

    public List getResponse() {
        return this.response;
    }

    public void setResponse(List response) {
        this.response = response;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String toString() {
        return "ResultBean [code=" + this.code + ", response=" + this.response + ", message=" + this.message + "]";
    }
}
