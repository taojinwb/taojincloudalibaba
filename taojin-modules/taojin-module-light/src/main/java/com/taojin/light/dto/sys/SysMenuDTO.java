package com.taojin.light.dto.sys;

import javax.persistence.*;
import java.sql.Timestamp;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-30
*/
@Data
public class SysMenuDTO {
		private Long menuId;

		@Excel(name ="菜单名称")
		private java.lang.String menuName;

		@JsonSerialize(using = ToStringSerializer.class)
		@Excel(name ="父菜单ID")
		private Long parentId;

		@Excel(name ="显示顺序")
		private java.lang.Integer orderNum;

		@Excel(name ="路由地址")
		private java.lang.String path;

		@Excel(name ="组件路径")
		private java.lang.String component;

		@Excel(name ="路由参数")
		private java.lang.String query;

		@Excel(name ="是否为外链（0是 1否）")
		private java.lang.String isFrame;

		@Excel(name ="是否缓存（0缓存 1不缓存）")
		private java.lang.String isCache;

		@Excel(name ="菜单类型（M目录 C菜单 F按钮）")
		private java.lang.String menuType;

		@Excel(name ="菜单状态（0显示 1隐藏）")
		private java.lang.String visible;

		@Excel(name ="菜单状态（0正常 1停用）")
		private java.lang.String status;

		@Excel(name ="权限标识")
		private java.lang.String perms;

		@Excel(name ="菜单图标")
		private java.lang.String icon;

		@Excel(name ="创建者")
		private java.lang.String createBy;

		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
		@Excel(name ="创建时间")
		private java.util.Date createTime;

		@Excel(name ="更新者")
		private java.lang.String updateBy;

		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
		@Excel(name ="更新时间")
		private java.util.Date updateTime;

		@Excel(name ="备注")
		private java.lang.String remark;

}