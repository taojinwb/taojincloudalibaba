package com.taojin.light.constant;

public interface LigthConstant {

    //灯控开关token前缀
    public static String token_light_open="token:light:open:";
    //灯控开关token前缀
    public static String token_light_close="token:light:close:";

    public static String order_light = "order.light";

    public static Long token_light_time= 1000*60*60*2L;//
}
