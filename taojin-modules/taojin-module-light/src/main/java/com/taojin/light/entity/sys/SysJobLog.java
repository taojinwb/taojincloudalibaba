package com.taojin.light.entity.sys;

import javax.persistence.*;
import java.sql.Timestamp;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import com.taojin.light.entity.BaseEntity;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-26
*/
@Entity
@Data
@Table(name = "sys_job_log", schema = "taojin", catalog = "")
public class SysJobLog extends BaseEntity {

/** 任务日志ID*/
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "job_log_id")
		private Long jobLogId;

		/** 任务名称*/
		@Column(name = "job_name")
		@Excel(name ="任务名称")
		private java.lang.String jobName;

		/** 任务组名*/
		@Column(name = "job_group")
		@Excel(name ="任务组名")
		private java.lang.String jobGroup;

		/** 调用目标字符串*/
		@Column(name = "invoke_target")
		@Excel(name ="调用目标字符串")
		private java.lang.String invokeTarget;

		/** 日志信息*/
		@Column(name = "job_message")
		@Excel(name ="日志信息")
		private java.lang.String jobMessage;

		/** 执行状态（0正常 1失败）*/
		@Column(name = "status")
		@Excel(name ="执行状态（0正常 1失败）")
		private java.lang.String status;

		/** 异常信息*/
		@Column(name = "exception_info")
		@Excel(name ="异常信息")
		private java.lang.String exceptionInfo;

//		/** 创建时间*/
//		@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
//		@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
//		@Column(name = "create_time")
//		@Excel(name ="创建时间")
//		private java.util.Date createTime;

}