package com.taojin.light.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(
        value = "设备控制DTO",
        description = "设备控制DTO"
)
public class AtSetDTO {
    @ApiModelProperty(
            value = "设备ID",
            name = "deviceId",
            required = true
    )
    private String deviceId;
    @ApiModelProperty(
            value = "设备通路数字（0：所有，1~N 为单独控制）",
            name = "routeid",
            required = true
    )
    private int routeid;
    @ApiModelProperty(
            value = "执行的动作（0：关闭，1：打开  ?:获取状态）",
            name = "exeaction",
            required = true
    )
    private String exeaction;
    @ApiModelProperty(
            value = "与设备建立链接的soket服务器IP(不用填写)",
            name = "socketIp",
            required = false
    )
    private String socketIp;

    public AtSetDTO() {
    }

    public String getDeviceId() {
        return this.deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public int getRouteid() {
        return this.routeid;
    }

    public void setRouteid(int routeid) {
        this.routeid = routeid;
    }

    public String getExeaction() {
        return this.exeaction;
    }

    public void setExeaction(String exeaction) {
        this.exeaction = exeaction;
    }

    public String getSocketIp() {
        return this.socketIp;
    }

    public void setSocketIp(String socketIp) {
        this.socketIp = socketIp;
    }

    public String toString() {
        return "AtSetDTO [deviceId=" + this.deviceId + ", routeid=" + this.routeid + ", exeaction=" + this.exeaction + ", socketIp=" + this.socketIp + "]";
    }
}
