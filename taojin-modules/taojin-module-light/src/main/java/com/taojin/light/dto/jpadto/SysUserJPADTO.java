package com.taojin.light.dto.jpadto;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.taojin.light.entity.sys.SysDept;
import com.taojin.framework.config.JpaDto;
import lombok.*;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 用户对象 sys_user
 *
 * @author sujg
 */
@Data
@JpaDto
@AllArgsConstructor
@NoArgsConstructor
public class SysUserJPADTO
{
    private static final long serialVersionUID = 1L;


    /** 用户ID */
    private Long userId;

    /** 部门ID */
    private Long deptId;

    /** 用户账号 */
    private String userName;

    /** 用户昵称 */
    private String nickName;

    /** 用户邮箱 */
    private String email;

    /** 手机号码 */
    private String phonenumber;

    /** 用户性别 */
    private String sex;

    /** 用户头像 */
    private String avatar;

    /** 密码 */
    private String password;

    /** 帐号状态（0正常 1停用） */
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 最后登录IP */
    private String loginIp;
    /** unionid */
    private String unionid;
    /** 最后登录时间 */


    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date loginDate;

    /** 部门对象 */

    private SysDept dept;

    /** 角色对象 */
    private List<SysRoleJPADTO> roles;

    /** 角色组 */
    private Long[] roleIds;

    /** 岗位组 */
    private Long[] postIds;

    /** 角色ID */
    private Long roleId;

    private String salt;

    private String tenantid;


    /** 父部门id*/
    private Long parentId;

    /** 祖级列表*/
    private java.lang.String ancestors;

    /** 部门名称*/
    private java.lang.String deptName;

    /** 显示顺序*/
    private java.lang.Integer orderNum;

    /** 负责人*/
    private java.lang.String leader;

    /** 联系电话*/
    private java.lang.String phone;

    /** 创建者*/
    private java.lang.String createBy;

    /** 创建时间*/


    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private java.util.Date createTime;

    /** 更新者*/
    private java.lang.String updateBy;

    /** 更新时间*/


    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private java.util.Date updateTime;


    /** 角色名称*/
    private java.lang.String roleName;

    /** 角色权限字符串*/
    private java.lang.String roleKey;

    /** 显示顺序*/
    private java.lang.Integer roleSort;

    /** 数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）*/
    private java.lang.String dataScope;

    /** 菜单树选择项是否关联显示*/
    private java.lang.Integer menuCheckStrictly;

    /** 部门树选择项是否关联显示*/
    private java.lang.Integer deptCheckStrictly;


    /** 备注*/
    @Column(name = "remark")
    @Excel(name ="备注")
    private java.lang.String remark;



    public boolean isAdmin()
    {
        return isAdmin(this.userId);
    }

    public static boolean isAdmin(Long userId)
    {
        return userId != null && 1L == userId;
    }

}
