package com.taojin.light.entity.sys;

import javax.persistence.*;
import java.sql.Timestamp;
import org.jeecgframework.poi.excel.annotation.Excel;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import com.taojin.light.entity.BaseEntity;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
/**
 *实体类
 *
 * @author sujg
 * @data  2023-10-26
*/
@Entity
@Data
@Table(name = "sys_post", schema = "", catalog = "")
public class SysPost extends BaseEntity {

/** 岗位ID*/
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "post_id")
		private Long postId;

		/** 岗位编码*/
		@Column(name = "post_code")
		@Excel(name ="岗位编码")
		private java.lang.String postCode;

		/** 岗位名称*/
		@Column(name = "post_name")
		@Excel(name ="岗位名称")
		private java.lang.String postName;

		/** 显示顺序*/
		@Column(name = "post_sort")
		@Excel(name ="显示顺序")
		private java.lang.Integer postSort;

		/** 备注*/
		@Column(name = "remark")
		@Excel(name ="备注")
		private java.lang.String remark;

}