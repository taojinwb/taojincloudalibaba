package com.taojin.light.repository.sys;

import com.taojin.light.entity.sys.SysConfig;
import com.taojin.light.entity.sys.SysRoleMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface SysRoleMenuRepository extends  JpaRepository<SysRoleMenu, Long>, JpaSpecificationExecutor<SysRoleMenu> {

    int countByMenuId(Long menuId);

   void deleteSysRoleMenuByRoleId(Long roleId);
   void deleteSysRoleMenusByRoleIdIn(Long[] roleIds);
}
