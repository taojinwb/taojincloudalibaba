package com.taojin.light.service.sys;

import com.taojin.light.dto.LoginUser;
import com.taojin.light.framework.util.JwtUtil;
import com.taojin.light.repository.sys.SysUserRepository;
import com.taojin.light.framework.factory.AsyncFactory;
import com.taojin.light.framework.constant.CacheConstant;
import com.taojin.framework.constant.SysConstants;
import com.taojin.framework.constant.UserConstants;
import com.taojin.framework.exception.BaseException;
import com.taojin.framework.manager.AsyncManager;
import com.taojin.framework.redis.RedisUtil;
import com.taojin.light.framework.security.context.AuthenticationContextHolder;
import com.taojin.framework.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * 登录校验方法
 *
 * @author sujg
 */
@Component
@Transactional
@Slf4j
public class SysLoginService
{
    @Autowired
    private TokenService tokenService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private SysUserService userService;

    @Autowired
    private SysConfigService configService;
    @Autowired
    private SysUserRepository sysUserRepository;

    /**
     * 登录验证
     *
     * @param username 用户名
     * @param password 密码
     * @param code 验证码
     * @param uuid 唯一标识
     * @return 结果
     */
    public String login(String username, String password, String code, String uuid)
    {

        // 验证码校验
        validateCaptcha(username, code, uuid);
        // 登录前置校验
        loginPreCheck(username, password);
        // 用户验证
        Authentication authentication = null;
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
        AuthenticationContextHolder.setContext(authenticationToken);
        // 该方法会去调用UserDetailsService.loadUserByUsername
        authentication = authenticationManager.authenticate(authenticationToken);

        AuthenticationContextHolder.clearContext();

        AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, SysConstants.LOGIN_SUCCESS, MessageUtils.message("user.login.success")));
        LoginUser loginUser = (LoginUser) authentication.getPrincipal();

        // 生成token,使用用户名进行签名和验证。方便后面解密
        String token = tokenService.createToken(loginUser);// JwtUtil.sign(loginUser.getUsername(), loginUser.getUsername());
        redisUtil.set(CacheConstant.LOGIN_TOKEN_KEY+token,loginUser);
        redisUtil.set(CacheConstant.LOGIN_UNIONID_TOKEN_KEY+token,loginUser.getUnionid());
        redisUtil.expire(CacheConstant.LOGIN_TOKEN_KEY + token, JwtUtil.EXPIRE_TIME * 2 / 1000);

        return token;
    }

    /**
     * 校验验证码
     *
     * @param username 用户名
     * @param code 验证码
     * @param uuid 唯一标识
     * @return 结果
     */
    public void validateCaptcha(String username, String code, String uuid)
    {
        boolean captchaEnabled = configService.selectCaptchaEnabled();
        if (captchaEnabled)
        {
            String verifyKey = CacheConstant.CAPTCHA_CODE_KEY + MF.nvl(uuid, "");
            String captcha = redisUtil.get(verifyKey);
            redisUtil.del(verifyKey);
            if (captcha == null)
            {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, SysConstants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.expire")));
                throw new BaseException("验证码无效");
            }
            if (!code.equalsIgnoreCase(captcha))
            {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, SysConstants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.error")));
                throw new BaseException("验证码错误");
            }
        }
    }

    /**
     * 登录前置校验
     * @param username 用户名
     * @param password 用户密码
     */
    public void loginPreCheck(String username, String password)
    {
        // 用户名或密码为空 错误
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password))
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, SysConstants.LOGIN_FAIL, MessageUtils.message("not.null")));
            throw new BaseException("用户密码为空");
        }
        // 密码如果不在指定范围内 错误
        if (password.length() < UserConstants.PASSWORD_MIN_LENGTH
                || password.length() > UserConstants.PASSWORD_MAX_LENGTH)
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, SysConstants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
            throw new BaseException("密码长度不正确");
        }
        // 用户名不在指定范围内 错误
        if (username.length() < UserConstants.USERNAME_MIN_LENGTH
                || username.length() > UserConstants.USERNAME_MAX_LENGTH)
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, SysConstants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
            throw new BaseException("用户名长度不正确");
        }
        // IP黑名单校验
        String blackStr = configService.selectConfigByKey("sys.login.blackIPList");
        if (IpUtils.isMatchedIp(blackStr, IpUtils.getIpAddr()))
        {
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, SysConstants.LOGIN_FAIL, MessageUtils.message("login.blocked")));
            throw new BaseException("IP在黑名单中");
        }
    }

    /**
     * 记录登录信息
     *
     * @param userId 用户ID
     */
    public void recordLoginInfo(Long userId)
    {
        sysUserRepository.UpdateUserLogin(IpUtils.getIpAddr(),DateUtils.getNowDate(),userId);
    }
}
